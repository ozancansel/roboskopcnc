#include "typedobject.h"

void TypedObject::addType(const QString type){
    m_type_hierarchy.append(type);
}

bool TypedObject::is(const QString type){
    return m_type_hierarchy.contains(type);
}
