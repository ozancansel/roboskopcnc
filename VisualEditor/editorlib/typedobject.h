#ifndef TYPEDOBJECT_H
#define TYPEDOBJECT_H

#include <QObject>
#include "itypedobject.h"

class TypedObject : public virtual ITypedObject
{

public:

    virtual bool is(const QString type);

protected:

    virtual void    addType(const QString type);

private:

    QStringList m_type_hierarchy;

};

#endif // TYPEDOBJECT_H
