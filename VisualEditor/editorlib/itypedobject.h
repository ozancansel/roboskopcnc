#ifndef ITYPEDOBJECT_H
#define ITYPEDOBJECT_H

#include <QString>

class ITypedObject
{

public:

    virtual ~ITypedObject() { }
    virtual bool is(const QString type) = 0;
    virtual QString typeName() = 0;

protected:

    virtual void addType(const QString type) = 0;

};

#endif // ITYPEDOBJECT_H
