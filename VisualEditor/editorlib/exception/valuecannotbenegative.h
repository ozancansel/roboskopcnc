#ifndef VALUECANNOTBENEGATIVE_H
#define VALUECANNOTBENEGATIVE_H

#include <QException>

class ValueCannotBeNegative : public QException
{

public:

    void raise() const { throw *this; }
    ValueCannotBeNegative *clone() const { return new ValueCannotBeNegative(*this); }
};

#endif // VALUECANNOTBENEGATIVE_H
