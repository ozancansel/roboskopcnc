#ifndef INCOMPATIBLECOMMAND_H
#define INCOMPATIBLECOMMAND_H

#include <QException>

class IncompatibleCommand : public QException
{
public:
    void raise() const { throw *this; }
    IncompatibleCommand* clone() { return new IncompatibleCommand(*this); }
};

#endif // INCOMPATIBLECOMMAND_H
