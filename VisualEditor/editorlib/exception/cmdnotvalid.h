#ifndef CMDNOTVALID_H
#define CMDNOTVALID_H

#include <QException>
#include <QSharedPointer>
#include "command/icmd.h"

class CmdNotValid : public QException
{
public:
    CmdNotValid(QSharedPointer<ICmd> cmd) : Cmd(cmd) {  }
    void raise() const { throw *this; }
    CmdNotValid* clone() { return new CmdNotValid(*this); }

    QSharedPointer<ICmd>    Cmd;
};

#endif // CMDNOTVALID_H
