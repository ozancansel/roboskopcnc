#ifndef PROPERTYNOTSPECIFIED_H
#define PROPERTYNOTSPECIFIED_H

#include <QException>

class PropertyNotSpecified : public QException
{

public:

    PropertyNotSpecified(QString propertyName) : Property(propertyName) { }
    QString     Property;
    void    raise() const { throw *this; }
    PropertyNotSpecified* clone() { return new PropertyNotSpecified(*this); }

};

#endif // PROPERTYNOTSPECIFIED_H
