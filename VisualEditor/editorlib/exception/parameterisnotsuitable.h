#ifndef PARAMETERISNOTSUITABLE_H
#define PARAMETERISNOTSUITABLE_H

#include <QException>

class ParameterIsNotSuitable : public QException
{

public:

    ParameterIsNotSuitable(QString parameter) : Parameter(parameter) { }

    void raise() const { throw *this; }
    ParameterIsNotSuitable* clone() { return new ParameterIsNotSuitable(*this); }
    QString     Parameter;
};

#endif // PARAMETERISNOTSUITABLE_H
