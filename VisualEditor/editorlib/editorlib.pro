#-------------------------------------------------
#
# Project created by QtCreator 2017-12-19T11:39:00
#
#-------------------------------------------------

QT       -= gui

TARGET = editorlib
TEMPLATE = lib

DEFINES += EDITORLIB_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        editorlib.cpp \
    command/cmd.cpp \
    command/nc/homingcmd.cpp \
    command/nc/movecmd.cpp \
    command/nc/linearmovecmd.cpp \
    command/nc/rapidmovecmd.cpp \
    command/nc/dwellcmd.cpp \
    typedobject.cpp \
    command/nc/nccmd.cpp \
    command/program/roboskopcmd.cpp \
    command/program/programcmd.cpp \
    command/program/mainprogramcmd.cpp \
    command/program/preprogramcmd.cpp \
    parse/blockparser.cpp \
    parse/homingblockparser.cpp \
    parse/linearmoveblockparser.cpp \
    parse/rapidmoveblockparser.cpp \
    parse/dwellblockparser.cpp \
    parse/mainprogramblockparser.cpp \
    parse/iblockparser.cpp \
    generate/codegenerator.cpp \
    generate/nc/homingcodegenerator.cpp \
    generate/nc/dwellcodegenerator.cpp \
    generate/nc/linearmovecodegenerator.cpp \
    generate/nc/movecodegenerator.cpp \
    generate/nc/rapidmovecodegenerator.cpp \
    generate/compoundcodegenerator.cpp \
    generate/program/mainprogramcodegenerator.cpp \
    generate/program/preprogramcodegenerator.cpp \
    parse/step3blockparser.cpp \
    parse/compoundparser.cpp \
    command/program/step3program.cpp \
    parse/preprogramblockparser.cpp \
    generate/program/step3programuploadgenerator.cpp \
    beautify/beautifier.cpp

HEADERS += \
        editorlib.h \
        editorlib_global.h \
    command/cmd.h \
    command/nc/homingcmd.h \
    command/nc/movecmd.h \
    command/nc/linearmovecmd.h \
    command/nc/rapidmovecmd.h \
    command/nc/dwellcmd.h \
    typedobject.h \
    command/nc/nccmd.h \
    command/program/roboskopcmd.h \
    command/program/programcmd.h \
    command/program/mainprogramcmd.h \
    command/program/preprogramcmd.h \
    parse/blockparser.h \
    parse/homingblockparser.h \
    parse/linearmoveblockparser.h \
    parse/rapidmoveblockparser.h \
    parse/dwellblockparser.h \
    parse/mainprogramblockparser.h \
    exception/valuecannotbenegative.h \
    exception/propertynotspecified.h \
    parse/iblockparser.h \
    command/icmd.h \
    itypedobject.h \
    exception/parameterisnotsuitable.h \
    exception/incompatiblecommand.h \
    generate/icodegenerator.h \
    generate/codegenerator.h \
    generate/nc/homingcodegenerator.h \
    generate/nc/dwellcodegenerator.h \
    generate/nc/linearmovecodegenerator.h \
    generate/nc/movecodegenerator.h \
    generate/nc/rapidmovecodegenerator.h \
    generate/compoundcodegenerator.h \
    generate/program/mainprogramcodegenerator.h \
    generate/program/preprogramcodegenerator.h \
    exception/cmdnotvalid.h \
    parse/step3blockparser.h \
    parse/compoundparser.h \
    command/program/step3program.h \
    parse/preprogramblockparser.h \
    generate/program/step3programuploadgenerator.h \
    beautify/ibeautifier.h \
    beautify/beautifier.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../Lib/release/ -lLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../Lib/debug/ -lLib
else:unix: LIBS += -L$$OUT_PWD/../../Lib/ -lLib

INCLUDEPATH += $$PWD/../../Lib
DEPENDPATH += $$PWD/../../Lib
