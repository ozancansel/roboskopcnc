#ifndef COMMAND_H
#define COMMAND_H

#include "typedobject.h"
#include "icmd.h"
#include <QObject>

class Cmd : public QObject, public virtual ICmd , public TypedObject
{

public:

    static const QString TYPE;
    Cmd(QObject *parent = nullptr);
    virtual QString typeName() override;

};

#endif // COMMAND_H
