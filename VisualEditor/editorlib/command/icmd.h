#ifndef ICMD_H
#define ICMD_H

#include "itypedobject.h"

class ICmd : public virtual ITypedObject
{

public:

    virtual ~ICmd() { }
    virtual bool isValid() = 0;

};

#endif // ICMD_H
