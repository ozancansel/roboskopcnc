#ifndef NCCMD_H
#define NCCMD_H

#include "../cmd.h"

class NcCmd : public Cmd
{

public:

    static QString TYPE;
    NcCmd(QObject* parent = nullptr);
    virtual QString typeName() override;

};

#endif // NCCMD_H
