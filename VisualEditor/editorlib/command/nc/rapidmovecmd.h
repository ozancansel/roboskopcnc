#ifndef RAPIDMOVECMD_H
#define RAPIDMOVECMD_H

#include "movecmd.h"

class RapidMoveCmd : public MoveCmd
{

public:

    static QString TYPE;
    RapidMoveCmd(QObject* parent = nullptr);
    QString typeName() override;

};

#endif // RAPIDMOVECMD_H
