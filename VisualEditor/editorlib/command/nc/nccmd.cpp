#include "nccmd.h"

QString NcCmd::TYPE = "Nc";

NcCmd::NcCmd(QObject* parent)
    :
      Cmd(parent)
{
    addType(NcCmd::TYPE);
}

QString NcCmd::typeName(){
    return NcCmd::TYPE;
}
