#ifndef MOVECMD_H
#define MOVECMD_H

#include "nccmd.h"

class MoveCmd : public NcCmd
{

public:

    static QString TYPE;
    MoveCmd(QObject* parent = nullptr);

    virtual QString typeName() override;
    virtual bool    isValid();
    void    setX(const float x);
    void    setY(const float y);
    void    setZ(const float z);
    float   x();
    float   y();
    float   z();
    //x-y-zNull method returns false if the parameter is set
    bool    xNull();
    bool    yNull();
    bool    zNull();

private:

    float   m_x;
    float   m_y;
    float   m_z;

};

#endif // MOVECMD_H
