#include "dwellcmd.h"

const QString DwellCmd::TYPE = "Dwell";

DwellCmd::DwellCmd(QObject* parent)
    :
      NcCmd(parent) ,
      m_time(-1)
{
    addType(DwellCmd::TYPE);
}

//Polymorphic
QString DwellCmd::typeName(){
    return DwellCmd::TYPE;
}

bool DwellCmd::isValid(){
    //Time could not be zero or negative
    if(m_time <= 0)
        return false;

    return true;
}
//Polymorphic end

void DwellCmd::setTime(int val){
    m_time = val;
}

int DwellCmd::timeInMs(){
    if(!isValid())
        return -1;
    return m_time;
}

float DwellCmd::timeInSec(){
    if(!isValid())
        return -1;
    return m_time / 1000.0;
}

