#ifndef LINEARMOVECMD_H
#define LINEARMOVECMD_H

#include "movecmd.h"

class LinearMoveCmd : public MoveCmd
{

public:

    static QString TYPE;
    LinearMoveCmd(QObject* parent = nullptr);
    QString typeName() override;
    bool    isValid() override;
    float           feedRate();
    void            setFeedRate(float val);

private:

    float   m_feedRate;

};

#endif // LINEARMOVECMD_H
