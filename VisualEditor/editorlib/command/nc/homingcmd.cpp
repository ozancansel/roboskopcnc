#include "homingcmd.h"

QString HomingCmd::TYPE = "Homing";

HomingCmd::HomingCmd(QObject* parent)
    :
      NcCmd(parent)
{
    addType(HomingCmd::TYPE);
}

QString HomingCmd::typeName(){
    return TYPE;
}

bool HomingCmd::isValid(){
    return true;
}
