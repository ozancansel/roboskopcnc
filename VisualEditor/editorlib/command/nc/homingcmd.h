#ifndef HOMINGCMD_H
#define HOMINGCMD_H

#include "nccmd.h"

class HomingCmd : public NcCmd
{

public:

    static QString TYPE;
    HomingCmd(QObject* parent = nullptr);
    virtual QString typeName() override;
    virtual bool isValid() override;

};

#endif // HOMINGCMD_H
