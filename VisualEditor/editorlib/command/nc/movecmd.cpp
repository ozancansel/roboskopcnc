#include "movecmd.h"
#include <math.h>

QString MoveCmd::TYPE = "MoveCmd";

MoveCmd::MoveCmd(QObject* parent)
    :
      NcCmd(parent) ,
      m_x(NAN) ,
      m_y(NAN) ,
      m_z(NAN)
{
    addType(MoveCmd::TYPE);
}

//Polymorphic
QString MoveCmd::typeName(){
    return MoveCmd::TYPE;
}

bool MoveCmd::isValid(){
    //If none of the values is specified
    //return false
    if(xNull() && yNull() && zNull())
        return false;

    return true;
}

void MoveCmd::setX(const float x){
    m_x = x;
}

void MoveCmd::setY(const float y){
    m_y = y;
}

void MoveCmd::setZ(const float z){
    m_z = z;
}

float MoveCmd::x(){
    return m_x;
}

float MoveCmd::y(){
    return m_y;
}

float MoveCmd::z(){
    return m_z;
}

bool MoveCmd::xNull(){
    return qIsNaN(m_x);
}

bool MoveCmd::yNull(){
    return qIsNaN(m_y);
}

bool MoveCmd::zNull(){
    return qIsNaN(m_z);
}
