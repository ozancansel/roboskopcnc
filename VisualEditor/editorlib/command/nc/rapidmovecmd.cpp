#include "rapidmovecmd.h"

QString RapidMoveCmd::TYPE = "RapidMove";

RapidMoveCmd::RapidMoveCmd(QObject* parent)
    :
      MoveCmd(parent)
{

    addType(RapidMoveCmd::TYPE);
}

QString RapidMoveCmd::typeName(){
    return RapidMoveCmd::TYPE;
}
