#include "linearmovecmd.h"
#include <math.h>

QString LinearMoveCmd::TYPE = "LinearMove";

LinearMoveCmd::LinearMoveCmd(QObject* parent)
    :
      MoveCmd(parent) ,
      m_feedRate(NAN)
{
    addType(LinearMoveCmd::TYPE);
}

//Polymorphic
QString LinearMoveCmd::typeName(){
    return LinearMoveCmd::TYPE;
}

bool LinearMoveCmd::isValid(){
    //Check parent first
    if(!MoveCmd::isValid()){
        return false;
    }
    if(qIsNaN(m_feedRate))
        return false;

    return true;
}
//Polymorphic End

float LinearMoveCmd::feedRate(){
    return m_feedRate;
}

void LinearMoveCmd::setFeedRate(float val){
    m_feedRate = val;
}
