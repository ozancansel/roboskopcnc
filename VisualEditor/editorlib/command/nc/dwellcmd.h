#ifndef DWELLCMD_H
#define DWELLCMD_H

#include "nccmd.h"

class DwellCmd : public NcCmd
{
public:

    static const QString TYPE;
    DwellCmd(QObject* parent = nullptr);
    QString typeName() override;
    bool    isValid() override;
    void    setTime(int val);
    int     timeInMs();
    float   timeInSec();

private:

    int    m_time;
};

#endif // DWELLCMD_H
