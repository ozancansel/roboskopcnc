#include "cmd.h"
#include <QVariant>

const QString Cmd::TYPE = "Cmd";

Cmd::Cmd(QObject *parent)
    :
      QObject(parent)
{
    setProperty("type" , QVariant("Cmd"));
    TypedObject::addType(TYPE);
}

QString Cmd::typeName(){
    return TYPE;
}
