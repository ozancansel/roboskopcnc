#include "programcmd.h"

QString ProgramCmd::TYPE = "MainProg";

ProgramCmd::ProgramCmd(QObject* parent)
    :
      RoboskopCmd(parent)
{
    addType(ProgramCmd::TYPE);
}

bool ProgramCmd::addCommand(QSharedPointer<ICmd> cmd){
    if(!checkCmd(cmd))
        return false;

    m_commands.append(cmd);
}

//Polymorphic
QString ProgramCmd::typeName(){
    return ProgramCmd::TYPE;
}

bool ProgramCmd::checkCmd(QSharedPointer<ICmd> cmd){
    return !cmd.isNull();
}

//Polymorphic End
QList<QSharedPointer<ICmd>> ProgramCmd::commands(){
    return m_commands;
}
