#ifndef MAINPROGRAMCMD_H
#define MAINPROGRAMCMD_H

#include "programcmd.h"

class MainProgramCmd : public ProgramCmd
{

public:

    static const QString TYPE;
    static QStringList    ALLOWED_CMDS;
    MainProgramCmd(QObject* parent = nullptr);
    virtual QString typeName() override;
    virtual bool isValid() override;
    virtual bool checkCmd(QSharedPointer<ICmd> cmd);
    bool    loopForever();
    void    setLoopForever(bool val);

private:

    bool    m_loopForever;

};

#endif // MAINPROGRAMCMD_H
