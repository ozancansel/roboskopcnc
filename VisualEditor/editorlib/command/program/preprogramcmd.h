#ifndef PREPROGRAMCMD_H
#define PREPROGRAMCMD_H

#include "programcmd.h"

class PreProgramCmd : public ProgramCmd
{

public:

    static const QStringList    ALLOWED_CMDS;
    static const QString TYPE;
    PreProgramCmd(QObject* parent = nullptr);
    virtual QString typeName() override;
    virtual bool    isValid() override;
    virtual bool    checkCmd(QSharedPointer<ICmd> cmd) override;

};

#endif // PREPROGRAMCMD_H
