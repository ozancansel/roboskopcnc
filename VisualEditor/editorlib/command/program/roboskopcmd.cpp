#include "roboskopcmd.h"

QString RoboskopCmd::TYPE = "RCmd";
RoboskopCmd::RoboskopCmd(QObject* parent)
    :
      Cmd(parent)
{
    addType(RoboskopCmd::TYPE);
}

QString RoboskopCmd::typeName(){
    return RoboskopCmd::TYPE;
}
