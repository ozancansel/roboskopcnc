#include "mainprogramcmd.h"
#include "../nc/dwellcmd.h"
#include "../nc/homingcmd.h"
#include "../nc/linearmovecmd.h"
#include "../nc/rapidmovecmd.h"

const QString MainProgramCmd::TYPE = "MainProgCmd";
QStringList MainProgramCmd::ALLOWED_CMDS = {
    DwellCmd::TYPE ,
    HomingCmd::TYPE ,
    LinearMoveCmd::TYPE,
    RapidMoveCmd::TYPE
};

MainProgramCmd::MainProgramCmd(QObject* parent)
    :
      ProgramCmd(parent) ,
      m_loopForever(false)
{
    addType(TYPE);
}

QString MainProgramCmd::typeName(){
    return TYPE;
}

bool MainProgramCmd::isValid(){
    return true;
}

bool MainProgramCmd::loopForever(){
    return m_loopForever;
}

void MainProgramCmd::setLoopForever(bool val){
    m_loopForever = val;
}

bool MainProgramCmd::checkCmd(QSharedPointer<ICmd> cmd){
    //Firstly, let the parent check
    if(!ProgramCmd::checkCmd(cmd)){
        return false;
    }
    QString type = cmd.data()->typeName();
    bool res = ALLOWED_CMDS.contains(type);

    return res;
}
