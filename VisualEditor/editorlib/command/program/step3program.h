#ifndef STEP3PROGRAM_H
#define STEP3PROGRAM_H

#include "roboskopcmd.h"
#include "mainprogramcmd.h"
#include "preprogramcmd.h"

class Step3Program : public RoboskopCmd
{

public:

    static const QString TYPE;
    Step3Program(QObject* parent = nullptr);
    QString typeName() override;
    void setMainProgram(const QSharedPointer<MainProgramCmd> program);
    void setPreProgram(const QSharedPointer<PreProgramCmd> program);
    QSharedPointer<MainProgramCmd>  mainProgram();
    QSharedPointer<PreProgramCmd>   preProgram();
    bool    isValid();

private:

    QSharedPointer<MainProgramCmd>  m_mainProgram;
    QSharedPointer<PreProgramCmd>   m_preProgram;

};

#endif // STEP3PROGRAM_H
