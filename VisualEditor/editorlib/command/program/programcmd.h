#ifndef PROGRAMCMD_H
#define PROGRAMCMD_H

#include "roboskopcmd.h"
#include <QSharedPointer>

class ProgramCmd : public RoboskopCmd
{

public:

    static QString TYPE;
    ProgramCmd(QObject* parent = nullptr);
    QString typeName() override;
    bool    addCommand(QSharedPointer<ICmd> cmd);
    virtual bool checkCmd(QSharedPointer<ICmd> cmd);
    QList<QSharedPointer<ICmd>>  commands();

private:

    QList<QSharedPointer<ICmd>>  m_commands;

};

#endif // PROGRAMCMD_H
