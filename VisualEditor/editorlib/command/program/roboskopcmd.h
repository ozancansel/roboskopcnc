#ifndef ROBOSKOPCMD_H
#define ROBOSKOPCMD_H

#include "../cmd.h"

class RoboskopCmd : public Cmd
{

public:

    static QString TYPE;
    RoboskopCmd(QObject* parent = nullptr);
    virtual QString typeName() override;

};

#endif // ROBOSKOPCMD_H
