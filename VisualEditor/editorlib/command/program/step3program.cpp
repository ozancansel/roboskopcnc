#include "step3program.h"

const QString Step3Program::TYPE = "step3";

Step3Program::Step3Program(QObject* parent)
    :
      RoboskopCmd(parent)
{
    addType(TYPE);
}

QString Step3Program::typeName(){
    return TYPE;
}

void Step3Program::setMainProgram(const QSharedPointer<MainProgramCmd> program){
    m_mainProgram = program;
}

void Step3Program::setPreProgram(const QSharedPointer<PreProgramCmd> program){
    m_preProgram = program;
}

QSharedPointer<MainProgramCmd>  Step3Program::mainProgram(){
    return m_mainProgram;
}

QSharedPointer<PreProgramCmd>   Step3Program::preProgram(){
    return m_preProgram;
}

bool Step3Program::isValid(){
    if(m_mainProgram.isNull() || !m_mainProgram.data()->isValid())
        return false;

    if(!m_preProgram.isNull() && !m_preProgram.data()->isValid())
        return false;

    return true;
}
