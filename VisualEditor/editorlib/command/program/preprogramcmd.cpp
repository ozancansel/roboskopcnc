#include "preprogramcmd.h"
#include "../nc/dwellcmd.h"
#include "../nc/homingcmd.h"
#include "../nc/linearmovecmd.h"
#include "../nc/rapidmovecmd.h"

const QStringList PreProgramCmd::ALLOWED_CMDS = {
    DwellCmd::TYPE ,
    HomingCmd::TYPE ,
    LinearMoveCmd::TYPE ,
    RapidMoveCmd::TYPE
};

const QString PreProgramCmd::TYPE =  "preProg";

PreProgramCmd::PreProgramCmd(QObject* parent)
    :
      ProgramCmd(parent)
{
    addType(TYPE);
}

QString PreProgramCmd::typeName(){
    return TYPE;
}

bool PreProgramCmd::isValid(){
    return true;
}

bool PreProgramCmd::checkCmd(QSharedPointer<ICmd> cmd){
    //Let the parent check first
    if(!ProgramCmd::checkCmd(cmd)){
        return false;
    }

    return ALLOWED_CMDS.contains(cmd.data()->typeName());
}
