#ifndef STEP3PARSER_H
#define STEP3PARSER_H

#include "blockparser.h"
#include "compoundparser.h"

class Step3BlockParser : public BlockParser
{

    Q_OBJECT

public:

    static const QString TYPE;
    static const QString BLOCKS_LABEL;
    static void registerQmlType();
    Step3BlockParser(QObject* parent = nullptr);
    virtual QString parseableBlockName() override;
    virtual QSharedPointer<ICmd>    parseInternal(const QJsonObject &jsonData) override;


private:

    CompoundParser  m_compoundParser;

};

#endif // STEP3PARSER_H
