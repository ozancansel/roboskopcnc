#include "preprogramblockparser.h"
#include "exception/propertynotspecified.h"
#include "exception/parameterisnotsuitable.h"
#include "command/program/preprogramcmd.h"
#include "parse/dwellblockparser.h"
#include "parse/homingblockparser.h"
#include "parse/linearmoveblockparser.h"
#include "parse/rapidmoveblockparser.h"
#include <QJsonArray>
#include <QJsonObject>

const QString PreProgramBlockParser::STATEMENTS_LABEL = "statements";
const QString PreProgramBlockParser::TYPE = "pre_program";

PreProgramBlockParser::PreProgramBlockParser(QObject* parent)
    :
      BlockParser(parent)
{
    m_compoundParser.addParser(QSharedPointer<IBlockParser>(new DwellBlockParser()));
    m_compoundParser.addParser(QSharedPointer<IBlockParser>(new HomingBlockParser()));
    m_compoundParser.addParser(QSharedPointer<IBlockParser>(new RapidMoveBlockParser()));
    m_compoundParser.addParser(QSharedPointer<IBlockParser>(new LinearMoveBlockParser()));
}

QString PreProgramBlockParser::parseableBlockName(){
    return TYPE;
}

QSharedPointer<ICmd> PreProgramBlockParser::parseInternal(const QJsonObject &jsonData){
    if(!jsonData.keys().contains(STATEMENTS_LABEL))
        throw PropertyNotSpecified(STATEMENTS_LABEL);
    if(!jsonData[STATEMENTS_LABEL].isArray())
        throw ParameterIsNotSuitable(STATEMENTS_LABEL);

    QJsonArray statementsArr = jsonData[STATEMENTS_LABEL].toArray();
    QSharedPointer<PreProgramCmd> preProgCmd(new PreProgramCmd());

    foreach (QJsonValue cmdVal, statementsArr) {
        if(!cmdVal.isObject())
            throw ParameterIsNotSuitable(QString(STATEMENTS_LABEL).append(" nested obj"));

        QJsonObject cmdObj = cmdVal.toObject();
        if(m_compoundParser.canParse(cmdObj)){
            QSharedPointer<ICmd> cmd = m_compoundParser.parse(cmdObj);

            //If parsed command is suitable for pre program
            if(preProgCmd.data()->checkCmd(cmd)){
                //Adding command to program
                preProgCmd.data()->addCommand(cmd);
            } else {
                throw ParameterIsNotSuitable(QString(STATEMENTS_LABEL).append(" => ").append(cmd.data()->typeName()).append(" checkCmd() returns false"));
            }
        } else {
            throw ParameterIsNotSuitable(QString(STATEMENTS_LABEL).append(" canParse() returns false "));
        }
    }

    return preProgCmd;
}
