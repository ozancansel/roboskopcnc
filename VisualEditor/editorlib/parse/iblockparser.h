#ifndef IBLOCKPARSER_H
#define IBLOCKPARSER_H

#include <QString>
#include <QJsonObject>
#include <QSharedPointer>
#include "command/cmd.h"

class IBlockParser
{

public:

    virtual ~IBlockParser() { }
    virtual QString parseableBlockName() = 0;
    virtual bool    canParse(const QJsonObject& obj) = 0;
    virtual QSharedPointer<ICmd> parse(const QJsonObject& obj) = 0;

};

#endif // IBLOCKPARSER_H
