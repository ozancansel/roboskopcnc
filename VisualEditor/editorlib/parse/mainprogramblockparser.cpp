#include "mainprogramblockparser.h"
#include "dwellblockparser.h"
#include "homingblockparser.h"
#include "linearmoveblockparser.h"
#include "rapidmoveblockparser.h"
#include "command/program/mainprogramcmd.h"
#include <QSharedPointer>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QtQml/QtQml>

const QString MainProgramBlockParser::TYPE = "main_program";
const QString MainProgramBlockParser::LOOP_FOREVER_LABEL = "loop_forever";
const QString MainProgramBlockParser::STATEMENTS_LABEL = "statements";

void MainProgramBlockParser::registerQmlType(){
    qmlRegisterType<MainProgramBlockParser>("editorlib" , 1 , 0 , "MainProgramBlockParser");
}

MainProgramBlockParser::MainProgramBlockParser(QObject* parent)
    :
      BlockParser(parent)
{
    //Adding parsers
    m_nestedParser.addParser(QSharedPointer<IBlockParser>(new DwellBlockParser()));
    m_nestedParser.addParser(QSharedPointer<IBlockParser>(new HomingBlockParser()));
    m_nestedParser.addParser(QSharedPointer<IBlockParser>(new LinearMoveBlockParser()));
    m_nestedParser.addParser(QSharedPointer<IBlockParser>(new RapidMoveBlockParser()));
}

QSharedPointer<ICmd> MainProgramBlockParser::parseInternal(const QJsonObject &jsonData){
    //Label checking

    //Check whether statements is specified
    if(!jsonData.keys().contains(STATEMENTS_LABEL))
        throw PropertyNotSpecified(STATEMENTS_LABEL);

    //Check whether loop_forever label is specified
    if(!jsonData.keys().contains(LOOP_FOREVER_LABEL))
        throw PropertyNotSpecified(LOOP_FOREVER_LABEL);

    //Type checking
    if(!jsonData["loop_forever"].isBool())
        throw ParameterIsNotSuitable(LOOP_FOREVER_LABEL);

    //Check whether statements is an array
    if(!jsonData[STATEMENTS_LABEL].isArray())
        throw ParameterIsNotSuitable(STATEMENTS_LABEL);

    QJsonArray  arr = jsonData[STATEMENTS_LABEL].toArray();
    MainProgramCmd* mainProgCmd = new MainProgramCmd();
    mainProgCmd->setLoopForever(jsonData[LOOP_FOREVER_LABEL].toBool());

    foreach (QJsonValue arrVal, arr) {
        //If iterated value is not a json object
        if(!arrVal.isObject())
            throw ParameterIsNotSuitable(STATEMENTS_LABEL);

        QJsonObject iteratedObj = arrVal.toObject();

        //If parser can parse
        if(m_nestedParser.canParse(iteratedObj)){
            QSharedPointer<ICmd> cmd = m_nestedParser.parse(iteratedObj);

            //If cmd is incompatible
            if(!mainProgCmd->checkCmd(QSharedPointer<ICmd>(cmd)))
                throw IncompatibleCommand();

            //If cmd could not be added
            if(mainProgCmd->addCommand(QSharedPointer<ICmd>(cmd)))
                throw IncompatibleCommand();
        } else {
            throw IncompatibleCommand();
        }
    }

    return QSharedPointer<ICmd>(mainProgCmd);
}

QString MainProgramBlockParser::parseableBlockName(){
    return TYPE;
}
