#ifndef GROUPPARSER_H
#define GROUPPARSER_H

#include "blockparser.h"

class CompoundParser : public BlockParser
{

public:

    CompoundParser(QObject* parent = nullptr);
    virtual QSharedPointer<ICmd> parseInternal(const QJsonObject &jsonData) override;
    void    addParser(QSharedPointer<IBlockParser> parser);
    virtual QString parseableBlockName() override;

private:

    QList<QSharedPointer<IBlockParser>> m_parsers;
    QString                             m_parseableBlocksStr;
    QSharedPointer<IBlockParser>        retrieveSuitableParser(const QJsonObject& jsonData);

};

#endif // GROUPPARSER_H
