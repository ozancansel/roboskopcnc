#include "dwellblockparser.h"
#include <QJsonObject>
#include <QSharedPointer>
#include <QDebug>
#include "exception/valuecannotbenegative.h"
#include "exception/propertynotspecified.h"
#include "command/nc/dwellcmd.h"

const QString DwellBlockParser::TYPE = "dwell";
const QString DwellBlockParser::TIME_LABEL = "time";

DwellBlockParser::DwellBlockParser(QObject* parent)
    :
      BlockParser(parent)
{

}

QString DwellBlockParser::parseableBlockName(){
    return DwellBlockParser::TYPE;
}


QSharedPointer<ICmd> DwellBlockParser::parseInternal(const QJsonObject &jsonData){
    if(!jsonData.keys().contains(TIME_LABEL) || jsonData[TIME_LABEL].isNull()){
    //If time parameters is not specified
        throw PropertyNotSpecified(TIME_LABEL);
    }

    //If reached here, time parameters is specified and not null
    int value = jsonData[TIME_LABEL].toInt();

    //If value is negative
    if(value < 0)
        throw ValueCannotBeNegative();


    DwellCmd* cmd = new DwellCmd();
    cmd->setTime(value);

    return QSharedPointer<ICmd>((ICmd*)cmd);
}
