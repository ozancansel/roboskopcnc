#ifndef DWELLCMDPARSER_H
#define DWELLCMDPARSER_H

#include "blockparser.h"

class DwellBlockParser : public BlockParser
{

public:

    static const QString TYPE;
    static const QString TIME_LABEL;
    DwellBlockParser(QObject* parent = nullptr);
    virtual QString             parseableBlockName() override;

protected:

    virtual QSharedPointer<ICmd> parseInternal(const QJsonObject &jsonData) override;

};

#endif // DWELLCMDPARSER_H
