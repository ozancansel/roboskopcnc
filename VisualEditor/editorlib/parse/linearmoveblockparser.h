#ifndef LINEARMOVEBLOCKPARSER_H
#define LINEARMOVEBLOCKPARSER_H

#include "blockparser.h"

class LinearMoveBlockParser : public BlockParser
{

public:

    static const QString TYPE;
    LinearMoveBlockParser(QObject* parent = nullptr);
    virtual QString             parseableBlockName() override;

protected:

    virtual QSharedPointer<ICmd> parseInternal(const QJsonObject& obj) override;

};

#endif // LINEARMOVEBLOCKPARSER_H
