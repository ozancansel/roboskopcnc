#include "rapidmoveblockparser.h"
#include <QSharedPointer>
#include <QJsonObject>
#include "command/nc/rapidmovecmd.h"
#include "exception/propertynotspecified.h"

const QString RapidMoveBlockParser::TYPE = "rapid_move";

RapidMoveBlockParser::RapidMoveBlockParser(QObject* parent)
    :
      BlockParser(parent)
{

}

QSharedPointer<ICmd> RapidMoveBlockParser::parseInternal(const QJsonObject &jsonData){
    if(jsonData["x"].isUndefined())
        throw PropertyNotSpecified("x");
    if(jsonData["y"].isUndefined())
        throw PropertyNotSpecified("y");
    if(jsonData["z"].isUndefined())
        throw PropertyNotSpecified("z");

    float x , y , z;
    x = jsonData["x"].toDouble();
    y = jsonData["y"].toDouble();
    z = jsonData["z"].toDouble();

    RapidMoveCmd* cmd = new RapidMoveCmd();

    cmd->setX(x);
    cmd->setY(y);
    cmd->setZ(z);

    return QSharedPointer<ICmd>(cmd);
}

QString RapidMoveBlockParser::parseableBlockName(){
    return TYPE;
}
