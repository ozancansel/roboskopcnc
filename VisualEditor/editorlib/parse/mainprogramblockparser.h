#ifndef MAINPROGRAMBLOCKPARSER_H
#define MAINPROGRAMBLOCKPARSER_H

#include "blockparser.h"
#include "compoundparser.h"

//Exceptions
#include "exception/propertynotspecified.h"
#include "exception/parameterisnotsuitable.h"
#include "exception/incompatiblecommand.h"

class MainProgramBlockParser : public BlockParser
{

    Q_OBJECT

public:

    static const QString LOOP_FOREVER_LABEL;
    static const QString STATEMENTS_LABEL;
    static const QString TYPE;
    static void  registerQmlType();
    MainProgramBlockParser(QObject* parent = nullptr);
    virtual QString             parseableBlockName() override;

protected:

    virtual QSharedPointer<ICmd> parseInternal(const QJsonObject &jsonData) override;

private:

    CompoundParser m_nestedParser;

};

#endif // MAINPROGRAMBLOCKPARSER_H
