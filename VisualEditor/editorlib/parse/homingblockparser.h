#ifndef HOMINGBLOCKPARSER_H
#define HOMINGBLOCKPARSER_H

#include "blockparser.h"

class HomingBlockParser : public BlockParser
{

public:

    static const QString TYPE;
    HomingBlockParser(QObject* parent = nullptr);
    virtual QString             parseableBlockName() override;

protected:

    virtual QSharedPointer<ICmd> parseInternal(const QJsonObject &jsonData) override;
};

#endif // HOMINGBLOCKPARSER_H
