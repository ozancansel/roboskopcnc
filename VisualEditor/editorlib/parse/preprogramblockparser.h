#ifndef PREPROGRAMPARSER_H
#define PREPROGRAMPARSER_H

#include "blockparser.h"
#include "compoundparser.h"

class PreProgramBlockParser : public BlockParser
{

public:

    static const QString STATEMENTS_LABEL;
    static const QString TYPE;
    PreProgramBlockParser(QObject* parent = nullptr);
    QString parseableBlockName() override;

protected:

    QSharedPointer<ICmd> parseInternal(const QJsonObject &jsonData) override;

private:

    CompoundParser  m_compoundParser;

};

#endif // PREPROGRAMPARSER_H
