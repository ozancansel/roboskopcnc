#include "step3blockparser.h"
#include "mainprogramblockparser.h"
#include "preprogramblockparser.h"
#include "command/program/step3program.h"
#include <QtQml/QtQml>

const QString Step3BlockParser::TYPE = "step3";
const QString Step3BlockParser::BLOCKS_LABEL = "blocks";

void Step3BlockParser::registerQmlType(){
    qmlRegisterType<Step3BlockParser>("editorlib" , 1 , 0 , "Step3BlockParser");
}

Step3BlockParser::Step3BlockParser(QObject* parent)
    :
      BlockParser(parent)
{
    m_compoundParser.addParser(QSharedPointer<IBlockParser>(new MainProgramBlockParser()));
    m_compoundParser.addParser(QSharedPointer<IBlockParser>(new PreProgramBlockParser()));
}

QSharedPointer<ICmd> Step3BlockParser::parseInternal(const QJsonObject &jsonData){

    //If blocks doesn't specified
    if(!jsonData.keys().contains(BLOCKS_LABEL))
        throw PropertyNotSpecified(BLOCKS_LABEL);

    if(!jsonData[BLOCKS_LABEL].isArray())
        throw ParameterIsNotSuitable(BLOCKS_LABEL);

    QJsonArray blocksArray = jsonData[BLOCKS_LABEL].toArray();
    QSharedPointer<Step3Program> program(new Step3Program());

    foreach (QJsonValue arrVal, blocksArray) {

        //Check data
        if(!arrVal.isObject())
            throw ParameterIsNotSuitable(QString(BLOCKS_LABEL).append(" => nested obj"));

        QJsonObject iteratedObj = arrVal.toObject();

        //If it can be parsed
        if(m_compoundParser.canParse(iteratedObj)){
            QSharedPointer<ICmd> cmd = m_compoundParser.parse(iteratedObj);
            if(cmd.data()->is(MainProgramCmd::TYPE))
                program.data()->setMainProgram(cmd.dynamicCast<MainProgramCmd>());
            else if(cmd.data()->is(PreProgramCmd::TYPE))
                program.data()->setPreProgram(cmd.dynamicCast<PreProgramCmd>());
        } else
            throw ParameterIsNotSuitable(QString(BLOCKS_LABEL).append(" => obj couldn't be parsed"));
    }

    return program;
}

QString Step3BlockParser::parseableBlockName(){
    return TYPE;
}
