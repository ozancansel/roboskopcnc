#ifndef RAPIDMOVEBLOCKPARSER_H
#define RAPIDMOVEBLOCKPARSER_H

#include "blockparser.h"

class RapidMoveBlockParser : public BlockParser
{

public:

    static const QString TYPE;
    RapidMoveBlockParser(QObject* parent = nullptr);
    virtual QString             parseableBlockName() override;

protected:

    virtual QSharedPointer<ICmd> parseInternal(const QJsonObject &jsonData) override;

};

#endif // RAPIDMOVEBLOCKPARSER_H
