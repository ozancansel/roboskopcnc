#include "linearmoveblockparser.h"
#include <QJsonObject>
#include <QSharedPointer>
#include "command/nc/linearmovecmd.h"
#include "exception/propertynotspecified.h"
const QString LinearMoveBlockParser::TYPE = "linear_move";

LinearMoveBlockParser::LinearMoveBlockParser(QObject* parent)
    :
      BlockParser(parent)
{

}

QSharedPointer<ICmd> LinearMoveBlockParser::parseInternal(const QJsonObject &obj){

    if(obj["x"].isUndefined())
        throw PropertyNotSpecified("x");
    if(obj["y"].isUndefined())
        throw  PropertyNotSpecified("y");
    if(obj["z"].isUndefined())
        throw PropertyNotSpecified("z");
    if(obj["f"].isUndefined())
        throw PropertyNotSpecified("f");

    float x , y , z , f;
    x = obj["x"].toDouble();
    y = obj["y"].toDouble();
    z = obj["z"].toDouble();
    f = obj["f"].toDouble();

    LinearMoveCmd *cmd = new LinearMoveCmd();
    cmd->setX(x);
    cmd->setY(y);
    cmd->setZ(z);
    cmd->setFeedRate(f);

    return QSharedPointer<ICmd>(cmd);
}

QString LinearMoveBlockParser::parseableBlockName(){
    return TYPE;
}
