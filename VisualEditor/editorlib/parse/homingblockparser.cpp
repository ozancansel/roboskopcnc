#include "homingblockparser.h"
#include <QJsonObject>
#include <QSharedPointer>
#include "command/nc/homingcmd.h"

const QString HomingBlockParser::TYPE = "homing";

HomingBlockParser::HomingBlockParser(QObject* parent)
    :
      BlockParser(parent)
{

}

QSharedPointer<ICmd> HomingBlockParser::parseInternal(const QJsonObject &jsonData){
    Q_UNUSED(jsonData)
    HomingCmd* cmd = new HomingCmd();

    return QSharedPointer<ICmd>(cmd);
}

QString HomingBlockParser::parseableBlockName(){
    return TYPE;
}
