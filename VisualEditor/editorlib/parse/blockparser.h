#ifndef BLOCKPARSER_H
#define BLOCKPARSER_H

#include <QObject>
#include "command/cmd.h"
#include "iblockparser.h"

class BlockParser : public QObject , public IBlockParser
{

public:

    static const QString NAME_LABEL;
    BlockParser(QObject* parent = nullptr);
    virtual bool                    canParse(const QJsonObject &obj) override;
    virtual QSharedPointer<ICmd>     parse(const QJsonObject& jsonData) override;

protected:

    virtual  QSharedPointer<ICmd>    parseInternal(const QJsonObject& jsonData) = 0;

};

#endif // BLOCKPARSER_H
