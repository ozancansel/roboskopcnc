#include "blockparser.h"
#include "exception/parameterisnotsuitable.h"
#include <QJsonObject>
#include <QSharedPointer>

const QString BlockParser::NAME_LABEL = "name";

BlockParser::BlockParser(QObject* parent)
    :
      QObject(parent)
{

}

bool BlockParser::canParse(const QJsonObject& jsonData){
    if(!jsonData.keys().contains("name"))
        return false;
    QStringList parseableBlocks = parseableBlockName().split(",");
    return parseableBlocks.contains(jsonData["name"].toString());
}

QSharedPointer<ICmd> BlockParser::parse(const QJsonObject &jsonData){

    //If obj is empty
    if(jsonData.isEmpty())
        throw ParameterIsNotSuitable("jsonData");

    if(!canParse(jsonData))
        return QSharedPointer<ICmd>();

    return parseInternal(jsonData);
}
