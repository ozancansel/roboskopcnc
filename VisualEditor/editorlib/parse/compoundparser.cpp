#include "compoundparser.h"
#include <QSharedPointer>
#include <QJsonObject>

CompoundParser::CompoundParser(QObject* parent)
    :
      BlockParser(parent)
{

}

QSharedPointer<ICmd> CompoundParser::parseInternal(const QJsonObject &jsonData){
    QSharedPointer<IBlockParser> parser = retrieveSuitableParser(jsonData);

    //If could not find suitable parser
    if(parser.isNull())
        return QSharedPointer<ICmd>();

    return parser.data()->parse(jsonData);
}

void CompoundParser::addParser(QSharedPointer<IBlockParser> parser){
    //The parseableBlocksStr can be splitted via ','
    if(m_parsers.length() == 0)
        m_parseableBlocksStr.append(parser.data()->parseableBlockName());
    else
        m_parseableBlocksStr.append(QString(","))
                            .append(parser.data()->parseableBlockName());

    //Add parser
    m_parsers.append(parser);
}

QString CompoundParser::parseableBlockName(){
    return m_parseableBlocksStr;
}

QSharedPointer<IBlockParser> CompoundParser::retrieveSuitableParser(const QJsonObject &jsonData){
    foreach (QSharedPointer<IBlockParser> parser, m_parsers) {
        if(parser.data()->canParse(jsonData))
            return parser;
    }

    return QSharedPointer<IBlockParser>();
}
