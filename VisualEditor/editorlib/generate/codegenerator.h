#ifndef CODEGENERATOR_H
#define CODEGENERATOR_H

#include "icodegenerator.h"
#include <QObject>

class CodeGenerator : public QObject , public ICodeGenerator
{

public:

    CodeGenerator(QObject* parent = nullptr);
    QString  generate(const QSharedPointer<ICmd> cmd) override;

protected:

    virtual QString generateInternal(const QSharedPointer<ICmd> cmd) = 0;

};

#endif // CODEGENERATOR_H
