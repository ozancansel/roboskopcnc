#ifndef MULTICODEGENERATOR_H
#define MULTICODEGENERATOR_H

#include "codegenerator.h"

class CompoundCodeGenerator : public CodeGenerator
{

public:

    CompoundCodeGenerator(QObject* parent = nullptr);
    bool    canGenerate(const QSharedPointer<ICmd> cmd) override;
    void    addGenerator(QSharedPointer<ICodeGenerator> generator);

protected:

    QString generateInternal(const QSharedPointer<ICmd> cmd) override;
    QList<QSharedPointer<ICodeGenerator>>   m_generators;
    QSharedPointer<ICodeGenerator>  retrieveSuitableGenerator(const QSharedPointer<ICmd> cmd);

};

#endif // MULTICODEGENERATOR_H
