#ifndef HOMINGCODEGENERATOR_H
#define HOMINGCODEGENERATOR_H

#include "../codegenerator.h"

class HomingCodeGenerator : public CodeGenerator
{

public:

    HomingCodeGenerator(QObject* parent = nullptr);
    bool canGenerate(const QSharedPointer<ICmd> cmd) override;

protected:

    QString generateInternal(const QSharedPointer<ICmd> cmd) override;

};

#endif // HOMINGCODEGENERATOR_H
