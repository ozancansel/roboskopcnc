#ifndef DWELLGENERATOR_H
#define DWELLGENERATOR_H

#include "../codegenerator.h"

class DwellCodeGenerator : public CodeGenerator
{

public:

    DwellCodeGenerator(QObject* parent = nullptr);
    bool        canGenerate(const QSharedPointer<ICmd> cmd) override;

protected:

    QString     generateInternal(const QSharedPointer<ICmd> cmd) override;

};

#endif // DWELLGENERATOR_H
