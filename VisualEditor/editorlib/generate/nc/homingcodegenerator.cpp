#include "homingcodegenerator.h"
#include "command/nc/homingcmd.h"

HomingCodeGenerator::HomingCodeGenerator(QObject* parent)
    :
      CodeGenerator(parent)
{

}

bool HomingCodeGenerator::canGenerate(const QSharedPointer<ICmd> cmd){
    return cmd.data()->is(HomingCmd::TYPE);
}

QString HomingCodeGenerator::generateInternal(const QSharedPointer<ICmd> cmd){
    return QString("$H\n");
}
