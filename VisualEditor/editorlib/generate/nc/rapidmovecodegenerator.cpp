#include "rapidmovecodegenerator.h"
#include "command/nc/rapidmovecmd.h"

RapidMoveCodeGenerator::RapidMoveCodeGenerator(QObject* parent)
    :
      MoveCodeGenerator(parent)
{

}

bool RapidMoveCodeGenerator::canGenerate(const QSharedPointer<ICmd> cmd){
    return cmd.data()->is(RapidMoveCmd::TYPE);
}

QString RapidMoveCodeGenerator::generateInternal(const QSharedPointer<ICmd> cmd){
    QString code = QString("G0 %0\n")
                        .arg(MoveCodeGenerator::generateInternal(cmd));

    return code;
}
