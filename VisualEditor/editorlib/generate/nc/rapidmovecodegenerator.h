#ifndef RAPIDMOVEGENERATOR_H
#define RAPIDMOVEGENERATOR_H

#include "movecodegenerator.h"

class RapidMoveCodeGenerator : public MoveCodeGenerator
{

public:

    RapidMoveCodeGenerator(QObject* parent = nullptr);
    bool    canGenerate(const QSharedPointer<ICmd> cmd) override;

protected:

    QString generateInternal(const QSharedPointer<ICmd> cmd) override;

};

#endif // RAPIDMOVEGENERATOR_H
