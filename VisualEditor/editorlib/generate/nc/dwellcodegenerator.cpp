#include "dwellcodegenerator.h"
#include "command/nc/dwellcmd.h"
#include <QSharedPointer>

DwellCodeGenerator::DwellCodeGenerator(QObject* parent)
    :
      CodeGenerator(parent)
{

}

QString DwellCodeGenerator::generateInternal(const QSharedPointer<ICmd> cmd){
    QSharedPointer<DwellCmd> cmdCast = cmd.dynamicCast<DwellCmd>();

    QString code = QString("G4 P%0\n")
                        .arg(QString::number(cmdCast.data()->timeInSec() , 'f' , 3));

    return code;
}

bool DwellCodeGenerator::canGenerate(const QSharedPointer<ICmd> cmd){
    return cmd.data()->is(DwellCmd::TYPE);
}
