#ifndef MOVEGENERATOR_H
#define MOVEGENERATOR_H

#include "../codegenerator.h"

class MoveCodeGenerator : public CodeGenerator
{

public:

    MoveCodeGenerator(QObject* parent = nullptr);
    virtual bool canGenerate(const QSharedPointer<ICmd> cmd) override;

protected:

    virtual QString generateInternal(const QSharedPointer<ICmd> cmd) override;

};

#endif // MOVEGENERATOR_H
