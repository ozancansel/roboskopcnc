#include "linearmovecodegenerator.h"
#include "command/nc/linearmovecmd.h"

LinearMoveCodeGenerator::LinearMoveCodeGenerator(QObject* parent)
    :
      MoveCodeGenerator(parent)
{

}

bool LinearMoveCodeGenerator::canGenerate(const QSharedPointer<ICmd> cmd){
    return cmd.data()->is(LinearMoveCmd::TYPE);
}

QString LinearMoveCodeGenerator::generateInternal(const QSharedPointer<ICmd> cmd){
    QSharedPointer<LinearMoveCmd> cmdCast = cmd.dynamicCast<LinearMoveCmd>();
    QString code = QString("G1 %0 F%1\n")
                        .arg(MoveCodeGenerator::generateInternal(cmd))
                        .arg(QString::number(cmdCast.data()->feedRate() , 'f' , 3));

    return code;
}
