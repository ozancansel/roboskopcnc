#include "movecodegenerator.h"
#include "command/nc/movecmd.h"

MoveCodeGenerator::MoveCodeGenerator(QObject* parent)
    :
      CodeGenerator(parent)
{

}

bool MoveCodeGenerator::canGenerate(const QSharedPointer<ICmd> cmd){
    return cmd.data()->is(MoveCmd::TYPE);
}

QString MoveCodeGenerator::generateInternal(const QSharedPointer<ICmd> cmd){

    QSharedPointer<MoveCmd> cmdCast = cmd.dynamicCast<MoveCmd>();

    QString code;

    if(!cmdCast.data()->xNull())
        code.append(QString("X%0 ").arg(QString::number(cmdCast.data()->x() , 'f' , 3)));

    if(!cmdCast.data()->yNull())
        code.append(QString("Y%0 ").arg(QString::number(cmdCast.data()->y() , 'f' , 3)));

    if(!cmdCast.data()->zNull())
        code.append(QString("Z%0 ").arg(QString::number(cmdCast.data()->z() , 'f' , 3)));

    return code.trimmed();
}
