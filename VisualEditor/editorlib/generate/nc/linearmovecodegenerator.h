#ifndef LINEARMOVECODEGENERATOR_H
#define LINEARMOVECODEGENERATOR_H

#include "movecodegenerator.h"

class LinearMoveCodeGenerator : public MoveCodeGenerator
{

public:

    LinearMoveCodeGenerator(QObject* parent = nullptr);
    bool    canGenerate(const QSharedPointer<ICmd> cmd) override;

protected:

    QString generateInternal(const QSharedPointer<ICmd> cmd) override;

};

#endif // LINEARMOVECODEGENERATOR_H
