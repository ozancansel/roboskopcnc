#include "compoundcodegenerator.h"

CompoundCodeGenerator::CompoundCodeGenerator(QObject* parent)
    :
      CodeGenerator(parent)
{

}

bool CompoundCodeGenerator::canGenerate(const QSharedPointer<ICmd> cmd){
    foreach (QSharedPointer<ICodeGenerator> generator, m_generators) {
        if(generator.data()->canGenerate(cmd))
            return true;
    }

    return false;
}

void CompoundCodeGenerator::addGenerator(QSharedPointer<ICodeGenerator> generator){
    m_generators.append(generator);
}

QString CompoundCodeGenerator::generateInternal(const QSharedPointer<ICmd> cmd){
    QSharedPointer<ICodeGenerator> generator = retrieveSuitableGenerator(cmd);
    return generator.data()->generate(cmd);
}

QSharedPointer<ICodeGenerator> CompoundCodeGenerator::retrieveSuitableGenerator(const QSharedPointer<ICmd> cmd){

    foreach (QSharedPointer<ICodeGenerator> generator, m_generators) {
        if(generator.data()->canGenerate(cmd))
            return generator;
    }

    return QSharedPointer<ICodeGenerator>();
}
