#include "codegenerator.h"
#include "exception/cmdnotvalid.h"
#include "exception/parameterisnotsuitable.h"

CodeGenerator::CodeGenerator(QObject* parent)
    :
      QObject(parent)
{

}

QString CodeGenerator::generate(const QSharedPointer<ICmd> cmd){

    if(cmd.isNull())
        throw ParameterIsNotSuitable("cmd");

    if(!cmd.data()->isValid())
        throw CmdNotValid(cmd);

    if(!canGenerate(cmd))
        return "";

    return generateInternal(cmd);
}
