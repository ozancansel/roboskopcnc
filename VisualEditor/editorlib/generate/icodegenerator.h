#ifndef ICODEGENERATOR_H
#define ICODEGENERATOR_H

#include <QString>
#include <QSharedPointer>
#include "command/icmd.h"

class ICodeGenerator
{

public:

    virtual ~ICodeGenerator() { }
    virtual QString generate(const QSharedPointer<ICmd> cmd) = 0;
    virtual bool    canGenerate(const QSharedPointer<ICmd> cmd) = 0;

};

#endif // ICODEGENERATOR_H
