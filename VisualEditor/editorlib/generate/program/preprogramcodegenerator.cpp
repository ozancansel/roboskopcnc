#include "preprogramcodegenerator.h"
#include "command/program/preprogramcmd.h"
#include "generate/nc/dwellcodegenerator.h"
#include "generate/nc/homingcodegenerator.h"
#include "generate/nc/linearmovecodegenerator.h"
#include "generate/nc/rapidmovecodegenerator.h"

PreProgramCodeGenerator::PreProgramCodeGenerator(QObject* parent)
    :
      CodeGenerator(parent)
{
    m_statementsGenerator.addGenerator(QSharedPointer<ICodeGenerator>(new DwellCodeGenerator()));
    m_statementsGenerator.addGenerator(QSharedPointer<ICodeGenerator>(new HomingCodeGenerator()));
    m_statementsGenerator.addGenerator(QSharedPointer<ICodeGenerator>(new LinearMoveCodeGenerator()));
    m_statementsGenerator.addGenerator(QSharedPointer<ICodeGenerator>(new RapidMoveCodeGenerator()));
}

bool PreProgramCodeGenerator::canGenerate(const QSharedPointer<ICmd> cmd){
    return cmd.data()->is(PreProgramCmd::TYPE);
}

QString PreProgramCodeGenerator::generateInternal(const QSharedPointer<ICmd> cmd){
    QSharedPointer<PreProgramCmd> preProgCmd = cmd.dynamicCast<PreProgramCmd>();
    QString code;

    foreach (QSharedPointer<ICmd> currCmd, preProgCmd.data()->commands()) {
        code.append(m_statementsGenerator.generate(currCmd));
    }

    return code;
}
