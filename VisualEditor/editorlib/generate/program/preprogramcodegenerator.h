#ifndef PREPROGRAMCODEGENERATOR_H
#define PREPROGRAMCODEGENERATOR_H

#include "../codegenerator.h"
#include "../compoundcodegenerator.h"

class PreProgramCodeGenerator : public CodeGenerator
{

public:

    PreProgramCodeGenerator(QObject* parent = nullptr);
    bool canGenerate(const QSharedPointer<ICmd> cmd) override;

protected:

    QString generateInternal(const QSharedPointer<ICmd> cmd) override;
    CompoundCodeGenerator   m_statementsGenerator;

};

#endif // PREPROGRAMCODEGENERATOR_H
