#include "step3programuploadgenerator.h"
#include "command/program/step3program.h"
#include "mainprogramcodegenerator.h"
#include "preprogramcodegenerator.h"
#include <QRegularExpression>

Step3ProgramUploadGenerator::Step3ProgramUploadGenerator(QObject* parent)
    :
      CodeGenerator(parent) ,
      m_splitExpr(".{1,50}")
{

    //This is set due to slice string to 50 characters strings, special character
    m_splitExpr.setPatternOptions(QRegularExpression::DotMatchesEverythingOption);
    m_generator.addGenerator(QSharedPointer<ICodeGenerator>(new MainProgramCodeGenerator()));
    m_generator.addGenerator(QSharedPointer<ICodeGenerator>(new PreProgramCodeGenerator()));
}

bool Step3ProgramUploadGenerator::canGenerate(const QSharedPointer<ICmd> cmd){
    return cmd.data()->is(Step3Program::TYPE);
}

QString Step3ProgramUploadGenerator::generateInternal(const QSharedPointer<ICmd> cmd){
    QSharedPointer<Step3Program> program = cmd.dynamicCast<Step3Program>();

    QString code;

    //Check main program is not null
    //Generate main program data
    if(!program.data()->mainProgram().isNull()){
        QString mainProgCode = m_generator.generate(program.data()->mainProgram());

        //Slicing long strings
        QStringList splitted = mainProgCode.split(m_splitExpr);
        foreach (QString part, splitted) {
            //Wrap splitted string for microcontroller understandable command
            //_a,<MainProgramId>,<content>@
            code.append(QString("_a,1,%0@").arg(part));
        }
    }

    //Check pre program is not null
    if(!program.data()->preProgram().isNull()){
        //Generate program code
        QString preProgCode = m_generator.generate(program.data()->preProgram());

        QStringList splitted = preProgCode.split(m_splitExpr);

        foreach (QString part, splitted) {
            //Generated code wrapping for writing sd card
            //_a,<Pre Program Id>,<content>@
            //It is microcontroller command for append
            code.append(QString("_a,2,%0@").arg(part));
        }
    }

    return code;
}
