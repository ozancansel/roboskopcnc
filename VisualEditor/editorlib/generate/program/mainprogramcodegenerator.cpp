#include "mainprogramcodegenerator.h"
#include "exception/propertynotspecified.h"
#include "exception/parameterisnotsuitable.h"
#include "command/program/mainprogramcmd.h"
#include "generate/nc/dwellcodegenerator.h"
#include "generate/nc/homingcodegenerator.h"
#include "generate/nc/linearmovecodegenerator.h"
#include "generate/nc/rapidmovecodegenerator.h"

MainProgramCodeGenerator::MainProgramCodeGenerator(QObject* parent)
    :
      CodeGenerator(parent)
{
    m_statementsGenerator.addGenerator(QSharedPointer<ICodeGenerator>(new DwellCodeGenerator()));
    m_statementsGenerator.addGenerator(QSharedPointer<ICodeGenerator>(new HomingCodeGenerator()));
    m_statementsGenerator.addGenerator(QSharedPointer<ICodeGenerator>(new LinearMoveCodeGenerator()));
    m_statementsGenerator.addGenerator(QSharedPointer<ICodeGenerator>(new RapidMoveCodeGenerator()));
}

bool MainProgramCodeGenerator::canGenerate(const QSharedPointer<ICmd> cmd){
    return cmd.data()->is(MainProgramCmd::TYPE);
}

QString MainProgramCodeGenerator::generateInternal(const QSharedPointer<ICmd> cmd){
    QSharedPointer<MainProgramCmd> mainProgCmd = cmd.dynamicCast<MainProgramCmd>();

    QString code;

    //Iterate over commands
    foreach (QSharedPointer<ICmd> cmdIter, mainProgCmd.data()->commands()) {
        code.append(m_statementsGenerator.generate(cmdIter));
    }

    return code;
}
