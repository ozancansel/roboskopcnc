#ifndef STEP3PROGRAMCODEGENERATOR_H
#define STEP3PROGRAMCODEGENERATOR_H

#include "../codegenerator.h"
#include "../compoundcodegenerator.h"
#include <QRegularExpression>

class Step3ProgramUploadGenerator : public CodeGenerator
{

public:

    Step3ProgramUploadGenerator(QObject* parent = nullptr);
    bool canGenerate(const QSharedPointer<ICmd> cmd) override;

protected:

    QString generateInternal(const QSharedPointer<ICmd> cmd) override;

private:

    CompoundCodeGenerator   m_generator;
    QRegularExpression      m_splitExpr;

};

#endif // STEP3PROGRAMCODEGENERATOR_H
