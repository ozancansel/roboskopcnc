#ifndef MAINPROGRAMCODEGENERATOR_H
#define MAINPROGRAMCODEGENERATOR_H

#include "../compoundcodegenerator.h"

class MainProgramCodeGenerator : public CodeGenerator
{

public:

    MainProgramCodeGenerator(QObject* parent = nullptr);
    bool canGenerate(const QSharedPointer<ICmd> cmd) override;
    QString generateInternal(const QSharedPointer<ICmd> cmd) override;

private:

    CompoundCodeGenerator   m_statementsGenerator;

};

#endif // MAINPROGRAMCODEGENERATOR_H
