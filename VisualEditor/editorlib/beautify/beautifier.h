#ifndef BEAUTIFIER_H
#define BEAUTIFIER_H

#include <QObject>
#include "ibeautifier.h"

class Beautifier : public QObject , public IBeautifier
{

public:

    Beautifier( QObject* parent = nullptr );
    virtual void    beautify( QString &str ) override;
    virtual void    beautify( QString &str , int startIdx );
    virtual void    beautify( QString &str , int startIdx , int endIdx );

protected:

    virtual void    internalBeautify(QString &str) = 0;

};

#endif // BEAUTIFIER_H
