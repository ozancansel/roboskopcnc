#ifndef IBEAUTIFIER_H
#define IBEAUTIFIER_H

#include <QString>

class IBeautifier
{

public:

    virtual void    beautify(QString& text) = 0;
    virtual void    beautify(QString& text , int startIdx) = 0;
    virtual void    beautify(QString& text , int startIdx , int endIdx) = 0;

};

#endif // IBEAUTIFIER_H
