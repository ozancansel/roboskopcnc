#include "cmdtest.h"
#include "command/emptycmdfortest.h"

void CmdTest::test_should_register_itself_to_type_hiearchy(){
    EmptyCmdForTest emptyCmd;

    QVERIFY(emptyCmd.is(emptyCmd.typeName()));
}
