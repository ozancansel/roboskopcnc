#ifndef MOCKEDCMD_H
#define MOCKEDCMD_H

#include <gmock/gmock.h>
#include "command/icmd.h"

class MockedCmd : public ICmd
{

public:

    MOCK_METHOD0(isValid , bool());
    MOCK_METHOD1(is , bool(const QString));
    MOCK_METHOD0(typeName , QString());
    MOCK_METHOD1(addType , void(const QString));

};

#endif // MOCKEDCMD_H
