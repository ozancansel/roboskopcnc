#ifndef NCCMDTEST_H
#define NCCMDTEST_H

#include <QObject>
#include <QtTest/QtTest>

class NcCmdTest : public QObject
{

    Q_OBJECT

public:

    NcCmdTest(QObject* parent = nullptr);

private slots:

    void    test_should_register_itself_to_hiearchy();

};

#endif // NCCMDTEST_H
