#ifndef DWELLCMDTEST_H
#define DWELLCMDTEST_H

#include <QObject>
#include <QtTest/QtTest>

class DwellCmdTest : public QObject
{

    Q_OBJECT

private slots:

    void    test_typeName_should_returns_non_null_string();
    void    test_shoud_register_itself_to_type_hiearchy();
    void    test_isValid_if_time_is_not_specified_should_return_false();
    void    test_isValid_if_time_is_set_as_negative_value_should_return_false();
    void    test_isValid_if_time_is_set_as_zero_should_return_false();
    void    test_isValid_if_time_is_set_as_positive_should_return_true();
    void    test_timeInMs_if_is_not_valid_should_return_minus_one();
    void    test_timeInMs_if_is_valid_should_return_in_ms();
    void    test_timeInSec_if_is_not_valid_should_return_minus_one();
    void    test_timeInSec_if_is_valid_should_return_in_seconds();

};

#endif // DWELLCMDTEST_H
