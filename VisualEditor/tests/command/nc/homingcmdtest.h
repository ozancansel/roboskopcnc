#ifndef HOMINGCMDTEST_H
#define HOMINGCMDTEST_H

#include <QObject>
#include <QtTest/QtTest>

class HomingCmdTest : public QObject
{

    Q_OBJECT

public:

    HomingCmdTest(QObject* parent = nullptr);

private slots:

    void    test_is_valid_should_always_returns_true();
    void    test_should_register_itself_to_type_hierarchy();

};

#endif // HOMINGCMDTEST_H
