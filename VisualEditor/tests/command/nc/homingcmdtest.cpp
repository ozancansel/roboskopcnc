#include "homingcmdtest.h"
#include "command/nc/homingcmd.h"

HomingCmdTest::HomingCmdTest(QObject* parent)
    :
      QObject(parent)
{

}

void HomingCmdTest::test_is_valid_should_always_returns_true(){
    HomingCmd   cmd;

    QVERIFY(cmd.isValid());
}

void HomingCmdTest::test_should_register_itself_to_type_hierarchy(){
    HomingCmd cmd;

    QVERIFY(cmd.is(cmd.typeName()));
}
