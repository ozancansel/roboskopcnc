#include "dwellcmdtest.h"
#include "command/nc/dwellcmd.h"

void DwellCmdTest::test_typeName_should_returns_non_null_string(){
    DwellCmd cmd;

    QVERIFY(!cmd.typeName().isEmpty());
}

void DwellCmdTest::test_shoud_register_itself_to_type_hiearchy(){
    DwellCmd cmd;

    QVERIFY(cmd.is(cmd.typeName()));
}

void DwellCmdTest::test_isValid_if_time_is_not_specified_should_return_false(){
    DwellCmd cmd;

    QVERIFY(!cmd.isValid());
}

void DwellCmdTest::test_isValid_if_time_is_set_as_negative_value_should_return_false(){
    DwellCmd cmd;

    cmd.setTime(-15);

    QVERIFY(!cmd.isValid());
}

void DwellCmdTest::test_isValid_if_time_is_set_as_zero_should_return_false(){
    DwellCmd cmd;

    cmd.setTime(0);

    QVERIFY(!cmd.isValid());
}

void DwellCmdTest::test_isValid_if_time_is_set_as_positive_should_return_true(){
    DwellCmd cmd;

    cmd.setTime(15);

    QVERIFY(cmd.isValid());
}

void DwellCmdTest::test_timeInMs_if_is_not_valid_should_return_minus_one(){
    DwellCmd cmd;

    QCOMPARE(cmd.timeInMs() , -1);
}

void DwellCmdTest::test_timeInMs_if_is_valid_should_return_in_ms(){
    DwellCmd cmd;

    cmd.setTime(100);
    QCOMPARE(cmd.timeInMs() , 100);
}

void DwellCmdTest::test_timeInSec_if_is_not_valid_should_return_minus_one(){
    DwellCmd cmd;

    cmd.setTime(-100);
    QCOMPARE(cmd.timeInSec() , -1.0);
}

void DwellCmdTest::test_timeInSec_if_is_valid_should_return_in_seconds(){
    DwellCmd cmd;

    cmd.setTime(1000);

    QCOMPARE(cmd.timeInSec() , 1.0);
}
