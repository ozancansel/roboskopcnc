#ifndef LINEARMOVECMDTEST_H
#define LINEARMOVECMDTEST_H

#include "movecmdtest.h"

class LinearMoveCmdTest : public QObject
{
    Q_OBJECT

public:

    explicit LinearMoveCmdTest(QObject *parent = nullptr);

private slots:

    //Derived
    void    test_typeName_should_be_non_empty();
    void    test_should_add_itself_to_hiearchy();

};

#endif // LINEARMOVECMDTEST_H
