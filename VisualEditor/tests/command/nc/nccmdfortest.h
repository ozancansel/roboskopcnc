#ifndef NCCMDFORTEST_H
#define NCCMDFORTEST_H

#include "command/nc/nccmd.h"

class NcCmdForTest : public NcCmd
{

public:

    NcCmdForTest();
    bool    isValid();

};

#endif // NCCMDFORTEST_H
