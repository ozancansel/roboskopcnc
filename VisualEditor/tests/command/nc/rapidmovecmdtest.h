#ifndef RAPIDMOVECMDTEST_H
#define RAPIDMOVECMDTEST_H

#include <QObject>
#include <QtTest/QtTest>

class RapidMoveCmdTest : public QObject
{

    Q_OBJECT

private slots:

    void    test_typeName_should_return_non_null_string();
    void    test_should_register_itself_to_type_hierarchy();

};

#endif // RAPIDMOVECMDTEST_H
