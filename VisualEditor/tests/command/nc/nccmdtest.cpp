#include "nccmdtest.h"
#include "nccmdfortest.h"

NcCmdTest::NcCmdTest(QObject* parent)
    :
      QObject(parent)
{

}

void NcCmdTest::test_should_register_itself_to_hiearchy(){
    NcCmdForTest   cmd;

    QVERIFY(cmd.is(cmd.typeName()));
}
