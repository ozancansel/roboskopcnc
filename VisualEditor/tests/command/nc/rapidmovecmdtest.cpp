#include "rapidmovecmdtest.h"
#include "command/nc/rapidmovecmd.h"

void RapidMoveCmdTest::test_typeName_should_return_non_null_string(){
    RapidMoveCmd cmd;

    QVERIFY(!cmd.typeName().isNull());
}

void RapidMoveCmdTest::test_should_register_itself_to_type_hierarchy(){
    RapidMoveCmd cmd;
    QVERIFY(cmd.is(cmd.typeName()));
}
