#ifndef MOVECMDTEST_H
#define MOVECMDTEST_H

#include <QObject>
#include <QtTest/QtTest>
#include "command/nc/movecmd.h"

class MoveCmdTest : public QObject
{
    Q_OBJECT

public:

    MoveCmdTest(QObject* parent = nullptr);

private slots:

    virtual void    test_typeName_should_be_non_empty();
    virtual void    test_should_add_itself_to_hiearchy();
    void    test_isValid_if_just_x_specified_should_return_true();
    void    test_isValid_if_just_y_specified_should_return_true();
    void    test_isValid_if_just_z_specified_should_return_true();
    void    test_isValid_if_just_xy_specified_should_return_true();
    void    test_isValid_if_just_yz_specified_should_return_true();
    void    test_isValid_if_just_xz_specified_should_return_true();
    void    test_isValid_if_xyz_specified_should_return_true();
    void    test_isValid_if_xyz_are_not_specified_should_return_true();
    void    test_x_getter_setter();
    void    test_y_getter_setter();
    void    test_z_getter_setter();
    void    test_xNull_if_x_is_set_a_value_should_return_false();
    void    test_xNull_if_x_is_nan_should_return_true();
    void    test_yNull_if_y_is_set_a_value_should_return_false();
    void    test_yNull_if_y_is_nan_should_return_true();
    void    test_zNull_if_z_is_set_a_value_should_return_false();
    void    test_zNull_if_z_is_null_should_return_true();

};

#endif // MOVECMDTEST_H
