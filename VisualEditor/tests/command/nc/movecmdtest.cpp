#include "movecmdtest.h"
#include "command/nc/movecmd.h"
#include "math.h"

MoveCmdTest::MoveCmdTest(QObject* parent)
    :
      QObject(parent)
{
}

void MoveCmdTest::test_typeName_should_be_non_empty(){
    MoveCmd cmd;

    QVERIFY(!cmd.typeName().isEmpty());
}

void MoveCmdTest::test_should_add_itself_to_hiearchy(){
    MoveCmd cmd;

    QVERIFY(cmd.is(cmd.typeName()));
}

void MoveCmdTest::test_isValid_if_just_x_specified_should_return_true(){
    MoveCmd cmd;

    cmd.setX(10);
    QVERIFY(cmd.isValid());
}

void MoveCmdTest::test_isValid_if_just_y_specified_should_return_true(){
    MoveCmd cmd;
    cmd.setY(10);

    QVERIFY(cmd.isValid());
}

void MoveCmdTest::test_isValid_if_just_z_specified_should_return_true(){
    MoveCmd cmd;

    cmd.setZ(10);

    QVERIFY(cmd.isValid());
}

void MoveCmdTest::test_isValid_if_just_xy_specified_should_return_true(){
    MoveCmd cmd;

    cmd.setX(10);
    cmd.setY(10);

    QVERIFY(cmd.isValid());
}

void MoveCmdTest::test_isValid_if_just_yz_specified_should_return_true(){
    MoveCmd cmd;

    cmd.setY(10);
    cmd.setZ(10);

    QVERIFY(cmd.isValid());
}

void MoveCmdTest::test_isValid_if_just_xz_specified_should_return_true(){
    MoveCmd cmd;

    cmd.setX(10);
    cmd.setZ(10);

    QVERIFY(cmd.isValid());
}

void MoveCmdTest::test_isValid_if_xyz_specified_should_return_true(){
    MoveCmd cmd;


    cmd.setX(10);
    cmd.setY(10);
    cmd.setZ(10);

    QVERIFY(cmd.isValid());
}

void MoveCmdTest::test_isValid_if_xyz_are_not_specified_should_return_true(){
    MoveCmd cmd;


    cmd.setX(10);
    cmd.setY(10);
    cmd.setZ(10);

    QVERIFY(cmd.isValid());
}

void MoveCmdTest::test_x_getter_setter(){
    MoveCmd cmd;

    cmd.setX(10);

    QCOMPARE(cmd.x() , 10.0);
}

void MoveCmdTest::test_y_getter_setter(){
    MoveCmd cmd;
    cmd.setY(10);
    QCOMPARE(cmd.y() , 10.0);
}

void MoveCmdTest::test_z_getter_setter(){
    MoveCmd cmd;
    cmd.setZ(10);
    QCOMPARE(cmd.z() , 10.0);
}

void MoveCmdTest::test_xNull_if_x_is_set_a_value_should_return_false(){
    MoveCmd cmd;
    cmd.setX(10);
    QVERIFY(!cmd.xNull());
}

void MoveCmdTest::test_xNull_if_x_is_nan_should_return_true(){
    MoveCmd cmd;
    cmd.setX(NAN);
    QVERIFY(cmd.xNull());
}

void MoveCmdTest::test_yNull_if_y_is_set_a_value_should_return_false(){
    MoveCmd cmd;
    cmd.setY(10);
    QVERIFY(!cmd.yNull());
}

void MoveCmdTest::test_yNull_if_y_is_nan_should_return_true(){
    MoveCmd cmd;
    cmd.setY(NAN);
    QVERIFY(cmd.yNull());
}

void MoveCmdTest::test_zNull_if_z_is_set_a_value_should_return_false(){
    MoveCmd cmd;
    cmd.setZ(10);
    QVERIFY(!cmd.zNull());
}

void MoveCmdTest::test_zNull_if_z_is_null_should_return_true(){
    MoveCmd cmd;
    cmd.setZ(NAN);
    QVERIFY(cmd.zNull());
}
