#include "commandgeneratorfortest.h"
#include <QSharedPointer>

QSharedPointer<DwellCmd> CommandGeneratorForTest::generateDwellCmd(int time){
    DwellCmd* dwellCmd = new DwellCmd();

    dwellCmd->setTime(time);

    return QSharedPointer<DwellCmd>(dwellCmd);
}

QSharedPointer<LinearMoveCmd> CommandGeneratorForTest::generateLinearMoveCmd(float x,float y,float z,float f){
    LinearMoveCmd* linearMoveCmd = new LinearMoveCmd();

    linearMoveCmd->setX(x);
    linearMoveCmd->setY(y);
    linearMoveCmd->setZ(z);
    linearMoveCmd->setFeedRate(f);

    return QSharedPointer<LinearMoveCmd>(linearMoveCmd);
}

QSharedPointer<RapidMoveCmd> CommandGeneratorForTest::generateRapidMoveCmd(float x,float y,float z){
    RapidMoveCmd* rapidMoveCmd = new RapidMoveCmd();

    rapidMoveCmd->setX(x);
    rapidMoveCmd->setY(y);
    rapidMoveCmd->setZ(z);

    return QSharedPointer<RapidMoveCmd>(rapidMoveCmd);
}

QSharedPointer<HomingCmd> CommandGeneratorForTest::generateHomingCmd(){
    HomingCmd* homingCmd = new HomingCmd();

    return QSharedPointer<HomingCmd>(homingCmd);
}
