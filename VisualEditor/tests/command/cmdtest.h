#ifndef CMDTEST_H
#define CMDTEST_H

#include <QObject>
#include <QtTest/QtTest>

class CmdTest : public QObject
{

    Q_OBJECT

private slots:

    void    test_should_register_itself_to_type_hiearchy();
};

#endif // CMDTEST_H
