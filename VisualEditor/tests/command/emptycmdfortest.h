#ifndef EMPTYCMD_H
#define EMPTYCMD_H

#include "command/cmd.h"

class EmptyCmdForTest : public Cmd
{

public:

    EmptyCmdForTest();
    bool        isValid();

};

#endif // EMPTYCMD_H
