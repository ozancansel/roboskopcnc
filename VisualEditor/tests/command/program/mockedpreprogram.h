#ifndef MOCKEDPREPROGRAM_H
#define MOCKEDPREPROGRAM_H

#include <gmock/gmock.h>
#include "command/program/preprogramcmd.h"

class MockedPreProgram : public PreProgramCmd
{

public:

    MOCK_METHOD0(isValid , bool());

};

#endif // MOCKEDPREPROGRAM_H
