#ifndef STEP3PROGRAMTEST_H
#define STEP3PROGRAMTEST_H

#include <QObject>
#include <QtTest/QtTest>
#include <gmock/gmock.h>

class Step3ProgramTest : public QObject
{

    Q_OBJECT

private slots:

    void    test_isValid_if_main_program_is_null_should_return_false();
    void    test_isValid_if_main_program_is_not_valid_should_return_false();
    void    test_isValid_if_pre_program_is_not_valid_should_return_false();
    void    test_isValid_if_main_program_is_ok_and_pre_program_is_null_should_return_true();
    void    test_isValid_if_main_program_is_ok_and_pre_program_is_ok_should_return_true();

};

#endif // STEP3PROGRAMTEST_H
