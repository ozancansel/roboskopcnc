#ifndef PROGRAMCMDTEST_H
#define PROGRAMCMDTEST_H

#include <QObject>
#include <QtTest/QtTest>
#include "command/program/programcmd.h"

class MockedProgramCmd : public ProgramCmd
{

    public:

};

class ProgramCmdTest : public QObject
{

    Q_OBJECT

public:

    void    test_typeName_should_equal_to_TYPE();
    void    test_should_register_itself_to_hiearchy();
    void    test_addCommand_if_the_command_is_suitable_should_return_true();
    void    test_addCommand_if_the_command_is_not_suitable_should_return_false();

};

#endif // PROGRAMCMDTEST_H
