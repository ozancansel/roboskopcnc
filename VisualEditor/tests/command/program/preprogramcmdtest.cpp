#include "preprogramcmdtest.h"
#include "command/program/preprogramcmd.h"
#include "command/nc/homingcmd.h"
#include "command/nc/linearmovecmd.h"
#include "command/nc/rapidmovecmd.h"
#include "command/nc/dwellcmd.h"
#include "command/program/mainprogramcmd.h"
#include <QSharedPointer>

void PreProgramCmdTest::test_typeName_should_return_TYPE(){
    PreProgramCmd   cmd;

    QVERIFY(!cmd.typeName().isEmpty());
}

void PreProgramCmdTest::test_should_register_itself_to_type_hiearchy(){
    PreProgramCmd   cmd;
    QVERIFY(cmd.is(cmd.typeName()));
}

void PreProgramCmdTest::test_isValid_should_return_true(){
    PreProgramCmd cmd;
    QVERIFY(cmd.isValid());
}

void PreProgramCmdTest::test_checkCmd_if_it_is_attempted_to_add_dwell_cmd_should_return_true(){
    PreProgramCmd   cmd;
    DwellCmd*   dwellCmd = new DwellCmd();
    QVERIFY(cmd.checkCmd(QSharedPointer<Cmd>(dwellCmd)));
}

void PreProgramCmdTest::test_checkCmd_if_it_is_attempted_to_add_homing_cmd_should_return_true(){
    PreProgramCmd   cmd;
    HomingCmd*      homingCmd = new HomingCmd();
    QVERIFY(cmd.checkCmd(QSharedPointer<Cmd>(homingCmd)));
}

void PreProgramCmdTest::test_checkCmd_if_it_is_attempted_to_add_linear_move_cmd_should_return_true(){
    PreProgramCmd   cmd;
    LinearMoveCmd*  moveCmd = new LinearMoveCmd();
    QVERIFY(cmd.checkCmd(QSharedPointer<Cmd>(moveCmd)));
}

void PreProgramCmdTest::test_checkCmd_if_it_is_attempted_to_add_rapid_move_cmd_should_return_true(){
    PreProgramCmd   cmd;
    RapidMoveCmd*   rapidMove = new RapidMoveCmd();
    QVERIFY(cmd.checkCmd(QSharedPointer<Cmd>(rapidMove)));
}

void PreProgramCmdTest::test_checkCmd_if_it_is_attempted_to_add_main_program_cmd_should_return_false(){
    PreProgramCmd   cmd;
    MainProgramCmd  *mainProgCmd = new MainProgramCmd();
    QVERIFY(!cmd.checkCmd(QSharedPointer<Cmd>(mainProgCmd)));
}
