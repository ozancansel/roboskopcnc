#include "step3programtest.h"
#include "command/program/step3program.h"
#include "mockedmainprogram.h"
#include "mockedpreprogram.h"

void Step3ProgramTest::test_isValid_if_main_program_is_not_valid_should_return_false(){
    Step3Program    program;
    QSharedPointer<MockedMainProgram> mainProg(new MockedMainProgram());

    program.setMainProgram(mainProg);

    EXPECT_CALL(*(mainProg.data()) , isValid())
            .WillOnce(::testing::Return(false));

    QVERIFY(!program.isValid());
}

void Step3ProgramTest::test_isValid_if_main_program_is_null_should_return_false(){
    Step3Program    program;

    QVERIFY(!program.isValid());
}

void Step3ProgramTest::test_isValid_if_pre_program_is_not_valid_should_return_false(){
    Step3Program    program;
    QSharedPointer<MockedMainProgram> mainProg(new MockedMainProgram());
    QSharedPointer<MockedPreProgram>    preProg(new MockedPreProgram());

    EXPECT_CALL(*(mainProg.data()) , isValid())
            .WillOnce(::testing::Return(true));

    EXPECT_CALL(*(preProg.data()) , isValid())
            .WillOnce(::testing::Return(false));

    program.setMainProgram(mainProg);
    program.setPreProgram(preProg);

    QVERIFY(!program.isValid());
}

void Step3ProgramTest::test_isValid_if_main_program_is_ok_and_pre_program_is_null_should_return_true(){
    Step3Program    program;
    QSharedPointer<MockedMainProgram> mainProg(new MockedMainProgram());
    QSharedPointer<MockedPreProgram>    preProg(new MockedPreProgram());

    EXPECT_CALL(*(mainProg.data()) , isValid())
            .WillOnce(::testing::Return(true));
    program.setMainProgram(mainProg);

    QVERIFY(program.isValid());
}

void Step3ProgramTest::test_isValid_if_main_program_is_ok_and_pre_program_is_ok_should_return_true(){
    Step3Program    program;
    QSharedPointer<MockedMainProgram> mainProg(new MockedMainProgram());
    QSharedPointer<MockedPreProgram>    preProg(new MockedPreProgram());

    EXPECT_CALL(*(mainProg.data()) , isValid())
            .WillOnce(::testing::Return(true));

    EXPECT_CALL(*(preProg.data()) , isValid())
            .WillOnce(::testing::Return(true));

    program.setMainProgram(mainProg);
    program.setPreProgram(preProg);

    QVERIFY(program.isValid());
}
