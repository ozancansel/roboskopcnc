#ifndef MAINPROGRAMCMDTEST_H
#define MAINPROGRAMCMDTEST_H

#include <QObject>
#include <QtTest/QtTest>

class MainProgramCmdTest : public QObject
{
    Q_OBJECT

private slots:

    void    test_typeName_should_be_equal_to_its_TYPE();
    void    test_should_register_itself_to_type_hierarchy();
    void    test_checkCmd_if_it_is_attempted_to_add_dwell_cmd_should_return_true();
    void    test_checkCmd_if_it_is_attempted_to_add_homing_cmd_should_return_true();
    void    test_checkCmd_if_it_is_attempted_to_add_linear_move_cmd_should_return_true();
    void    test_checkCmd_if_it_is_attempted_to_add_rapid_move_cmd_should_return_true();
    void    test_checkCmd_if_it_is_attempted_to_add_main_program_cmd_should_return_false();
    void    test_checkCmd_if_it_is_attempted_to_add_pre_program_cmd_should_return_false();
    void    test_addCmd_if_the_command_is_suitable_should_add_command();
    void    test_addCmd_if_the_command_is_not_suitable_should_not_add();
    void    test_isValid_should_return_true();

};

#endif // MAINPROGRAMCMDTEST_H
