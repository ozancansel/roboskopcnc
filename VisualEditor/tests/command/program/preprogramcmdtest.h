#ifndef PREPROGRAMCMDTEST_H
#define PREPROGRAMCMDTEST_H

#include <QtTest/QtTest>
#include <QObject>

class PreProgramCmdTest : public QObject
{

    Q_OBJECT

private slots:

    void    test_typeName_should_return_TYPE();
    void    test_should_register_itself_to_type_hiearchy();
    void    test_isValid_should_return_true();
    void    test_checkCmd_if_it_is_attempted_to_add_homing_cmd_should_return_true();
    void    test_checkCmd_if_it_is_attempted_to_add_linear_move_cmd_should_return_true();
    void    test_checkCmd_if_it_is_attempted_to_add_rapid_move_cmd_should_return_true();
    void    test_checkCmd_if_it_is_attempted_to_add_dwell_cmd_should_return_true();
    void    test_checkCmd_if_it_is_attempted_to_add_main_program_cmd_should_return_false();

};

#endif // PREPROGRAMCMDTEST_H
