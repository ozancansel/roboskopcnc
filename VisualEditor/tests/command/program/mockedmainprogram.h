#ifndef MOCKEDMAINPROGRAM_H
#define MOCKEDMAINPROGRAM_H

#include <gmock/gmock.h>
#include <QObject>
#include <QtTest/QtTest>
#include "command/program/mainprogramcmd.h"

class MockedMainProgram : public MainProgramCmd
{

public:

    MOCK_METHOD0(isValid , bool());

};

#endif // MOCKEDMAINPROGRAM_H
