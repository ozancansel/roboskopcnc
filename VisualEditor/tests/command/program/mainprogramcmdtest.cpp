#include "mainprogramcmdtest.h"
#include "command/program/mainprogramcmd.h"
#include "command/nc/dwellcmd.h"
#include "command/nc/homingcmd.h"
#include "command/nc/linearmovecmd.h"
#include "command/nc/rapidmovecmd.h"
#include "command/program/mainprogramcmd.h"
#include "command/program/preprogramcmd.h"
#include <QSharedPointer>

void MainProgramCmdTest::test_typeName_should_be_equal_to_its_TYPE(){
    MainProgramCmd  cmd;

    QCOMPARE(cmd.typeName() , MainProgramCmd::TYPE);
}

void MainProgramCmdTest::test_should_register_itself_to_type_hierarchy(){
    MainProgramCmd cmd;
    QVERIFY(cmd.is(cmd.typeName()));
}

void MainProgramCmdTest::test_checkCmd_if_it_is_attempted_to_add_dwell_cmd_should_return_true(){
    MainProgramCmd cmd;
    Cmd*       dwellCmd = new DwellCmd();
    bool res = cmd.checkCmd(QSharedPointer<Cmd>(dwellCmd));
    QVERIFY(res);
}

void MainProgramCmdTest::test_checkCmd_if_it_is_attempted_to_add_homing_cmd_should_return_true(){
    MainProgramCmd  cmd;
    Cmd       *homingCmd = new HomingCmd();
    QVERIFY(cmd.checkCmd(QSharedPointer<Cmd>(homingCmd)));
}

void MainProgramCmdTest::test_checkCmd_if_it_is_attempted_to_add_linear_move_cmd_should_return_true(){
    MainProgramCmd  cmd;
    Cmd   *linearMoveCmd = new LinearMoveCmd();
    QVERIFY(cmd.checkCmd(QSharedPointer<Cmd>(linearMoveCmd)));
}

void MainProgramCmdTest::test_checkCmd_if_it_is_attempted_to_add_rapid_move_cmd_should_return_true(){
    MainProgramCmd  cmd;
    RapidMoveCmd    *rapidMoveCmd = new RapidMoveCmd();
    QVERIFY(cmd.checkCmd(QSharedPointer<Cmd>(rapidMoveCmd)));
}

void MainProgramCmdTest::test_checkCmd_if_it_is_attempted_to_add_main_program_cmd_should_return_false(){
    MainProgramCmd  cmd1;
    MainProgramCmd  *cmd2 = new MainProgramCmd();
    QVERIFY(!cmd1.checkCmd(QSharedPointer<Cmd>(cmd2)));
}

void MainProgramCmdTest::test_checkCmd_if_it_is_attempted_to_add_pre_program_cmd_should_return_false(){
    MainProgramCmd  cmd1;
    PreProgramCmd   *preProgramCmd = new PreProgramCmd();
    QVERIFY(!cmd1.checkCmd(QSharedPointer<Cmd>(preProgramCmd)));
}

void MainProgramCmdTest::test_addCmd_if_the_command_is_suitable_should_add_command(){
    MainProgramCmd  cmd;
    RapidMoveCmd*   rapidCmd = new RapidMoveCmd();
    QSharedPointer<Cmd> ptr(rapidCmd);
    cmd.addCommand(ptr);
    QVERIFY(cmd.commands().contains(ptr));
}

void MainProgramCmdTest::test_addCmd_if_the_command_is_not_suitable_should_not_add(){
    MainProgramCmd  cmd;
    PreProgramCmd   *preProgCmd = new PreProgramCmd();
    QSharedPointer<Cmd> ptr(preProgCmd);
    cmd.addCommand(ptr);
    QVERIFY(!cmd.commands().contains(ptr));
}

void MainProgramCmdTest::test_isValid_should_return_true(){
    MainProgramCmd  cmd;

    QVERIFY(cmd.isValid());
}
