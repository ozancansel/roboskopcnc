#ifndef COMMANDGENERATORFORTEST_H
#define COMMANDGENERATORFORTEST_H

#include "command/nc/dwellcmd.h"
#include "command/nc/linearmovecmd.h"
#include "command/nc/rapidmovecmd.h"
#include "command/nc/homingcmd.h"

class CommandGeneratorForTest
{
public:
    static QSharedPointer<DwellCmd> generateDwellCmd(int time = 1000);
    static QSharedPointer<LinearMoveCmd> generateLinearMoveCmd(float x = 10, float y = 10 , float z = 10 , float f = 200);
    static QSharedPointer<RapidMoveCmd> generateRapidMoveCmd(float x = 10 , float y = 10 , float z = 10);
    static QSharedPointer<HomingCmd>    generateHomingCmd();
};

#endif // COMMANDGENERATORFORTEST_H
