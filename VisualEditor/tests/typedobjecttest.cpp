#include "typedobjecttest.h"
#include "emptytypedobjectfortest.h"

TypedObjectTest::TypedObjectTest(QObject *parent)
    :
      QObject(parent)
{

}

void TypedObjectTest::test_is_method(){
     EmptyTypedObjectForTest obj;

    //The type is added in constructor
    QVERIFY(obj.is("example-1"));
    QVERIFY(obj.is("type"));
}
