#include <QtTest/QtTest>
#include "typedobjecttest.h"
#include "command/cmdtest.h"
#include "command/nc/nccmdtest.h"
#include "command/nc/homingcmdtest.h"
#include "command/nc/movecmdtest.h"
#include "command/nc/dwellcmdtest.h"
#include "command/nc/linearmovecmdtest.h"
#include "command/nc/rapidmovecmdtest.h"
#include "command/program/mainprogramcmdtest.h"
#include "command/program/preprogramcmdtest.h"
#include "command/program/step3programtest.h"
#include "parse/blockparsertest.h"
#include "parse/dwellparsertest.h"
#include "parse/rapidmoveblockparsertest.h"
#include "parse/linearmoveblockparsertest.h"
#include "parse/mainprogramblockparsertest.h"
#include "parse/preprogramblockparsertest.h"
#include "parse/step3blockparsertest.h"
#include "parse/groupparsertest.h"
#include "generate/codegeneratortest.h"
#include "generate/nc/dwellcodegeneratortest.h"
#include "generate/nc/movecodegeneratortest.h"
#include "generate/nc/linearmovecodegeneratortest.h"
#include "generate/nc/rapidmovecodegeneratortest.h"
#include "generate/nc/homingcodegeneratortest.h"
#include "generate/compouncodegeneratortests.h"
#include "generate/program/mainprogramcodegeneratortest.h"
#include "generate/program/preprogramgeneratortest.h"
#include "generate/program/step3programuploadgeneratortest.h"
#include "beautify/beautifiertest.h"
#include <gmock/gmock.h>

int main(int argc, char *argv[]) {


    ::testing::GTEST_FLAG(throw_on_failure) = true;
    ::testing::InitGoogleMock(&argc, argv);

    QGuiApplication app(argc , argv);
    app.setAttribute(Qt::AA_Use96Dpi, true);
    TypedObjectTest typedObjTest;
    QTest::qExec(&typedObjTest);

    //Command Test START
    CmdTest             cmdTest;
    NcCmdTest           ncCmdTest;
    HomingCmdTest       homingCmdTest;
    MoveCmdTest         moveCmdTest;
    DwellCmdTest        dwellCmdTest;
    LinearMoveCmdTest   linearMoveCmdTest;
    RapidMoveCmdTest    rapidMoveCmdTest;

    MainProgramCmdTest  mainProgCmdTest;
    PreProgramCmdTest   preProgCmdTest;
    Step3ProgramTest    step3ProgramTest;

    QTest::qExec(&cmdTest);
    QTest::qExec(&ncCmdTest);
    QTest::qExec(&homingCmdTest);
    QTest::qExec(&moveCmdTest);
    QTest::qExec(&dwellCmdTest);
    QTest::qExec(&linearMoveCmdTest);
    QTest::qExec(&rapidMoveCmdTest);

    QTest::qExec(&mainProgCmdTest);
    QTest::qExec(&preProgCmdTest);
    QTest::qExec(&step3ProgramTest);
    //Command Test END

    //Parser Test START
    BlockParserTest blockParserTest;
    DwellParserTest dwellParserTest;
    RapidMoveBlockParserTest    rapidMoveParseTest;
    LinearMoveBlockParserTest   linearMoveParseTest;
    GroupParserTest             parserTest;

    MainProgramBlockParserTest  mainProgParseTest;
    PreProgramBlockParserTest   preProgParseTest;
    Step3BlockParserTest        step3ParseTest;

    QTest::qExec(&blockParserTest);
    QTest::qExec(&dwellParserTest);
    QTest::qExec(&rapidMoveParseTest);
    QTest::qExec(&linearMoveParseTest);
    QTest::qExec(&parserTest);
    QTest::qExec(&mainProgParseTest);
    QTest::qExec(&preProgParseTest);
    QTest::qExec(&step3ParseTest);
    //Parser Test END

    //Generator test START
    CodeGeneratorTest       codeGenTest;
    DwellCodeGeneratorTest  dwellGenTest;
    MoveCodeGeneratorTest   moveCodeGenTest;
    LinearMoveCodeGeneratorTest linearMoveCodeGenTest;
    RapidMoveCodeGeneratorTest  rapidMoveCodeGenTest;
    HomingCodeGeneratorTest     homingCodeGenTest;
    CompounCodeGeneratorTests   compoundGenTest;
    MainProgramCodeGeneratorTest mainProgCodeGenTest;
    PreProgramGeneratorTest     preProgCodeGenTest;
    Step3ProgramUploadGeneratorTest step3ProgramUploadGenTest;

    QTest::qExec(&codeGenTest);
    QTest::qExec(&dwellGenTest);
    QTest::qExec(&moveCodeGenTest);
    QTest::qExec(&linearMoveCodeGenTest);
    QTest::qExec(&rapidMoveCodeGenTest);
    QTest::qExec(&homingCodeGenTest);
    QTest::qExec(&compoundGenTest);
    QTest::qExec(&mainProgCodeGenTest);
    QTest::qExec(&preProgCodeGenTest);
    QTest::qExec(&step3ProgramUploadGenTest);
    //Generator test END

    //Beautify test START
    BeautifierTest  beautifyTest;

    QTest::qExec(&beautifyTest);
    //Beautify test END

    return 0;
}
