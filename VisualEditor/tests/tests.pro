#-------------------------------------------------
#
# Project created by QtCreator 2017-12-19T11:39:15
#
#-------------------------------------------------

QT       += testlib
QT       += gui quick

TARGET = tst_teststest
CONFIG   += console warn_on depend_includepath testcase

TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    main.cpp \
    parse/dwellparsertest.cpp \
    parse/blockparsertest.cpp \
    parse/emptyblockparserfortest.cpp \
    command/emptycmdfortest.cpp \
    parse/rapidmoveblockparsertest.cpp \
    parse/linearmoveblockparsertest.cpp \
    emptytypedobjectfortest.cpp \
    typedobjecttest.cpp \
    command/cmdtest.cpp \
    command/nc/nccmdtest.cpp \
    command/nc/homingcmdtest.cpp \
    command/nc/nccmdfortest.cpp \
    command/nc/movecmdtest.cpp \
    command/nc/dwellcmdtest.cpp \
    command/nc/linearmovecmdtest.cpp \
    command/nc/rapidmovecmdtest.cpp \
    command/program/mainprogramcmdtest.cpp \
    command/program/preprogramcmdtest.cpp \
    parse/mainprogramblockparsertest.cpp \
    command/program/programcmdtest.cpp \
    parse/groupparsertest.cpp \
    parse/jsonblockgenerator.cpp \
    generate/codegeneratortest.cpp \
    generate/nc/dwellcodegeneratortest.cpp \
    command/commandgeneratorfortest.cpp \
    generate/nc/movecodegeneratortest.cpp \
    generate/nc/linearmovecodegeneratortest.cpp \
    generate/nc/rapidmovecodegeneratortest.cpp \
    generate/nc/homingcodegeneratortest.cpp \
    generate/compouncodegeneratortests.cpp \
    generate/program/mainprogramcodegeneratortest.cpp \
    generate/program/preprogramgeneratortest.cpp \
    parse/step3blockparsertest.cpp \
    command/program/step3programtest.cpp \
    parse/programtesthelper.cpp \
    parse/preprogramblockparsertest.cpp \
    generate/program/step3programuploadgeneratortest.cpp \
    beautify/beautifiertest.cpp \
    beautify/beautifiermock.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    parse/dwellparsertest.h \
    parse/blockparsertest.h \
    parse/emptyblockparserfortest.h \
    command/emptycmdfortest.h \
    parse/rapidmoveblockparsertest.h \
    parse/linearmoveblockparsertest.h \
    parse/homingblockparsertest.h \
    emptytypedobjectfortest.h \
    typedobjecttest.h \
    command/cmdtest.h \
    command/nc/nccmdtest.h \
    command/nc/homingcmdtest.h \
    command/nc/nccmdfortest.h \
    command/nc/movecmdtest.h \
    command/nc/dwellcmdtest.h \
    command/nc/linearmovecmdtest.h \
    command/nc/rapidmovecmdtest.h \
    command/program/mainprogramcmdtest.h \
    command/program/preprogramcmdtest.h \
    parse/mainprogramblockparsertest.h \
    command/program/programcmdtest.h \
    parse/groupparsertest.h \
    parse/mockblockparser.h \
    parse/jsonblockgenerator.h \
    generate/codegeneratortest.h \
    generate/mockedcodegenerator.h \
    generate/nc/dwellcodegeneratortest.h \
    command/mockedcmd.h \
    command/commandgeneratorfortest.h \
    generate/nc/movecodegeneratortest.h \
    generate/nc/linearmovecodegeneratortest.h \
    generate/nc/rapidmovecodegeneratortest.h \
    generate/nc/homingcodegeneratortest.h \
    generate/compouncodegeneratortests.h \
    generate/program/mainprogramcodegeneratortest.h \
    generate/program/preprogramgeneratortest.h \
    parse/step3blockparsertest.h \
    command/program/step3programtest.h \
    command/program/mockedmainprogram.h \
    command/program/mockedpreprogram.h \
    parse/programtesthelper.h \
    parse/preprogramblockparsertest.h \
    generate/program/step3programuploadgeneratortest.h \
    beautify/beautifiertest.h \
    beautify/beautifiermock.h

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../editorlib/release/ -leditorlib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../editorlib/debug/ -leditorlib
else:unix: LIBS += -L$$OUT_PWD/../editorlib/ -leditorlib

INCLUDEPATH += $$PWD/../editorlib
DEPENDPATH += $$PWD/../editorlib

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../Lib/release/ -lLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../Lib/debug/ -lLib
else:unix: LIBS += -L$$OUT_PWD/../../Lib/ -lLib

LIBS += -lgmock -L/usr/lib/libgmock.a

INCLUDEPATH += $$PWD/../../Lib
DEPENDPATH += $$PWD/../../Lib
