#include "dwellparsertest.h"
#include "exception/propertynotspecified.h"
#include "exception/valuecannotbenegative.h"
#include "command/nc/dwellcmd.h"
#include <QSharedPointer>

DwellParserTest::DwellParserTest(QObject* parent)
    :
      QObject(parent)
{

}

void DwellParserTest::test_if_time_is_null_throws_time_not_specified_exception(){
    QJsonObject obj;
    obj["name"] = "dwell";
    DwellBlockParser    parser;


    QVERIFY_EXCEPTION_THROWN(
                parser.parse(obj);
                ,
                PropertyNotSpecified
                );
}

void DwellParserTest::test_if_time_is_negative_throws_value_cannot_be_negative_exception(){
    QJsonObject obj;
    obj["name"] = DwellBlockParser::TYPE;
    obj["time"] = -1;
    DwellBlockParser    parser;

    QVERIFY_EXCEPTION_THROWN(parser.parse(obj) , ValueCannotBeNegative);
}

void DwellParserTest::test_if_time_is_positive_should_return_dwell_cmd(){
    QJsonObject obj;
    obj["name"] = DwellBlockParser::TYPE;
    obj["time"] = 1000;
    DwellBlockParser parser;

    QSharedPointer<ICmd> cmd = parser.parse(obj);

    QVERIFY(!cmd.isNull());
    QVERIFY(cmd.data()->typeName() == DwellCmd::TYPE);
}
