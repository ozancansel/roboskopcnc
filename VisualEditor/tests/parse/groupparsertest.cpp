#include "groupparsertest.h"
#include "parse/compoundparser.h"
#include "parse/dwellblockparser.h"
#include "parse/homingblockparser.h"
#include "mockblockparser.h"
#include "command/mockedcmd.h"
#include <gmock/gmock.h>

void GroupParserTest::test_parse_if_no_parsers_is_added_should_return_false(){
    CompoundParser parser;

    QJsonObject obj;

    obj["name"] = "deneme";
    QVERIFY(!parser.canParse(obj));
}

void GroupParserTest::test_parse_if_multiple_parsers_is_added(){
    CompoundParser parser;
    MockBlockParser *mockParser = new MockBlockParser();
    MockBlockParser *mockParser2 = new MockBlockParser();
    MockedCmd         *cmd1 = new MockedCmd();
    MockedCmd         *cmd2 = new MockedCmd();

    using ::testing::Return;
    using ::testing::_;

    EXPECT_CALL(*mockParser , parseableBlockName())
            .WillRepeatedly(Return("mocked"));

    EXPECT_CALL(*mockParser , parse(_))
            .WillRepeatedly(Return(QSharedPointer<ICmd>(cmd1)));

    //When first parse call, it says I cannot parse this object
    //at second call, it says I can
    EXPECT_CALL(*mockParser , canParse(_))
            .WillOnce(Return(true))
            .WillRepeatedly(Return(false));

    EXPECT_CALL(*mockParser2 , parseableBlockName())
            .WillRepeatedly(Return("mocked2"));

    EXPECT_CALL(*mockParser2 , parse(_))
            .WillRepeatedly(Return(QSharedPointer<ICmd>(cmd2)));

    EXPECT_CALL(*mockParser2 , canParse(_))
            .WillOnce(Return(true))
            .WillRepeatedly(Return(false));

    parser.addParser(QSharedPointer<IBlockParser>(mockParser));
    parser.addParser(QSharedPointer<IBlockParser>(mockParser2));

    QJsonObject obj , obj2;
    obj["name"] = "mocked";
    obj2["name"] = "mocked2";

    QSharedPointer<ICmd> res1 = parser.parse(obj);
    QSharedPointer<ICmd> res2 = parser.parse(obj2);
    QSharedPointer<ICmd> res3 = parser.parse(obj2);
    QVERIFY(!res1.isNull());
    QVERIFY(!res2.isNull());
    QVERIFY(res3.isNull());
}
