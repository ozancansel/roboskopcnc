#ifndef LINEARMOVEBLOCKPARSERTEST_H
#define LINEARMOVEBLOCKPARSERTEST_H

#include <QObject>
#include <QtTest/QtTest>

class LinearMoveBlockParserTest : public QObject
{
    Q_OBJECT

public:

    LinearMoveBlockParserTest();

private slots:

    void    test_if_x_is_not_specified_should_throw_value_not_specified_exception();
    void    test_if_y_is_not_specified_should_throw_value_not_specified_exception();
    void    test_if_z_is_not_specified_should_throw_value_not_specified_exception();
    void    test_if_feedRate_is_not_specified_should_throw_value_not_specified_exception();
    void    test_if_xyzf_are_specified_should_return_linear_move_cmd();

};

#endif // LINEARMOVEBLOCKPARSERTEST_H
