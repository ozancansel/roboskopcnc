#include "preprogramblockparsertest.h"
#include "parse/preprogramblockparser.h"
#include "parse/jsonblockgenerator.h"
#include "command/program/preprogramcmd.h"
#include "command/nc/dwellcmd.h"
#include "command/nc/homingcmd.h"
#include "command/nc/linearmovecmd.h"
#include "command/nc/rapidmovecmd.h"
#include "programtesthelper.h"
#include "exception/propertynotspecified.h"
#include "exception/parameterisnotsuitable.h"

void PreProgramBlockParserTest::test_parseableBlockName_should_equal_to_TYPE(){
    PreProgramBlockParser preProgParser;

    QCOMPARE(PreProgramBlockParser::TYPE , preProgParser.parseableBlockName());
}

void PreProgramBlockParserTest::test_parse_if_statements_contains_multiple_commands_should_parse_correctly(){
    PreProgramBlockParser    parser;
    QJsonArray  arr;
    QJsonObject dwellCmd = JsonBlockGenerator::generateDwellBlock();
    QJsonObject homingCmd = JsonBlockGenerator::generateHomingBlock();
    arr.append(dwellCmd);
    arr.append(homingCmd);
    QJsonObject obj = JsonBlockGenerator::generatePreProgBlock(arr);

    QSharedPointer<PreProgramCmd> preProgramCmd = parser.parse(obj).dynamicCast<PreProgramCmd>();

    QVERIFY(ProgramTestHelper::checkCommand(DwellCmd::TYPE , preProgramCmd.data()->commands()));
    QVERIFY(ProgramTestHelper::checkCommand(HomingCmd::TYPE , preProgramCmd.data()->commands()));

}

void PreProgramBlockParserTest::test_parse_if_statements_label_doesnt_specified_should_throw_property_not_specified_ex(){
    PreProgramBlockParser    parser;
    QJsonObject obj = JsonBlockGenerator::generatePreProgBlock();
    obj.remove(PreProgramBlockParser::STATEMENTS_LABEL);

    QVERIFY_EXCEPTION_THROWN(parser.parse(obj) , PropertyNotSpecified);
}

void PreProgramBlockParserTest::test_parse_if_statements_label_is_specified_but_its_type_is_not_json_array_should_throw_parameters_is_not_suitable_ex(){
    PreProgramBlockParser parser;
    QJsonObject obj = JsonBlockGenerator::generatePreProgBlock();
    obj.remove(PreProgramBlockParser::STATEMENTS_LABEL);
    obj[PreProgramBlockParser::STATEMENTS_LABEL] = QString("Hello");

    QVERIFY_EXCEPTION_THROWN(parser.parse(obj) , ParameterIsNotSuitable);
}

void PreProgramBlockParserTest::test_parse_should_parse_nested_dwell_cmd(){
    PreProgramBlockParser    parser;
    QJsonObject dwellObj = JsonBlockGenerator::generateDwellBlock();
    QJsonArray arr;
    arr.append(dwellObj);
    QJsonObject obj = JsonBlockGenerator::generatePreProgBlock(arr);

    QSharedPointer<PreProgramCmd> preProgCmd = parser.parse(obj).dynamicCast<PreProgramCmd>();

    QVERIFY(ProgramTestHelper::checkCommand(DwellCmd::TYPE , preProgCmd.data()->commands()));
}

void PreProgramBlockParserTest::test_parse_should_parse_nested_homing_cmd(){
    PreProgramBlockParser    parser;
    QJsonObject homingObj = JsonBlockGenerator::generateHomingBlock();
    QJsonArray arr;
    arr.append(homingObj);
    QJsonObject obj = JsonBlockGenerator::generatePreProgBlock(arr);

    QSharedPointer<PreProgramCmd> preProgCmd = parser.parse(obj).dynamicCast<PreProgramCmd>();

    QVERIFY(ProgramTestHelper::checkCommand(HomingCmd::TYPE , preProgCmd.data()->commands()));
}

void PreProgramBlockParserTest::test_parse_should_parse_nested_linear_move_cmd(){
    PreProgramBlockParser    parser;
    QJsonObject linearObj = JsonBlockGenerator::generateLinearMoveBlock();
    QJsonArray arr;
    arr.append(linearObj);
    QJsonObject obj = JsonBlockGenerator::generatePreProgBlock(arr);

    QSharedPointer<PreProgramCmd> preProgCmd = parser.parse(obj).dynamicCast<PreProgramCmd>();

    QVERIFY(ProgramTestHelper::checkCommand(LinearMoveCmd::TYPE , preProgCmd.data()->commands()));
}

void PreProgramBlockParserTest::test_parse_should_parse_nested_rapid_move_cmd(){
    PreProgramBlockParser    parser;
    QJsonObject rapidMoveObj = JsonBlockGenerator::generateRapidMoveBlock();
    QJsonArray arr;
    arr.append(rapidMoveObj);
    QJsonObject obj = JsonBlockGenerator::generatePreProgBlock(arr);

    QSharedPointer<PreProgramCmd> preProgCmd = parser.parse(obj).dynamicCast<PreProgramCmd>();

    QVERIFY(ProgramTestHelper::checkCommand(RapidMoveCmd::TYPE , preProgCmd.data()->commands()));
}

void PreProgramBlockParserTest::test_parse_should_return_pre_program_cmd(){
    PreProgramBlockParser    parser;
    QJsonObject obj = JsonBlockGenerator::generatePreProgBlock();

    QSharedPointer<ICmd> preProgCmd = parser.parse(obj);

    QVERIFY(preProgCmd.data()->is(PreProgramCmd::TYPE));
}
