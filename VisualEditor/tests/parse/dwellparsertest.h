#ifndef DWELLPARSERTEST_H
#define DWELLPARSERTEST_H

#include <QObject>
#include <QtTest/QtTest>
#include "parse/dwellblockparser.h"

class DwellParserTest : public QObject
{

    Q_OBJECT

public:

    DwellParserTest(QObject* parent = nullptr);

private slots:

    void    test_if_time_is_null_throws_time_not_specified_exception();
    void    test_if_time_is_negative_throws_value_cannot_be_negative_exception();
    void    test_if_time_is_positive_should_return_dwell_cmd();

};

#endif // DWELLPARSERTEST_H
