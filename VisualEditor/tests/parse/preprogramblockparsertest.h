#ifndef PREPROGRAMBLOCKPARSERTEST_H
#define PREPROGRAMBLOCKPARSERTEST_H

#include <QtTest/QtTest>
#include <QObject>

class PreProgramBlockParserTest : public QObject
{

    Q_OBJECT

private slots:

    void    test_parseableBlockName_should_equal_to_TYPE();
    void    test_parse_should_return_pre_program_cmd();
    void    test_parse_should_parse_nested_dwell_cmd();
    void    test_parse_should_parse_nested_homing_cmd();
    void    test_parse_should_parse_nested_linear_move_cmd();
    void    test_parse_should_parse_nested_rapid_move_cmd();
    void    test_parse_if_statements_label_doesnt_specified_should_throw_property_not_specified_ex();
    void    test_parse_if_statements_label_is_specified_but_its_type_is_not_json_array_should_throw_parameters_is_not_suitable_ex();
    void    test_parse_if_statements_contains_multiple_commands_should_parse_correctly();

};

#endif // PREPROGRAMBLOCKPARSERTEST_H
