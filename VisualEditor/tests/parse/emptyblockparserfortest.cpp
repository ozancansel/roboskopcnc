#include "emptyblockparserfortest.h"
#include "command/emptycmdfortest.h"
#include <QSharedPointer>

QString EmptyBlockParserForTest::parseableBlockName(){
    return "empty";
}

QSharedPointer<ICmd> EmptyBlockParserForTest::parseInternal(const QJsonObject &jsonData){
    Q_UNUSED(jsonData)
    return QSharedPointer<ICmd>(new EmptyCmdForTest());
}
