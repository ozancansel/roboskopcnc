#include "jsonblockgenerator.h"
#include "parse/linearmoveblockparser.h"
#include "parse/dwellblockparser.h"
#include "parse/homingblockparser.h"
#include "parse/rapidmoveblockparser.h"
#include "parse/mainprogramblockparser.h"
#include "parse/preprogramblockparser.h"
#include "parse/step3blockparser.h"

QJsonObject JsonBlockGenerator::generateLinearMoveBlock(){
    QJsonObject obj;

    obj[BlockParser::NAME_LABEL] = LinearMoveBlockParser::TYPE;
    obj["x"] = 10;
    obj["y"] = 10;
    obj["z"] = 10;
    obj["f"] = 10;

    return obj;
}

QJsonObject JsonBlockGenerator::generateDwellBlock(){
    QJsonObject obj;

    obj[BlockParser::NAME_LABEL] = DwellBlockParser::TYPE;
    obj["time"] = 1000;

    return obj;
}

QJsonObject JsonBlockGenerator::generateHomingBlock(){
    QJsonObject obj;
    obj[BlockParser::NAME_LABEL] = HomingBlockParser::TYPE;
    return obj;
}

QJsonObject JsonBlockGenerator::generateRapidMoveBlock(){
    QJsonObject obj;
    obj[BlockParser::NAME_LABEL] = RapidMoveBlockParser::TYPE;
    obj["x"] = 10;
    obj["y"] = 10;
    obj["z"] = 10;
    return obj;
}

QJsonObject JsonBlockGenerator::generateMainProgBlock(QJsonArray arr){
    QJsonObject obj;
    obj[BlockParser::NAME_LABEL] = MainProgramBlockParser::TYPE;
    obj[MainProgramBlockParser::LOOP_FOREVER_LABEL] = false;
    obj[MainProgramBlockParser::STATEMENTS_LABEL] = arr;
    return obj;
}

QJsonObject JsonBlockGenerator::generatePreProgBlock(QJsonArray arr){
    QJsonObject obj;

    obj[BlockParser::NAME_LABEL] = PreProgramBlockParser::TYPE;
    obj[PreProgramBlockParser::STATEMENTS_LABEL] = arr;

    return obj;
}

QJsonObject JsonBlockGenerator::generateStep3Program(QJsonObject mainProg, QJsonObject preProg){
    QJsonObject obj;
    QJsonArray arr;
    if(!mainProg.isEmpty())
        arr.append(mainProg);
    if(!preProg.isEmpty())
        arr.append(preProg);
    obj[BlockParser::NAME_LABEL] = Step3BlockParser::TYPE;
    obj[Step3BlockParser::BLOCKS_LABEL] = arr;

    return obj;
}
