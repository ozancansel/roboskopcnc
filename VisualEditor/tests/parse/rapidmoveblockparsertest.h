#ifndef RAPIDMOVEBLOCKPARSERTEST_H
#define RAPIDMOVEBLOCKPARSERTEST_H

#include <QObject>
#include <QtTest/QtTest>

class RapidMoveBlockParserTest : public QObject
{

    Q_OBJECT

private slots:

    void    test_if_x_not_specified_should_throws_property_not_specified();
    void    test_if_y_not_specified_should_throws_property_not_specified();
    void    test_if_z_not_specified_should_throws_property_not_specified();
    void    test_if_all_values_are_specified_shoudl_return_rapid_move_cmd();

};

#endif // RAPIDMOVEBLOCKPARSERTEST_H
