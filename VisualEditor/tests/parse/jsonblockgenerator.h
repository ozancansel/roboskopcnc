#ifndef JSONBLOCKGENERATOR_H
#define JSONBLOCKGENERATOR_H

#include <QJsonObject>
#include <QJsonArray>

class JsonBlockGenerator
{

public:

    static QJsonObject generateLinearMoveBlock();
    static QJsonObject generateDwellBlock();
    static QJsonObject generateHomingBlock();
    static QJsonObject generateRapidMoveBlock();
    static QJsonObject generateMainProgBlock(QJsonArray arr = QJsonArray());
    static QJsonObject generatePreProgBlock(QJsonArray arr = QJsonArray());
    static QJsonObject generateStep3Program(QJsonObject mainProg , QJsonObject preProg);

};

#endif // JSONBLOCKGENERATOR_H
