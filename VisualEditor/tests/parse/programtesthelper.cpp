#include "programtesthelper.h"

bool ProgramTestHelper::checkCommand(const QString cmdName, QList<QSharedPointer<ICmd> > commands){
    foreach (QSharedPointer<ICmd> cmd, commands) {
        if(cmd.data()->typeName() == cmdName)
            return true;
    }

    return false;
}
