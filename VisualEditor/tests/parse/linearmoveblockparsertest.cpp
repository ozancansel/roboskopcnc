#include "linearmoveblockparsertest.h"
#include <QJsonObject>
#include <QSharedPointer>
#include "command/nc/linearmovecmd.h"
#include "parse/linearmoveblockparser.h"
#include "exception/propertynotspecified.h"

LinearMoveBlockParserTest::LinearMoveBlockParserTest()
{

}

void LinearMoveBlockParserTest::test_if_feedRate_is_not_specified_should_throw_value_not_specified_exception(){

    QJsonObject obj;

    obj["name"] = LinearMoveBlockParser::TYPE;
    obj["x"] = 10;
    obj["y"] = 10;
    obj["z"] = 10;

    LinearMoveBlockParser   parser;

    QVERIFY_EXCEPTION_THROWN(parser.parse(obj) , PropertyNotSpecified);

}

void LinearMoveBlockParserTest::test_if_x_is_not_specified_should_throw_value_not_specified_exception(){

    QJsonObject obj;

    obj["name"] = LinearMoveBlockParser::TYPE;
    obj["f"] = 10;
    obj["y"] = 10;
    obj["z"] = 10;

    LinearMoveBlockParser   parser;

    QVERIFY_EXCEPTION_THROWN(parser.parse(obj) , PropertyNotSpecified);

}

void LinearMoveBlockParserTest::test_if_y_is_not_specified_should_throw_value_not_specified_exception(){
    QJsonObject obj;

    obj["name"] = LinearMoveBlockParser::TYPE;
    obj["f"] = 10;
    obj["x"] = 10;
    obj["z"] = 10;

    LinearMoveBlockParser   parser;

    QVERIFY_EXCEPTION_THROWN(parser.parse(obj) , PropertyNotSpecified);
}

void LinearMoveBlockParserTest::test_if_z_is_not_specified_should_throw_value_not_specified_exception(){
    QJsonObject obj;

    obj["name"] = LinearMoveBlockParser::TYPE;
    obj["f"] = 10;
    obj["x"] = 10;
    obj["y"] = 10;

    LinearMoveBlockParser   parser;

    QVERIFY_EXCEPTION_THROWN(parser.parse(obj) , PropertyNotSpecified);
}

void LinearMoveBlockParserTest::test_if_xyzf_are_specified_should_return_linear_move_cmd(){
    QJsonObject obj;

    obj["name"] = LinearMoveBlockParser::TYPE;
    obj["f"] = 10;
    obj["x"] = 10;
    obj["y"] = 10;
    obj["z"] = 10;

    LinearMoveBlockParser   parser;

    QSharedPointer<ICmd> cmd = parser.parse(obj);

    QVERIFY(!cmd.isNull());
    QCOMPARE(cmd.data()->typeName() , LinearMoveCmd::TYPE);
}
