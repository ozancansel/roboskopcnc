#ifndef MOCKBLOCKPARSER_H
#define MOCKBLOCKPARSER_H

#include <gmock/gmock.h>
#include "parse/iblockparser.h"
#include <QJsonObject>
#include <QSharedPointer>

class MockBlockParser : public IBlockParser
{

public:

    virtual ~MockBlockParser() { }

    MOCK_METHOD0(parseableBlockName , QString());
    MOCK_METHOD1(canParse , bool(const QJsonObject& obj));
    MOCK_METHOD1(parse , QSharedPointer<ICmd>(const QJsonObject& obj));

};

#endif // MOCKBLOCKPARSER_H
