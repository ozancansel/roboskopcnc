#ifndef STEP3BLOCKPARSERTEST_H
#define STEP3BLOCKPARSERTEST_H

#include <QObject>
#include <QtTest/QtTest>

class Step3BlockParserTest : public QObject
{

    Q_OBJECT

private slots:

    void    test_parseableBlockName();
    void    test_parseInternal_should_parse_main_prog();
    void    test_parseInternal_should_parse_pre_prog();
    void    test_parseInternal_should_parse_pre_and_main_prog();

};

#endif // STEP3BLOCKPARSERTEST_H
