#include "blockparsertest.h"
#include "parse/blockparser.h"
#include "emptyblockparserfortest.h"
#include "exception/parameterisnotsuitable.h"
#include <QJsonObject>
#include <QSharedPointer>

BlockParserTest::BlockParserTest(QObject* parent)
    :
      QObject(parent)
{

}

void BlockParserTest::test_if_name_is_not_specified_returns_null_pointer(){
    BlockParser* parser = new EmptyBlockParserForTest();

    QJsonObject obj;
    obj["name"] = "something";

    QSharedPointer<ICmd> cmd =  parser->parse(obj);

    QVERIFY(cmd.isNull());
}

void BlockParserTest::test_if_name_specified_but_doesnt_match_parseable_block_returns_null_pointer(){
    BlockParser* parser = new EmptyBlockParserForTest();
    QJsonObject obj;
    obj["name"] = "deneme-farkli";
    QSharedPointer<ICmd> cmd = parser->parse(obj);

    QVERIFY(cmd.isNull());
}

void BlockParserTest::test_if_obj_is_json_object_is_empty_should_throw_parameter_not_suitable_exception(){
    BlockParser* parser = new EmptyBlockParserForTest();
    QJsonObject obj;

    QVERIFY_EXCEPTION_THROWN( parser->parse(obj); , ParameterIsNotSuitable);
}
