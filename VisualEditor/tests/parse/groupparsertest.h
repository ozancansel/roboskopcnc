#ifndef GROUPPARSERTEST_H
#define GROUPPARSERTEST_H

#include "parse/compoundparser.h"
#include <QObject>
#include <QtTest/QtTest>

class GroupParserTest : public QObject
{

    Q_OBJECT

private slots:

    void    test_parse_if_no_parsers_is_added_should_return_false();
    void    test_parse_if_multiple_parsers_is_added();

};

#endif // GROUPPARSERTEST_H
