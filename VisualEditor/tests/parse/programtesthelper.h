#ifndef PROGRAMTESTHELPER_H
#define PROGRAMTESTHELPER_H

#include <QList>
#include <QSharedPointer>
#include "command/icmd.h"

class ProgramTestHelper
{

public:

    static bool checkCommand(const QString cmdName , QList<QSharedPointer<ICmd>> commands);

};

#endif // PROGRAMTESTHELPER_H
