#ifndef EMPTYBLOCKPARSERFORTEST_H
#define EMPTYBLOCKPARSERFORTEST_H

#include "parse/blockparser.h"

class EmptyBlockParserForTest : public BlockParser
{

public:

    QString parseableBlockName() override;

protected:

    QSharedPointer<ICmd> parseInternal(const QJsonObject &jsonData) override;

};

#endif // EMPTYBLOCKPARSERFORTEST_H
