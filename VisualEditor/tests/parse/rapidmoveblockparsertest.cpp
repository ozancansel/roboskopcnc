#include "rapidmoveblockparsertest.h"
#include "parse/rapidmoveblockparser.h"
#include "exception/propertynotspecified.h"
#include "command/nc/rapidmovecmd.h"
#include <QJsonObject>
#include <QSharedPointer>

void RapidMoveBlockParserTest::test_if_x_not_specified_should_throws_property_not_specified(){
    QJsonObject obj;
    obj["name"] = RapidMoveBlockParser::TYPE;
    obj["y"] = 10;
    obj["z"] = 10;

    RapidMoveBlockParser parser;

    QVERIFY_EXCEPTION_THROWN(
            parser.parse(obj); ,
            PropertyNotSpecified
            );
}

void RapidMoveBlockParserTest::test_if_y_not_specified_should_throws_property_not_specified(){
    QJsonObject obj;

    obj["name"] = RapidMoveBlockParser::TYPE;
    obj["x"] = 10;
    obj["z"] = 10;

    RapidMoveBlockParser parser;

    QVERIFY_EXCEPTION_THROWN(
                parser.parse(obj) ,
                PropertyNotSpecified
                );
}

void RapidMoveBlockParserTest::test_if_z_not_specified_should_throws_property_not_specified(){
    QJsonObject obj;

    obj["name"] = RapidMoveBlockParser::TYPE;
    obj["x"] = 10;
    obj["y"] = 10;

    RapidMoveBlockParser parser;

    QVERIFY_EXCEPTION_THROWN(
                parser.parse(obj) ,
                PropertyNotSpecified
                );
}

void RapidMoveBlockParserTest::test_if_all_values_are_specified_shoudl_return_rapid_move_cmd(){
    QJsonObject obj;

    obj["name"] = RapidMoveBlockParser::TYPE;
    obj["x"] = 10;
    obj["y"] = 10;
    obj["z"] = 10;

    RapidMoveBlockParser parser;

    QSharedPointer<ICmd> cmd = parser.parse(obj);

    QVERIFY(!cmd.isNull());
    QCOMPARE(cmd.data()->typeName() , RapidMoveCmd::TYPE);
}
