#ifndef BLOCKPARSERTEST_H
#define BLOCKPARSERTEST_H

#include <QObject>
#include <QtTest/QtTest>

class BlockParserTest : public QObject
{

    Q_OBJECT

public:

    BlockParserTest(QObject* parent = nullptr);

private slots:

    void    test_if_name_is_not_specified_returns_null_pointer();
    void    test_if_name_specified_but_doesnt_match_parseable_block_returns_null_pointer();
    void    test_if_obj_is_json_object_is_empty_should_throw_parameter_not_suitable_exception();

};

#endif // BLOCKPARSERTEST_H
