#include "step3blockparsertest.h"
#include "parse/step3blockparser.h"
#include "parse/jsonblockgenerator.h"
#include "command/program/mainprogramcmd.h"
#include "command/program/step3program.h"
#include "command/nc/homingcmd.h"
#include "command/nc/dwellcmd.h"
#include "programtesthelper.h"

void Step3BlockParserTest::test_parseableBlockName(){
    QCOMPARE(Step3BlockParser().parseableBlockName() , Step3BlockParser::TYPE);
}

void Step3BlockParserTest::test_parseInternal_should_parse_main_prog(){
    Step3BlockParser    parser;
    QJsonObject         mainProgObj = JsonBlockGenerator::generateMainProgBlock();
    QJsonObject obj = JsonBlockGenerator::generateStep3Program(mainProgObj , QJsonObject());

    QSharedPointer<Step3Program> cmd = parser.parse(obj).dynamicCast<Step3Program>();

    QVERIFY(!cmd.data()->mainProgram().isNull());
}

void Step3BlockParserTest::test_parseInternal_should_parse_pre_prog(){
    Step3BlockParser    parser;
    QJsonObject         preProgObj = JsonBlockGenerator::generatePreProgBlock();
    QJsonObject obj = JsonBlockGenerator::generateStep3Program(QJsonObject() , preProgObj);

    QSharedPointer<Step3Program> cmd = parser.parse(obj).dynamicCast<Step3Program>();

    QVERIFY(!cmd.data()->preProgram().isNull());
}

void Step3BlockParserTest::test_parseInternal_should_parse_pre_and_main_prog(){
    Step3BlockParser parser;
    QJsonObject preProgObj = JsonBlockGenerator::generatePreProgBlock();
    QJsonObject mainProgObj = JsonBlockGenerator::generateMainProgBlock();
    QJsonObject obj = JsonBlockGenerator::generateStep3Program(mainProgObj , preProgObj);

    QSharedPointer<Step3Program> program = parser.parse(obj).dynamicCast<Step3Program>();

    QVERIFY(!program.data()->mainProgram().isNull());
    QVERIFY(!program.data()->preProgram().isNull());
}
