#ifndef MAINPROGRAMBLOCKPARSERTEST_H
#define MAINPROGRAMBLOCKPARSERTEST_H

#include <QObject>
#include <QtTest/QtTest>
#include "command/icmd.h"

class MainProgramBlockParserTest : public QObject
{
    Q_OBJECT

private slots:

    void    test_parseableBlockName_should_equal_to_TYPE();
    void    test_parse_should_return_main_program_cmd();
    void    test_parse_should_parse_nested_dwell_cmd();
    void    test_parse_should_parse_nested_homing_cmd();
    void    test_parse_should_parse_nested_linear_move_cmd();
    void    test_parse_should_parse_nested_rapid_move_cmd();
    void    test_parse_should_throw_incompatible_command_ex_when_parse_program_cmd_inherited_cmds_is_intent();
    void    test_parse_if_loop_forever_label_doesnt_specified_should_throw_property_not_specified_ex();
    void    test_parse_if_loop_forever_is_specified_but_its_type_is_not_boolean_should_throw_parameter_is_not_suitable_exception();
    void    test_parse_loop_forever_should_be_set();
    void    test_parse_if_statements_label_doesnt_specified_should_throw_property_not_specified_ex();
    void    test_parse_if_statements_label_specified_but_its_type_is_not_json_array_should_throw_parameter_is_not_suitable_exception();
    void    test_parse_if_statements_contains_multiple_commands_should_parse_correctly();


};

#endif // MAINPROGRAMBLOCKPARSERTEST_H
