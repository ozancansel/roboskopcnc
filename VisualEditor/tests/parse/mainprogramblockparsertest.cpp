#include "mainprogramblockparsertest.h"
#include "parse/mainprogramblockparser.h"
#include "parse/dwellblockparser.h"
#include "parse/linearmoveblockparser.h"
#include "parse/rapidmoveblockparser.h"
#include "parse/homingblockparser.h"
#include "mockblockparser.h"
#include "command/program/mainprogramcmd.h"
#include "command/nc/dwellcmd.h"
#include "command/nc/homingcmd.h"
#include "command/nc/linearmovecmd.h"
#include "command/nc/rapidmovecmd.h"
#include "exception/incompatiblecommand.h"
#include "jsonblockgenerator.h"
#include "programtesthelper.h"
#include <QJsonObject>
#include <QSharedPointer>

void MainProgramBlockParserTest::test_parseableBlockName_should_equal_to_TYPE(){
    MainProgramBlockParser  parser;
    QCOMPARE(parser.parseableBlockName() , MainProgramBlockParser::TYPE);
}

void MainProgramBlockParserTest::test_parse_should_return_main_program_cmd(){
   MainProgramBlockParser parser;

   QJsonObject obj = JsonBlockGenerator::generateMainProgBlock();

   QSharedPointer<ICmd> cmd = parser.parse(obj);
   QCOMPARE(cmd.data()->typeName() , MainProgramCmd::TYPE);
}

void MainProgramBlockParserTest::test_parse_should_parse_nested_dwell_cmd(){
    MainProgramBlockParser parser;
    QJsonObject dwellCmd = JsonBlockGenerator::generateRapidMoveBlock();
    QJsonArray arr;
    arr.append(dwellCmd);
    QJsonObject obj = JsonBlockGenerator::generateMainProgBlock(arr);

    QSharedPointer<MainProgramCmd> cmd = parser.parse(obj).dynamicCast<MainProgramCmd>();
    QCOMPARE(cmd.data()->commands().length() , 1);
}

void MainProgramBlockParserTest::test_parse_should_parse_nested_homing_cmd(){
    MainProgramBlockParser  parser;

    QJsonObject homingCmd = JsonBlockGenerator::generateHomingBlock();
    QJsonArray arr;
    arr.append(homingCmd);
    QJsonObject obj = JsonBlockGenerator::generateMainProgBlock(arr);

    QSharedPointer<MainProgramCmd> cmd = parser.parse(obj).dynamicCast<MainProgramCmd>();

    QVERIFY(ProgramTestHelper::checkCommand(HomingCmd::TYPE , cmd.data()->commands()));
    QVERIFY(cmd.data()->commands().length() == 1);
}

void MainProgramBlockParserTest::test_parse_should_parse_nested_linear_move_cmd(){
    MainProgramBlockParser parser;

    QJsonObject linearMoveCmd = JsonBlockGenerator::generateLinearMoveBlock();
    QJsonArray  array;
    array.append(linearMoveCmd);
    QJsonObject obj = JsonBlockGenerator::generateMainProgBlock(array);

    QSharedPointer<MainProgramCmd> res =  parser.parse(obj).dynamicCast<MainProgramCmd>();
    QVERIFY(ProgramTestHelper::checkCommand(LinearMoveCmd::TYPE , res.data()->commands()));
    QVERIFY(res.data()->commands().length() == 1);
}

void MainProgramBlockParserTest::test_parse_should_parse_nested_rapid_move_cmd(){
    MainProgramBlockParser parser;

    QJsonObject rapidMoveCmd = JsonBlockGenerator::generateRapidMoveBlock();
    QJsonArray array;
    array.append(rapidMoveCmd);
    QJsonObject obj = JsonBlockGenerator::generateMainProgBlock(array);

    QSharedPointer<MainProgramCmd> cmd = parser.parse(obj).dynamicCast<MainProgramCmd>();

    QVERIFY(ProgramTestHelper::checkCommand(RapidMoveCmd::TYPE , cmd.data()->commands()));
    QCOMPARE(1 , cmd.data()->commands().length());
}

void MainProgramBlockParserTest::test_parse_should_throw_incompatible_command_ex_when_parse_program_cmd_inherited_cmds_is_intent(){
    MainProgramBlockParser parser;

    QJsonObject mainProgNestedObj = JsonBlockGenerator::generateMainProgBlock();
    QJsonArray array;
    array.append(mainProgNestedObj);
    QJsonObject obj = JsonBlockGenerator::generateMainProgBlock(array);

    QVERIFY_EXCEPTION_THROWN(    parser.parse(obj); , IncompatibleCommand );
}

void MainProgramBlockParserTest::test_parse_if_loop_forever_label_doesnt_specified_should_throw_property_not_specified_ex(){\
    MainProgramBlockParser parser;

    QJsonObject obj = JsonBlockGenerator::generateMainProgBlock();
    obj.remove(MainProgramBlockParser::LOOP_FOREVER_LABEL);

    QVERIFY_EXCEPTION_THROWN( parser.parse(obj) , PropertyNotSpecified );
}

void MainProgramBlockParserTest::test_parse_if_loop_forever_is_specified_but_its_type_is_not_boolean_should_throw_parameter_is_not_suitable_exception(){
    MainProgramBlockParser parser;

    QJsonObject obj = JsonBlockGenerator::generateMainProgBlock();
    obj[MainProgramBlockParser::LOOP_FOREVER_LABEL] = QString("Not Suitable String");

    QVERIFY_EXCEPTION_THROWN( parser.parse(obj) , ParameterIsNotSuitable );
}

void MainProgramBlockParserTest::test_parse_loop_forever_should_be_set(){
    MainProgramBlockParser parser;

    QJsonObject obj = JsonBlockGenerator::generateMainProgBlock();
    obj[MainProgramBlockParser::LOOP_FOREVER_LABEL] = true;

    QSharedPointer<MainProgramCmd> cmd = parser.parse(obj).dynamicCast<MainProgramCmd>();

    QVERIFY(cmd.data()->loopForever());
}

void MainProgramBlockParserTest::test_parse_if_statements_label_doesnt_specified_should_throw_property_not_specified_ex(){
    MainProgramBlockParser parser;

    QJsonObject obj = JsonBlockGenerator::generateMainProgBlock();
    obj.remove(MainProgramBlockParser::STATEMENTS_LABEL);

    QVERIFY_EXCEPTION_THROWN(parser.parse(obj) , PropertyNotSpecified );
}

void MainProgramBlockParserTest::test_parse_if_statements_label_specified_but_its_type_is_not_json_array_should_throw_parameter_is_not_suitable_exception(){
    MainProgramBlockParser parser;

    QJsonObject obj = JsonBlockGenerator::generateMainProgBlock();
    obj[MainProgramBlockParser::STATEMENTS_LABEL] = 0;

    QVERIFY_EXCEPTION_THROWN( parser.parse(obj) , ParameterIsNotSuitable );
}

void MainProgramBlockParserTest::test_parse_if_statements_contains_multiple_commands_should_parse_correctly(){
    MainProgramBlockParser parser;

    QJsonObject rapidMoveBlock = JsonBlockGenerator::generateRapidMoveBlock();
    QJsonObject linearMoveBlock = JsonBlockGenerator::generateLinearMoveBlock();
    QJsonArray array;
    array.append(rapidMoveBlock);
    array.append(linearMoveBlock);
    QJsonObject obj = JsonBlockGenerator::generateMainProgBlock(array);

    QSharedPointer<MainProgramCmd> cmd = parser.parse(obj).dynamicCast<MainProgramCmd>();

    QCOMPARE(cmd.data()->commands().length() , 2);
    QVERIFY(ProgramTestHelper::checkCommand(RapidMoveCmd::TYPE , cmd.data()->commands()));
    QVERIFY(ProgramTestHelper::checkCommand(LinearMoveCmd::TYPE , cmd.data()->commands()));
}
