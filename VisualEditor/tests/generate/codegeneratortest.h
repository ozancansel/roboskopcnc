#ifndef CODEGENERATORTEST_H
#define CODEGENERATORTEST_H

#include <QObject>
#include <QtTest/QtTest>

class CodeGeneratorTest : public QObject
{

    Q_OBJECT

private slots:

    void    test_generate_if_canGenerate_returns_false_should_return_empty_string();
    void    test_generate_if_canGenerate_returns_true_should_call_generateInternal_method();
    void    test_generate_if_cmd_is_not_valid_should_throw_cmd_not_valid_exception();
    void    test_generate_if_cmd_is_null_should_throw_parameter_is_not_suitable_exception();

};

#endif // CODEGENERATORTEST_H
