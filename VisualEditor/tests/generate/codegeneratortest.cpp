#include "codegeneratortest.h"
#include "generate/codegenerator.h"
#include "mockedcodegenerator.h"
#include "exception/cmdnotvalid.h"
#include "exception/parameterisnotsuitable.h"
#include "command/mockedcmd.h"

void CodeGeneratorTest::test_generate_if_canGenerate_returns_false_should_return_empty_string(){
    MockedCodeGenerator mockedGenerator;
    MockedCmd* mockedCmd =  new MockedCmd();

    EXPECT_CALL(mockedGenerator , canGenerate(::testing::_))
            .WillOnce(::testing::Return(false));
    EXPECT_CALL(*mockedCmd , isValid())
            .WillRepeatedly(::testing::Return(true));

    QString str = mockedGenerator.generate(QSharedPointer<ICmd>(mockedCmd));

    QVERIFY(str.isEmpty());
}

void CodeGeneratorTest::test_generate_if_canGenerate_returns_true_should_call_generateInternal_method(){
    MockedCodeGenerator mockedGenerator;
    MockedCmd* mockedCmd =  new MockedCmd();
    EXPECT_CALL(*mockedCmd , isValid())
            .WillRepeatedly(::testing::Return(true));
    EXPECT_CALL(mockedGenerator , canGenerate(::testing::_))
            .WillOnce(::testing::Return(true));
    EXPECT_CALL(mockedGenerator , generateInternal(::testing::_))
            .Times(1);

    mockedGenerator.generate(QSharedPointer<ICmd>(mockedCmd));
}

void CodeGeneratorTest::test_generate_if_cmd_is_not_valid_should_throw_cmd_not_valid_exception(){
    MockedCodeGenerator mockedGenerator;
    MockedCmd *cmd = new MockedCmd();

    EXPECT_CALL(*cmd , isValid())
            .WillOnce(::testing::Return(false));

    QVERIFY_EXCEPTION_THROWN(mockedGenerator.generate(QSharedPointer<ICmd>(cmd)); , CmdNotValid);
}

void CodeGeneratorTest::test_generate_if_cmd_is_null_should_throw_parameter_is_not_suitable_exception(){
    MockedCodeGenerator mockedGenerator;

    QVERIFY_EXCEPTION_THROWN(mockedGenerator.generate(QSharedPointer<ICmd>()) , ParameterIsNotSuitable);
}
