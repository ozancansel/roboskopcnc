#ifndef DWELLCODEGENERATORTEST_H
#define DWELLCODEGENERATORTEST_H

#include <QObject>
#include <QtTest/QtTest>

class DwellCodeGeneratorTest : public QObject
{

    Q_OBJECT

private slots:


    void    test_canGenerate_if_valid_dwellCmd_is_sent_should_return_true();
    void    test_canGenerate_if_different_cmd_is_sent_should_return_false();
    void    test_generate();

};

#endif // DWELLCODEGENERATORTEST_H
