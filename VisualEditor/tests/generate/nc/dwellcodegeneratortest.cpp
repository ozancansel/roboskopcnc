#include "dwellcodegeneratortest.h"
#include "generate/nc/dwellcodegenerator.h"
#include "command/nc/dwellcmd.h"
#include "command/commandgeneratorfortest.h"
#include "command/mockedcmd.h"
#include "command/emptycmdfortest.h"
#include <gmock/gmock.h>

void DwellCodeGeneratorTest::test_canGenerate_if_valid_dwellCmd_is_sent_should_return_true(){
    DwellCodeGenerator generator;
    QSharedPointer<DwellCmd> dwellCmd = CommandGeneratorForTest::generateDwellCmd();

    QVERIFY(generator.canGenerate(dwellCmd));
}

void DwellCodeGeneratorTest::test_generate(){
    DwellCodeGenerator generator;
    QSharedPointer<DwellCmd> dwellCmd = CommandGeneratorForTest::generateDwellCmd(1000);

    QCOMPARE(generator.generate(dwellCmd) , QString("G4 P1.000\n"));
}

void DwellCodeGeneratorTest::test_canGenerate_if_different_cmd_is_sent_should_return_false(){
    DwellCodeGenerator generator;
    EmptyCmdForTest *emptyCmd = new EmptyCmdForTest();
    QVERIFY(!generator.canGenerate(QSharedPointer<ICmd>(emptyCmd)));
}
