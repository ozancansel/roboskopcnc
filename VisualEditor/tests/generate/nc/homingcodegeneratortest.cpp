#include "homingcodegeneratortest.h"
#include "command/nc/homingcmd.h"
#include "command/mockedcmd.h"
#include "generate/nc/homingcodegenerator.h"
#include "command/commandgeneratorfortest.h"
#include <gmock/gmock.h>
#include <QSharedPointer>

void HomingCodeGeneratorTest::test_canGenerate_if_any_commands_are_sent_which_is_different_from_homing_cmd_should_return_false(){
    HomingCodeGenerator generator;
    MockedCmd *cmd = new MockedCmd();

    EXPECT_CALL(*cmd , is(::testing::_))
            .WillOnce(::testing::Return(false));
    QVERIFY(!generator.canGenerate(QSharedPointer<ICmd>(cmd)));
}

void HomingCodeGeneratorTest::test_generate(){
    HomingCodeGenerator generator;
    QSharedPointer<HomingCmd> cmd = CommandGeneratorForTest::generateHomingCmd();

    QString generatedCode = generator.generate(cmd);

    //Should be non empty
    QVERIFY(!generatedCode.isEmpty());
    QCOMPARE(generatedCode , QString("$H\n"));
}
