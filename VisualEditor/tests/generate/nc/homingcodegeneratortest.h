#ifndef HOMINGCODEGENERATORTEST_H
#define HOMINGCODEGENERATORTEST_H

#include <QtTest/QtTest>
#include <QObject>

class HomingCodeGeneratorTest : public QObject
{

    Q_OBJECT

private slots:

    void    test_canGenerate_if_any_commands_are_sent_which_is_different_from_homing_cmd_should_return_false();
    void    test_generate();


};

#endif // HOMINGCODEGENERATORTEST_H
