#include "movecodegeneratortest.h"
#include "command/nc/linearmovecmd.h"
#include "generate/nc/movecodegenerator.h"
#include "command/commandgeneratorfortest.h"
#include "command/emptycmdfortest.h"

void MoveCodeGeneratorTest::test_canGenerate_if_different_cmd_is_sent_should_return_false(){
    MoveCodeGenerator generator;
    EmptyCmdForTest *emptyCmd = new EmptyCmdForTest();
    QVERIFY(!generator.canGenerate(QSharedPointer<ICmd>(emptyCmd)));
}

void MoveCodeGeneratorTest::test_generate_if_just_x_specified(){
    MoveCodeGenerator moveGenerator;
    QSharedPointer<RapidMoveCmd> rapidMoveCmd = CommandGeneratorForTest::generateRapidMoveCmd(10 , NAN , NAN);

    QCOMPARE(moveGenerator.generate(rapidMoveCmd) , QString("X10.000"));
}

void MoveCodeGeneratorTest::test_generate_if_just_y_specified(){
    MoveCodeGenerator moveGenerator;
    QSharedPointer<RapidMoveCmd> rapidMoveCmd = CommandGeneratorForTest::generateRapidMoveCmd(NAN , 10 , NAN);
    QCOMPARE(moveGenerator.generate(rapidMoveCmd) , QString("Y10.000"));
}

void MoveCodeGeneratorTest::test_generate_if_just_z_specified(){
    MoveCodeGenerator moveGenerator;
    QSharedPointer<RapidMoveCmd> rapidMoveCmd = CommandGeneratorForTest::generateRapidMoveCmd(NAN , NAN , 10);
    QCOMPARE(moveGenerator.generate(rapidMoveCmd) , QString("Z10.000"));}

void MoveCodeGeneratorTest::test_generate_if_just_xy_specified(){
    MoveCodeGenerator moveGenerator;
    QSharedPointer<RapidMoveCmd> rapidMoveCmd = CommandGeneratorForTest::generateRapidMoveCmd(5 , 10 , NAN);
    QCOMPARE(moveGenerator.generate(rapidMoveCmd) , QString("X5.000 Y10.000"));
}

void MoveCodeGeneratorTest::test_generate_if_just_xz_specified(){
    MoveCodeGenerator moveGenerator;
    QSharedPointer<RapidMoveCmd> rapidMoveCmd = CommandGeneratorForTest::generateRapidMoveCmd(5 , NAN , -10);
    QCOMPARE(moveGenerator.generate(rapidMoveCmd) , QString("X5.000 Z-10.000"));
}

void MoveCodeGeneratorTest::test_generate_if_just_yz_specified(){
    MoveCodeGenerator moveGenerator;
    QSharedPointer<RapidMoveCmd> rapidMoveCmd = CommandGeneratorForTest::generateRapidMoveCmd(NAN , 10 , 5);
    QCOMPARE(moveGenerator.generate(rapidMoveCmd) , QString("Y10.000 Z5.000"));
}

void MoveCodeGeneratorTest::test_generate_if_just_xyz_specified(){
    MoveCodeGenerator moveGenerator;
    QSharedPointer<RapidMoveCmd> rapidMoveCmd = CommandGeneratorForTest::generateRapidMoveCmd(5 , 10 , 15);
    QCOMPARE(moveGenerator.generate(rapidMoveCmd) , QString("X5.000 Y10.000 Z15.000"));
}
