#ifndef MOVECODEGENERATORTEST_H
#define MOVECODEGENERATORTEST_H

#include <QObject>
#include <QtTest/QtTest>

class MoveCodeGeneratorTest : public QObject
{

    Q_OBJECT

private slots:

    void    test_canGenerate_if_different_cmd_is_sent_should_return_false();
    void    test_generate_if_just_x_specified();
    void    test_generate_if_just_y_specified();
    void    test_generate_if_just_z_specified();
    void    test_generate_if_just_xy_specified();
    void    test_generate_if_just_xz_specified();
    void    test_generate_if_just_yz_specified();
    void    test_generate_if_just_xyz_specified();

};

#endif // MOVECODEGENERATORTEST_H
