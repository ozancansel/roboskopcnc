#ifndef LINEARMOVECODEGENERATORTEST_H
#define LINEARMOVECODEGENERATORTEST_H

#include <QObject>
#include <QtTest/QtTest>

class LinearMoveCodeGeneratorTest : public QObject
{

    Q_OBJECT

private slots:

    void    test_canGenerate_if_different_cmd_is_sent_should_return_false();
    void    test_generate();

};

#endif // LINEARMOVECODEGENERATORTEST_H
