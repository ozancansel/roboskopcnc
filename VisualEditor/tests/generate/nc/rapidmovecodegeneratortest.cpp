#include "rapidmovecodegeneratortest.h"
#include "generate/nc/rapidmovecodegenerator.h"
#include "command/commandgeneratorfortest.h"
#include "command/emptycmdfortest.h"

void RapidMoveCodeGeneratorTest::test_canGenerate_if_different_cmd_is_sent_should_return_false(){
    RapidMoveCodeGenerator generator;
    EmptyCmdForTest *emptyCmd = new EmptyCmdForTest();

    QVERIFY(!generator.canGenerate(QSharedPointer<ICmd>(emptyCmd)));
}

void  RapidMoveCodeGeneratorTest::test_generate(){
    RapidMoveCodeGenerator  generator;
    QSharedPointer<RapidMoveCmd> cmd = CommandGeneratorForTest::generateRapidMoveCmd(5 , 10 , 15);

    QCOMPARE(generator.generate(cmd) , QString("G0 X5.000 Y10.000 Z15.000\n"));
}
