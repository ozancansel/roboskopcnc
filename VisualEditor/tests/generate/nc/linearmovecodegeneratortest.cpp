#include "linearmovecodegeneratortest.h"
#include "generate/nc/linearmovecodegenerator.h"
#include "command/commandgeneratorfortest.h"
#include "command/emptycmdfortest.h"

void LinearMoveCodeGeneratorTest::test_canGenerate_if_different_cmd_is_sent_should_return_false(){
    LinearMoveCodeGenerator generator;
    EmptyCmdForTest *emptyCmd = new EmptyCmdForTest();

    QVERIFY(!generator.canGenerate(QSharedPointer<ICmd>(emptyCmd)));
}

void LinearMoveCodeGeneratorTest::test_generate(){
    LinearMoveCodeGenerator generator;
    QSharedPointer<LinearMoveCmd> cmd = CommandGeneratorForTest::generateLinearMoveCmd(5 , 10 , 15 , 300);

    QCOMPARE(generator.generate(cmd) , QString("G1 X5.000 Y10.000 Z15.000 F300.000\n"));
}
