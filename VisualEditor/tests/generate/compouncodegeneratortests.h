#ifndef COMPOUNCODEGENERATORTESTS_H
#define COMPOUNCODEGENERATORTESTS_H

#include <QtTest/QtTest>
#include <QObject>

class CompounCodeGeneratorTests : public QObject
{

    Q_OBJECT

private slots:

    void    test_addGenerator();
    void    test_canGenerate_if_suitable_generator_is_added_should_return_true();
    void    test_canGenerate_if_there_are_not_any_suitable_generators_should_return_false();
    void    test_generate();

};

#endif // COMPOUNCODEGENERATORTESTS_H
