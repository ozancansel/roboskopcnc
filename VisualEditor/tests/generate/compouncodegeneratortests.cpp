#include "compouncodegeneratortests.h"
#include "generate/mockedcodegenerator.h"
#include "generate/compoundcodegenerator.h"
#include "command/mockedcmd.h"

void CompounCodeGeneratorTests::test_addGenerator(){
    CompoundCodeGenerator codeGenerator;
    MockedCodeGenerator     *gen1 = new MockedCodeGenerator();
    MockedCmd               *mockedCmd = new MockedCmd();

    QSharedPointer<ICmd> sCmd(mockedCmd);

    EXPECT_CALL(*gen1 , canGenerate(::testing::_))
            .WillOnce(::testing::Return(true));

    QVERIFY(!codeGenerator.canGenerate(sCmd));

    codeGenerator.addGenerator(QSharedPointer<ICodeGenerator>(gen1));

    QVERIFY(codeGenerator.canGenerate(sCmd));
}

void CompounCodeGeneratorTests::test_canGenerate_if_suitable_generator_is_added_should_return_true(){
    CompoundCodeGenerator codeGenerator;
    MockedCodeGenerator     *gen1 = new MockedCodeGenerator();
    MockedCmd               *mockedCmd = new MockedCmd();

    EXPECT_CALL(*gen1 , canGenerate(::testing::_))
            .WillOnce(::testing::Return(true));

    codeGenerator.addGenerator(QSharedPointer<ICodeGenerator>(gen1));

    QVERIFY(codeGenerator.canGenerate(QSharedPointer<ICmd>(mockedCmd)));

}

void CompounCodeGeneratorTests::test_canGenerate_if_there_are_not_any_suitable_generators_should_return_false(){
    CompoundCodeGenerator codeGenerator;
    MockedCodeGenerator     *gen1 = new MockedCodeGenerator();
    MockedCmd               *mockedCmd = new MockedCmd();

    EXPECT_CALL(*gen1 , canGenerate(::testing::_))
            .WillOnce(::testing::Return(false));

    codeGenerator.addGenerator(QSharedPointer<ICodeGenerator>(gen1));

    QVERIFY(!codeGenerator.canGenerate(QSharedPointer<ICmd>(mockedCmd)));
}

void CompounCodeGeneratorTests::test_generate(){
    CompoundCodeGenerator codeGenerator;
    MockedCodeGenerator     *gen1 = new MockedCodeGenerator();
    MockedCmd               *mockedCmd = new MockedCmd();
    QSharedPointer<ICodeGenerator>  sGen(gen1);
    QSharedPointer<ICmd> mCmd(mockedCmd);
    QString generatedCode = "generated";

    EXPECT_CALL(*mockedCmd , isValid())
            .WillRepeatedly(::testing::Return(true));

    EXPECT_CALL(*gen1 , canGenerate(::testing::_))
            .WillRepeatedly(::testing::Return(true));

    EXPECT_CALL(*gen1 , generateInternal(::testing::_))
            .WillRepeatedly(::testing::Return(generatedCode));

    codeGenerator.addGenerator(sGen);

    QString res=  codeGenerator.generate(mCmd);

    QCOMPARE(res , generatedCode);
}
