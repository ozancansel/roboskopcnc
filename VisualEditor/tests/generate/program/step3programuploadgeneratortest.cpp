#include "step3programuploadgeneratortest.h"
#include "generate/program/step3programuploadgenerator.h"
#include "command/program/preprogramcmd.h"
#include "command/program/mainprogramcmd.h"
#include "command/program/step3program.h"
#include "command/nc/dwellcmd.h"
#include "command/commandgeneratorfortest.h"
#include "exception/cmdnotvalid.h"

void Step3ProgramUploadGeneratorTest::test_generate_if_just_pre_prog_is_specified_should_throw_cmd_not_valid_exception(){
    Step3ProgramUploadGenerator generator;
    QSharedPointer<Step3Program> step3ProgramCmd(new Step3Program());
    QSharedPointer<PreProgramCmd> preProgCmd(new PreProgramCmd());
    QSharedPointer<ICmd> dwellCmd = CommandGeneratorForTest::generateDwellCmd();

    preProgCmd.data()->addCommand(dwellCmd);

    step3ProgramCmd.data()->setPreProgram(preProgCmd);

    QVERIFY_EXCEPTION_THROWN(generator.generate(step3ProgramCmd) , CmdNotValid);
}

void Step3ProgramUploadGeneratorTest::test_generate_if_main_prog_is_specified(){
    Step3ProgramUploadGenerator generator;
    QSharedPointer<Step3Program> step3ProgramCmd(new Step3Program());
    QSharedPointer<MainProgramCmd>  mainProgCmd(new MainProgramCmd());
    QSharedPointer<ICmd> dwellCmd = CommandGeneratorForTest::generateDwellCmd();

    QVERIFY(dwellCmd.data()->isValid());
    mainProgCmd.data()->addCommand(dwellCmd);

    step3ProgramCmd.data()->setMainProgram(mainProgCmd);

    QString str = generator.generate(step3ProgramCmd);

    QVERIFY(str.contains("_a,1"));
    QVERIFY(!str.contains("_a,2"));
}

void Step3ProgramUploadGeneratorTest::test_generate_if_main_and_pre_prog_is_specified(){
    Step3ProgramUploadGenerator generator;
    QSharedPointer<Step3Program> step3ProgramCmd(new Step3Program());
    QSharedPointer<MainProgramCmd>  mainProgCmd(new MainProgramCmd());
    QSharedPointer<PreProgramCmd> preProgCmd(new PreProgramCmd());
    QSharedPointer<ICmd> dwellCmd = CommandGeneratorForTest::generateDwellCmd();

    preProgCmd.data()->addCommand(dwellCmd);
    mainProgCmd.data()->addCommand(dwellCmd);

    step3ProgramCmd.data()->setMainProgram(mainProgCmd);
    step3ProgramCmd.data()->setPreProgram(preProgCmd);

    QString str = generator.generate(step3ProgramCmd);

    QVERIFY(str.contains("_a,2"));
    QVERIFY(str.contains("_a,1"));
}
