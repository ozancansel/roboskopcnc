#ifndef PREPROGRAMGENERATORTEST_H
#define PREPROGRAMGENERATORTEST_H

#include <QObject>
#include <QtTest/QtTest>

class PreProgramGeneratorTest : public QObject
{
    Q_OBJECT

private slots:

    void    test_generate_randomly_specified_multiple_commands_which_are_supported();
    void    test_generate_should_generate_linear_move_cmd();
    void    test_generate_should_generate_rapid_move_cmd();
    void    test_generate_should_generate_dwell_cmd();
    void    test_generate_should_generate_homing_cmd();

};

#endif // PREPROGRAMGENERATORTEST_H
