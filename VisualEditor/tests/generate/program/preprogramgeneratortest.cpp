#include "preprogramgeneratortest.h"
#include "command/program/preprogramcmd.h"
#include "command/nc/linearmovecmd.h"
#include "command/commandgeneratorfortest.h"
#include "generate/program/preprogramcodegenerator.h"

void PreProgramGeneratorTest::test_generate_randomly_specified_multiple_commands_which_are_supported(){
    QSharedPointer<DwellCmd> dwell = CommandGeneratorForTest::generateDwellCmd();
    QSharedPointer<LinearMoveCmd> linearMoveCmd =  CommandGeneratorForTest::generateLinearMoveCmd();
    PreProgramCmd* cmd = new PreProgramCmd();
    cmd->addCommand(dwell);
    cmd->addCommand(linearMoveCmd);

    PreProgramCodeGenerator generator;

    QString str = generator.generate(QSharedPointer<ICmd>(cmd));

    QCOMPARE(str , QString("G4 P1.000\nG1 X10.000 Y10.000 Z10.000 F200.000\n"));
}

void PreProgramGeneratorTest::test_generate_should_generate_dwell_cmd(){
    PreProgramCodeGenerator generator;

    QSharedPointer<DwellCmd> dwell = CommandGeneratorForTest::generateDwellCmd();
    PreProgramCmd* cmd = new PreProgramCmd();
    cmd->addCommand(dwell);

    QVERIFY(!generator.generate(QSharedPointer<ICmd>(cmd)).isEmpty());
}

void PreProgramGeneratorTest::test_generate_should_generate_homing_cmd(){
    PreProgramCodeGenerator generator;

    QSharedPointer<HomingCmd> dwell = CommandGeneratorForTest::generateHomingCmd();
    PreProgramCmd* cmd = new PreProgramCmd();
    cmd->addCommand(dwell);

    QVERIFY(!generator.generate(QSharedPointer<ICmd>(cmd)).isEmpty());
}

void PreProgramGeneratorTest::test_generate_should_generate_linear_move_cmd(){
    PreProgramCodeGenerator generator;

    QSharedPointer<LinearMoveCmd> dwell = CommandGeneratorForTest::generateLinearMoveCmd();
    PreProgramCmd* cmd = new PreProgramCmd();
    cmd->addCommand(dwell);

    QVERIFY(!generator.generate(QSharedPointer<ICmd>(cmd)).isEmpty());
}

void PreProgramGeneratorTest::test_generate_should_generate_rapid_move_cmd(){
    PreProgramCodeGenerator generator;

    QSharedPointer<RapidMoveCmd> dwell = CommandGeneratorForTest::generateRapidMoveCmd();
    PreProgramCmd* cmd = new PreProgramCmd();
    cmd->addCommand(dwell);

    QVERIFY(!generator.generate(QSharedPointer<ICmd>(cmd)).isEmpty());
}
