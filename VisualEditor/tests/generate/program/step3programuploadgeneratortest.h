#ifndef STEP3PROGRAMUPLOADGENERATORTEST_H
#define STEP3PROGRAMUPLOADGENERATORTEST_H

#include <QObject>
#include <QtTest/QtTest>

class Step3ProgramUploadGeneratorTest : public QObject
{

    Q_OBJECT

private slots:

    void    test_generate_if_main_prog_is_specified();
    void    test_generate_if_just_pre_prog_is_specified_should_throw_cmd_not_valid_exception();
    void    test_generate_if_main_and_pre_prog_is_specified();

};

#endif // STEP3PROGRAMUPLOADGENERATORTEST_H
