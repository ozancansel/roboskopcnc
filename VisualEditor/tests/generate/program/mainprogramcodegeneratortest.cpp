#include "mainprogramcodegeneratortest.h"
#include "command/program/mainprogramcmd.h"
#include "command/nc/linearmovecmd.h"
#include "command/commandgeneratorfortest.h"
#include "generate/program/mainprogramcodegenerator.h"

void MainProgramCodeGeneratorTest::test_generate_randomly_specified_multiple_commands_which_are_supported(){
    QSharedPointer<DwellCmd> dwell = CommandGeneratorForTest::generateDwellCmd();
    QSharedPointer<LinearMoveCmd> linearMoveCmd =  CommandGeneratorForTest::generateLinearMoveCmd();
    MainProgramCmd* cmd = new MainProgramCmd();
    cmd->addCommand(dwell);
    cmd->addCommand(linearMoveCmd);

    MainProgramCodeGenerator generator;

    QString str = generator.generate(QSharedPointer<ICmd>(cmd));

    QCOMPARE(str , QString("G4 P1.000\nG1 X10.000 Y10.000 Z10.000 F200.000\n"));
}

void MainProgramCodeGeneratorTest::test_generate_should_generate_dwell_cmd(){
    MainProgramCodeGenerator generator;

    QSharedPointer<DwellCmd> dwell = CommandGeneratorForTest::generateDwellCmd();
    MainProgramCmd* cmd = new MainProgramCmd();
    cmd->addCommand(dwell);

    QVERIFY(!generator.generate(QSharedPointer<ICmd>(cmd)).isEmpty());
}

void MainProgramCodeGeneratorTest::test_generate_should_generate_homing_cmd(){
    MainProgramCodeGenerator generator;

    QSharedPointer<HomingCmd> dwell = CommandGeneratorForTest::generateHomingCmd();
    MainProgramCmd* cmd = new MainProgramCmd();
    cmd->addCommand(dwell);

    QVERIFY(!generator.generate(QSharedPointer<ICmd>(cmd)).isEmpty());
}

void MainProgramCodeGeneratorTest::test_generate_should_generate_linear_move_cmd(){
    MainProgramCodeGenerator generator;

    QSharedPointer<LinearMoveCmd> dwell = CommandGeneratorForTest::generateLinearMoveCmd();
    MainProgramCmd* cmd = new MainProgramCmd();
    cmd->addCommand(dwell);

    QVERIFY(!generator.generate(QSharedPointer<ICmd>(cmd)).isEmpty());
}

void MainProgramCodeGeneratorTest::test_generate_should_generate_rapid_move_cmd(){
    MainProgramCodeGenerator generator;

    QSharedPointer<RapidMoveCmd> dwell = CommandGeneratorForTest::generateRapidMoveCmd();
    MainProgramCmd* cmd = new MainProgramCmd();
    cmd->addCommand(dwell);

    QVERIFY(!generator.generate(QSharedPointer<ICmd>(cmd)).isEmpty());
}
