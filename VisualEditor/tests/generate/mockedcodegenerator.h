#ifndef MOCKEDCODEGENERATOR_H
#define MOCKEDCODEGENERATOR_H

#include "generate/codegenerator.h"
#include "gmock/gmock.h"
#include <QSharedPointer>

class MockedCodeGenerator : public CodeGenerator
{

public:

    MOCK_METHOD1(generateInternal , QString(const QSharedPointer<ICmd>));
    MOCK_METHOD1(canGenerate , bool(const QSharedPointer<ICmd>));

};

#endif // MOCKEDCODEGENERATOR_H
