#ifndef EMPTYTYPEDOBJECTFORTEST_H
#define EMPTYTYPEDOBJECTFORTEST_H

#include "typedobject.h"

class EmptyTypedObjectForTest : public TypedObject
{
public:
    EmptyTypedObjectForTest();
    virtual QString typeName();
};

#endif // EMPTYTYPEDOBJECTFORTEST_H
