#ifndef TYPEDOBJECTTESTS_H
#define TYPEDOBJECTTESTS_H

#include <QObject>
#include <QtTest/QtTest>
#include "typedobject.h"

class TypedObjectTest : public QObject
{

    Q_OBJECT

public:

    TypedObjectTest(QObject* parent = nullptr);

private slots:

    void    test_is_method();

};

#endif // TYPEDOBJECTTESTS_H
