#ifndef BEAUTIFIERMOCK_H
#define BEAUTIFIERMOCK_H

#include <gmock/gmock.h>
#include "beautify/beautifier.h"

class BeautifierMock : public Beautifier
{

public:

    MOCK_METHOD1(beautify , void(QString&));
    MOCK_METHOD2(beautify , void(QString& , int));
    MOCK_METHOD3(beautify , void(QString& , int , int));

    MOCK_METHOD1(internalBeautify , void(QString&));

};

#endif // BEAUTIFIERMOCK_H
