#ifndef BEAUTIFIERTEST_H
#define BEAUTIFIERTEST_H

#include <QObject>
#include <QtTest/QtTest>
#include <gmock/gmock.h>

class BeautifierTest : public QObject
{

    Q_OBJECT

private slots:

    void    test_beautify_if_start_idx_is_lower_than_zero_should_throw_index_outbound_exception();
    void    test_beautify_if_end_idx_is_equal_or_greater_than_length_should_throw_parameter_is_wrong_exception();
    void    test_beautify_if_start_idx_is_greater_than_end_idx_should_throw_parameter_is_wrong_exception();
    void    test_beautify_if_start_idx_is_specified_should_send_to_internalBeautify_as_a_substring_of_text();
    void    test_beautify_is_start_idx_and_end_idx_is_specified_should_send_to_internalBeautify_as_a_substring_of_text();

};

#endif // BEAUTIFIERTEST_H
