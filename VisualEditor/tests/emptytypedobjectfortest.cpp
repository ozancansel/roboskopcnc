#include "emptytypedobjectfortest.h"

EmptyTypedObjectForTest::EmptyTypedObjectForTest()
{
    addType("type");
    addType("example-1");
}

QString EmptyTypedObjectForTest::typeName(){
    return "type";
}
