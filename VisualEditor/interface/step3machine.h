#ifndef STEP3MACHINE_H
#define STEP3MACHINE_H

#include <QQuickItem>
#include "serial/serialcomm.h"

class Step3Machine : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(SerialComm* port READ port WRITE setPort NOTIFY portChanged)

public:

    static void         registerQmlType();
    Step3Machine(QQuickItem* parent = nullptr);
    void                setPort(SerialComm* comm);
    SerialComm* port();

signals:

    QString                portChanged();

public slots:

    QString                incMoveXYZ(float x, float y, float z , float feedRate);
    QString                linearMoveXYZ(float x , float y , float z , float feedRate);
    QString                rapidMoveXYZ(float x , float y , float z);
    QString                linearMoveX(float x , float feedRate);
    QString                rapidMoveX(float x);
    QString                linearMoveY(float y , float feedRate);
    QString                rapidMoveY(float y);
    QString                linearMoveZ(float z , float feedRate);
    QString                rapidMoveZ(float z);
    QString                arcCw(float radius , float feedRate);
    QString                arcCw(float x , float y , float z , float radius , float feedRate);
    QString                arcCcw(float radius , float feedRate);
    QString                arcCcw(float x , float y , float z , float radius , float feedRate);
    QString                dwell(int ms);
    QString                waitForFinish();
    QString                reset();
    QString                hold();
    QString                resume();
    QString                unlock();

private:

    SerialComm* m_comm;
    QIODevice*  m_device;
    float       m_virtualX;
    float       m_virtualY;
    float       m_virtualZ;

    bool        waitForOk();
};

#endif // STEP3MACHINE_H
