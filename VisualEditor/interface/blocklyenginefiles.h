#ifndef BLOCKLYENGINE_H
#define BLOCKLYENGINE_H

#include <QQuickItem>

#define ENGINE_VERSION_LABEL        "version"
#define ENGINE_VERSION              1

class BlocklyEngineFiles : public QQuickItem
{

    Q_OBJECT
    Q_PROPERTY(QString indexHtmlPath READ indexHtmlPath NOTIFY indexHtmlPathChanged)

public:

    static void registerQmlType();
    BlocklyEngineFiles(QQuickItem* parent = nullptr);
    void        ensureEngineFiles();
    QString     indexHtmlPath();
    void        setIndexHtmlPath(QString val);
    int         readVersion();

signals:

    void        indexHtmlPathChanged();

private:

    QString     m_htmlPath;

    bool        copyDirectoryRecursively(QString root, QString target , bool overwrite);
    void        syncEngineSources();

};

#endif // BLOCKLYENGINE_H
