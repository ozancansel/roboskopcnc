import QtQuick 2.6
import "../../shared_qml"
import "../../shared_qml/component"

Item{

    property font   font    :   Qt.font({
                                            family: 'Linux Libertine',
                                        })

    id      :   settingControl

    Text {
        id      :   settingText
        width   :   settingControl.width * 0.5 - leftPadding
        height  :   settingControl.height
        fontSizeMode    :   Text.Fit
        wrapMode        :   Text.Wrap
        font            :   settingControl.font
        text            :   "X Step(mm)"
        verticalAlignment   :   Text.AlignVCenter
        leftPadding     :   Responsive.getH(40)
        color           :   "white"
        z               :   2
    }

    BackgroundPanel {
        anchors.fill    :   parent
        opacity         :   0.7
    }
}
