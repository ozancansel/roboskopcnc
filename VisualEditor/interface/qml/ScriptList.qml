import QtQuick 2.0
import QtQuick.Controls 2.2
import QtWebView 1.1
import "../shared_qml/component"

PageTemplate {

    property Database   db                  :   ({})
    property CurrentProgram currentProgram  :   ({})
    property WebView blocklyWebEngine       :   ({})
    property bool           isCurrent       :   false

    id          :   scriptList
    headerText  :   "Programlar"
    headerBackground.opacity    :   0.7

    onIsCurrentChanged  :   {
        if(isCurrent)
            refresh()
    }

    function refresh(){
        var idx = scriptListView.currentIndex
        scriptsListModel.clear()
        var scripts = db.readAllScripts()
        for(var i = 0; i < scripts.length; i++){
            scriptsListModel.append(scripts[i])
        }
        if(scriptsListModel.count >= idx + 1)
            scriptListView.currentIndex = idx
    }

    function update(){
        blocklyWebEngine.runJavaScript("getXml()" , function(result){
            db.updateScript(currentProgram.name , result)
            refresh()
        })
    }

    ListModel   {
        id  :   scriptsListModel
    }

    RToastMessage{
        id          :   programIsRunningDialog
        x           :   parent.width / 2 - width / 2
        y           :   parent.height / 2 - height /2
        message     :   "Şu anda başka bir program çalışıyor\nLütfen önce programı durdurun."
        headerText  :   "Hata !"
    }

    RToastMessage{
        id              :   scriptExistsDialog
        x               :   parent.width / 2 - width / 2
        y               :   parent.height / 2 - height /2
        width           :   Responsive.getH(550)
        height          :   Responsive.getV(400)
        message         :   "Bu program zaten mevcut !"
        headerText      :   "Hata !"
        font.pixelSize  :   Responsive.getV(40)
    }

    RMessageBox{
        id          :   deleteScriptDialog
        x           :   parent.width / 2 - width / 2
        y           :   parent.height / 2 - height / 2
        message     :   "'" + scriptsListModel.get(scriptListView.currentIndex).name + "' isimli programı silinecek, onaylıyor musunuz ?"
        headerText  :   "Programı Sil"
        onAccepted  :   {
            //Retrieve item from list
            var item = scriptsListModel.get(scriptListView.currentIndex)
            //Delete script from database
            db.deleteScript(item.id)
            if(currentProgram.name === item.name){
                currentProgram.close()
            }
            //Refresh list
            refresh()
        }
    }

    RDialogTemplate {
        id              :   insertProgramDialog
        x               :   parent.width / 2 - width / 2
        y               :   parent.height / 2 - height / 2
        headerText      :   "Kaydet"
        width           :   Responsive.getH(1100)
        height          :   Responsive.getV(650)
        contentItem     :   Item    {
            Column{
                anchors.horizontalCenter    :   parent.horizontalCenter
                y   :   Responsive.getV(80)
                RTextField {
                    id              :   scriptNameInput
                    width           :   Responsive.getH(900)
                    font.pixelSize  :   Responsive.getV(100)
                    placeholderText :   "Isim giriniz..."
                }
            }
        }
        buttonGroup     :   [
            {text : "Kaydet" , role : DialogButtonBox.AcceptRole} ,
            {text : "İptal" , role : DialogButtonBox.RejectRole }
        ]
        onOpened        :   scriptNameInput.text = ""
        onAccepted      :   {
            blocklyWebEngine.runJavaScript("getXml()" , function(result){
                //If any program is not opened, opens
                if(currentProgram.name === ""){
                    //Insert script into table
                    var rowid = db.insertScriptIfNotExists(scriptNameInput.text , result)
                    if(rowid === -1){
                        scriptExistsDialog.open()
                    }
                    else{
                        //Refresh list
                        refresh()
                        //And open as a current program
                        currentProgram.open(scriptNameInput.text , result)
                    }
                }
            })
        }
    }

    RMessageBox{
        id          :   updateProgramDialog
        x           :   parent.width / 2 - width / 2
        y           :   parent.height / 2 - height / 2
        headerText  :   "Kaydet"
        message     :   currentProgram.name + " isimli program kaydedilsin mi ?"
        onAccepted  :   update()
    }

    Item{
        anchors.centerIn    :   parent
        width               :   Responsive.getH(1000)
        height              :   Responsive.getV(600)
        visible             :   scriptsListModel.count === 0
        Text {
            id                  :   portNotFoundText
            text                :   "Kayıtlı Program Yok"
            font.pixelSize      :   Responsive.getV(80)
            font.family         :   "Linux Libertine"
            color               :   "white"
            anchors.centerIn    :   parent
            horizontalAlignment :   Text.AlignHCenter
            verticalAlignment   :   Text.AlignVCenter
            z                   :   2
        }

        BackgroundPanel{
            id                  :   portNotFoundPanel
            anchors.fill        :   parent
            borderVisible       :   true
            opacity             :   0.75
        }
    }
    ListView    {
        id              :   scriptListView
        model           :   scriptsListModel
        anchors.top     :   parent.top

        //If header visible then margin otherwise not
        anchors.topMargin   :   Responsive.getV(200)
        anchors.bottom  :   buttonsRow.top
        anchors.left    :   parent.left
        anchors.right   :   parent.right
        clip            :   true
        delegate        :   Item{
            readonly property bool isCurrent    :   name === currentProgram.name
            id          :   listItem
            height      :   Responsive.getV(150)
            width       :   scriptListView.width

            Row{
                anchors.centerIn    :   parent
                spacing             :   Responsive.getH(50)

                Text {
                    text                        :   listItem.isCurrent && currentProgram.isRunning ? name + "  (Çalışıyor)" : name
                    font.pixelSize              :   Responsive.getV(60)
                    color                       :   "white"
                    anchors.verticalCenter      :   parent.verticalCenter
                }

                Rectangle   {
                    id          :   currentProgramState
                    height      :   Responsive.getV(100)
                    width       :   height
                    radius      :   height * 0.5
                    color       :   "#38d763"
                    visible     :   listItem.isCurrent
                    border.width:   Responsive.getV(2)
                    border.color:   "white"
                }

                SequentialAnimation   {
                    id          :   runningAnimation
                    running     :   listItem.isCurrent & currentProgram.isRunning
                    loops       :   Animation.Infinite

                    ScriptAction{
                        script  :   currentProgramState.color = "#38d763"
                    }

                    ColorAnimation {
                        target  :   currentProgramState
                        from    :   "#38d763"
                        to      :   "#38adf9"
                        duration:   500
                        property:   "color"
                    }

                    ColorAnimation {
                        target  :   currentProgramState
                        from    :   "#38adf9"
                        to      :   "#38d763"
                        duration:   500
                        property:   "color"
                    }

                    ScriptAction{
                        script  :   currentProgramState.color = "#38d763"
                    }
                }
            }

            MouseArea{
                id          :   area
                onClicked   :   scriptListView.currentIndex = index
                anchors.fill:   parent
            }
        }

        highlight       :   Rectangle{
            color   :   "#ff4237"
        }
    }

    Row {
        id                          :   buttonsRow
        anchors.bottom              :   parent.bottom
        anchors.bottomMargin        :   Responsive.getV(20)
        anchors.horizontalCenter    :   parent.horizontalCenter
        spacing                     :   Responsive.getH(30)
        RButton {
            id              :   deleteButton
            width           :   Responsive.getH(400)
            height          :   Responsive.getV(150)
            text            :   "Sil"
            font.pixelSize  :   Responsive.getV(50)
            background.opacity  :   0.8
            onClicked       :   {
                //If there is no item in list
                if(scriptsListModel.count === 0)
                    return   //returns

                deleteScriptDialog.open()
            }
        }

        RButton{
            id              :   selectButton
            width           :   Responsive.getH(400)
            height          :   Responsive.getV(150)
            text            :   "Aç"
            font.pixelSize  :   Responsive.getV(50)
            background.opacity  :   0.8
            onClicked       :   {
                //If there is no program return
                if(scriptsListModel.count === 0)
                    return
                //If there is no selected item return
                if(scriptListView.currentIndex < 0)
                    return

                //If any program is running then display warning message
                if(currentProgram.isRunning){
                    programIsRunningDialog.open()
                    return
                }
                var item = scriptsListModel.get(scriptListView.currentIndex)
                currentProgram.open(item.name , item.xml)
            }
        }

        RButton{
            id              :   closeButton
            width           :   Responsive.getH(400)
            height          :   Responsive.getV(150)
            text            :   "Kapat"
            font.pixelSize  :   Responsive.getV(50)
            background.opacity  :   0.8
            onClicked       :   {
                currentProgram.close()
            }
        }

        RButton {
            id              :   saveButton
            width           :   Responsive.getH(400)
            height          :   Responsive.getV(150)
            text            :   "Kaydet"
            font.pixelSize  :   Responsive.getV(50)
            background.opacity  :   0.8
            onClicked       :   {
                if(currentProgram.name === "")
                    insertProgramDialog.open()
                else
                    updateProgramDialog.open()
            }
        }
    }
}
