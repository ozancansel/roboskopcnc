import QtQuick 2.0
import "../shared_qml/component"
import "../shared_qml"

Item {

    property  Programmer    programmerPage  :   ({})

    RButton{
        width               :   Responsive.getH(500)
        height              :   Responsive.getV(500)
        text                :   "Üret"
        font.pixelSize      :   Responsive.getV(120)
        anchors.centerIn    :   parent
        onClicked           :   {
            programmerPage.blocklyEngine.runJavaScript("generate()" , function(result){
                console.log("Generated code : " + result);
            });
        }
    }
}
