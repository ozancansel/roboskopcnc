import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.0
import RLib 1.0
import ScriptEditor 1.0
import "qml"
import "shared_qml"
import "shared_qml/component"

ApplicationWindow {

    readonly property SerialComm    port    :   Platform.isMobile ? bt : serial

    property variant    pages   :   ["Programlar" , "Kontrol" , "Program" , "Bağlantı" , "Ayarlar"]

    id          :   mainWindow
    visible     :   true
    visibility  :   Window.FullScreen
    title       :   qsTr("Machinventor Programmer")

    SerialPort  {
        id              :   serial
        debugEnabled    :   true
        baud            :   SerialComm.B115200
    }

    BlocklyEngineFiles{
        id          :   blocklyEngineFiles
    }

    BluetoothPort   {
        id              :   bt
        debugEnabled    :   true
    }

    Axis3Machine{
        id              :   axis3Machine
        port            :   mainWindow.port
    }

    Axis3MacroEngine{
        id              :   engine
        machine         :   axis3Machine
    }

    Database  {
        id                      :   db
        Component.onCompleted   :   db.init()
    }

    CurrentProgram  {
        id  :   currProgram
    }

    FontLoader{
        id      :   libertineFont
        source  :   "/res/font/LinLibertine_Re-4.7.5.ttf"
    }

    RAppMenu {
        id      :   appMenu
        width   :   mainWindow.width / 3
        height  :   mainWindow.height
    }


    SwipeView {
        id          :   swipeView
        anchors.fill:   parent
        currentIndex:   footerSwipe.currentIndex
        interactive :   swipeInteractiveButton.checked

        ScriptList  {
            id                          :   scripts
            db                          :   db
            currentProgram              :   currProgram
            isCurrent                   :   swipeView.currentIndex === 0
            blocklyWebEngine            :   programmingPage.blocklyEngine
        }

//        Control {
//            engine                  :   engine
//            port                    :   mainWindow.port
//            blocklyEngineWeb        :   programmingPage.blocklyEngine
//            db                      :   db
//            currentProgram          :   currProgram
//            onSwipeToBluetoothPage  :   {
//                swipeView.setCurrentIndex(3)
//                connection.startScan()
//            }
//        }

        Simulation{
            id              :   simulation
            programmerPage  :   programmingPage
        }

        Programmer  {
            id              :   programmingPage
            engineFiles     :   blocklyEngineFiles
            currentProgram  :   currProgram
        }

        Connection  {
            id      :   connection
            comm    :   port
            headerBackground.opacity    :   0.7
        }

        Settings{
            id      :   settingsPage
            port    :   mainWindow.port
        }

        Component.onCompleted: setCurrentIndex(1)
    }

    background  :   RBackground {
        alignTo :   mainWindow
        sourceHd:   "/res/background/main-background.jpg"
        source2k:   "/res/background/main-background.jpg"
        source4k:   "/res/background/main-background.jpg"
    }

    footer  :   Item    {
        height      :   40
        width       :   mainWindow.width

        Item{
            anchors.fill    :   swipeInteractiveButton
            visible         :   !swipeInteractiveButton.checked
            Rectangle{
                anchors.fill    :   parent
                color           :   "red"
                opacity         :   0.7
            }
            Text {
                id      :   name
                text    :   qsTr("Kilitli")
                anchors.centerIn    :   parent
                color   :   "white"
                font.family :   "Linux Libertine"
                font.pixelSize  :   23
            }
        }

        RIconButton {
            id          :   swipeInteractiveButton
            checkable   :   true
            checked     :   true
            height      :   parent.height
            width       :   Responsive.getH(300)
            z           :   3
        }

        SwipeView{
            id          :   footerSwipe
            currentIndex:   swipeView.currentIndex
            z           :   2
            interactive :   swipeInteractiveButton.checked
            Repeater{
                model   :   swipeView.count
                Item { }
            }
            background  :   Item    {   }
            anchors.fill    :   parent
        }

        PageIndicator   {
            id              :   pageIndicator
            count           :   swipeView.count
            currentIndex    :   swipeView.currentIndex
            anchors.centerIn:   parent
            z               :   5
            delegate        :   Rectangle   {
                implicitWidth   :   20
                implicitHeight  :   20
                radius          :   width / 2
                color           :   "green"
                opacity         :   index == pageIndicator.currentIndex ? 0.95 : pressed ? 0.7 : 0.45

                MouseArea {
                    anchors.fill    :   parent
                    onClicked       :   swipeView.setCurrentIndex(index)
                }
            }
        }
    }
}
