import QtQuick 2.0
import QtQuick.Controls 2.1
import "../"

SpinBox{
    property real factor        :   Math.pow(10, decimals)
    property int  decimals      :   3
    property real realValue     :   0.0
    property real realFrom      :   0.0
    property real realTo        :   100.0
    property real realStepSize  :   1.0
    property string suffix      :   ""
    property string prefix      :   ""
    readonly property color     buttonColor :   "#191919"
    readonly property color     pressedColor:   "#A8A8A8"
    id          :   spinbox
    stepSize    :   realStepSize*factor
    value       :   realValue*factor
    to          :   realTo*factor
    from        :   realFrom*factor
    editable    :   true

    function    getVal(){
        return value / factor
    }

    up.indicator  :   Item{ }

    down.indicator  :    Item{  }

    background  :   BackgroundPanel {
        height          :   spinbox.height
        width           :   spinbox.width
        borderVisible   :   true
    }

    contentItem :   Item    {
        width       :   spinbox.width
        height      :   spinbox.height
        Text {
            id                  :   name
            anchors.centerIn    :   parent
            font                :   spinbox.font
            color               :   "white"
            text                :   prefix + textFromValue(value , locale) + suffix
        }

    }

    validator   :   DoubleValidator {
        bottom  :   Math.min(spinbox.from, spinbox.to)  *   spinbox.factor
        top     :   Math.max(spinbox.from, spinbox.to)    *   spinbox.factor
    }

    textFromValue: function(value, locale) {
        var val = parseFloat(value * 1.0/factor).toFixed(decimals);
        return val;
    }

    valueFromText:  function(value , local){
        var val = parseFloat(value) * factor
        return val
    }
}
