import QtQuick 2.7
import QtQuick.Controls 2.2
import "../"

Dialog  {

    property string     message     :   ""
    property string     headerText  :   ""
    property alias      messageFont :   messageText.font
    property alias      headerFont  :   headerT.font
    property bool       alignCenter :   false

    property int        displayTime :   5000

    id          :   toastDialog
    z           :   2
    closePolicy :   Dialog.NoAutoClose
    onAccepted  :   close()

    enter       :   Transition {
        SequentialAnimation{
            NumberAnimation { property : "opacity"; from : 0.0; to : 1.0; duration : 500}
            ScriptAction{ script :
                    if(displayTime > 0)
                        disappearTimer.start(); }
        }
    }

    SequentialAnimation {
        id          :   disapperAnimation
        NumberAnimation { target : toastDialog; property : "opacity"; from : 1.0; to : 0.0; duration : 350 }
        ScriptAction    { script : close(); }
    }

    Timer{
        id          :   disappearTimer
        interval    :   displayTime
        onTriggered :   disapperAnimation.start()
    }

    header      :   Item    {

        height      :   Responsive.getV(105)
        width       :   toastDialog.width

        Text {
            id              :   headerT
            text            :   headerText
            x               :   Responsive.getH(70)
            y               :   Responsive.getV(35)
            font.pixelSize  :   Responsive.getV(40)
            font.family     :   "Linux Libertine"
            color           :   "white"
        }

        Rectangle{
            id              :   bottomLine
            anchors.left    :   parent.left
            anchors.right   :   parent.right
            anchors.bottom  :   parent.bottom
            height          :   Responsive.getV(2)
        }
    }

    contentItem :   Item {
        id              :   panel

        Text {
            id                  :   messageText
            x                   :   alignCenter ? 0 : Responsive.getH(20)
            y                   :    Responsive.getV(35)
            text                :   message
            font.pixelSize      :   toastDialog.font.pixelSize
            color               :   "white"
            width               :   parent.width
            horizontalAlignment :   alignCenter ? Text.AlignHCenter : Text.AlignLeft
        }
    }

    footer      :   DialogButtonBox {
        id              :   dialogBox
        standardButtons :   toastDialog.standardButtons
        position        :   DialogButtonBox.Footer
        alignment       :   Qt.AlignTop | Qt.AlignCenter
        spacing         :   Responsive.getH(20)
        height          :   Responsive.getV(130)
        width           :   toastDialog.width
        RButton {
            height                      :   Responsive.getV(90)
            width                       :   Responsive.getH(160)
            text                        :   "Tamam"
            font.pixelSize              :   Responsive.getV(30)
            DialogButtonBox.buttonRole  :   DialogButtonBox.AcceptRole
        }
        background      :   Item {
            id          :   name
        }
    }

    background  :   BackgroundPanel {
        height          :   toastDialog.height
        width           :   toastDialog.width
        borderVisible   :   true
    }
}
