import QtQuick 2.0
import "component"
import "../qml"

Item {

    property string     headerText  :   ""
    property alias      headerFont  :   headerTextItem.font
    property bool       headerVisible   :   true
    property alias      headerBackground    :   headerPanel

    Rectangle{
        id          :   mainBackground
        anchors.fill:   parent
//        color       :   "#383838"
        color       :   "transparent"
    }

    Item{

        width       :   Responsive.getH(1880)
        height      :   Responsive.getV(150)
        anchors.horizontalCenter    :   parent.horizontalCenter
        y           :   Responsive.getV(20)

        Text {
            id                  :   headerTextItem
            text                :   headerText
            font.pixelSize      :   Responsive.getV(67)
            color               :   "white"
            font.family         :   "Linux Libertine"
            anchors.centerIn    :   parent
            horizontalAlignment :   Text.AlignHCenter
            verticalAlignment   :   Text.AlignVCenter
            z                   :   2
        }

        BackgroundPanel {
            id          :   headerPanel
            anchors.fill:   parent
            visible     :   headerVisible
            borderVisible   :   true
            border.width    :   Responsive.getV(2)
        }
    }
}
