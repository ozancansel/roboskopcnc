Blockly.Blocks['linear_move_y'] = {
  init: function () {
    this.appendDummyInput()
      .appendField("İlerle y:")
      .appendField(new Blockly.FieldNumber(0), "y")
      .appendField("  hız:")
      .appendField(new Blockly.FieldNumber(500, 0), "feedRate");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
    this.setTooltip("Y kızağını hareket ettirir.");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['linear_move_y_at_max'] = {
  init: function () {
    this.appendDummyInput()
      .appendField("İlerle Maks  y:")
      .appendField(new Blockly.FieldNumber(0), "y");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
    this.setTooltip("Y kızağını maksimum hızda hareket ettirir.");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['linear_move_x'] = {
  init: function () {
    this.appendDummyInput()
      .appendField("İlerle x:")
      .appendField(new Blockly.FieldNumber(0), "x")
      .appendField("  hız:")
      .appendField(new Blockly.FieldNumber(500, 0), "feedRate");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
    this.setTooltip("X kızağını hareket ettirir.");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['linear_move_x_at_max'] = {
  init: function () {
    this.appendDummyInput()
      .appendField("İlerle Maks.  x:")
      .appendField(new Blockly.FieldNumber(0), "x");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
    this.setTooltip("X kızağını maksimum hızda hareket ettirir.");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['dwell'] = {
  init: function () {
    this.appendDummyInput()
      .appendField("Bekle ms:")
      .appendField(new Blockly.FieldNumber(1000, 1), "duration");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['linear_move_z'] = {
  init: function () {
    this.appendDummyInput()
      .appendField("İlerle z:")
      .appendField(new Blockly.FieldNumber(0), "z")
      .appendField(" hız:")
      .appendField(new Blockly.FieldNumber(500, 0), "feedRate");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
    this.setTooltip("Z kızağını hareket ettirir.");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['linear_move_z_at_max'] = {
  init: function () {
    this.appendDummyInput()
      .appendField("İlerle Maks  z:")
      .appendField(new Blockly.FieldNumber(0), "z");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
    this.setTooltip("Z kızağını maksimum hızda hareket ettirir.");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['linear_move_xy'] = {
  init: function () {
    this.appendDummyInput()
      .appendField("İlerle İnterpolasyon(xy)   x:")
      .appendField(new Blockly.FieldNumber(0), "x")
      .appendField("  y:")
      .appendField(new Blockly.FieldNumber(0), "y")
      .appendField(" hız :")
      .appendField(new Blockly.FieldNumber(0, 0), "feedRate");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(225);
    this.setTooltip("X ve Y eksenini interpolasyon olarak hareket ettirir.");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['linear_move_xy_at_max'] = {
  init: function () {
    this.appendDummyInput()
      .appendField("İlerle İnterpolasyon(xy) Maks  x:")
      .appendField(new Blockly.FieldNumber(0), "x")
      .appendField("  y:")
      .appendField(new Blockly.FieldNumber(0), "y");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
    this.setTooltip("X ve y kızağını maksimum hızda interpolasyon ilerletir.");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['linear_move'] = {
  init: function () {
    this.appendDummyInput()
      .appendField("İlerle x:")
      .appendField(new Blockly.FieldNumber(0), "x")
      .appendField("  y:")
      .appendField(new Blockly.FieldNumber(0), "y")
      .appendField("  z:")
      .appendField(new Blockly.FieldNumber(0), "z")
      .appendField(" hız:")
      .appendField(new Blockly.FieldNumber(0, 0), "feedRate");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
    this.setTooltip("X, Y ve Z kızağını interpolasyon olarak sürer.");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['rapid_move'] = {
  init: function () {
    this.appendDummyInput()
      .appendField("İlerle Maks  x:")
      .appendField(new Blockly.FieldNumber(0), "x")
      .appendField("  y:")
      .appendField(new Blockly.FieldNumber(0), "y")
      .appendField("  z:")
      .appendField(new Blockly.FieldNumber(0), "z");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['infinite_loop'] = {
  init: function () {
    this.appendDummyInput()
      .appendField("Sürekli Çalıştır");
    this.appendStatementInput("statement")
      .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
    this.setTooltip("İçerisindeki blokları sürekil çalıştırır");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['repeat_n_times'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldNumber(10, 0), "loop_count")
        .appendField("kez tekrarla");
    this.appendStatementInput("loop_scope")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(120);
    this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['do_homing'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Homing Yap");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['pre_program'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Programdan Önce");
    this.appendStatementInput("statements")
        .setCheck(null);
    this.setInputsInline(false);
    this.setColour(230);
    this.setTooltip("");
    this.setHelpUrl("");
    this.setDeletable(false);
  }
};

Blockly.Blocks['main_program'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Ana Program");
    this.appendDummyInput()
        .appendField("Sürekli Tekrarla")
        .appendField(new Blockly.FieldCheckbox("TRUE"), "loop_forever");
    this.appendStatementInput("statements")
        .setCheck(null);
    this.setColour(230);
    this.setTooltip("");
    this.setHelpUrl("");
    this.setDeletable(false);
  }
};

Blockly.Blocks['post_program'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Program Bitince");
    this.appendStatementInput("statements")
        .setCheck(null);
    this.setColour(230);
    this.setTooltip("");
    this.setHelpUrl("");
    this.setDeletable(false);    
  }
};