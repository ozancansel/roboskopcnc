
Blockly.JavaScript['linear_move_y'] = function(block) {
  var number_y = block.getFieldValue('y');
  var number_feedrate = block.getFieldValue('feedRate');
  // TODO: Assemble JavaScript into code variable.
  var code = 'linearMoveY(' + number_y + ',' + number_feedrate + ');\n';
  return code;
};

Blockly.JavaScript['linear_move_y_at_max'] = function(block) {
  var number_y = block.getFieldValue('y');
  // TODO: Assemble JavaScript into code variable.
  var code = '...;\n';
  var code = 'rapidMoveY(' + number_y + ');\n';
  return code;
};

Blockly.JavaScript['linear_move_x'] = function(block) {
  var number_x = block.getFieldValue('x');
  var number_feedrate = block.getFieldValue('feedRate');
  // TODO: Assemble JavaScript into code variable.
  var code = 'linearMoveX(' + number_x + ',' + number_feedrate + ');\n';
  return code;
};

Blockly.JavaScript['linear_move_x_at_max'] = function(block) {
  var number_x = block.getFieldValue('x');
  // TODO: Assemble JavaScript into code variable.
  var code = 'rapidMoveX(' + number_x + ');\n';  
  return code;
};

Blockly.JavaScript['delay_ms'] = function(block) {
  var number_duration = block.getFieldValue('duration');
  // TODO: Assemble JavaScript into code variable.
  var code = 'dwell(' + number_duration +');\n';
  return code;
};

Blockly.JavaScript['linear_move_z'] = function(block) {
  var number_z = block.getFieldValue('z');
  var number_feedrate = block.getFieldValue('feedRate');
  // TODO: Assemble JavaScript into code variable.
  var code = 'linearMoveZ(' + number_z + ',' + number_feedrate + ');\n';
  return code;
};

Blockly.JavaScript['linear_move_z_at_max'] = function(block) {
  var number_z = block.getFieldValue('z');
  // TODO: Assemble JavaScript into code variable.
  var code = 'rapidMoveZ(' + number_z + ');\n';  
  return code;
};

Blockly.JavaScript['linear_move_xy'] = function(block) {
  var number_x = block.getFieldValue('x');
  var number_y = block.getFieldValue('y');
  var number_feedrate = block.getFieldValue('feedRate');
  // TODO: Assemble JavaScript into code variable.
  var code = 'ilerleXY(' + number_x + ',' + number_y + ',' + number_feedrate +  ');\n';  
  return code;
};

Blockly.JavaScript['linear_move_xy_at_max'] = function(block) {
  var number_x = block.getFieldValue('x');
  var number_y = block.getFieldValue('y');
  // TODO: Assemble JavaScript into code variable.
  var code = 'ilerleXY(' + number_x + ',' + number_y + ');\n';  
  
  return code;
};

Blockly.JavaScript['linear_move_xyz'] = function(block) {
  var number_x = block.getFieldValue('x');
  var number_y = block.getFieldValue('y');
  var number_z = block.getFieldValue('z');
  var number_feedrate = block.getFieldValue('feedRate');
  // TODO: Assemble JavaScript into code variable.
  var code = 'linearMoveXYZ(' + number_x + ',' + number_y + ',' + number_z + ',' + number_feedrate + ');\n';  
  return code;
};

Blockly.JavaScript['linear_move_xyz_at_max'] = function(block) {
  var number_x = block.getFieldValue('x');
  var number_y = block.getFieldValue('y');
  var number_z = block.getFieldValue('z');
  // TODO: Assemble JavaScript into code variable.
  var code = 'rapidMoveXYZ(' + number_x + ',' + number_y + ',' + number_z + ');\n';    
  return code;
};

Blockly.JavaScript['infinite_loop'] = function(block) {
  var statements_scope = Blockly.JavaScript.statementToCode(block, 'statement');
  var code = 'while(true){\n' + statements_scope + '}\n';
  return code;
};

Blockly.JavaScript['repeat_n_times'] = function(block) {
  var number_loop_count = block.getFieldValue('loop_count');
  var statements_loop_scope = Blockly.JavaScript.statementToCode(block, 'loop_scope');
  var code = '';
  for(var i = 0; i < number_loop_count; i++){
    code += statements_loop_scope;
  }
  return code;
};

Blockly.JavaScript['do_homing'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'homing();\n';
  return code;
};

Blockly.JavaScript['pre_program'] = function(block) {
  var statements_statements = Blockly.JavaScript.statementToCode(block, 'statements');
  var lines = statements_statements.split('\n');
  var code = '';
  for(var i = 0; i < lines.length; i++) {
    if(lines[i].length <= 1)
      continue;

    code += 'preProgram.' + lines[i].trim() + '\n';
  }
  return code.trim();
};

Blockly.JavaScript['main_program'] = function(block) {
  var statements_statements = Blockly.JavaScript.statementToCode(block, 'statements');
  var lines = statements_statements.split('\n');
  var code = '';
  for(var i = 0; i < lines.length; i++) {
    if(lines[i].length <= 1)
      continue;

    code += 'mainProgram.' + lines[i].trim() + '\n';
  }
  return code.trim();
};

Blockly.JavaScript['post_program'] = function(block) {
  var statements_statements = Blockly.JavaScript.statementToCode(block, 'statements');
  var lines = statements_statements.split('\n');
  var code = '';
  for(var i = 0; i < lines.length; i++) {
    if(lines[i].length <= 1)
      continue;

    code += 'preProgram.' + lines[i].trim() + '\n';
  }
  // TODO: Assemble JavaScript into code variable.
  return code.trim();
};

