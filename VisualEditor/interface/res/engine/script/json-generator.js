
String.prototype.insert = function (index, string) {
    if (index > 0)
      return this.substring(0, index) + string + this.substring(index, this.length);
    else
      return string + this;
  };

function formatJson(txt){
    var opened = false;
    var paranthesis = 0;
    txt = txt.trim();
    for(var i = 0; i < txt.length - 1; i++){
        if(txt[i] == '{'){
            paranthesis++;
            opened = true;
        }
        else if(txt[i] == '}')
            paranthesis--;

        if( paranthesis === 0 && opened){
            if(txt[i + 1] === ','){
                opened = false;
                continue;
            }
            if(txt.lastIndexOf('}') == i){
                break;
            }
            opened = false;
            txt = txt.insert(i + 1, ',');
            i++;
        }
    }

    return txt;
}

function statementsToJson(txt){
    return '[' + txt + ']';
}

Blockly.JavaScript['linear_move_y'] = function(block) {
    var number_y = block.getFieldValue('y');
    var number_feedrate = block.getFieldValue('feedRate');
    // TODO: Assemble JavaScript into code variable.
    var code = 'linearMoveY(' + number_y + ',' + number_feedrate + ');\n';
    return code;
  };
  
  Blockly.JavaScript['linear_move_y_at_max'] = function(block) {
    var number_y = block.getFieldValue('y');
    // TODO: Assemble JavaScript into code variable.
    var code = '...;\n';
    var code = 'rapidMoveY(' + number_y + ');\n';
    return code;
  };
  
  Blockly.JavaScript['linear_move_x'] = function(block) {
    var number_x = block.getFieldValue('x');
    var number_feedrate = block.getFieldValue('feedRate');
    // TODO: Assemble JavaScript into code variable.
    var code = 'linearMoveX(' + number_x + ',' + number_feedrate + ');\n';
    return code;
  };
  
  Blockly.JavaScript['linear_move_x_at_max'] = function(block) {
    var number_x = block.getFieldValue('x');
    // TODO: Assemble JavaScript into code variable.
    var code = 'rapidMoveX(' + number_x + ');\n';  
    return code;
  };
  
  Blockly.JavaScript['dwell'] = function(block) {
    var number_duration = block.getFieldValue('duration');
    // TODO: Assemble JavaScript into code variable.
    var code = 'dwell(' + number_duration +');\n';
    var obj = {
        name : "dwell" ,
        time : number_duration
    };
    return JSON.stringify(obj);
  };
  
  Blockly.JavaScript['linear_move_z'] = function(block) {
    var number_z = block.getFieldValue('z');
    var number_feedrate = block.getFieldValue('feedRate');
    // TODO: Assemble JavaScript into code variable.
    var code = 'linearMoveZ(' + number_z + ',' + number_feedrate + ');\n';
    return code;
  };
  
  Blockly.JavaScript['linear_move_z_at_max'] = function(block) {
    var number_z = block.getFieldValue('z');
    // TODO: Assemble JavaScript into code variable.
    var code = 'rapidMoveZ(' + number_z + ');\n';  
    return code;
  };
  
  Blockly.JavaScript['linear_move_xy'] = function(block) {
    var number_x = block.getFieldValue('x');
    var number_y = block.getFieldValue('y');
    var number_feedrate = block.getFieldValue('feedRate');
    // TODO: Assemble JavaScript into code variable.
    var code = 'ilerleXY(' + number_x + ',' + number_y + ',' + number_feedrate +  ');\n';  
    return code;
  };
  
  Blockly.JavaScript['linear_move_xy_at_max'] = function(block) {
    var number_x = block.getFieldValue('x');
    var number_y = block.getFieldValue('y');
    // TODO: Assemble JavaScript into code variable.
    var code = 'ilerleXY(' + number_x + ',' + number_y + ');\n';  
    
    return code;
  };
  
  Blockly.JavaScript['linear_move'] = function(block) {
    var number_x = block.getFieldValue('x');
    var number_y = block.getFieldValue('y');
    var number_z = block.getFieldValue('z');
    var number_feedrate = block.getFieldValue('feedRate');
    var obj = {
        name    :   "linear_move" ,
        x       :   number_x ,
        y       :   number_y ,
        z       :   number_z ,
        f       :   number_feedrate
    };
    return JSON.stringify(obj);
  };
  
  Blockly.JavaScript['rapid_move'] = function(block) {
    var number_x = block.getFieldValue('x');
    var number_y = block.getFieldValue('y');
    var number_z = block.getFieldValue('z');

    // TODO: Assemble JavaScript into code variable.
    var obj = {
        name    :   "rapid_move" ,
        x       :   number_x ,
        y       :   number_y ,
        z       :   number_z ,
    };

    return JSON.stringify(obj);
  };
  
  Blockly.JavaScript['infinite_loop'] = function(block) {
    var statements_scope = Blockly.JavaScript.statementToCode(block, 'statement');
    var code = 'while(true){\n' + statements_scope + '}\n';
    return code;
  };
  
  Blockly.JavaScript['repeat_n_times'] = function(block) {
    var number_loop_count = block.getFieldValue('loop_count');
    var statements_loop_scope = Blockly.JavaScript.statementToCode(block, 'loop_scope');
    if(statements_loop_scope.length == 0)
        return '';

     statements_loop_scope = formatJson(statements_loop_scope);
    var code = '';
    for(var i = 0; i < number_loop_count; i++){
        code += statements_loop_scope;
    }
    code = formatJson(code)
    return code;
  };
  
  Blockly.JavaScript['do_homing'] = function(block) {
    // TODO: Assemble JavaScript into code variable.
    var obj = {
        name : "do_homing"
    };
    return JSON.stringify(obj);
  };
  
  Blockly.JavaScript['pre_program'] = function(block) {
    var statements_statements = Blockly.JavaScript.statementToCode(block, 'statements');
    var res = formatJson(statements_statements);
    res = statementsToJson(res);

    var obj = {
        name        :   "pre_program" ,
        statements  :   JSON.parse(res)
    }

    return JSON.stringify(obj);
  };
  
  Blockly.JavaScript['main_program'] = function(block) {
    var statements_statements = Blockly.JavaScript.statementToCode(block, 'statements');
    var loopForever = block.getFieldValue('loop_forever');
    var res = formatJson(statements_statements);
    res = statementsToJson(res);
    
    var obj = {
        name        :   "main_program" ,
        loopForever :   loopForever,         
        statements  :   JSON.parse(res)
    };

    return JSON.stringify(obj);
  };
  
  Blockly.JavaScript['post_program'] = function(block) {
    var statements_statements = Blockly.JavaScript.statementToCode(block, 'statements');
    var res = formatJson(statements_statements);
    res = statementsToJson(res);
    
    var obj = {
        name        :   "post_program" ,
        statements  :   JSON.parse(res)
    };

    return JSON.stringify(obj);
  };
  
  