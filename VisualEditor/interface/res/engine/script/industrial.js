var Industrial = {};

function init() {
    var blocklyDiv = document.getElementById('blocklyDiv');
    var demoWorkspace = Blockly.inject(blocklyDiv,
        {
            grid:
            {
                spacing: 15,
                length: 1.5,
                colour: '#ccc',
                snap: true
            },
            media: 'media/',
            toolbox: document.getElementById('toolbox'),
            zoom:
            {
                controls: true,
                wheel: true,
                startScale: 1.0,
                maxScale: 3,
                minScale: 0.3,
                scaleSpeed: 1.1
            },
            trashcan: true,
            scrollbars: true
        });
    Blockly.Xml.domToWorkspace(document.getElementById('static-blocks') , demoWorkspace);
    var preProgramBlock = demoWorkspace.getBlockById('pre-prog');
    var mainProgramBlock = demoWorkspace.getBlockById('main-prog');
    var postProgramBlock = demoWorkspace.getBlockById('post-prog');

    preProgramBlock.moveBy(50 , 120);
    mainProgramBlock.moveBy( 220, 120);
    postProgramBlock.moveBy( 400 , 120);

    Industrial.workspace = demoWorkspace;
};

function generate() {
    var code = Blockly.JavaScript.workspaceToCode(Industrial.workspace);
    var obj = {
        name  : "step3" ,
        blocks : JSON.parse(statementsToJson(formatJson(code)))
    }

    return JSON.stringify(obj);
}

function getXml(){
    var dom = Blockly.Xml.workspaceToDom(Industrial.workspace);
    var xml = Blockly.Xml.domToText(dom);

    return xml;
}

function bindXml(text){
    //First clear workspace
    Industrial.workspace.clear();
    //Xml to dom
    var dom = Blockly.Xml.textToDom(text);
    //Dom to workspace
    Blockly.Xml.domToWorkspace(dom , Industrial.workspace);
}

function clearWorkspace(){
    Industrial.workspace.clear();
}