#include "blocklyenginefiles.h"
#include "platform.h"
#include <QDir>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>

void BlocklyEngineFiles::registerQmlType(){
    qmlRegisterType<BlocklyEngineFiles>("ScriptEditor" , 1 , 0 , "BlocklyEngineFiles");
}

BlocklyEngineFiles::BlocklyEngineFiles(QQuickItem* parent) : QQuickItem(parent)
{
    ensureEngineFiles();
}

//Getter
QString BlocklyEngineFiles::indexHtmlPath(){
    return m_htmlPath;
}

//Setter
void BlocklyEngineFiles::setIndexHtmlPath(QString val){
    m_htmlPath = val;
    emit indexHtmlPathChanged();
}

void BlocklyEngineFiles::ensureEngineFiles(){

    //If desktop no need to check,  just read from qrc
    if(Platform().isDesktop()){
        setIndexHtmlPath("qrc:/res/engine/index.html");
        return;
    }

#ifdef QT_DEBUG
    syncEngineSources();
#endif

    //From here written for mobile
    QDir baseDir;

    //If engine dir exists
    if(baseDir.exists("engine")){
        //Checking version
        int version = readVersion();
        if(version == -1){
            syncEngineSources();
        }
    } else {
        syncEngineSources();
    }

    //Sets index html path
    QString indexHtmlPath = QString("file://%0")
                        .arg(baseDir.absolutePath().append("/engine/index.html"));
    qDebug() << "htmlPath => " << indexHtmlPath;
    setIndexHtmlPath(indexHtmlPath);
}

int BlocklyEngineFiles::readVersion(){
    QDir d;
    QString fPath = d.filePath("engine/engine.json");
    QFile   f(fPath);

    //If doesn't exists return -1 , which means could't found file
    if(!f.exists())
        return -1;

    //It couldn't open so sync files again
    if(!f.open(QIODevice::ReadOnly))
        return -1;
    //Reads json data
    QJsonDocument doc = QJsonDocument::fromJson(f.readAll());
    //Close file
    f.close();
    //Converst to json object
    QJsonObject obj = doc.object();
    //Reads version from json
    int version = obj[ENGINE_VERSION_LABEL].toInt();

    return version;
}

void BlocklyEngineFiles::syncEngineSources(){
    qDebug() << "Sync sources";
    QDir baseDir;

    //If engines exists but need to be sync, then removing all files
    if(baseDir.exists("engine")){
        //Creates dir for engine
        QDir oldEngineDir(baseDir.filePath("engine"));

        //Removes old engine files
        oldEngineDir.removeRecursively();
    }

    //Creates engine dir
    baseDir.mkdir("engine");
    //Change dir to sources
    baseDir.cd("engine");

    //Copy engine files
    copyDirectoryRecursively(":/res/engine" , baseDir.absolutePath() , true);
}

bool BlocklyEngineFiles::copyDirectoryRecursively(QString root, QString target, bool overwrite){
    QDir    rootDir(root);
    QDir    targetDir(target);

    if(!targetDir.exists())
        targetDir.mkpath(target);
    foreach (auto info, rootDir.entryInfoList(QStringList() << "*" , QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Files)) {
        qDebug() << info.absolutePath();
        QString     relativeToRoot = rootDir.relativeFilePath(info.filePath());
        QString     targetPath = QDir::cleanPath(targetDir.filePath(relativeToRoot));
        if(info.isDir()){
            copyDirectoryRecursively(info.filePath() , targetPath , overwrite);
        } else {
            if(overwrite && QFile(targetPath).exists()){
                QFile(targetPath).remove();
            }
            QFile::copy(info.filePath() , targetPath);
        }
    }

    return true;
}
