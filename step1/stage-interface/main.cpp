#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "serial/serialport.h"
#include "serial/bluetoothport.h"
#include "serial/serialcomm.h"
#include "controller/stagecontroller.h"
#include "platform.h"

int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    SerialPort::registerQmlType();
    BluetoothPort::registerQmlType();
    SerialComm::registerQmlType();
    StageController::registerQmlType();
    Platform::registerQmlType();


    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
