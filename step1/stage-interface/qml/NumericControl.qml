import QtQuick 2.0
import RLib 1.0
import "../shared_qml/component"
import "../qml"

Item {

    readonly property int multiplier        :   grblSettings.homingCycleEnabled ? -1 : 1
    property string         enteredNumber   :   ""
    Component.onCompleted: forceActiveFocus()

    FontLoader  {
        id          :   numericFont
        source      :   "/res/font/digital-7.ttf"
    }

    function go(x){
        var pos = stage.position
        var movement = (x * multiplier) - pos
        stage.move(movement , settings.feedRate)
    }

    function rebindPos(){
        stagePosition.text = Qt.binding(function(){
            return  ((stage.position * multiplier) / settings.unitMultiplier).toFixed(settings.decimals)
        });
    }

    Timer   {
        id          :   toPosTimer
        interval    :   2000
        onTriggered :   {
            rebindPos()
        }
    }

    Row {
        x                       :   Responsive.getH(20)
        y                       :   Responsive.getV(20)
        spacing                 :   Responsive.getH(40)

        Column{
            spacing         :   Responsive.getV(20)

            BackgroundPanel {
                id                  :   posDisplay
                width               :   Responsive.getH(650)
                height              :   Responsive.getV(700)
                opacity             :   0.9

                Text {
                    id                  :   stagePosition
                    text                :   {
                        return ((stage.position * multiplier) / settings.unitMultiplier).toFixed(settings.decimals)
                    }
                    font.family         :   numericFont.name
                    font.pixelSize      :   Responsive.getV(500)
                    anchors.fill        :   parent
                    anchors.margins     :   height * 0.1
                    color               :   "white"
                    verticalAlignment   :   Text.AlignVCenter
                    horizontalAlignment :   Text.AlignHCenter
                    fontSizeMode        :   Text.Fit
                }

                color           :   {
                    if(stage.comm.state !== SerialComm.Connected)
                        return "#191919"

                    if(stage.machineState === StageProxy.Idle)
                        return "#191919"
                    else if(stage.machineState === StageProxy.Run)
                        return "#1E90FF"
                    else if(stage.machineState === StageProxy.Alarm)
                        return "#ff1a1a"
                    else if(stage.machineState === StageProxy.Jog)
                        return "#1E90FF"
                    else if(stage.machineState === StageProxy.Hold)
                        return "#ffff66"
                    else if(stage.machineState === StageProxy.Unknown)
                        return "white"
                }
            }
        }

        Row {
            spacing             :   Responsive.getH(20)
            Column{

                Grid {
                    id          :   numericGrid
                    rows        :   4
                    columns     :   3
                    spacing     :   Responsive.getV(10)
                    Repeater{
                        model   :   9

                        RButton {
                            id  :   numberButton
                            width   :   Responsive.getH(300)
                            height  :   Responsive.getV(250)
                            text    :   index + 1
                            font.pixelSize  :   Responsive.getV(180)
                            font.family     :   numericFont.name
                            onClicked       :   {
                                enteredNumber = enteredNumber.concat(numberButton.text)
                                stagePosition.text = enteredNumber
                                toPosTimer.restart()
                            }
                            background.opacity  :   0.9
                        }
                    }
                }

                Row{
                    spacing     :   Responsive.getV(10)

                    RButton{
                        id                  :   clearButton
                        width               :   Responsive.getH(300)
                        height              :   Responsive.getV(250)
                        text                :   "Temizle"
                        font.pixelSize      :   Responsive.getV(50)
                        onClicked           :   {
                            enteredNumber = ""
                            stagePosition.text = enteredNumber
                        }
                        background.opacity  :   0.9
                    }

                    RButton{
                        width       :   Responsive.getH(300)
                        height      :   Responsive.getV(250)
                        text        :   "0"
                        font.family :   numericFont.name
                        font.pixelSize  :   Responsive.getV(180)
                        onClicked   :   {
                            enteredNumber = enteredNumber.concat("0")
                            stagePosition.text = enteredNumber
                            toPosTimer.restart()
                        }
                        background.opacity  :   0.9
                    }

                    RButton{
                        width       :   Responsive.getH(300)
                        height      :   Responsive.getV(250)
                        text        :   "."
                        font.pixelSize  :   Responsive.getV(180)
                        onClicked   :   {
                            if(enteredNumber.length == 0)
                                return
                            if(enteredNumber.indexOf(".") > 0){
                                return
                            } else {
                                enteredNumber += "."
                            }

                            stagePosition.text = enteredNumber

                            toPosTimer.restart()
                        }
                        background.opacity  :   0.9
                    }
                }
            }

            RButton {
                id                  :   moveButton
                height              :   Responsive.getV(1020)
                width               :   Responsive.getH(250)
                text                :   "Git"
                font.pixelSize      :   Responsive.getV(120)
                background.opacity  :   0.9
                onClicked           :   {
                    if(stage.machineState !== StageProxy.Idle)
                        return

                    var val = parseFloat(enteredNumber)
                    go(val * settings.unitMultiplier)
                    enteredNumber = ""
                    rebindPos()
                }
            }
        }
    }

    Keys.onPressed: {
        //If backspace pressed
        if(event.key === Qt.Key_Backspace){
            console.log("Back Pressed")
            enteredNumber = enteredNumber.slice(0 , enteredNumber.length - 1)
            stagePosition.text = enteredNumber;
        }
    }
    Keys.onDigit0Pressed: {
        enteredNumber = enteredNumber.concat("0")
        stagePosition.text = enteredNumber
        toPosTimer.restart()
    }
    Keys.onDigit1Pressed: {
        enteredNumber = enteredNumber.concat("1")
        stagePosition.text = enteredNumber
        toPosTimer.restart()
    }
    Keys.onDigit2Pressed: {
        enteredNumber = enteredNumber.concat("2")
        stagePosition.text = enteredNumber
        toPosTimer.restart()
    }
    Keys.onDigit3Pressed: {
        enteredNumber = enteredNumber.concat("3")
        stagePosition.text = enteredNumber
        toPosTimer.restart()
    }
    Keys.onDigit4Pressed: {
        enteredNumber = enteredNumber.concat("4")
        stagePosition.text = enteredNumber
        toPosTimer.restart()
    }
    Keys.onDigit5Pressed: {
        enteredNumber = enteredNumber.concat("5")
        stagePosition.text = enteredNumber
        toPosTimer.restart()
    }
    Keys.onDigit6Pressed: {
        enteredNumber = enteredNumber.concat("6")
        stagePosition.text = enteredNumber
        toPosTimer.restart()
    }
    Keys.onDigit7Pressed: {
        enteredNumber = enteredNumber.concat("7")
        stagePosition.text = enteredNumber
        toPosTimer.restart()
    }
    Keys.onDigit8Pressed: {
        enteredNumber = enteredNumber.concat("8")
        stagePosition.text = enteredNumber
        toPosTimer.restart()
    }
    Keys.onDigit9Pressed: {
        enteredNumber = enteredNumber.concat("9")
        stagePosition.text = enteredNumber
        toPosTimer.restart()
    }
    Keys.onReturnPressed: {
        var val = parseFloat(enteredNumber)
        go(val * settings.unitMultiplier)
        enteredNumber = ""
        rebindPos()
    }
}
