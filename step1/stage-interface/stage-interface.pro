QT += quick serialport bluetooth
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

sharedQml.commands = cp -rf $$absolute_path($$PWD/../../Lib/qml/*) $$PWD/shared_qml

QMAKE_EXTRA_TARGETS += sharedQml
PRE_TARGETDEPS += sharedQml

SOURCES += main.cpp

RESOURCES += qml.qrc \
    res.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    main.qml \
    qml/NumericControl.qml

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../Lib/release/ -lLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../Lib/debug/ -lLib
else:unix: LIBS += -L$$OUT_PWD/../../Lib/ -lLib

INCLUDEPATH += $$PWD/../../Lib
DEPENDPATH += $$PWD/../../Lib

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../step1lib/release/ -lstep1lib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../step1lib/debug/ -lstep1lib
else:unix: LIBS += -L$$OUT_PWD/../step1lib/ -lstep1lib

INCLUDEPATH += $$PWD/../step1lib
DEPENDPATH += $$PWD/../step1lib
