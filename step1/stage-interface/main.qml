import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Window 2.3
import step1 1.0
import RLib 1.0
import "qml"
import "shared_qml"
import "shared_qml/component"

ApplicationWindow {

    readonly property SerialComm    port    :   Platform.isMobile ? bt : serial

    id          :   mainWindow
    visible     :   true
//    width       :   640
//    height      :   480
    visibility  :   Window.FullScreen
    title       :   qsTr("OzMach Step-1")

    Component.onCompleted: {
//        Responsive.viewHeight= height
//        Responsive.viewWidth= width
    }

    SerialPort  {
        id              :   serial
        debugEnabled    :   true
        baud            :   SerialComm.B115200
    }

    BluetoothPort   {
        id              :   bt
        debugEnabled    :   true
    }


    StageController {
        id:   controller
        serial: port
    }


    RBackground {
        alignTo :   mainWindow
        sourceHd:   "/res/img/background.jpg"
        source2k:   "/res/img/background.jpg"
        source4k:   "/res/img/background.jpg"
    }

    SwipeView{
        id:swipeView
        anchors.fill:   parent
        currentIndex:   pageIndicator.currentIndex

        NumericControl  {

        }

        Connection  {
            comm : mainWindow.port
        }
    }

    footer : Item   {
        height : 40
        width : mainWindow.width

        Item{
            anchors.fill    :   swipeInteractiveButton
            visible         :   !swipeInteractiveButton.checked
            Rectangle{
                anchors.fill    :   parent
                color           :   "red"
                opacity         :   0.7
            }
            Text {
                id      :   name
                text    :   qsTr("Kilitli")
                anchors.centerIn    :   parent
                color   :   "white"
                font.family :   "Linux Libertine"
                font.pixelSize  :   23
            }
        }

        RIconButton {
            id          :   swipeInteractiveButton
            checkable   :   true
            checked     :   true
            height      :   parent.height
            width       :   Responsive.getH(300)
            z           :   3
        }

        SwipeView {
            id          :   footerSwipe
            currentIndex:   swipeView.currentIndex
            z           :   2
            interactive :   swipeInteractiveButton.checked
            Repeater{
                model   :   swipeView.count
                Item { }
            }
            background  :   Item    {   }
            anchors.fill    :   parent
        }

        PageIndicator   {
            id              :   pageIndicator
            count           :   swipeView.count
            currentIndex    :   swipeView.currentIndex
            anchors.centerIn:   parent
            z               :   5
            delegate        :   Rectangle   {
                implicitWidth   :   20
                implicitHeight  :   20
                radius          :   width / 2
                color           :   "green"
                opacity         :   index == pageIndicator.currentIndex ? 0.95 : pressed ? 0.7 : 0.45

                MouseArea {
                    anchors.fill    :   parent
                    onClicked       :   swipeView.setCurrentIndex(index)
                }
            }
        }
    }

}
