pragma Singleton
import QtQuick 2.0
import QtQuick.Window 2.2

Item    {

    property real viewHeight : 1080
    property real viewWidth : 1920
    readonly property real vFactor : (Screen.height - 40) / viewHeight
    readonly property real hFactor : Screen.width / viewWidth

    function getV(y){
        return vFactor * y
    }

    function getH(x){
        return hFactor * x
    }
}
