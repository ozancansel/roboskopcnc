#include "setpositioncommand.h"

SetPositionCommand::SetPositionCommand(QObject* parent)
    :
      SetCommand(parent) ,
      m_pos(0)
{   }

QString SetPositionCommand::generate(){
    return QString("%0%1 %2\n")
            .arg(SetCommand::generate())
            .arg("--pos")
            .arg(position());
}

void SetPositionCommand::setPosition(int pos){
    m_pos = pos;
}

int SetPositionCommand::position(){
    return m_pos;
}
