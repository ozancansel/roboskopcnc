#include "referencecommand.h"

ReferenceCommand::ReferenceCommand(QObject* parent)
    :
      Command("reference" , parent)
{

}

QString ReferenceCommand::generate(){
    return QString("%0\n")
            .arg(name());
}
