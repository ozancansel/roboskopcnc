#ifndef RETRIEVESETTINGSCOMMAND_H
#define RETRIEVESETTINGSCOMMAND_H

#include "command.h"

class RetrieveSettingsCommand : public Command
{
public:
    RetrieveSettingsCommand(QObject* parent = nullptr);
    QString generate() override;
};

#endif // RETRIEVESETTINGSCOMMAND_H
