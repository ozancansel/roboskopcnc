#include "setreferenceforwardspeedcmd.h"
#include "exception/valuenotpositiveexception.h"

SetReferenceForwardSpeedCmd::SetReferenceForwardSpeedCmd(QObject* parent)
    :
      SetCommand(parent)
{   }

void SetReferenceForwardSpeedCmd::setForwardSpeed(int speed){
    if(speed <= 0)
        ValueNotPositiveException("forwardSpeed" , "Speed value cannot be negative.").raise();

    m_speed = speed;
}

int SetReferenceForwardSpeedCmd::forwardSpeed(){
    return m_speed;
}

QString SetReferenceForwardSpeedCmd::generate(){
    return QString("%0%1 %2\n")
            .arg(SetCommand::generate())
            .arg("--refForwardS")
            .arg(forwardSpeed());
}
