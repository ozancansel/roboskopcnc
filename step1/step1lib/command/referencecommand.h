#ifndef REFERENCECOMMAND_H
#define REFERENCECOMMAND_H

#include "command.h"

class ReferenceCommand : public Command
{

public:

    ReferenceCommand(QObject* parent = nullptr);
    QString generate() override;

};

#endif // REFERENCECOMMAND_H
