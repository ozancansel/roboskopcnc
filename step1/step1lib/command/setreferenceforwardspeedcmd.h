#ifndef SETREFERENCEFORWARDSPEEDCMD_H
#define SETREFERENCEFORWARDSPEEDCMD_H

#include "setcommand.h"

class SetReferenceForwardSpeedCmd : public SetCommand
{

public:

    SetReferenceForwardSpeedCmd(QObject* parent = nullptr);
    void setForwardSpeed(int speed);
    int forwardSpeed();
    QString generate() override;

private:

    int m_speed;

};

#endif // SETREFERENCEFORWARDSPEEDCMD_H
