#include "checkcommand.h"

CheckCommand::CheckCommand(QObject * parent)
    :
      Command("check" , parent)
{   }

QString CheckCommand::generate(){
    return QString("%0\n")
            .arg(name());
}
