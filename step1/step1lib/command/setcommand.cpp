#include "setcommand.h"

SetCommand::SetCommand(QObject* parent)
    :
      Command("set" , parent)
{   }

QString SetCommand::generate(){
    return QString("%0 ")
            .arg(name());
}
