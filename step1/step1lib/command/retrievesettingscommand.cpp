#include "retrievesettingscommand.h"

RetrieveSettingsCommand::RetrieveSettingsCommand(QObject* parent)
    :
      Command("settings",parent)
{   }

QString RetrieveSettingsCommand::generate(){
    return QString("%0 --all\n")
            .arg(name());
}
