#include "movecmd.h"
#include "step1cmdoptions.h"
#include "exception/valuenotpositiveexception.h"

MoveCmd::MoveCmd(QObject* parent)
    :
      Command("move" , parent) ,
      m_position(0) ,
      m_speed(0)
{   }

void MoveCmd::setPosition(int position){
    m_position = position;
}

void MoveCmd::setSpeed(int speed){
    //If value is negative
    if(speed <= 0){
        //Throw exception
        ValueNotPositiveException("speed" , "speed cannot be negative or zero").raise();
    }

    m_speed = speed;
}

int MoveCmd::position(){
    return m_position;
}

int MoveCmd::speed(){
    return m_speed;
}

QString MoveCmd::generate(){
    QString cmd = QString("%0 %1%2 %3\n")
            .arg(name())
            .arg(options() & Step1CmdOptions::SYNC ? "--sync " : "")
            .arg(position())
            .arg(speed());

    return cmd;
}
