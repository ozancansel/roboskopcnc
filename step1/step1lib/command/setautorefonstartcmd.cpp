#include "setautorefonstartcmd.h"

SetAutoRefOnStartCmd::SetAutoRefOnStartCmd(QObject* parent)
    :
      SetCommand(parent) ,
      m_autoRefOnStart(false)
{

}

void SetAutoRefOnStartCmd::setAutoRefOnStart(bool enabled){
    m_autoRefOnStart = enabled;
}

bool SetAutoRefOnStartCmd::autoRefOnStart(){
    return m_autoRefOnStart;
}

QString SetAutoRefOnStartCmd::generate(){
    return QString("%0%1 %2\n")
            .arg(SetCommand::generate())
            .arg("--refOnStart")
            .arg(m_autoRefOnStart ? 1 : 0);
}
