#ifndef SETPOSITIONCOMMAND_H
#define SETPOSITIONCOMMAND_H

#include "setcommand.h"

class SetPositionCommand : public SetCommand
{
public:
    SetPositionCommand(QObject* parent = nullptr);
    QString generate() override;
    void setPosition(int pos);
    int position();

private:

    int m_pos;
};

#endif // SETPOSITIONCOMMAND_H
