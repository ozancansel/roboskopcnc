#ifndef SETCOMMAND_H
#define SETCOMMAND_H

#include "command.h"

class SetCommand : public Command
{
public:
    SetCommand(QObject* parent = nullptr);
    virtual QString generate();
};

#endif // SETCOMMAND_H
