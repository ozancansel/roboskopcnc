#ifndef SETREFERENCEBACKWARDSPEEDCMD_H
#define SETREFERENCEBACKWARDSPEEDCMD_H

#include "setcommand.h"

class SetReferenceBackwardSpeedCmd : public SetCommand
{

public:

    SetReferenceBackwardSpeedCmd(QObject* parent = nullptr);
    void setBackwardSpeed(int speed);
    int backwardSpeed();
    QString generate() override;

private:

    int m_speed;

};

#endif // SETREFERENCEBACKWARDSPEEDCMD_H
