#ifndef SETPULLOFFDISTANCECMD_H
#define SETPULLOFFDISTANCECMD_H

#include "setcommand.h"

class SetPullOffDistanceCmd : public SetCommand
{

public:

    SetPullOffDistanceCmd(QObject* parent = nullptr);
    void setPullOffDistance(int distance);
    int pullOffDistance();
    QString generate() override;

private:

    int m_distance;

};

#endif // SETPULLOFFDISTANCECMD_H
