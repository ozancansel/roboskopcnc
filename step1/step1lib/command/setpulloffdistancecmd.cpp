#include "setpulloffdistancecmd.h"
#include "exception/valuenotpositiveexception.h"

SetPullOffDistanceCmd::SetPullOffDistanceCmd(QObject* parent)
    :
      SetCommand(parent) ,
      m_distance(0)
{

}

void SetPullOffDistanceCmd::setPullOffDistance(int distance){
    if(distance <= 0)
        ValueNotPositiveException("pullOffDistance" , "PullOffDistance value should be positive").raise();

    m_distance = distance;
}

int SetPullOffDistanceCmd::pullOffDistance(){
    return m_distance;
}

QString SetPullOffDistanceCmd::generate(){
    return QString("%0%1 %2\n")
            .arg(SetCommand::generate())
            .arg("--refBackwardM")
            .arg(pullOffDistance());
}
