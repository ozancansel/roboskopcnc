#ifndef SETMICROSTEPPINGCOMMAND_H
#define SETMICROSTEPPINGCOMMAND_H

#include "setcommand.h"

class SetMicrostepCommand : public SetCommand
{
public:

    SetMicrostepCommand(QObject* parent = nullptr);
    void setMicrostep(int microstep);
    int microstep();
    QString generate() override;

private:

    int m_microstep;

};

#endif // SETMICROSTEPPINGCOMMAND_H
