#ifndef HAVEOPTIONS_H
#define HAVEOPTIONS_H


class HaveOptions
{

public:

    HaveOptions();
    void setOptions(int opts);
    int options();

private:

    int     m_options;

};

#endif // HAVEOPTIONS_H
