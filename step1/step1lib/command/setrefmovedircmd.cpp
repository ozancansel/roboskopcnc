#include "setrefmovedircmd.h"

SetRefMoveDirCmd::SetRefMoveDirCmd(QObject* parent)
    :
      SetCommand(parent)
{   }

void SetRefMoveDirCmd::setRefMoveDir(bool direction){
    m_direction = direction;
}

bool SetRefMoveDirCmd::refMoveDirection(){
    return m_direction;
}

QString SetRefMoveDirCmd::generate(){
    return QString("%0%1 %2\n")
            .arg(SetCommand::generate())
            .arg("--refMoveDir")
            .arg(m_direction ? 1 : -1);
}
