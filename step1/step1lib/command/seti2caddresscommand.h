#ifndef SETI2CADDRESSCOMMAND_H
#define SETI2CADDRESSCOMMAND_H

#include "setcommand.h"

class SetI2cAddressCommand : public SetCommand
{
public:

    SetI2cAddressCommand(QObject* parent = nullptr);
    void setAddress(int addr);
    int address();
    QString generate() override;

private:

    int m_addr;

};

#endif // SETI2CADDRESSCOMMAND_H
