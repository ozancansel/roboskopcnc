#ifndef SETAUTOREFONSTARTCMD_H
#define SETAUTOREFONSTARTCMD_H

#include "setcommand.h"

class SetAutoRefOnStartCmd : public SetCommand
{

public:

    SetAutoRefOnStartCmd(QObject* parent = nullptr);
    void setAutoRefOnStart(bool enabled);
    bool autoRefOnStart();
    QString generate() override;

private:

    bool m_autoRefOnStart;


};

#endif // SETAUTOREFONSTARTCMD_H
