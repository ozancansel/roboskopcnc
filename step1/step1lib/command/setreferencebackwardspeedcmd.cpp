#include "setreferencebackwardspeedcmd.h"
#include "exception/valuenotpositiveexception.h"

SetReferenceBackwardSpeedCmd::SetReferenceBackwardSpeedCmd(QObject* parent)
    :
      SetCommand(parent),
      m_speed(0)
{   }

void SetReferenceBackwardSpeedCmd::setBackwardSpeed(int speed){
    if(speed <= 0)
        ValueNotPositiveException("backwardSpeed" , "speed value should be positive").raise();

    m_speed = speed;
}

int SetReferenceBackwardSpeedCmd::backwardSpeed(){
    return m_speed;
}

QString SetReferenceBackwardSpeedCmd::generate(){
    return QString("%0%1 %2\n")
            .arg(SetCommand::generate())
            .arg("--refBackwardS")
            .arg(backwardSpeed());
}
