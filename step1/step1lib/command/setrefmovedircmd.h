#ifndef SETREFMOVEDIRCMD_H
#define SETREFMOVEDIRCMD_H

#include "setcommand.h"

class SetRefMoveDirCmd : public SetCommand
{

public:

    SetRefMoveDirCmd(QObject* parent = nullptr);
    void setRefMoveDir(bool direction);
    bool refMoveDirection();
    QString generate() override;

private:

    bool m_direction;

};

#endif // SETREFMOVEDIRCMD_H
