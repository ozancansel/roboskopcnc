#include "setaccelcommand.h"
#include "exception/valuenotpositiveexception.h"

SetAccelCommand::SetAccelCommand(QObject* parent)
    :
      SetCommand(parent)
{

}

void SetAccelCommand::setAccel(int accel){
    //If acceleration is non positive
    if(accel <= 0)
        //Throws error
        ValueNotPositiveException("accel" , "acceleration could not be zero or negative").raise();

    m_accel = accel;
}

int SetAccelCommand::accel(){
    return m_accel;
}

QString SetAccelCommand::generate(){
    return QString("%0%1 %2\n")
            .arg(SetCommand::generate())
            .arg("--accel")
            .arg(accel());
}
