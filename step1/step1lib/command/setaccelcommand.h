#ifndef SETACCELCOMMAND_H
#define SETACCELCOMMAND_H

#include "setcommand.h"

class SetAccelCommand : public SetCommand
{
public:

    SetAccelCommand(QObject* parent = nullptr);
    QString generate() override;
    void setAccel(int accel);
    int accel();

private:

    int m_accel;

};

#endif // SETACCELCOMMAND_H
