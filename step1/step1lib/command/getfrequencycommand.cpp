#include "getfrequencycommand.h"

GetFrequencyCommand::GetFrequencyCommand(QObject* parent)
    :
      Command("frequency" , parent)
{   }

QString GetFrequencyCommand::generate(){
    return QString("%0\n")
            .arg(name());
}
