#ifndef STEP1CMDOPTIONS_H
#define STEP1CMDOPTIONS_H

#include <QQuickItem>

#define OPTS_SOFT (1 << 0)
#define OPTS_HARD (1 << 1)
#define OPTS_SYNC (1 << 2)

class Step1CmdOptions : public QQuickItem
{

    Q_OBJECT
    Q_ENUMS(Options)

public:

    const static int SOFT;
    const static int HARD;
    const static int SYNC;

    enum Options { Soft = OPTS_SOFT , Hard = OPTS_HARD , Sync = OPTS_SYNC };

};

#endif // STEP1CMDOPTIONS_H
