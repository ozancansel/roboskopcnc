#include "stopcmd.h"
#include "step1cmdoptions.h"

StopCmd::StopCmd(QObject* parent)
    :
        Command("stop" , parent)
{

}

QString StopCmd::generate(){

    QString cmd = QString("%0 %1%3")
                        .arg(name())
                        .arg(options() & Step1CmdOptions::SOFT ? "--soft " : "")
                        .arg(options() & Step1CmdOptions::SYNC ? "--sync " : "");

    cmd = cmd.trimmed();
    cmd.append("\n");

    return cmd;
}


