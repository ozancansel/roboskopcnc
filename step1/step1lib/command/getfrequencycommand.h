#ifndef GETFREQUENCYCOMMAND_H
#define GETFREQUENCYCOMMAND_H

#include "command.h"

class GetFrequencyCommand : public Command
{

public:

    GetFrequencyCommand(QObject* parent = nullptr);
    QString generate() override;

};

#endif // GETFREQUENCYCOMMAND_H
