#include "seti2caddresscommand.h"
#include "exception/valueoutboundexception.h"

SetI2cAddressCommand::SetI2cAddressCommand(QObject* parent)
    :
      SetCommand(parent)
{

}

void SetI2cAddressCommand::setAddress(int addr){
    if(addr < 0 || addr > 255)
        ValueOutboundException("address" , "i2c address should be between 0 and 255 values").raise();

    m_addr = addr;
}

int SetI2cAddressCommand::address(){
    return m_addr;
}

QString SetI2cAddressCommand::generate(){
    return QString("%0%1 %2\n")
            .arg(SetCommand::generate())
            .arg("--i2cAddr")
            .arg(address());
}
