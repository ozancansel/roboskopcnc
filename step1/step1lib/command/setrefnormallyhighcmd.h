#ifndef SETREFNORMALLYHIGHCMD_H
#define SETREFNORMALLYHIGHCMD_H

#include "setcommand.h"

class SetRefNormallyHighCmd : public SetCommand
{

public:

    SetRefNormallyHighCmd(QObject* parent = nullptr);
    void setRefNormallyHigh(bool enabled);
    bool refNormallyHigh();
    QString generate() override;

private:

    bool m_enabled;

};

#endif // SETREFNORMALLYHIGHCMD_H
