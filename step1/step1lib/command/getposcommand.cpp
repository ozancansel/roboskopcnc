#include "getposcommand.h"

GetPosCommand::GetPosCommand(QObject* parent)
    :
      Command("pos" , parent)
{   }

QString GetPosCommand::generate(){
    return QString("%0\n")
            .arg(name());
}
