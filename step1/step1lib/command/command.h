#ifndef COMMAND_H
#define COMMAND_H

#include <QObject>


class Command : public QObject
{

public:

    Command(QObject* parent = nullptr);
    Command(QString name , QObject* parent = nullptr);
    virtual QString generate() = 0;
    QString name();
    void setName(QString cmd);

private:

    QString m_cmdName;

};

#endif // COMMAND_H
