#include "setafterreferenceposcmd.h"

SetAfterReferencePosCmd::SetAfterReferencePosCmd(QObject* parent)
    :
      SetCommand(parent),
      m_afterPos(0)
{

}

void SetAfterReferencePosCmd::setAfterPos(int pos){
    m_afterPos = pos;
}

int SetAfterReferencePosCmd::afterPos(){
    return m_afterPos;
}

QString SetAfterReferencePosCmd::generate(){
    return QString("%0%1 %2\n")
            .arg(SetCommand::generate())
            .arg("--refAfterPos")
            .arg(afterPos());
}
