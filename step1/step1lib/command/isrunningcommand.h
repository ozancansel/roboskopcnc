#ifndef ISRUNNINGCOMMAND_H
#define ISRUNNINGCOMMAND_H

#include "command.h"

class IsRunningCommand : public Command
{
public:
    IsRunningCommand(QObject* parent = nullptr);
    QString generate() override;
};

#endif // ISRUNNINGCOMMAND_H
