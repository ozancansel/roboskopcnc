#ifndef CHECKCOMMAND_H
#define CHECKCOMMAND_H

#include "command.h"

class CheckCommand : public Command
{

public:

    CheckCommand(QObject* parent = nullptr);
    QString generate() override;

};

#endif // CHECKCOMMAND_H
