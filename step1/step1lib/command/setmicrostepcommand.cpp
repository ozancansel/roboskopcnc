#include "setmicrostepcommand.h"
#include "exception/valueoutboundexception.h"

SetMicrostepCommand::SetMicrostepCommand(QObject* parent)
    :
      SetCommand(parent) ,
      m_microstep(0)
{

}

void SetMicrostepCommand::setMicrostep(int microstep){
    if(microstep < 0 || microstep > 4)
        ValueOutboundException("microstep" , "microstep should between '0' and '4' values").raise();

    m_microstep = microstep;
}

int SetMicrostepCommand::microstep(){
    return m_microstep;
}

QString SetMicrostepCommand::generate(){
    return QString("%0%1 %2\n")
            .arg(SetCommand::generate())
            .arg("--microstepping")
            .arg(microstep());
}
