#ifndef SETAFTERREFERENCEPOSCMD_H
#define SETAFTERREFERENCEPOSCMD_H

#include "setcommand.h"

class SetAfterReferencePosCmd : public SetCommand
{

public:

    SetAfterReferencePosCmd(QObject* parent = nullptr);
    void setAfterPos(int pos);
    int afterPos();
    QString generate() override;

private:

    int m_afterPos;

};

#endif // SETAFTERREFERENCEPOSCMD_H
