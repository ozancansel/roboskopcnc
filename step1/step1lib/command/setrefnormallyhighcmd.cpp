#include "setrefnormallyhighcmd.h"

SetRefNormallyHighCmd::SetRefNormallyHighCmd(QObject* parent)
    :
      SetCommand(parent) ,
      m_enabled(false)
{   }

void SetRefNormallyHighCmd::setRefNormallyHigh(bool enabled){
    m_enabled = enabled;
}

bool SetRefNormallyHighCmd::refNormallyHigh(){
    return m_enabled;
}

QString SetRefNormallyHighCmd::generate(){
    return QString("%0%1 %2\n")
            .arg(SetCommand::generate())
            .arg("--refNormallyH")
            .arg(m_enabled ? 1 : 0);
}
