#include "command.h"

Command::Command(QObject *parent)
    :
      QObject(parent)
{

}

Command::Command(QString cmdName , QObject* parent)
    :
      QObject(parent) ,
      m_cmdName(cmdName)
{   }

QString Command::name(){
    return m_cmdName;
}

void Command::setName(QString cmd){
    m_cmdName = cmd;
}
