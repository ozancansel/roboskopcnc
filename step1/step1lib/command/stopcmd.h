#ifndef STOPCMD_H
#define STOPCMD_H

#include "command.h"
#include "haveoptions.h"

class StopCmd : public Command , public HaveOptions
{

public:

    StopCmd(QObject* parent = nullptr);
    QString generate() override;

};

#endif // STOPCMD_H
