#include "isrunningcommand.h"

IsRunningCommand::IsRunningCommand(QObject* parent)
    :
      Command("running" , parent)
{   }

QString IsRunningCommand::generate(){
    return QString("%0\n")
            .arg(name());
}
