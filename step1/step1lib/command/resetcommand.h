#ifndef RESETCOMMAND_H
#define RESETCOMMAND_H

#include "command.h"
#include "haveoptions.h"

class ResetCommand : public Command , public HaveOptions
{

public:

    ResetCommand(QObject* parent = nullptr);
    QString generate() override;

};

#endif // RESETCOMMAND_H
