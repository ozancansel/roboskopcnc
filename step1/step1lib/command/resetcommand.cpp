#include "resetcommand.h"
#include "step1cmdoptions.h"

ResetCommand::ResetCommand(QObject* parent)
    :
      Command("reset" , parent)
{   }

QString ResetCommand::generate(){
    return QString("%0%1\n")
            .arg(name())
            .arg(options() & Step1CmdOptions::HARD ? " --hard" : "");
}
