#ifndef GETPOSCOMMAND_H
#define GETPOSCOMMAND_H

#include "command.h"

class GetPosCommand : public Command
{

public:

    GetPosCommand(QObject* parent= nullptr);

    QString generate() override;

};

#endif // GETPOSCOMMAND_H
