#ifndef MOVECMD_H
#define MOVECMD_H

#include "command.h"
#include "haveoptions.h"

class MoveCmd : public Command , public HaveOptions
{

public:

    MoveCmd(QObject* parent = nullptr);
    void setPosition(int position);
    void setSpeed(int speed);
    int position();
    int speed();
    virtual QString generate() override;

private:

    int m_position;
    int m_speed;

};

#endif // MOVECMD_H
