#include "stagecontroller.h"

void StageController::registerQmlType(){
    qmlRegisterType<StageController>("step1" , 1 , 0 , "StageController");
}

StageController::StageController(QQuickItem* parent)
    :
      Step1RawController(parent)
{

}

int StageController::move(int position, int speed, int opts){
    return Step1RawController::move(position * stepsPerMm() , speed * stepsPerMm() , opts);
}

int StageController::jog(int magnitude, int speed, int opts){
    return Step1RawController::jog(magnitude * stepsPerMm() , speed * stepsPerMm() , opts);
}

float StageController::posInMm(){
    return position() / stepsPerMm();
}

float StageController::stepsPerMm(){
    return m_stepsPerMm;
}

void StageController::setStepPerMm(float val){
    m_stepsPerMm = val;
}

float StageController::toMm(float steps){
    return steps / stepsPerMm();
}

float StageController::mmToSteps(float pos){
    return pos * stepsPerMm();
}
