#ifndef STAGECONTROLLER_H
#define STAGECONTROLLER_H

#include "step1rawcontroller.h"

class StageController : public Step1RawController
{

    Q_OBJECT

public:

    static void registerQmlType();
    StageController(QQuickItem* parent = nullptr);

public slots:

    virtual int move(int position, int speed, int opts) override;
    virtual int jog(int magnitude, int speed, int opts) override;
    virtual float posInMm();
    virtual float stepsPerMm();
    void setStepPerMm(float val);
    float toMm(float steps);
    float mmToSteps(float pos);

private:

    float m_stepsPerMm;

};

#endif // STAGECONTROLLER_H
