#ifndef STEP1RAWCONTROLLER_H
#define STEP1RAWCONTROLLER_H

#include <QQuickItem>
#include "serial/serialcomm.h"
#define DEFAULT_READLINE_TIMEOUT    100

class Step1RawController : public QQuickItem
{

    Q_OBJECT
    Q_ENUMS(ResponseCodes)
    Q_PROPERTY(int position READ position NOTIFY positionChanged)
    Q_PROPERTY(int frequency READ frequency NOTIFY frequencyChanged)
    Q_PROPERTY(bool running READ running NOTIFY runningChanged)
    Q_PROPERTY(SerialComm* serial READ serial WRITE setSerial NOTIFY serialChanged)
    Q_PROPERTY(bool handshaked READ handshaked NOTIFY handshakedChanged)

public:

    enum ResponseCodes { UnknownCode = -1 , GenericError = 0 , Success = 1 , PosNotSpecified = 2 , SpeedNotSpecified = 3 , ValueShouldBePositive = 4 ,
                       AccelNotSpecified = 5 , ValueNotSpecified = 6 , MicrostepOutboundary = 7 ,
                       InvalidI2cAddr = 8 , ReferencePullOffErr = 9 , InvalidValue = 10 , HandshakeFailure = 11 , ConnectionFailure = 12 };
    static void registerQmlType();
    Step1RawController(QQuickItem* parent = nullptr);
    virtual int position();
    virtual int frequency();
    virtual void send(QString str);
    virtual QString readLine(int timeout = -1);
    SerialComm* serial();
    virtual bool handshaked();
    void setSerial(SerialComm* serial);
    bool running();
    bool isError(QString line);
    bool isOk(QString line);
    ResponseCodes extractErrorCode(QString line);

public slots:

    virtual int move(int updatePosition , int speed , int opts = 0);
    virtual int jog(int magnitude , int speed , int opts = 0);
    int stop(int opts = 0);
    int reference();
    int updatePosition();
    int reset(int options = 0);
    int updateFrequency();
    int updateRunningFlag();
    virtual int handshake();
    void connectionStateChanged();


signals:

    void positionChanged();
    void frequencyChanged();
    void runningChanged();
    void serialChanged();
    void handshakedChanged();

private:

    int m_lastPos;
    int m_frequency;
    bool m_running;
    bool m_handshaked;
    SerialComm* m_serial;

};

#endif // STEP1RAWCONTROLLER_H
