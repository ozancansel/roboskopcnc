#include "step1rawcontroller.h"
#include "command/movecmd.h"
#include "command/jogcommand.h"
#include "command/checkcommand.h"
#include "command/stopcmd.h"
#include "command/referencecommand.h"
#include "command/getposcommand.h"
#include "command/resetcommand.h"
#include "command/getfrequencycommand.h"
#include "command/isrunningcommand.h"
#include "exception/valuenotpositiveexception.h"
#include <QStringRef>

void Step1RawController::registerQmlType(){
    qmlRegisterType<Step1RawController>("step1" , 1 , 0 , "Step1RawController");
}

Step1RawController::Step1RawController(QQuickItem* parent)
    :
      QQuickItem(parent) ,
      m_lastPos(0) ,
      m_frequency(0) ,
      m_running(false) ,
      m_handshaked(false) ,
      m_serial(nullptr)
{

}

int Step1RawController::handshake(){

    //If serial is not set
    if(m_serial == nullptr)
        return HandshakeFailure;

    if(m_serial->getState() != SerialComm::Connected)
        return HandshakeFailure;

    CheckCommand cmd;

    send(cmd.generate());

    QString res = readLine(DEFAULT_READLINE_TIMEOUT);

    //If ok returned
    if(isOk(res))
        return Success;
    else //Anything else assuming as an error
        return HandshakeFailure;
}

int Step1RawController::move(int position, int speed, int opts){
    if(!handshaked()){
        if(handshake() != Success){
            return HandshakeFailure;
        }
    }

    MoveCmd cmd;

    cmd.setPosition(position);
    cmd.setOptions(opts);

    try {
        cmd.setSpeed(speed);
    } catch(ValueNotPositiveException &ex){
        return ValueShouldBePositive;
    }

    send(cmd.generate());

    QString res = readLine(DEFAULT_READLINE_TIMEOUT);

    if(res.isEmpty())
        return GenericError;

    if(isOk(res)){
        return Success;
    } else if(isError(res)){
        return extractErrorCode(res);
    } else {
        return GenericError;
    }
}

int Step1RawController::jog(int magnitude, int speed, int opts){
    JogCommand cmd;

    //Set speed
    try{
        cmd.setSpeed(speed);
    } catch(const ValueNotPositiveException&){
        //Generated exception catching and returns the message before sending anything to step1 card
        return ValueShouldBePositive;
    }


    //Set position
    cmd.setPosition(magnitude);
    //Apply options to cmd
    cmd.setOptions(opts);

    send(cmd.generate());

    QString res = readLine(DEFAULT_READLINE_TIMEOUT);

    //If nothing is returned or timedout
    if(res.isEmpty())
        return GenericError;

    //If ok(;) received
    if(isOk(res))
        return Success;
    else if(isError(res))//If (ERR:dd) received
        return extractErrorCode(res);
    else
        //If something received but it doesn't sense
        return GenericError;
}

int Step1RawController::stop(int opts){
    if(!handshaked()){
        //If handshake is not successful
        if(handshake() != Success)
            return HandshakeFailure; //Return error code
    }

    //If the code is reached here, it means handshaked with step1 hardware
    StopCmd cmd;

    //Set options
    cmd.setOptions(opts);

    //Send generated command
    send(cmd.generate());

    //Wait for response
    QString res = readLine(DEFAULT_READLINE_TIMEOUT);

    if(res.isEmpty())
        return GenericError;

    if(isOk(res))   //If it is ok response
        return Success;
    else if(isError(res))   //If it is error response
        return extractErrorCode(res);
    else    //If it is something difference from error or ok message
        return GenericError;
}

int Step1RawController::reference(){
    //If wasnt handshaked
    if(!handshaked()){
        //Try to handshake
        if(handshake() != Success) //If could not be handshaked
            return HandshakeFailure; //Return handshake error
    }

    //Handshaked for now
    ReferenceCommand cmd;

    //Send command to step1
    send(cmd.generate());

    //Wait for response
    QString res = readLine(-1);

    //If nothing responded
    if(res.isEmpty())
        return GenericError;

    //If response is success
    if(isOk(res))
        return Success; //Return success code
    else if(isError(res))
        return extractErrorCode(res); //Extract error code and return it directly, this command can generate REFERENCE_PULL_OFF error
    else
        return GenericError; //If anything else than ok or error is responded then return generic error
}

int Step1RawController::updatePosition(){
    //If wasnt handshaked
    if(!handshaked()){
        //Try to handshake
        if(handshake() != Success)//If it hasn't succeeded
            return HandshakeFailure; //Return Errorr
    }

    //Create corresponding command object
    GetPosCommand cmd;

    //Generate command and send it to step1 hardware
    send(cmd.generate());

    //Wait for response
    QString res = readLine(DEFAULT_READLINE_TIMEOUT);

    //Check for error
    if(isError(res)) //If has an error
        return extractErrorCode(res); //Return extracted error code

    bool converted = false;//Create a flag to check whether the response can be converted to integer.
    int pos = res.toInt(&converted); //Try to convert response to int

    //If it is converted
    if(converted){
        m_lastPos = pos; //Update last position
        emit positionChanged(); //Notify listeners
        return Success; //return success message
    }
    else //If it could not be converted
        return GenericError; //Return generic error
}

int Step1RawController::reset(int options){
    //If serial wasn't set
    if(m_serial == nullptr)
        return ConnectionFailure; //Return connection failure error message

    //If serial is set but not connected
    if(m_serial->getState() != SerialComm::Connected)
        return ConnectionFailure; //Return connection failure error message

    //Create corresponding command object
    ResetCommand cmd;
    cmd.setOptions(options);

    //Generate command text and send it to step1 hardware
    send(cmd.generate());

    //Wait for response
    QString res = readLine(DEFAULT_READLINE_TIMEOUT);

    //If returned nothing
    if(res.isEmpty())
        return GenericError; //Return generic error

    //If result contains half of booted message
    //Notice: It is half because version can be changed but it doesn't matter
    if(res.contains("[Step-1 v"))
        return Success;
    else //If returned something but it doesn't make sense then should return Generic Error
        return GenericError;
}

int Step1RawController::updateFrequency(){

    //If wasnt handshaked
    if(!handshaked()){
        //Try to handshake
        if(handshake() != Success){
            //If could not be handshaked
            return HandshakeFailure;
        }
    }

    //if reached here, it has been handshaked
    //Create command object
    GetFrequencyCommand cmd;

    //Generate command string and send it to step1 hardware
    send(cmd.generate());

    //Wait for response
    QString res = readLine(DEFAULT_READLINE_TIMEOUT);

    //If nothing is returned
    if(res.isEmpty())
        return GenericError; //Return generic error
    //If error message is returned
    if(isError(res))
        return extractErrorCode(res); //Return extracted error code

    bool converted = false; //Create a flag to check whether it is converted
    int freq = res.toInt(&converted);

    //If converted successfully
    if(converted){
        m_frequency = freq; //Update frequency
        emit frequencyChanged(); //Notify listeners
        return Success; //Return success code
    } else //If could not be converteed
        return GenericError; //Return generic error code
}

int Step1RawController::position(){
    return m_lastPos;
}

int Step1RawController::frequency(){
    return m_frequency;
}

SerialComm* Step1RawController::serial(){
    return m_serial;
}

bool Step1RawController::handshaked(){
    return m_handshaked;
}

void Step1RawController::setSerial(SerialComm *serial){
    m_serial = serial;
}

void Step1RawController::send(QString str){

}

QString Step1RawController::readLine(int timeout){
    return "";
}

bool Step1RawController::isOk(QString line){
    return line.trimmed() == ";";
}

bool Step1RawController::isError(QString line){
    return line.trimmed().contains("ERR:");
}

Step1RawController::ResponseCodes Step1RawController::extractErrorCode(QString line){
    int startIdx = line.indexOf(":");
    if(startIdx < 0)
        return UnknownCode;
    QStringRef ref(&line , startIdx + 1, line.length() - startIdx - 1);
    return (ResponseCodes)ref.toInt();
}

void Step1RawController::connectionStateChanged(){
    if(m_serial->getState() == SerialComm::Disconnected){
        m_handshaked = false;
        emit handshakedChanged();
    }
}

int Step1RawController::updateRunningFlag(){
    //If wasnt handshaked
    if(!handshaked()){
        //Try to handshake
        if(handshake() != Success) //If it failed
            return HandshakeFailure; //Return Handshake Failure error
    }

    //Handshaked if it is reached here

    //Generate command object
    IsRunningCommand cmd;

    //Generate command text and send it to step1 hardware
    send(cmd.generate());

    //Wait for response
    QString response = readLine(DEFAULT_READLINE_TIMEOUT);

    //If response is yes
    if(response == "yes"){
        m_running = true; //Update runnning flag
        emit runningChanged(); //Notify listeners
        return Success; //Return success message
    } else if(response == "no"){ //If response is no
        m_running = false; //Update running flag
        emit runningChanged(); //Notify listeners
        return Success; //Return success message
    } else { //If response could not be interpereted
        return GenericError; //Returns error message
    }
}

bool Step1RawController::running(){
    return m_running;
}
