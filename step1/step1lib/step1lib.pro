#-------------------------------------------------
#
# Project created by QtCreator 2018-03-19T17:04:23
#
#-------------------------------------------------

QT       -= gui
QT       += quick

TARGET = step1lib
TEMPLATE = lib

DEFINES += STEP1LIB_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        step1lib.cpp \
    exception/descriptedexception.cpp \
    exception/valuenotpositiveexception.cpp \
    exception/valueoutboundexception.cpp \
    command/command.cpp \
    command/haveoptions.cpp \
    command/stopcmd.cpp \
    command/step1cmdoptions.cpp \
    command/movecmd.cpp \
    command/jogcommand.cpp \
    command/referencecommand.cpp \
    command/getposcommand.cpp \
    command/isrunningcommand.cpp \
    command/checkcommand.cpp \
    command/resetcommand.cpp \
    command/retrievesettingscommand.cpp \
    command/setcommand.cpp \
    command/setaccelcommand.cpp \
    command/setpositioncommand.cpp \
    command/setmicrostepcommand.cpp \
    command/seti2caddresscommand.cpp \
    command/setreferenceforwardspeedcmd.cpp \
    command/setreferencebackwardspeedcmd.cpp \
    command/setpulloffdistancecmd.cpp \
    command/setafterreferenceposcmd.cpp \
    command/setrefnormallyhighcmd.cpp \
    command/setautorefonstartcmd.cpp \
    command/setrefmovedircmd.cpp \
    controller/step1rawcontroller.cpp \
    command/getfrequencycommand.cpp \
    controller/stagecontroller.cpp

HEADERS += \
        step1lib.h \
        step1lib_global.h \ 
    exception/valuenotpositiveexception.h \
    exception/valueoutboundexception.h \
    exception/descriptedexception.h \
    command/command.h \
    command/haveoptions.h \
    command/stopcmd.h \
    command/step1cmdoptions.h \
    command/movecmd.h \
    command/jogcommand.h \
    command/referencecommand.h \
    command/getposcommand.h \
    command/isrunningcommand.h \
    command/checkcommand.h \
    command/resetcommand.h \
    command/retrievesettingscommand.h \
    command/setcommand.h \
    command/setaccelcommand.h \
    command/setpositioncommand.h \
    command/setmicrostepcommand.h \
    command/seti2caddresscommand.h \
    command/setreferenceforwardspeedcmd.h \
    command/setreferencebackwardspeedcmd.h \
    command/setpulloffdistancecmd.h \
    command/setafterreferenceposcmd.h \
    command/setrefnormallyhighcmd.h \
    command/setautorefonstartcmd.h \
    command/setrefmovedircmd.h \
    controller/step1rawcontroller.h \
    command/getfrequencycommand.h \
    controller/stagecontroller.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../Lib/release/ -lLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../Lib/debug/ -lLib
else:unix: LIBS += -L$$OUT_PWD/../../Lib/ -lLib

INCLUDEPATH += $$PWD/../../Lib
DEPENDPATH += $$PWD/../../Lib
