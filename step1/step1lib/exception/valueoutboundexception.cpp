#include "valueoutboundexception.h"

ValueOutboundException::ValueOutboundException(QString valueName, QString description)
    :
      DescriptedException(description) ,
      m_valueName(valueName)
{
    //Binded values
}

ValueOutboundException::ValueOutboundException(QString description)
    :
      DescriptedException(description)
{
    //Binded values
}

ValueOutboundException::ValueOutboundException() {

}

QString ValueOutboundException::valueName(){
    return m_valueName;
}

