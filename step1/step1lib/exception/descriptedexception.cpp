#include "descriptedexception.h"

DescriptedException::DescriptedException(QString description)
    :
      m_description(description)
{
    //Description binded member-wise
}

DescriptedException::DescriptedException()
    :
      m_description("Description not specified")
{
    //Binded with default text
}

QString DescriptedException::description(){
    return m_description;
}
