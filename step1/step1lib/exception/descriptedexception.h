#ifndef DESCRIPTEDEXCEPTION_H
#define DESCRIPTEDEXCEPTION_H

#include <QException>

class DescriptedException : public QException
{

public:

    DescriptedException(QString description);
    DescriptedException();
    QString description();

private:

    QString m_description;

};

#endif // DESCRIPTEDEXCEPTION_H
