#ifndef VALUEOUTBOUNDEXCEPTION_H
#define VALUEOUTBOUNDEXCEPTION_H

#include "descriptedexception.h"

class ValueOutboundException : public DescriptedException
{

public:
    ValueOutboundException(QString valueName , QString description);
    ValueOutboundException(QString description);
    ValueOutboundException();
    void raise() const { throw *this; }
    ValueOutboundException* clone() { return new ValueOutboundException(*this); }
    QString valueName();

private:

    QString m_valueName;

};

#endif // VALUEOUTBOUNDEXCEPTION_H
