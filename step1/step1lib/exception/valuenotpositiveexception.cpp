#include "valuenotpositiveexception.h"

ValueNotPositiveException::ValueNotPositiveException(QString valueName , QString description)
    :
      DescriptedException(description) ,
      m_value(valueName)
{
    //Binded values
}

ValueNotPositiveException::ValueNotPositiveException(QString description)
    :
      DescriptedException(description)
{
    //Binded values
}

ValueNotPositiveException::ValueNotPositiveException(){ }

QString ValueNotPositiveException::valueName(){
    return m_value;
}
