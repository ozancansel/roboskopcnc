#ifndef VALUENOTPOSITIVEEXCEPTION_H
#define VALUENOTPOSITIVEEXCEPTION_H

#include "descriptedexception.h"

class ValueNotPositiveException : public DescriptedException
{

public:

    ValueNotPositiveException(QString valueName , QString description);
    ValueNotPositiveException(QString description);
    ValueNotPositiveException();
    void raise() const { throw *this; }
    ValueNotPositiveException* clone() { return new ValueNotPositiveException(*this); }
    QString valueName();

private:

    QString m_value;

};

#endif // VALUENOTPOSITIVEEXCEPTION_H
