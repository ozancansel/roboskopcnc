#include <QtTest/QtTest>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <string.h>
#include "command/commandtest.h"
#include "command/haveoptionstest.h"
#include "command/stopcmdtest.h"
#include "command/movecmdtest.h"
#include "command/jogcommandtest.h"
#include "command/referencecommandtest.h"
#include "command/getposcmdtest.h"
#include "command/isrunningcommandtest.h"
#include "command/checkcommandtest.h"
#include "command/resetcommandtest.h"
#include "command/retrievesettingscommandtest.h"
#include "command/setcommandtest.h"
#include "command/setaccelcommandtest.h"
#include "command/setpositioncommandtest.h"
#include "command/setmicrostepcommandtest.h"
#include "command/seti2caddresscommandtest.h"
#include "command/setreferenceforwardspeedcmdtest.h"
#include "command/setreferencebackwardspeedcmdtest.h"
#include "command/setpulloffdistancecmdtest.h"
#include "command/setafterreferenceposcmdtest.h"
#include "command/setrefnormallyhighcmdtest.h"
#include "command/setautorefonstartcmdtest.h"
#include "command/setrefmovedircmdtest.h"
#include "controller/step1rawcontrollertest.h"
#include "controller/stagecontrollertest.h"

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
