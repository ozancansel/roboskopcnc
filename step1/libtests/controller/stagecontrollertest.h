#ifndef STAGECONTROLLERTEST_H
#define STAGECONTROLLERTEST_H

#include "controller/stagecontroller.h"
#include <gtest/gtest.h>
#include <gmock/gmock.h>

class StageControllerTest : public ::testing::Test
{   };

class StageControllerStepsPerMmMocked : public StageController
{
public:
    MOCK_METHOD0(stepsPerMm , float());
};

class StageControllerPositionMocked : public StageController
{
public:
    MOCK_METHOD0(stepsPerMm , float());
    MOCK_METHOD0(position , int());
};

#endif // STAGECONTROLLERTEST_H
