#include "step1rawcontrollertest.h"
#include "controller/step1rawcontroller.h"
#include "serialcommmock.h"
#include "step1rawcontrollermocked.h"
#include "command/step1cmdoptions.h"
#include "serialcommmock.h"

using namespace ::testing;

//handshake() TEST START
TEST(Step1RawControllerTest , handshake_if_serial_comm_is_not_assigned_should_return_generic_message){
    QSharedPointer<Step1RawControllerMocked> controller(new Step1RawControllerMocked);

    EXPECT_EQ(controller.data()->handshake() , Step1RawControllerMocked::HandshakeFailure);
}

TEST(Step1RawControllerTest , handshake_if_connection_state_is_not_connected_should_return_generic_error){
    QSharedPointer<Step1RawControllerMocked> controller(new Step1RawControllerMocked);
    QSharedPointer<SerialCommMock> serial(new SerialCommMock());
    controller.data()->setSerial(serial.data());
    EXPECT_CALL(*(serial.data()) , getState())
            .WillRepeatedly(::testing::Return(SerialCommMock::Disconnected));

    EXPECT_EQ(controller.data()->handshake() , Step1RawControllerMocked::HandshakeFailure);
}

TEST(Step1RawControllerTest , handshake_if_hardware_returned_ok_should_return_success){
    QSharedPointer<Step1RawControllerMocked> controller(new Step1RawControllerMocked());
    QSharedPointer<SerialCommMock> serial(new SerialCommMock());
    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .Times(::testing::AtLeast(1))
            .WillRepeatedly(::testing::Return(";"));

    EXPECT_CALL(*(serial.data()) , getState())
            .WillRepeatedly(::testing::Return(SerialCommMock::Connected));

    controller.data()->setSerial(serial.data());

    EXPECT_EQ(controller.data()->handshake() , Step1RawControllerMocked::Success);
}

TEST(Step1RawControllerTest , handshake_if_hardware_returned_error_should_return_generic_error){
    QSharedPointer<Step1RawControllerMocked> controller(new Step1RawControllerMocked());
    QSharedPointer<SerialCommMock> serial(new SerialCommMock());
    controller.data()->setSerial(serial.data());
    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .Times(::testing::AtLeast(1))
            .WillRepeatedly(::testing::Return("ERR:1"));

    EXPECT_CALL(*(serial.data()) , getState())
            .WillRepeatedly(::testing::Return(SerialCommMock::Connected));

    EXPECT_EQ(controller.data()->handshake() , Step1RawControllerMocked::HandshakeFailure);
}

TEST(Step1RawControllerTest , handshake_if_hardware_returned_nothing_should_return_generic_error){
    QSharedPointer<Step1RawControllerMocked> controller(new Step1RawControllerMocked());
    QSharedPointer<SerialCommMock> serial(new SerialCommMock());
    controller.data()->setSerial(serial.data());
    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .Times(::testing::AtLeast(1))
            .WillRepeatedly(::testing::Return(""));

    EXPECT_CALL(*(serial.data()) , getState())
            .WillRepeatedly(::testing::Return(SerialCommMock::Connected));

    EXPECT_EQ(controller.data()->handshake() , Step1RawControllerMocked::HandshakeFailure);
}

//handshake() TEST END

//isOk(QString) TEST START

TEST(Step1RawControllerTest , isOk_if_hardware_returned_ok_should_return_true) {
    Step1RawController controller;
    EXPECT_TRUE(controller.isOk(";"));
}

TEST(Step1RawControllerTest , isOk_if_hardware_returned_anything_else_than_ok_should_return_false) {
    Step1RawController controller;

    EXPECT_FALSE(controller.isOk("Err"));
}

//isOk(QString) TEST END

//isError(QString) TEST START

TEST(Step1RawControllerTest , isError_if_hardware_returned_error_should_return_true){
    Step1RawController controller;

    EXPECT_TRUE(controller.isError("ERR:9"));
}

TEST(Step1RawControllerTest , isError_if_hardware_returned_anything_else_than_error_should_return_false){
    Step1RawController controller;

    EXPECT_FALSE(controller.isError("Hello:"));
}

//isError(QString) TEST END

//extractErrorCode(QString) TEST START

TEST(Step1RawControllerTest , extractErrorCode_if_error_text_format_is_wrong_return_minus_one){
    Step1RawController controller;

    EXPECT_EQ(controller.extractErrorCode("wrong text") , -1);
}

TEST(Step1RawControllerTest , extractErrorCode_if_error_text_format_is_correct_then_should_return_error_code){
    Step1RawController controller;

    EXPECT_EQ(controller.extractErrorCode("ERR:7") , 7);
}

//extractErrorCode(QString) TEST END

//lastPosition() TEST START

TEST(Step1RawcontrollerTest , lastPosition_should_be_zero_initially){
    Step1RawController controller;

    EXPECT_EQ(controller.position() , 0);
}

//lastPosition() TEST END

//move(int , int , int) Test Start
TEST(Step1RawControllerTest , move_if_isnt_being_handshaked_should_try_handshake){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(::testing::Return(false));

    EXPECT_CALL(*(controller.data()) , handshake())
            .Times(1)
            .WillOnce(::testing::Return(Step1RawController::HandshakeFailure));

    controller.data()->move(5000 , 6000);
}

TEST(Step1RawControllerTest , move_if_handshake_tried_and_returned_handshake_failure_error_should_return_error_code){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(::testing::Return(false));

    EXPECT_CALL(*(controller.data()) , handshake())
            .Times(1)
            .WillRepeatedly(::testing::Return(Step1RawController::HandshakeFailure));

    EXPECT_EQ(controller.data()->move(5000 , 6000) , Step1RawController::HandshakeFailure);
}

TEST(Step1RawControllerTest , move_if_speed_is_negative_should_return_value_should_be_positive_error){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(::testing::Return(true));

    EXPECT_EQ(controller.data()->move(1000 , -15) , Step1RawController::ValueShouldBePositive);
}

TEST(Step1RawControllerTest , move_if_speed_is_zero_should_return_value_should_be_positive_error){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(::testing::Return(true));

    EXPECT_EQ(controller.data()->move(1000 , 0 ) , Step1RawController::ValueShouldBePositive);
}

TEST(Step1RawControllerTest , move_if_speed_is_positive_should_send_cmd_text){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(::testing::Return(true));
    EXPECT_CALL(*(controller.data()) , send(QString("move 450 400\n")))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return(";\n"));

    controller.data()->move(450 , 400);
}

TEST(Step1RawControllerTest , move_if_sync_opts_specified_should_apply){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(::testing::Return(true));
    EXPECT_CALL(*(controller.data()) , send(QString("move --sync 450 400\n")))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return(";\n"));

    controller.data()->move(450 , 400 , Step1CmdOptions::SYNC);
}

TEST(Step1RawControllerTest , move_if_hardware_return_ok_message_should_return_success_code){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(::testing::Return(true));
    EXPECT_CALL(*(controller.data()) , send(::testing::_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return(QString(";")));

    EXPECT_EQ(controller.data()->move(500 , 1000) , Step1RawController::Success);
}

TEST(Step1RawControllerTest , move_if_hardware_returned_position_not_specified_should_return_the_same_error_code){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(::testing::Return(true));
    EXPECT_CALL(*(controller.data()) , send(::testing::_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return("ERR:2"));
    int res = controller.data()->move(500 , 1000);
    EXPECT_EQ(res , Step1RawController::PosNotSpecified);
}

TEST(Step1RawControllerTest , move_if_hardware_returned_speed_not_specified_should_return_the_same_error_code){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(::testing::Return(true));
    EXPECT_CALL(*(controller.data()) , send(::testing::_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return("ERR:3"));
    EXPECT_EQ(controller.data()->move(500 , 1000) , Step1RawController::SpeedNotSpecified);
}

TEST(Step1RawControllerTest , move_if_hardware_returned_speed_not_positive_should_return_the_same_error_code){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(::testing::Return(true));
    EXPECT_CALL(*(controller.data()) , send(::testing::_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return("ERR:4"));

    EXPECT_EQ(controller.data()->move(500 , 1000) , Step1RawController::ValueShouldBePositive);
}

TEST(Step1RawControllerTest , move_if_readLine_timedout_should_return_generic_error_code){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(::testing::Return(true));
    EXPECT_CALL(*(controller.data()) , send(::testing::_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return(""));

    EXPECT_EQ(controller.data()->move(10000 , 12000) , Step1RawController::GenericError);
}

TEST(Step1RawControllerTest , move_if_step1_hardware_returned_unsensed_text_should_return_generic_error_code){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());
    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(::testing::Return(true));
    EXPECT_CALL(*(controller.data()) , send(::testing::_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return("blabla returned"));
    EXPECT_EQ(controller.data()->move(10000 , 12000) , Step1RawController::GenericError);
}


//move(int , int , int) Test end

//jog(int , int , int) Test Start

TEST(Step1RawControllerTest , jog_if_speed_is_specified_as_zero_should_return_value_should_be_positive_result){
    QSharedPointer<Step1RawControllerMocked> controller(new Step1RawControllerMocked());

    EXPECT_EQ(controller.data()->jog(-20000 , 0) , Step1RawController::ValueShouldBePositive);
}

TEST(Step1RawControllerTest , jog_if_speed_is_specified_as_negative_number_should_return_value_should_be_positive_result){
    QSharedPointer<Step1RawControllerMocked> controller(new Step1RawControllerMocked());
    EXPECT_EQ(controller.data()->jog(-20000 , -1) , Step1RawController::ValueShouldBePositive);
}

TEST(Step1RawControllerTest , jog_if_speed_specified_as_correct_should_send_text){
    QSharedPointer<Step1RawControllerMocked> controller(new Step1RawControllerMocked());

    EXPECT_CALL(*(controller.data()) , send(::testing::_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return(";"));

    EXPECT_NO_THROW(controller.data()->jog(15000 , 5000));
}

TEST(Step1RawControllerTest , jog_if_step1_hardware_returned_ok_message_should_return_success_code){
    QSharedPointer<Step1RawControllerMocked> controller(new Step1RawControllerMocked());

    EXPECT_CALL(*(controller.data()) , send(::testing::_))
            .Times(1);

    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return(";"));

    EXPECT_EQ(controller.data()->jog(15000 , 10000) , Step1RawController::Success);
}

TEST(Step1RawControllerTest , jog_if_step1_hardware_returned_position_is_not_specified_message_should_return_the_same_error_code){
    QSharedPointer<Step1RawControllerMocked> controller(new Step1RawControllerMocked());

    EXPECT_CALL(*(controller.data()) , send(::testing::_))
            .Times(1);

    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return("ERR:2"));

    EXPECT_EQ(controller.data()->jog(15000 , 10000) , Step1RawController::PosNotSpecified);
}

TEST(Step1RawControllerTest , jog_if_step1_hardware_returned_speed_not_specified_should_return_the_same_error_code){
    QSharedPointer<Step1RawControllerMocked> controller(new Step1RawControllerMocked());

    EXPECT_CALL(*(controller.data()) , send(::testing::_))
            .Times(1);

    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return("ERR:3"));

    EXPECT_EQ(controller.data()->jog(15000 , 10000) , Step1RawController::SpeedNotSpecified);
}

TEST(Step1RawControllerTest , jog_if_step1_hardware_returned_value_should_be_positive_then_should_return_the_same_error_code){
    QSharedPointer<Step1RawControllerMocked> controller(new Step1RawControllerMocked());

    EXPECT_CALL(*(controller.data()) , send(::testing::_))
            .Times(1);

    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return("ERR:4"));

    EXPECT_EQ(controller.data()->jog(15000 , 10000) , Step1RawController::ValueShouldBePositive);
}

TEST(Step1RawControllerTest , jog_if_readLine_timedout_should_return_generic_error_code){
    QSharedPointer<Step1RawControllerMocked> controller(new Step1RawControllerMocked());

    EXPECT_CALL(*(controller.data()) , send(::testing::_))
            .Times(1);

    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return(""));

    EXPECT_EQ(controller.data()->jog(10000 , 12000) , Step1RawController::GenericError);
}

TEST(Step1RawControllerTest , jog_if_step1_hardware_returned_unsensed_text_should_return_generic_error_code){
    QSharedPointer<Step1RawControllerMocked> controller(new Step1RawControllerMocked());

    EXPECT_CALL(*(controller.data()) , send(::testing::_))
            .Times(1);

    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return("blabla returned"));

    EXPECT_EQ(controller.data()->jog(10000 , 12000) , Step1RawController::GenericError);
}

//Jog(int , int , int) Test End

//stop(int) Test Start

TEST(Step1RawControllerTest , stop_if_didnt_handshake_should_handshake){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .Times(::testing::AtLeast(1))
            .WillRepeatedly(::testing::Return(false));

    EXPECT_CALL(*(controller.data()) , handshake())
            .Times(::testing::AtLeast(1));

    controller.data()->stop();
}

TEST(Step1RawControllerTest , stop_if_handshake_result_success_should_return_handshake_failure){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .Times(::testing::AtLeast(1))
            .WillRepeatedly(::testing::Return(false));

    EXPECT_CALL(*(controller.data()) , handshake())
            .Times(::testing::AtLeast(1))
            .WillRepeatedly(::testing::Return(Step1RawControllerHandshakeMocked::HandshakeFailure));

    EXPECT_EQ(controller.data()->stop() , Step1RawController::HandshakeFailure);
}

TEST(Step1RawControllerTest , stop_if_step1_hardware_returned_ok_message_should_return_success){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(Return(true));
    EXPECT_CALL(*(controller.data()) , send(::testing::_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return(";"));

    EXPECT_EQ(controller.data()->stop() , Step1RawController::Success);
}

TEST(Step1RawControllerTest , stop_if_readLine_timedout_should_return_generic_error_message){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(Return(true));
    EXPECT_CALL(*(controller.data()) , send(::testing::_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return(""));

    EXPECT_EQ(controller.data()->stop() , Step1RawController::GenericError);
}

TEST(Step1RawControllerTest , stop_if_step1_hardware_returned_error_message_should_return_error_code){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(Return(true));
    EXPECT_CALL(*(controller.data()) , send(::testing::_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(::testing::_))
            .WillOnce(::testing::Return("ERR:2")); //Step1 hardware never generates this error, but it is responded just for testing

    EXPECT_EQ(controller.data()->stop() , Step1RawController::PosNotSpecified);
}

//stop(int) Test End

//reference() Test Start
TEST(Step1RawControllerTest , reference_if_wasnt_handshaked_should_call_handshake){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(Return(false));

    EXPECT_CALL(*(controller.data()) , handshake())
            .Times(AtLeast(1));

    controller.data()->reference();
}

TEST(Step1RawControllerTest , reference_if_handshake_returned_handshake_failure_error_should_return_handshake_failure){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(Return(false));

    EXPECT_CALL(*(controller.data()) , handshake())
            .Times(AtLeast(1))
            .WillRepeatedly(Return(Step1RawController::HandshakeFailure));

    EXPECT_EQ(controller.data()->reference() , Step1RawController::HandshakeFailure);
}

TEST(Step1RawControllerTest , reference_if_step1_hardware_returned_ok_message_should_return_success){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(Return(true));

    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(AtLeast(1));

    EXPECT_CALL(*(controller.data()) , readLine(_))
            .Times(AtLeast(1))
            .WillOnce(Return(";")); //It never generates this error, but it is assumed for testing

    EXPECT_EQ(controller.data()->reference() , Step1RawController::Success);
}

TEST(Step1RawControllerTest , reference_if_step1_hardware_returned_nothing_should_return_generic_error){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(Return(true));

    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(AtLeast(1));

    EXPECT_CALL(*(controller.data()) , readLine(_))
            .Times(AtLeast(1))
            .WillOnce(Return("")); //It never generates this error, but it is assumed for testing

    EXPECT_EQ(controller.data()->reference() , Step1RawController::GenericError);
}

TEST(Step1RawControllerTest , reference_if_step1_hardware_returned_err_should_return_the_same_error_code){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(Return(true));

    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(AtLeast(1));

    EXPECT_CALL(*(controller.data()) , readLine(_))
            .Times(AtLeast(1))
            .WillOnce(Return("ERR:2")); //It never generates this error, but it is assumed for testing

    EXPECT_EQ(controller.data()->reference() , Step1RawController::PosNotSpecified);
}

//reference() TEST END

//position() TEST START

TEST(Step1RawControllerTest , updatePosition_if_wasnt_handshaked_should_try_to_handshake){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(Return(false));

    EXPECT_CALL(*(controller.data()) , handshake())
            .Times(AtLeast(1));

    controller.data()->updatePosition();
}

TEST(Step1RawControllerTest , updatePosition_if_handshake_returned_failure_should_return_handshake_failure_error){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(Return(false));

    EXPECT_CALL(*(controller.data()) , handshake())
            .Times(AtLeast(1))
            .WillOnce(Return(Step1RawController::HandshakeFailure));

    EXPECT_EQ(controller.data()->updatePosition() , Step1RawController::HandshakeFailure);
}

TEST(Step1RawControllerTest , updatePosition_if_step1_responded_with_position_should_update_the_last_position){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(Return(true));
    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(AtLeast(1));
    EXPECT_CALL(*(controller.data()) , readLine(_))
            .Times(AtLeast(1))
            .WillOnce(Return("1522"));

    EXPECT_EQ(controller.data()->updatePosition() , Step1RawController::Success);
    EXPECT_EQ(controller.data()->position() , 1522);
}

TEST(Step1RawControllerTest , updatePosition_if_step1_responded_with_error_should_return_error_code_directly){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(Return(true));
    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(AtLeast(1));
    EXPECT_CALL(*(controller.data()) , readLine(_))
            .Times(AtLeast(1))
            .WillOnce(Return("ERR:2"));

    EXPECT_EQ(controller.data()->updatePosition() , Step1RawController::PosNotSpecified);
}

TEST(Step1RawControllerTest , updatePosition_if_step1_returned_nothing_should_return_generic_error){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillRepeatedly(Return(true));
    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(AtLeast(1));
    EXPECT_CALL(*(controller.data()) , readLine(_))
            .Times(AtLeast(1))
            .WillOnce(Return(""));

    EXPECT_EQ(controller.data()->updatePosition() , Step1RawController::GenericError);
}

//position() TEST END

//reset(int) TEST START

TEST(Step1RawControllerTest , reset_if_there_is_serial_is_null_should_return_connection_failure_error){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());
    controller.data()->reset();
}

TEST(Step1RawControllerTest , reset_if_serial_state_is_not_connected_should_return_connection_failure_error){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());
    QSharedPointer<SerialCommMock> serial(new SerialCommMock());
    EXPECT_CALL(*(serial.data()) , getState())
            .WillOnce(Return(SerialCommMock::Disconnected));
    controller.data()->setSerial(serial.data());
    controller.data()->reset();
}

TEST(Step1RawControllerTest , reset_if_connection_established_should_send_text){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());
    QSharedPointer<SerialCommMock> serial(new SerialCommMock());

    EXPECT_CALL(*(serial.data()) , getState())
            .WillOnce(Return(SerialComm::Connected));
    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(AtLeast(1));
    EXPECT_CALL(*(controller.data()) , readLine(_))
            .Times(AtLeast(1))
            .WillRepeatedly(Return(""));

    controller.data()->setSerial(serial.data());
    controller.data()->reset();
}

TEST(Step1RawControllerTest , reset_if_step1_is_responded_with_booted_message_should_return_success){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());
    QSharedPointer<SerialCommMock> serial(new SerialCommMock());
    EXPECT_CALL(*(serial.data()) , getState())
            .WillOnce(Return(SerialComm::Connected));
    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(AtLeast(1));
    EXPECT_CALL(*(controller.data()) , readLine(DEFAULT_READLINE_TIMEOUT))
            .Times(1)
            .WillOnce(Return("[Step-1 v1.0]"));

    controller.data()->setSerial(serial.data());
    EXPECT_EQ(controller.data()->reset() , Step1RawController::Success);
}

TEST(Step1RawControllerTest , reset_if_step1_is_responded_with_nothing_should_return_success){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());
    QSharedPointer<SerialCommMock> serial(new SerialCommMock());
    EXPECT_CALL(*(serial.data()) , getState())
            .WillOnce(Return(SerialComm::Connected));
    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(AtLeast(1));
    EXPECT_CALL(*(controller.data()) , readLine(DEFAULT_READLINE_TIMEOUT))
            .Times(1)
            .WillOnce(Return(""));

    controller.data()->setSerial(serial.data());
    EXPECT_EQ(controller.data()->reset() , Step1RawController::GenericError);
}

TEST(Step1RawControllerTest , reset_if_step1_is_responded_something_strange_text_should_return_success){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());
    QSharedPointer<SerialCommMock> serial(new SerialCommMock());
    EXPECT_CALL(*(serial.data()) , getState())
            .WillOnce(Return(SerialComm::Connected));
    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(AtLeast(1));
    EXPECT_CALL(*(controller.data()) , readLine(DEFAULT_READLINE_TIMEOUT))
            .Times(1)
            .WillOnce(Return("qwe111qsdqqq"));

    controller.data()->setSerial(serial.data());
    EXPECT_EQ(controller.data()->reset() , Step1RawController::GenericError);
}

//reset(int) TEST END

//frequency() TEST START
TEST(Step1RawControllerTest , frequency_if_not_handshaked_and_handshake_returned_handshake_failure_should_return_same_error_code){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillOnce(Return(false));

    EXPECT_CALL(*(controller.data()) , handshake())
            .Times(1)
            .WillOnce(Return(Step1RawController::HandshakeFailure));

    EXPECT_EQ(controller.data()->updateFrequency() , Step1RawControllerHandshakeMocked::HandshakeFailure);
}

TEST(Step1RawControllerTest , frequency_if_step1_responded_with_a_number_should_return_success_message){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillOnce(Return(true));

    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(_))
            .WillOnce(Return("1234"));

    EXPECT_EQ(controller.data()->updateFrequency() , Step1RawController::Success);
}

TEST(Step1RawControllerTest , frequency_if_step1_responded_with_a_number_should_update_last_frequency){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillOnce(Return(true));

    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(_))
            .WillOnce(Return("1234"));

    controller.data()->updateFrequency();
    EXPECT_EQ(controller.data()->frequency() , 1234);
}

TEST(Step1RawControllerTest , frequency_if_step1_responded_with_nothing_should_return_generic_error){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillOnce(Return(true));
    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(DEFAULT_READLINE_TIMEOUT))
            .Times(1)
            .WillOnce(Return(""));

    EXPECT_EQ(controller.data()->updateFrequency() , Step1RawController::GenericError);
}

TEST(Step1RawControllerTest , frequency_if_step1_responde_with_error_message_should_return_same_error){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillOnce(Return(true));
    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(DEFAULT_READLINE_TIMEOUT))
            .Times(1)
            .WillOnce(Return("ERR:2")); //Step1 hardware never returns this error code but it is just for testing

    EXPECT_EQ(controller.data()->updateFrequency() , Step1RawController::PosNotSpecified);
}

//frequency() TEST END

//updateRunningFlag() TEST START

TEST(Step1RawControllerTest , updateRunningFlag_if_didnt_handshaked_should_try_handshake_and_return_generic_error_if_it_returned_error){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillOnce(Return(false));
    EXPECT_CALL(*(controller.data()) , handshake())
            .Times(AtLeast(1))
            .WillRepeatedly(Return(false));

    EXPECT_EQ(controller.data()->updateRunningFlag() , Step1RawController::HandshakeFailure);
}

TEST(Step1RawControllerTest , updateRunningFlag_if_step1_device_returned_yes_should_update_running_flag_and_return_success_message){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillOnce(Return(true));
    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(DEFAULT_READLINE_TIMEOUT))
            .WillOnce(Return("yes"));

    EXPECT_EQ(controller.data()->updateRunningFlag() , Step1RawController::Success);
    EXPECT_EQ(controller.data()->running() , true);
}

TEST(Step1RawControllerTest , updateRunningFlag_if_step1_device_retruned_no_should_update_running_flag_and_return_success_message){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillOnce(Return(true));
    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(DEFAULT_READLINE_TIMEOUT))
            .WillOnce(Return("no"));

    EXPECT_EQ(controller.data()->updateRunningFlag() , Step1RawController::Success);
    EXPECT_EQ(controller.data()->running() , false);
}

TEST(Step1RawControllerTest , updateRunningFlag_if_step1_device_returned_nothing_should_not_update_and_should_return_generic_error_message){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillOnce(Return(true));
    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(DEFAULT_READLINE_TIMEOUT))
            .WillOnce(Return(""));

    bool last = controller.data()->running();
    EXPECT_EQ(controller.data()->updateRunningFlag() , Step1RawController::GenericError);
    EXPECT_EQ(controller.data()->running() , last);
}

TEST(Step1RawControllerTest , updateRunningFlag_if_step1_device_returned_non_sense_text_should_not_update_running_flag_and_return_generic_error_message){
    QSharedPointer<Step1RawControllerHandshakeMocked> controller(new Step1RawControllerHandshakeMocked());

    EXPECT_CALL(*(controller.data()) , handshaked())
            .WillOnce(Return(true));
    EXPECT_CALL(*(controller.data()) , send(_))
            .Times(1);
    EXPECT_CALL(*(controller.data()) , readLine(DEFAULT_READLINE_TIMEOUT))
            .WillOnce(Return("qwerfqppq"));

    bool last = controller.data()->running();
    EXPECT_EQ(controller.data()->updateRunningFlag() , Step1RawController::GenericError);
    EXPECT_EQ(controller.data()->running() , last);
}

//updateRunningFlag() TEST END
