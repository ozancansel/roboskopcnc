#ifndef STEP1RAWCONTROLLERTEST_H
#define STEP1RAWCONTROLLERTEST_H

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include "step1rawcontrollermocked.h"

class Step1RawControllerTest : public ::testing::Test
{



};

class Step1RawControllerHandshakeMocked : public Step1RawControllerMocked{

  public:

    MOCK_METHOD0(handshake , int());
    MOCK_METHOD0(handshaked , bool());

};


#endif // STEP1RAWCONTROLLERTEST_H
