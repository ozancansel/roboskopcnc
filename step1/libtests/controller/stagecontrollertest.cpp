#include "stagecontrollertest.h"
#include "controller/stagecontroller.h"

//move(int , int , int) TEST START

using namespace ::testing;

TEST(StageControllerTest , toMm){
    QSharedPointer<StageControllerStepsPerMmMocked> controller(new StageControllerStepsPerMmMocked());

    EXPECT_CALL(*(controller.data()) , stepsPerMm())
            .WillRepeatedly(Return(100));

    EXPECT_EQ(controller.data()->toMm(100) , 1);
}

TEST(StageControllerTest , mmToSteps){
    QSharedPointer<StageControllerStepsPerMmMocked> controller(new StageControllerStepsPerMmMocked());

    EXPECT_CALL(*(controller.data()) , stepsPerMm())
            .WillRepeatedly(Return(100));

    EXPECT_EQ(controller.data()->mmToSteps(1) , 100);
}

TEST(StageControllerTest , posInMm){
    QSharedPointer<StageControllerPositionMocked> controller(new StageControllerPositionMocked());

    EXPECT_CALL(*(controller.data()) , stepsPerMm())
            .WillRepeatedly(Return(100));
    EXPECT_CALL(*(controller.data()) , position())
            .WillRepeatedly(Return(200));

    EXPECT_EQ(controller.data()->posInMm() , 2);
}

//move(int , int , int) TEST END
