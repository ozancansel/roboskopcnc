#ifndef STEP1RAWCONTROLLERMOCKED_H
#define STEP1RAWCONTROLLERMOCKED_H

#include <gmock/gmock.h>
#include "controller/step1rawcontroller.h"

class Step1RawControllerMocked : public Step1RawController
{

public:

    MOCK_METHOD1(readLine , QString(int));
    MOCK_METHOD1(send , void(QString));

};

#endif // STEP1RAWCONTROLLERMOCKED_H
