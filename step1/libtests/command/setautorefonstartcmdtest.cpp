#include "setautorefonstartcmdtest.h"
#include "command/setautorefonstartcmd.h"
TEST(SetAutoRefOnStartCmdTest , setAutoRefOnStart_setter_getter){
    SetAutoRefOnStartCmd cmd;
    cmd.setAutoRefOnStart(true);
    EXPECT_EQ(cmd.autoRefOnStart() , true);
}

TEST(SetAutoRefOnStartCmdTest , generate_if_enabled_is_set_to_true_should_generate_1){
    SetAutoRefOnStartCmd cmd;
    cmd.setAutoRefOnStart(true);
    EXPECT_EQ(cmd.generate() , "set --refOnStart 1\n");
}
TEST(SetAutoRefOnStartCmdTest , generate_if_enabled_is_set_to_false_should_generate_0){
    SetAutoRefOnStartCmd cmd;
    cmd.setAutoRefOnStart(false);
    EXPECT_EQ(cmd.generate() , "set --refOnStart 0\n");
}
