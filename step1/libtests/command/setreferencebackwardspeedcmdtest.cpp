#include "setreferencebackwardspeedcmdtest.h"
#include "command/setreferencebackwardspeedcmd.h"
#include "exception/valuenotpositiveexception.h"

TEST(SetReferenceBackwardSpeedCmdTest , if_backward_speed_is_set_to_negative_should_throw_value_not_positive_exception){
    SetReferenceBackwardSpeedCmd cmd;
    EXPECT_THROW(cmd.setBackwardSpeed(-15) , ValueNotPositiveException);
}
TEST(SetReferenceBackwardSpeedCmdTest , if_backward_speed_is_set_to_zero_should_throw_value_not_positive_exception){
    SetReferenceBackwardSpeedCmd cmd;
    EXPECT_THROW(cmd.setBackwardSpeed(0); , ValueNotPositiveException);
}
TEST(SetReferenceBackwardSpeedCmdTest , if_backward_speed_is_set_to_postiive_should_resume){
    SetReferenceBackwardSpeedCmd cmd;
    EXPECT_NO_THROW(cmd.setBackwardSpeed(15));
}
TEST(SetReferenceBackwardSpeedCmdTest , setBackwardSpeed_and_backwardSpeed){
    SetReferenceBackwardSpeedCmd cmd;
    cmd.setBackwardSpeed(15);
    EXPECT_EQ(cmd.backwardSpeed() , 15);
}
TEST(SetReferenceBackwardSpeedCmdTest , generate){
    SetReferenceBackwardSpeedCmd cmd;
    cmd.setBackwardSpeed(15);
    EXPECT_EQ(cmd.generate() , "set --refBackwardS 15\n");
}
