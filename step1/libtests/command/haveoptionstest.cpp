#include "haveoptionstest.h"
#include "command/haveoptions.h"

TEST(HaveOptionsTest , test_options){
    HaveOptions opts;
    opts.setOptions(15);
    EXPECT_EQ(opts.options() , 15);
}

TEST(HaveOptionsTest , test_options_should_be_zero_initially){
    HaveOptions opts;
    EXPECT_EQ(opts.options() , 0 );
}
