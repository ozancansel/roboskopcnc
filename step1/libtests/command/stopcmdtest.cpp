#include "stopcmdtest.h"
#include "command/stopcmd.h"
#include "command/step1cmdoptions.h"

TEST(StopCmdTest , cmdName){
    StopCmd cmd;
    EXPECT_EQ(cmd.name() , "stop");
}
TEST(StopCmdTest , without_any_options){
    StopCmd cmd;
    EXPECT_EQ(cmd.generate() , "stop\n");
}
TEST(StopCmdTest , with_soft_options){
    StopCmd cmd;
    cmd.setOptions(Step1CmdOptions::SOFT);
    EXPECT_EQ(cmd.generate() , "stop --soft\n");
}
TEST(StopCmdTest , with_soft_and_sync_options){
    StopCmd cmd;
    cmd.setOptions(Step1CmdOptions::SOFT | Step1CmdOptions::SYNC);
    EXPECT_EQ(cmd.generate() , "stop --soft --sync\n");
}
