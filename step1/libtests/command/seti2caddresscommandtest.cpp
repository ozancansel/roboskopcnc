#include "seti2caddresscommandtest.h"
#include "command/seti2caddresscommand.h"
#include "exception/valueoutboundexception.h"

TEST(SetI2cAddressCommandTest , if_i2c_addr_is_set_to_256_should_throw_value_outbound_exception){
    SetI2cAddressCommand cmd;
    EXPECT_THROW(cmd.setAddress(256) ,  ValueOutboundException);
}
TEST(SetI2cAddressCommandTest , if_i2c_addr_is_set_to_minus_one_should_throw_value_outbound_exception){
    SetI2cAddressCommand cmd;
    EXPECT_THROW(cmd.setAddress(-1) , ValueOutboundException);

}
TEST(SetI2cAddressCommandTest , if_i2c_addr_is_set_to_8_should_resume){
    SetI2cAddressCommand cmd;
    EXPECT_NO_THROW(cmd.setAddress(8));
}
TEST(SetI2cAddressCommandTest , setAddress_and_address){
    SetI2cAddressCommand cmd;
    cmd.setAddress(8);
    EXPECT_EQ(cmd.address() , 8);
}
TEST(SetI2cAddressCommandTest , generate){
    SetI2cAddressCommand cmd;
    cmd.setAddress(8);
    EXPECT_EQ(cmd.generate() , "set --i2cAddr 8\n");
}
