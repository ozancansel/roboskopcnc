#ifndef COMMANDMOCKED_H
#define COMMANDMOCKED_H

#include <gmock/gmock.h>
#include "command/command.h"

class CommandMocked : public Command
{

public:

    MOCK_METHOD0(generate , QString());

};

#endif // COMMANDMOCKED_H
