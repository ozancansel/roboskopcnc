#include "referencecommandtest.h"
#include "command/referencecommand.h"

TEST(ReferenceCommandTest , cmdName){
    ReferenceCommand cmd;
    EXPECT_EQ(cmd.name() , "reference");
}
TEST(ReferenceCommandTest , generate){
    ReferenceCommand cmd;
    EXPECT_EQ(cmd.generate() , "reference\n");
}
