#include "jogcommandtest.h"
#include "command/jogcommand.h"

TEST(JogCommandTest , cmdName){
    JogCommand cmd;

    EXPECT_EQ(cmd.name() , "jog");
}
