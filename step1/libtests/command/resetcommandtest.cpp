#include "resetcommandtest.h"
#include "command/resetcommand.h"
#include "command/step1cmdoptions.h"

TEST(ResetCommandTest , cmdName){
    ResetCommand cmd;
    EXPECT_EQ(cmd.name() , "reset");
}
TEST(ResetCommandTest , generate_if_there_is_no_option){
    ResetCommand cmd;
    EXPECT_EQ(cmd.generate() , "reset\n");
}
TEST(ResetCommandTest , generate_if_hard_option_is_specified){
    ResetCommand cmd;
    cmd.setOptions(Step1CmdOptions::HARD);
    EXPECT_EQ(cmd.generate() , "reset --hard\n");
}
