#include "setafterreferenceposcmdtest.h"
#include "command/setafterreferenceposcmd.h"

TEST(SetAfterReferencePosCmdTest , afterPos_getter_setter){
    SetAfterReferencePosCmd cmd;
    cmd.setAfterPos(15);
    EXPECT_EQ(cmd.afterPos() , 15);
}
TEST(SetAfterReferencePosCmdTest , generate){
    SetAfterReferencePosCmd cmd;
    cmd.setAfterPos(15);
    EXPECT_EQ(cmd.generate() , "set --refAfterPos 15\n");
}
