#include "movecmdtest.h"
#include "command/movecmd.h"
#include "command/step1cmdoptions.h"
#include "exception/valuenotpositiveexception.h"
#include <gmock/gmock.h>

TEST(MoveCmdTest , cmdName){
    MoveCmd cmd;
    EXPECT_EQ(cmd.name() , "move");
}
TEST(MoveCmdTest, setSpeed_if_value_is_valid_then_should_assign){
    MoveCmd cmd;
    cmd.setSpeed(15);
    EXPECT_EQ(cmd.speed() , 15);
}
TEST(MoveCmdTest , setSpeed_if_value_is_negative_then_should_throw_value_not_positive_exception){
    MoveCmd cmd;
    EXPECT_THROW(cmd.setSpeed(0) , ValueNotPositiveException);
}

TEST(MoveCmdTest , setPosition){
    MoveCmd cmd;
    cmd.setPosition(12);

    EXPECT_EQ(cmd.position() , 12);
}

TEST(MoveCmdTest , generate_without_any_options){
    MoveCmd cmd;

    cmd.setSpeed(1000);
    cmd.setPosition(1001);

    EXPECT_EQ(cmd.generate() , "move 1001 1000\n");
}
TEST(MoveCmdTest , generate_with_sync_options){
    MoveCmd cmd;

    cmd.setSpeed(1000);
    cmd.setPosition(1001);
    cmd.setOptions(Step1CmdOptions::SYNC);

    EXPECT_EQ(cmd.generate() , "move --sync 1001 1000\n");
}
