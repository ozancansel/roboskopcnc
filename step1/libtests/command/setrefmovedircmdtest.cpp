#include "setrefmovedircmdtest.h"
#include "command/setrefmovedircmd.h"

TEST(SetRefMoveDirCmdTest , refMoveDir_getter_setter){
    SetRefMoveDirCmd cmd;
    cmd.setRefMoveDir(true);
    EXPECT_EQ(cmd.refMoveDirection() , true);
}
TEST(SetRefMoveDirCmdTest , generate_if_direction_is_set_to_true_should_generate_1){
    SetRefMoveDirCmd cmd;
    cmd.setRefMoveDir(true);
    EXPECT_EQ(cmd.generate() , "set --refMoveDir 1\n");
}
TEST(SetRefMoveDirCmdTest , generate_if_direction_is_set_to_false_should_generate_minus_1){
    SetRefMoveDirCmd cmd;
    cmd.setRefMoveDir(false);
    EXPECT_EQ(cmd.generate() , "set --refMoveDir -1\n");
}
