#include "commandtest.h"
#include "commandmocked.h"

TEST(CommandTest , test_cmdName){
    CommandMocked cmd;

    QString cmdName = "testName";
    cmd.setName(cmdName);
    EXPECT_EQ(cmd.name() , cmdName);
}
