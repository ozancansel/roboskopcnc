#include "getposcmdtest.h"
#include "command/getposcommand.h"

TEST(GetPosCmdTest , test_cmdName){
    GetPosCommand cmd;

    EXPECT_EQ(cmd.name() , "pos");
}

TEST(GetPosCmdTest , test_generate){
    GetPosCommand cmd;

    EXPECT_EQ(cmd.generate() , "pos\n");
}
