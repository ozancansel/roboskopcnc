#include "setcommandtest.h"
#include "command/setcommand.h"

TEST(SetCommandTest , cmdName){
    SetCommand cmd;
    EXPECT_EQ(cmd.name() , "set");
}
TEST(SetCommandTest , generate){
    SetCommand cmd;
    EXPECT_EQ(cmd.generate() , "set ");
}
