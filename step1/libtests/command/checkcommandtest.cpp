#include "checkcommandtest.h"
#include "command/checkcommand.h"
#include <gtest/gtest.h>

TEST(CheckCommandTest , test_cmdName){
    CheckCommand cmd;

    EXPECT_EQ(cmd.name() , "check");
}

TEST(CheckCommandTest , test_generate){
    CheckCommand cmd;

    EXPECT_EQ(cmd.generate() , "check\n");
}
