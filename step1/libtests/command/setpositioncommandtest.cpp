#include "setpositioncommandtest.h"
#include "command/setpositioncommand.h"

TEST(SetPositionCommandTest , position_and_setPosition){
    SetPositionCommand cmd;
    cmd.setPosition(15);
    EXPECT_EQ(cmd.position() , 15);
}
TEST(SetPositionCommandTest , generate){
    SetPositionCommand cmd;
    cmd.setPosition(15);
    EXPECT_EQ(cmd.generate() , "set --pos 15\n");
}
