#include "getfrequencycommandtest.h"
#include "command/getfrequencycommand.h"

TEST(GetFrequencyCommandTest , name){
    GetFrequencyCommand cmd;

    EXPECT_EQ(cmd.name() , "frequency");
}

TEST(GetFrequencyCommandTest , generate){
    GetFrequencyCommand cmd;

    EXPECT_EQ(cmd.generate() , "frequency\n");
}
