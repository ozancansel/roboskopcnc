#include "isrunningcommandtest.h"
#include "command/isrunningcommand.h"

TEST(IsRunningCommandTest , test_cmdName){
    IsRunningCommand cmd;
    EXPECT_EQ(cmd.name() , "running");
}

TEST(IsRunningCommandTest , test_generate){
    IsRunningCommand cmd;
    EXPECT_EQ(cmd.generate() , "running\n");
}
