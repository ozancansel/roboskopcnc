#include "setmicrostepcommandtest.h"
#include "command/setmicrostepcommand.h"
#include "exception/valueoutboundexception.h"

TEST(SetMicrostepCommandTest , setMicrostep_if_value_is_minus_one_should_throw_value_outbound_exception){
    SetMicrostepCommand cmd;
    EXPECT_THROW(cmd.setMicrostep(-1) , ValueOutboundException);
}
TEST(SetMicrostepCommandTest , setMicrostep_if_value_is_five_should_throw_value_outbound_exception){
    SetMicrostepCommand cmd;
    EXPECT_THROW(cmd.setMicrostep(5); , ValueOutboundException);
}
TEST(SetMicrostepCommandTest , setMicrostep_if_value_is_2_should_resume){
    SetMicrostepCommand cmd;
    EXPECT_NO_THROW(cmd.setMicrostep(2));
}
TEST(SetMicrostepCommandTest , setMicrostep_and_microstep){
    SetMicrostepCommand cmd;
    cmd.setMicrostep(2);
    EXPECT_EQ(cmd.microstep() , 2);
}
TEST(SetMicrostepCommandTest , generate){
    SetMicrostepCommand cmd;
    cmd.setMicrostep(2);
    EXPECT_EQ(cmd.generate() , "set --microstepping 2\n");
}
