#include "retrievesettingscommandtest.h"
#include "command/retrievesettingscommand.h"

TEST(RetrieveSettingsCommandTest , cmdName){
    RetrieveSettingsCommand cmd;
    EXPECT_EQ(cmd.name() , "settings");
}
TEST(RetrieveSettingsCommandTest , generate){
    RetrieveSettingsCommand cmd;
    EXPECT_EQ(cmd.generate() , "settings --all\n");
}
