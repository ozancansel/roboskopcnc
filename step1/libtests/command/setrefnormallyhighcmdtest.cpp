#include "setrefnormallyhighcmdtest.h"
#include "command/setrefnormallyhighcmd.h"

TEST(SetRefNormallyHighCmdTest , refNormallyHigh_getter_setter){
    SetRefNormallyHighCmd cmd;
    cmd.setRefNormallyHigh(true);
    EXPECT_EQ(cmd.refNormallyHigh() , true);
}
TEST(SetRefNormallyHighCmdTest , test_generate_if_normally_high_is_set_true_should_generate_1){
    SetRefNormallyHighCmd cmd;
    cmd.setRefNormallyHigh(true);
    EXPECT_EQ(cmd.generate() , "set --refNormallyH 1\n");
}
TEST(SetRefNormallyHighCmdTest , generate_if_normally_high_is_set_to_false_should_generate_0){
    SetRefNormallyHighCmd cmd;
    cmd.setRefNormallyHigh(false);
    EXPECT_EQ(cmd.generate() , "set --refNormallyH 0\n");
}
