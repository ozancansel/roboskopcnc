#include "setpulloffdistancecmdtest.h"
#include "command/setpulloffdistancecmd.h"
#include "exception/valuenotpositiveexception.h"

TEST(SetPullOffDistanceCmdTest , if_pull_off_distance_is_set_to_negative_value_should_throw_value_not_positive_exception){
    SetPullOffDistanceCmd cmd;
    EXPECT_THROW(cmd.setPullOffDistance(-1); , ValueNotPositiveException);
}
TEST(SetPullOffDistanceCmdTest , if_pull_off_distance_is_set_to_zero_should_throw_value_not_positive_exception){
    SetPullOffDistanceCmd cmd;
    EXPECT_THROW(cmd.setPullOffDistance(0); , ValueNotPositiveException);
}
TEST(SetPullOffDistanceCmdTest , if_pull_off_distance_is_set_to_positive_value_should_resume){
    SetPullOffDistanceCmd cmd;
    EXPECT_NO_THROW(cmd.setPullOffDistance(15));
}
TEST(SetPullOffDistanceCmdTest , setPullOffDistance_and_pullOffDistance){
    SetPullOffDistanceCmd cmd;
    cmd.setPullOffDistance(15);
    EXPECT_EQ(cmd.pullOffDistance() , 15);
}
TEST(SetPullOffDistanceCmdTest , generate){
    SetPullOffDistanceCmd cmd;
    cmd.setPullOffDistance(15);
    EXPECT_EQ(cmd.generate() , "set --refBackwardM 15\n");
}
