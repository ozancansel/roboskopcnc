#include "setaccelcommandtest.h"
#include "command/setaccelcommand.h"
#include "exception/valuenotpositiveexception.h"

TEST(SetAccelCommandTest , setAccel_if_accel_is_negative_value_should_throw_value_not_positive_exception){
    SetAccelCommand cmd;
    EXPECT_THROW(cmd.setAccel(-12) , ValueNotPositiveException);
}
TEST(SetAccelCommandTest , setAccel_if_accel_is_zero_should_throw_value_not_positive_exception){
    SetAccelCommand cmd;
    EXPECT_THROW(cmd.setAccel(0); , ValueNotPositiveException);
}
TEST(SetAccelCommandTest , setAccel_if_accel_is_positive_should_resume){
    SetAccelCommand cmd;
    EXPECT_NO_THROW(cmd.setAccel(15));
}
TEST(SetAccelCommandTest , generate){
    SetAccelCommand cmd;
    cmd.setAccel(12);
    EXPECT_EQ(cmd.generate() , "set --accel 12\n");
}


