#include "setreferenceforwardspeedcmdtest.h"
#include "exception/valuenotpositiveexception.h"
#include "command/setreferenceforwardspeedcmd.h"

TEST(SetReferenceForwardSpeedCmdTest , if_speed_is_set_to_negative_should_throw_value_not_positive_exception){
    SetReferenceForwardSpeedCmd cmd;
    EXPECT_THROW(cmd.setForwardSpeed(-5); , ValueNotPositiveException);
}
TEST(SetReferenceForwardSpeedCmdTest , if_speed_is_set_to_zero_should_throw_value_not_positive_exception){
    SetReferenceForwardSpeedCmd cmd;
    EXPECT_THROW(cmd.setForwardSpeed(0); , ValueNotPositiveException);
}
TEST(SetReferenceForwardSpeedCmdTest , if_speed_is_postive_should_resume){
    SetReferenceForwardSpeedCmd cmd;
    EXPECT_NO_THROW(cmd.setForwardSpeed(15));
}
TEST(SetReferenceForwardSpeedCmdTest , setForwardSpeed_and_forwardSpeed){
    SetReferenceForwardSpeedCmd cmd;
    cmd.setForwardSpeed(15);
    EXPECT_EQ(cmd.forwardSpeed() , 15);
}
TEST(SetReferenceForwardSpeedCmdTest , generate){
    SetReferenceForwardSpeedCmd cmd;
    cmd.setForwardSpeed(15);
    EXPECT_EQ(cmd.generate() , "set --refForwardS 15\n");
}
