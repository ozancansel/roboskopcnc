#-------------------------------------------------
#
# Project created by QtCreator 2018-03-19T18:06:59
#
#-------------------------------------------------

QT       += testlib

QT       += gui quick

TARGET = tst_libteststest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    main.cpp \
    command/commandtest.cpp \
    command/commandmocked.cpp \
    command/haveoptionstest.cpp \
    command/stopcmdtest.cpp \
    command/movecmdtest.cpp \
    command/jogcommandtest.cpp \
    command/referencecommandtest.cpp \
    command/getposcmdtest.cpp \
    command/isrunningcommandtest.cpp \
    command/checkcommandtest.cpp \
    command/resetcommandtest.cpp \
    command/retrievesettingscommandtest.cpp \
    command/setcommandtest.cpp \
    command/setaccelcommandtest.cpp \
    command/setpositioncommandtest.cpp \
    command/setmicrostepcommandtest.cpp \
    command/seti2caddresscommandtest.cpp \
    command/setreferenceforwardspeedcmdtest.cpp \
    command/setreferencebackwardspeedcmdtest.cpp \
    command/setpulloffdistancecmdtest.cpp \
    command/setafterreferenceposcmdtest.cpp \
    command/setrefnormallyhighcmdtest.cpp \
    command/setautorefonstartcmdtest.cpp \
    command/setrefmovedircmdtest.cpp \
    controller/step1rawcontrollertest.cpp \
    command/getfrequencycommandtest.cpp \
    controller/stagecontrollertest.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"

LIBS += -lgmock -L/usr/lib/libgmock.a \
        -lgtest -L/usr/lib/libgtest.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../step1lib/release/ -lstep1lib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../step1lib/debug/ -lstep1lib
else:unix: LIBS += -L$$OUT_PWD/../step1lib/ -lstep1lib

INCLUDEPATH += $$PWD/../step1lib
DEPENDPATH += $$PWD/../step1lib

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../Lib/release/ -lLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../Lib/debug/ -lLib
else:unix: LIBS += -L$$OUT_PWD/../../Lib/ -lLib

INCLUDEPATH += $$PWD/../../Lib
DEPENDPATH += $$PWD/../../Lib

HEADERS += \
    command/commandtest.h \
    command/commandmocked.h \
    command/haveoptionstest.h \
    command/stopcmdtest.h \
    command/movecmdtest.h \
    command/jogcommandtest.h \
    command/referencecommandtest.h \
    command/getposcmdtest.h \
    command/isrunningcommandtest.h \
    command/checkcommandtest.h \
    command/resetcommandtest.h \
    command/retrievesettingscommandtest.h \
    command/setcommandtest.h \
    command/setaccelcommandtest.h \
    command/setpositioncommandtest.h \
    command/setmicrostepcommandtest.h \
    command/seti2caddresscommandtest.h \
    command/setreferenceforwardspeedcmdtest.h \
    command/setreferencebackwardspeedcmdtest.h \
    command/setpulloffdistancecmdtest.h \
    command/setafterreferenceposcmdtest.h \
    command/setrefnormallyhighcmdtest.h \
    command/setautorefonstartcmdtest.h \
    command/setrefmovedircmdtest.h \
    controller/step1rawcontrollertest.h \
    serialcommmock.h \
    controller/step1rawcontrollermocked.h \
    command/getfrequencycommandtest.h \
    controller/stagecontrollertest.h
