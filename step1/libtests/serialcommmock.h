#ifndef SERIALCOMMMOCK_H
#define SERIALCOMMMOCK_H

#include "serial/serialcomm.h"

class SerialCommMock : public SerialComm
{

public:

    MOCK_METHOD1(send , void(QString));
    MOCK_METHOD0(getState , ConnectionState());

};

#endif // SERIALCOMMMOCK_H
