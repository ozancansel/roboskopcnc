#include "rpmtest.h"
#include "step1link.h"
#include "parser.h"
#include "step1resultcodes.h"
#include <QThread>

TEST(RpmTest , should_return_non_zero_if_it_moving){
    Step1Link::reload();
    EXPECT_TRUE(Step1Link::setStopOnLimit(false));
    Step1Link::send("move 10000000 12000\n");
    EXPECT_TRUE(Parser::ok(Step1Link::readLine()));
    QThread::msleep(1000);
    Step1Link::send("rpm 200\n");
    QStringList l = Step1Link::readLine().split(" ");
    EXPECT_EQ(l.length() , 2);
    EXPECT_TRUE(l.at(1).toInt() > 0);
}

TEST(RpmTest , should_return_zero_if_not_moving){
    Step1Link::reload();
    Step1Link::send("rpm 200\n");
    QStringList l = Step1Link::readLine().split(" ");
    EXPECT_EQ(l.length() , 2);
    EXPECT_EQ(l.at(1).toInt() , 0);
}

TEST(RpmTest , if_rpm_per_revolution_not_specified_should_return_value_not_specified_error){
    Step1Link::reload();

    Step1Link::send("rpm\n");
    EXPECT_EQ(Parser::extractResult(Step1Link::readLine()) , ERR_VALUE_NOT_SPECIFIED);
}
