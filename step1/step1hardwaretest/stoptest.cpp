#include "stoptest.h"
#include "step1link.h"
#include "parser.h"
#include <QTime>
#include <QThread>

TEST(StopTest , stop_without_any_option){
    Step1Link::reload();

    Step1Link::send("stop\n");
    EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
    Step1Link::send("running\n");
    EXPECT_TRUE(Parser::no(Step1Link::readLine(100)));
}

TEST(StopTest , stop_with_soft_option){
    foreach(QString key , QStringList() << "--soft" << "-so"){
        Step1Link::reload();
        EXPECT_TRUE(Step1Link::setStopOnLimit(false));
        Step1Link::send("set --accel 1000\n");
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
        Step1Link::send("move 15000000 20000\n");
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
        Step1Link::send(QString("stop %0\n")
                        .arg(key));
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
        Step1Link::send("running\n");
        EXPECT_TRUE(Parser::yes(Step1Link::readLine(100)));
    }
}

TEST(StopTest , stop_with_soft_and_sync_option){
    foreach(QString key , QStringList() << "--soft" << "-so"){
        foreach(QString syncKey , QStringList() << "--sync" << "-s") {
            Step1Link::reload();
            EXPECT_TRUE(Step1Link::setStopOnLimit(false));
            Step1Link::send("move 1500000 20000\n");
            EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
            QThread::msleep(1000);
            Step1Link::send(QString("stop %0 %1\n")
                            .arg(key)
                            .arg(syncKey));
            QTime t;

            t.start();
            EXPECT_TRUE(Parser::ok(Step1Link::readLine(-1)));
            EXPECT_TRUE(t.elapsed() > 800);
            Step1Link::send("set --accel 10000\n");
            EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
        }
    }

    //Reverse
    foreach(QString key , QStringList() << "--soft" << "-so"){
        foreach(QString syncKey , QStringList() << "--sync" << "-s") {
            Step1Link::reload();
            EXPECT_TRUE(Step1Link::setStopOnLimit(false));
            Step1Link::send("move 1500000 20000\n");
            EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
            QThread::msleep(1000);
            Step1Link::send(QString("stop %0 %1\n")
                            .arg(syncKey)
                            .arg(key));
            QTime t;

            t.start();
            EXPECT_TRUE(Parser::ok(Step1Link::readLine(-1)));
            EXPECT_TRUE(t.elapsed() > 800);
            Step1Link::send("set --accel 10000\n");
            EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
        }
    }
}
