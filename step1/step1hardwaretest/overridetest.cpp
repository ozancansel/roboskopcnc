#include "overridetest.h"
#include "step1link.h"
#include "parser.h"
#include "step1resultcodes.h"
#include <QTime>
#include <QThread>


TEST(OverrideTest , override_if_no_option_is_specified_should_return_option_not_specified_error){
    Step1Link::reload();
    Step1Link::send("override\n");
    EXPECT_EQ(Parser::extractResult(Step1Link::readLine()) , ERR_OPTION_NOT_SPECIFIED);
}

TEST(OverrideTest , override_speed_if_it_is_moving_should_return_ok){
    foreach(QString key , QStringList() << "--speed" << "-sp"){
        Step1Link::reload();
        EXPECT_TRUE(Step1Link::setStopOnLimit(false));
        Step1Link::send("move 100000000 5000\n");
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(1000)));
        Step1Link::send(QString("override %0 8000\n")
                        .arg(key));
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(1000)));
    }
}

TEST(OverrideTest , override_speed_with_sync_if_it_is_moving_should_return_ok){
    foreach(QString key , QStringList() << "--speed" << "-sp"){
        foreach(QString syncKey , QStringList() << "--sync" << "-s"){
            Step1Link::reload();
            EXPECT_TRUE(Step1Link::setStopOnLimit(false));
            Step1Link::setAccel(1000);
            Step1Link::send("move 100000000 5000\n");
            EXPECT_TRUE(Parser::ok(Step1Link::readLine(7000)));
            QThread::msleep(5000);
            Step1Link::send(QString("override %0 %1 10000\n")
                            .arg(key)
                            .arg(syncKey));
            QTime t;
            t.start();
            EXPECT_TRUE(Parser::ok(Step1Link::readLine(8000)));
            EXPECT_TRUE(t.elapsed() > 3000); //It will take 5 seconds actually, but it is enough for testing
        }
    }
}

TEST(OverrideTest , override_speed_if_it_is_not_moving_should_return_motor_is_not_moving_msg){
    foreach(QString key , QStringList() << "--speed" << "-sp"){

        Step1Link::reload();

        Step1Link::send(QString("override %0 8000\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_MOTOR_IS_NOT_MOVING);
    }
}

TEST(OverrideTest , override_speed_if_speed_not_specified_should_return_value_not_specified_error){
    foreach(QString key , QStringList() << "--speed" << "-sp"){

        Step1Link::reload();

        Step1Link::send(QString("override %0\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_VALUE_NOT_SPECIFIED);
    }
}
