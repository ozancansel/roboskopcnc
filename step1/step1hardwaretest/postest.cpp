#include "postest.h"
#include "step1link.h"
#include "parser.h"

TEST(PosTest , pos){
    Step1Link::reload();

    //Firstly move to 10
    Step1Link::send("move --sync 10 1000\n");
    //Ensure that it is moved
    EXPECT_TRUE(Parser::ok(Step1Link::readLine()));
    //Retrieve cmd
    Step1Link::send("pos\n");

    QString res = Step1Link::readLine(100);
    EXPECT_EQ(res.trimmed() , "pos 10");
}
