#include "frequencytest.h"
#include "step1link.h"
#include "parser.h"

TEST(FrequencyTest , while_moving_should_return_non_zero_number){
    Step1Link::reload();
    //Firstly move
    EXPECT_TRUE(Step1Link::setStopOnLimit(false));
    Step1Link::send("move 1000000 10000\n");
    EXPECT_TRUE(Parser::ok(Step1Link::readLine()));
    Step1Link::send("frequency\n");
    QStringList l = Step1Link::readLine(100).split(" ");
    EXPECT_EQ(l.length() , 2);
    EXPECT_TRUE(Parser::isNumber(l.at(1)));
    EXPECT_TRUE(l.at(1).toInt() > 0);
}

TEST(FrequencyTest , if_it_is_not_moving_should_return_0){
    Step1Link::reload();
    //Firstly move
    Step1Link::send("frequency\n");
    QStringList l = Step1Link::readLine(100).split(" ");
    EXPECT_EQ(l.length() , 2);
    EXPECT_TRUE(Parser::isNumber(l.at(1)));
    EXPECT_EQ(l.at(1).toInt() , 0);
}
