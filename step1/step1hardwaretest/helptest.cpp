#include "helptest.h"
#include "step1link.h"
#include "parser.h"

//It is not strict test, but ensure that something returns when help is typed
TEST(HelpTest , should_return_lots_of_lines){
    Step1Link::reload();

    Step1Link::send("help\n");
    for(int i = 0; i < 10; i++){
        EXPECT_FALSE(Step1Link::readLine(100).isEmpty());
    }
}
