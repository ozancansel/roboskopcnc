#include "settingstest.h"
#include "step1link.h"
#include "step1resultcodes.h"
#include "parser.h"

TEST(SettingsTest , accel){
    foreach(QString key , QStringList() << "--accel" << "-a"){
        Step1Link::reload();
        EXPECT_TRUE(Step1Link::setAccel(10000));
        Step1Link::send(QString("settings %0\n")
                        .arg(key));
        QString str = Step1Link::readLine(500).trimmed();
        EXPECT_EQ(str , QString("accel 10000"));
    }
}

TEST(SettingsTest , i2cAddr){
    foreach(QString key , QStringList() << "--i2cAddr" << "-i"){
        Step1Link::reload();
        EXPECT_TRUE(Step1Link::setI2cAddr(8));
        Step1Link::send(QString("settings %0\n")
                        .arg(key));
        QString str = Step1Link::readLine(500).trimmed();
        EXPECT_EQ(str , QString("i2cAddr 8"));
    }
}

TEST(SettingsTest , microstepping){
    foreach(QString key , QStringList() << "--microstepping" << "-m"){
        Step1Link::reload();
        EXPECT_TRUE(Step1Link::setMicrostep(2));
        Step1Link::send(QString("settings %0\n")
                        .arg(key));
        QString str = Step1Link::readLine(500).trimmed();
        EXPECT_EQ(str , QString("microstepping 2"));
    }
}

TEST(SettingsTest , refForwardSpeed){
    foreach(QString key , QStringList() << "--refForwardS" << "-rfs"){
        Step1Link::reload();
        EXPECT_TRUE(Step1Link::setRefForwardSpeed(9000));
        Step1Link::send(QString("settings %0\n")
                        .arg(key));
        QString str = Step1Link::readLine(500).trimmed();
        EXPECT_EQ(str , QString("refForwardS 9000"));
    }
}

TEST(SettingsTest , refBackwardSpeed){
    foreach(QString key , QStringList() << "--refBackwardS" << "-rbs"){
        Step1Link::reload();
        EXPECT_TRUE(Step1Link::setRefBackwardSpeed(8000));
        Step1Link::send(QString("settings %0\n")
                        .arg(key));
        QString str = Step1Link::readLine(500).trimmed();
        EXPECT_EQ(str , QString("refBackwardS 8000"));
    }
}

TEST(SettingsTest , refBackwardVector){
    foreach(QString key , QStringList() << "--refBackwardV" << "-rbv"){
        Step1Link::reload();
        EXPECT_TRUE(Step1Link::setRefBackwardVector(7000));
        Step1Link::send(QString("settings %0\n")
                        .arg(key));
        QString str = Step1Link::readLine(500).trimmed();
        EXPECT_EQ(str , QString("refBackwardV 7000"));
    }
}

TEST(SettingsTest , refAfterPos){
    foreach(QString key , QStringList() << "--refAfterPos" << "-rap"){
        Step1Link::reload();
        EXPECT_TRUE(Step1Link::setRefAfterPos(6000));
        Step1Link::send(QString("settings %0\n")
                        .arg(key));
        QString str = Step1Link::readLine(500).trimmed();
        EXPECT_EQ(str , QString("refAfterPos 6000"));
    }
}

TEST(SettingsTest , endLogicTrue){
    foreach(QString key , QStringList() << "--endLogicTrue" << "-elt"){
        Step1Link::reload();
        EXPECT_TRUE(Step1Link::setEndLogicTrue(true));
        Step1Link::send(QString("settings %0\n")
                        .arg(key));
        QString str = Step1Link::readLine(500).trimmed();
        EXPECT_EQ(str , QString("endLogicTrue 1"));
    }
}


TEST(SettingsTest , refOnStart){
    foreach(QString key , QStringList() << "--refOnStart" << "-ros"){
        Step1Link::reload();
        EXPECT_TRUE(Step1Link::setRefOnStart(false));
        Step1Link::send(QString("settings %0\n")
                        .arg(key));
        QString str = Step1Link::readLine(500).trimmed();
        EXPECT_EQ(str , QString("refOnStart 0"));
    }
}

TEST(SettingsTest , refMoveDir){
    foreach(QString key , QStringList() << "--refMoveDir" << "-rmd"){
        Step1Link::reload();
        EXPECT_TRUE(Step1Link::setRefMoveDir(true));
        Step1Link::send(QString("settings %0\n")
                        .arg(key));
        QString str = Step1Link::readLine(500).trimmed();
        EXPECT_EQ(str , QString("refMoveDir 1"));
    }
}

TEST(SettingsTest , stopOnLimit){
    foreach(QString key , QStringList() << "--stopOnLimit" << "-sol"){
        Step1Link::reload();
        EXPECT_TRUE(Step1Link::setStopOnLimit(false));
        Step1Link::send(QString("settings %0\n")
                        .arg(key));
        QString str = Step1Link::readLine(500).trimmed();
        EXPECT_EQ(str , QString("stopOnLimit 0"));
    }
}
