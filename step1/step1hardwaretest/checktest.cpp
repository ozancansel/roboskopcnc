#include "checktest.h"
#include "step1link.h"
#include "parser.h"

TEST(CheckTest , check_test){
    Step1Link::reload();

    Step1Link::send("check\n");
    EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
}
