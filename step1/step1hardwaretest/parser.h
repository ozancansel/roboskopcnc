#ifndef PARSER_H
#define PARSER_H

#include <QString>

class Parser
{
public:
    static int extractResult(QString str);
    static bool yes(QString str);
    static bool no(QString str);
    static bool ok(QString str);
    static bool isNumber(QString str);
    static bool isBootMsg(QString str);
};

#endif // PARSER_H
