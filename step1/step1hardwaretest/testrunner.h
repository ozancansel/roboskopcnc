#ifndef TESTRUNNER_H
#define TESTRUNNER_H

#include <QObject>

class TestRunner : public QObject
{
    Q_OBJECT

public slots:

    void runTest();

signals:

    void ended();

};

#endif // TESTRUNNER_H
