#include "resttest.h"
#include "step1link.h"
#include "parser.h"
#include "step1resultcodes.h"

TEST(RestTest , if_motor_is_not_moving_should_return_ok){
    Step1Link::reload();

    Step1Link::send("rest\n");
    EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
}

TEST(RestTest , if_motor_is_moving_should_return_err_moving){
    Step1Link::reload();

    //Firstly ensure that it is moving
    EXPECT_TRUE(Step1Link::setStopOnLimit(false));
    Step1Link::send("move 1000000 2000\n");
    EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
    Step1Link::send("rest\n");
    EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_MOVING);
}
