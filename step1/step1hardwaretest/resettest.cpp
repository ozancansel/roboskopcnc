#include "resettest.h"
#include "step1link.h"
#include "parser.h"

TEST(ResetTest , reset_with_no_option){
    Step1Link::reload();

    Step1Link::send("reset\n");
    EXPECT_TRUE(Parser::isBootMsg(Step1Link::readLine(-1)));
}

TEST(ResetTest , reset_with_hard_option){
    foreach(QString key , QStringList() << "--hard" << "-h"){
        Step1Link::reload();

        Step1Link::send(QString("reset %0\n")
                        .arg(key));
        EXPECT_TRUE(Parser::isBootMsg(Step1Link::readLine(-1)));
    }
}
