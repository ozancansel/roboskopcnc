#include "settest.h"
#include "step1link.h"
#include "parser.h"
#include "step1resultcodes.h"
#include <QThread>

TEST(SetTest,  if_no_option_is_sent_shoudl_return_err_option_not_specified){
    Step1Link::reload();
    Step1Link::send("set\n");

    EXPECT_EQ(Parser::extractResult(Step1Link::readLine(1000)) , ERR_OPTION_NOT_SPECIFIED);
}

TEST(SetTest , while_referencing_should_return_err_currently_referencing_err){
    Step1Link::reload();
    Step1Link::send("set --endLogicTrue 0\n");
    EXPECT_TRUE(Parser::ok(Step1Link::readLine(1000)));
    EXPECT_TRUE(Step1Link::readSettings());
    EXPECT_EQ(Step1Link::endLogicTrue , 0);
    Step1Link::send("reference\n");
    QThread::msleep(500);
    Step1Link::send("set --accel 10000\n");
    QString res = Step1Link::readLine(15000);
    EXPECT_EQ(Parser::extractResult(res) , ERR_CURRENTLY_REFERENCING);
}

TEST(SetTest , set_accel_if_accel_value_is_positive_should_return_ok){
    foreach(QString accel , QStringList() << "--accel" << "-a"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 10000\n").arg(accel)); //Dont change acceleration value, reminainig tests are bounded for it
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(1000)));
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::acceleration , 10000);
    }
}

TEST(SetTest , set_accel_should_temp_option){
    foreach (QString accelKey, QStringList() << "--accel" << "-a") {
        foreach(QString tempKey , QStringList() << "--temp" << "-t"){
            Step1Link::reload();
            Step1Link::send(QString("set %0 %1 12000\n")
                            .arg(accelKey)
                            .arg(tempKey));
            EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
            EXPECT_TRUE(Step1Link::readSettings());
            EXPECT_EQ(Step1Link::acceleration , 12000);
            Step1Link::reload();
            EXPECT_TRUE(Step1Link::readSettings());
            EXPECT_EQ(Step1Link::acceleration , 10000);
        }
    }
}

TEST(SetTest,  set_accel_if_motor_is_moving_should_return_err_moving){

    foreach(QString accelKey , QStringList() << "--accel" << "-a"){
        Step1Link::reload();
        //Move stepper to infinite
        Step1Link::send("move 1000000 1000\n");
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(100))); //Read result
        Step1Link::send(QString("set %0 15000\n")
                        .arg(accelKey));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_MOVING);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::acceleration , 10000);
    }
}

TEST(SetTest , set_accel_if_accel_not_specified_should_return_value_not_specified_error){
    foreach(QString accelKey , QStringList() << "--accel" << "-a"){
        Step1Link::reload();
        Step1Link::send(QString("set %0\n")
                        .arg(accelKey));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_VALUE_NOT_SPECIFIED);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::acceleration , 10000);
    }
}

TEST(SetTest , set_accel_if_is_zero_should_return_value_should_be_postitive_err){
    foreach(QString accelKey , QStringList() << "--accel" << "-a"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 0\n")
                        .arg(accelKey));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_VALUE_SHOULD_BE_POSITIVE);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::acceleration , 10000);
    }
}

TEST(SetTest , set_accel_if_is_negative_should_return_value_should_be_positive_err){
    foreach(QString accelKey , QStringList() << "--accel" << "-a"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 -10\n")
                        .arg(accelKey));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_VALUE_SHOULD_BE_POSITIVE);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::acceleration , 10000);
    }
}

TEST(SetTest , set_accel_if_send_non_number_str_should_return_number_parser_err){
    foreach(QString accelKey , QStringList() << "--accel" << "-a"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 notnumbervalue:)\n")
                        .arg(accelKey));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_NUMBER_PARSE_ERR);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::acceleration , 10000);
    }
}

TEST(SetTest , set_pos){
    foreach(QString posKey , QStringList() << "--pos" << "-p"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 10\n")
                        .arg(posKey));
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(1000)));
        Step1Link::send("pos\n");
        QStringList l = Step1Link::readLine(1000).split(" ");

        EXPECT_EQ(l.length() , 2);
        EXPECT_EQ(l.at(1).toInt() , 10);
    }
}

TEST(SetTest , set_pos_if_position_not_sent_should_return_value_not_specified_err){
    foreach(QString posKey , QStringList() << "--pos" << "-p"){
        Step1Link::reload();
        Step1Link::send(QString("set %0\n")
                        .arg(posKey));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(1000)) , ERR_VALUE_NOT_SPECIFIED);
        Step1Link::send("pos\n");
        QStringList l = Step1Link::readLine(1000).split(" ");

        EXPECT_EQ(l.length() , 2);
        EXPECT_EQ(l.at(1).toInt() , 0);
    }
}

TEST(SetTest , set_pos_if_sent_non_number_string_should_return_number_parser_error){
    foreach(QString posKey , QStringList() << "--pos" << "-p"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 nonnumberstr\n")
                        .arg(posKey));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_NUMBER_PARSE_ERR);
        Step1Link::send("pos\n");
        QStringList l = Step1Link::readLine(1000).split(" ");

        EXPECT_EQ(l.length() , 2);
        EXPECT_EQ(l.at(1).toInt() , 0);
    }
}

TEST(SetTest , set_microstepping){
    foreach(QString microKey , QStringList() << "--microstepping" << "-m"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 0\n")
                        .arg(microKey));
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
    }
}

TEST(SetTest , set_microstepping_temp_option){
    foreach(QString microKey , QStringList() << "--microstepping" << "-m"){
        foreach(QString tempKey , QStringList() << "--temp" << "-t"){
            Step1Link::reload();
            Step1Link::send(QString("set %0 %1 1\n")
                            .arg(microKey)
                            .arg(tempKey));
            EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
            EXPECT_TRUE(Step1Link::readSettings());
            EXPECT_EQ(Step1Link::microstepping , 1);
            Step1Link::reload();
            EXPECT_TRUE(Step1Link::readSettings());
            EXPECT_EQ(Step1Link::microstepping , 0);
        }
    }
}

TEST(SetTest , set_microstepping_if_motor_is_moving_should_return_moving_err){
    foreach(QString microKey , QStringList() << "--microstepping" << "-m"){
        Step1Link::reload();
        Step1Link::send("move 10000000 1000\n");
        EXPECT_TRUE(Parser::ok(Step1Link::readLine()));
        Step1Link::send(QString("set %0 2\n")
                        .arg(microKey));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(200)) , ERR_MOVING);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::microstepping , 0);
    }
}

TEST(SetTest , set_microstepping_if_value_is_not_sent_should_return_value_not_specified){
    foreach(QString microKey , QStringList() << "--microstepping" << "-m"){
        Step1Link::reload();
        Step1Link::send(QString("set %0\n")
                        .arg(microKey));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_VALUE_NOT_SPECIFIED);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::microstepping , 0);
    }
}

TEST(SetTest , set_microstepping_if_value_lower_than_zero_should_return_microstep_outboundary_error){
    foreach(QString microKey , QStringList() << "--microstepping" << "-m"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 -1\n")
                        .arg(microKey));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_MICROSTEP_OUTBOUNDARY);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::microstepping , 0);
    }
}

TEST(SetTest , set_microstepping_if_value_is_greater_than_four_should_return_microstep_outboundary_error){
    foreach(QString microKey , QStringList() << "--microstepping" << "-m"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 5\n")
                        .arg(microKey));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_MICROSTEP_OUTBOUNDARY);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::microstepping , 0);
    }
}

TEST(SetTest , set_i2cAddr){
    foreach(QString i2cKey , QStringList() << "--i2cAddr" << "-i"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 8\n")
                        .arg(i2cKey));
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::i2cAddr , 8);
    }
}

TEST(SetTest , set_i2cAAddr_if_addr_is_lower_than_zero_should_return_invalid_i2c_addr){
    foreach(QString i2cKey , QStringList() << "--i2cAddr" << "-i"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 -1\n")
                        .arg(i2cKey));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_INVALID_VALUE);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::i2cAddr , 8);
    }
}

TEST(SetTest , set_i2cAAddr_if_addr_is_greater_than_127_should_return_invalid_i2c_addr){
    foreach(QString i2cKey , QStringList() << "--i2cAddr" << "-i"){

        Step1Link::reload();
        Step1Link::send(QString("set %0 128\n")
                        .arg(i2cKey));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_INVALID_VALUE);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::i2cAddr , 8);
    }
}

TEST(SetTest , set_i2cAddr_if_addr_is_non_number_string_should_return_number_parser_error){
    foreach(QString i2cKey , QStringList() << "--i2cAddr" << "-i"){

        Step1Link::reload();
        Step1Link::send(QString("set %0 nonnumberstr\n")
                        .arg(i2cKey));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_NUMBER_PARSE_ERR);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::i2cAddr , 8);
    }
}

TEST(SetTest , set_refForwardSpeed){
    foreach(QString i2cKey , QStringList() << "--refForwardS" << "-rfs"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 1000\n")
                        .arg(i2cKey));
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::refForwardSpeed , 1000);
    }
}

TEST(SetTest , set_refForwardSpeed_temp_option){
    foreach(QString i2cKey , QStringList() << "--refForwardS" << "-rfs"){
        foreach(QString tempKey , QStringList() << "--temp" << "-t"){
            Step1Link::reload();
            Step1Link::send(QString("set %0 %1 2000\n")
                            .arg(i2cKey)
                            .arg(tempKey));
            EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
            EXPECT_TRUE(Step1Link::readSettings());
            EXPECT_EQ(Step1Link::refForwardSpeed , 2000);
            Step1Link::reload();
            EXPECT_TRUE(Step1Link::readSettings());
            EXPECT_EQ(Step1Link::refForwardSpeed , 1000);
        }
    }
}

TEST(SetTest , set_refForwardSpeed_if_value_is_zero_should_return_value_invalid_value_err){
    foreach(QString key , QStringList() << "--refForwardS" << "-rfs"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 0\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_INVALID_VALUE);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::refForwardSpeed , 1000);
    }
}

TEST(SetTest , set_refForwardSpeed_if_value_is_negative_number_should_return_invalid_value_err){
    foreach(QString key , QStringList() << "--refForwardS" << "-rfs"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 -1\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_INVALID_VALUE);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::refForwardSpeed , 1000);
    }
}

TEST(SetTest , set_refForwardSpeed_if_value_is_non_number_string_should_return_number_parser_err){
    foreach(QString key , QStringList() << "--refForwardS" << "-rfs"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 nonnumberstr\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_NUMBER_PARSE_ERR);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::refForwardSpeed , 1000);
    }
}

TEST(SetTest , set_refForwardSpeed_if_value_isnt_sent_should_return_value_not_specified_err){
    foreach(QString key , QStringList() << "--refForwardS" << "-rfs"){
        Step1Link::reload();
        Step1Link::send(QString("set %0\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_VALUE_NOT_SPECIFIED);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::refForwardSpeed , 1000);
    }
}

TEST(SetTest , set_refBackwardSpeed){
    foreach(QString key , QStringList() << "--refBackwardS" << "-rbs"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 3000\n")
                        .arg(key));
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::refBackwardSpeed , 3000);
    }
}

TEST(SetTest , set_refBackwardSpeed_with_temp_option){
    foreach(QString key , QStringList() << "--refBackwardS" << "-rbs"){
        foreach(QString tKey , QStringList() << "--temp" << "-t"){
            Step1Link::reload();
            Step1Link::send(QString("set %0 %1 3500\n")
                            .arg(key)
                            .arg(tKey));
            EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
            EXPECT_TRUE(Step1Link::readSettings());
            EXPECT_EQ(Step1Link::refBackwardSpeed , 3500);
            Step1Link::reload();
            EXPECT_TRUE(Step1Link::readSettings());
            EXPECT_EQ(Step1Link::refBackwardSpeed , 3000);
        }
    }
}

TEST(SetTest , set_refBackwardSpeed_if_value_is_zero_should_return_value_invalid_value_err){
    foreach(QString key , QStringList() << "--refBackwardS" << "-rbs"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 0\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_INVALID_VALUE);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::refBackwardSpeed , 3000);
    }
}

TEST(SetTest , set_refBackwardSpeed_if_value_is_negative_number_should_return_invalid_value_err){
    foreach(QString key , QStringList() << "--refBackwardS" << "-rbs"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 -1\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_INVALID_VALUE);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::refBackwardSpeed , 3000);
    }
}

TEST(SetTest , set_refBackwardSpeed_if_value_is_non_number_string_should_return_number_parser_err){
    foreach(QString key , QStringList() << "--refBackwardS" << "-rbs"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 nonnumberstr\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_NUMBER_PARSE_ERR);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::refBackwardSpeed , 3000);
    }
}

TEST(SetTest , set_refBackwardSpeed_if_value_isnt_sent_should_return_value_not_specified_err){
    foreach(QString key , QStringList() << "--refBackwardS" << "-rbs"){

        Step1Link::reload();
        Step1Link::send(QString("set %0\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_VALUE_NOT_SPECIFIED);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::refBackwardSpeed , 3000);
    }
}


TEST(SetTest , set_refBackwardVector){
    foreach(QString key , QStringList() << "--refBackwardV" << "-rbv"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 4000\n")
                        .arg(key));
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
    }
}

TEST(SetTest , set_refBackwardVector_with_temp_option){
    foreach(QString key , QStringList() << "--refBackwardV" << "-rbv"){
        foreach(QString tKey , QStringList() << "--temp" << "-t"){
            Step1Link::reload();
            Step1Link::send(QString("set %0 %1 4500\n")
                            .arg(key)
                            .arg(tKey));
            EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
            EXPECT_TRUE(Step1Link::readSettings());
            EXPECT_EQ(Step1Link::refBackwardVector , 4500);
            Step1Link::reload();
            EXPECT_TRUE(Step1Link::readSettings());
            EXPECT_EQ(Step1Link::refBackwardVector , 4000);
        }
    }
}

TEST(SetTest , set_refBackwardVector_if_value_is_zero_should_return_value_invalid_value_err){
    foreach(QString key , QStringList() << "--refBackwardV" << "-rbv"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 0\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_INVALID_VALUE);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::refBackwardVector , 4000);
    }
}

TEST(SetTest , set_refBackwardVector_if_value_is_negative_number_should_return_invalid_value_err){
    foreach(QString key , QStringList() << "--refBackwardV" << "-rbv"){

        Step1Link::reload();
        Step1Link::send(QString("set %0 -1\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_INVALID_VALUE);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::refBackwardVector , 4000);
    }
}

TEST(SetTest , set_refBackwardVector_if_value_is_non_number_string_should_return_number_parser_err){
    foreach(QString key , QStringList() << "--refBackwardV" << "-rbv"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 nonnumberstr\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_NUMBER_PARSE_ERR);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::refBackwardVector , 4000);
    }
}

TEST(SetTest , set_refBackwardVector_if_value_isnt_sent_should_return_value_not_specified_err){
    foreach(QString key , QStringList() << "--refBackwardV" << "-rbv"){

        Step1Link::reload();
        Step1Link::send(QString("set %0\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_VALUE_NOT_SPECIFIED);
        EXPECT_TRUE(Step1Link::readSettings());
        EXPECT_EQ(Step1Link::refBackwardVector , 4000);
    }
}


TEST(SetTest , set_refAfterPosition){
    foreach(QString key , QStringList() << "--refAfterPos" << "-rap"){

        Step1Link::reload();
        Step1Link::send(QString("set %0 1000\n")
                        .arg(key));
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
    }
}

TEST(SetTest , set_refAfterPosition_with_temp_option){
    foreach(QString key , QStringList() << "--refAfterPos" << "-rap"){
        foreach(QString tKey , QStringList() << "--temp" << "-t"){
            Step1Link::reload();
            Step1Link::send(QString("set %0 %1 1000\n")
                            .arg(key)
                            .arg(tKey));
            EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
        }
    }
}

TEST(SetTest , set_refAfterPosition_if_value_is_non_number_string_should_return_number_parser_err){
    foreach(QString key , QStringList() << "--refAfterPos" << "-rap"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 nonnumberstr\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_NUMBER_PARSE_ERR);
    }
}

TEST(SetTest , set_refAfterPosition_if_value_isnt_sent_should_return_value_not_specified_err){
    foreach(QString key , QStringList() << "--refAfterPos" << "-rap"){
        Step1Link::reload();
        Step1Link::send(QString("set %0\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_VALUE_NOT_SPECIFIED);
    }
}

TEST(SetTest , set_endLogicTrue_0){
    foreach(QString key , QStringList() << "--endLogicTrue" << "-elt"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 0\n")
                        .arg(key));
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
    }
}

TEST(SetTest , set_endLogicTrue_1){
    foreach(QString key , QStringList() << "--endLogicTrue" << "-elt"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 1\n")
                        .arg(key));
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
    }
}

TEST(SetTest , set_endLogicTrue_temp_option){
    foreach(QString key , QStringList() << "--endLogicTrue" << "-elt"){
        foreach(QString tKey , QStringList() << "--temp" << "-t"){
            Step1Link::reload();
            Step1Link::send(QString("set %0 %1 1\n")
                            .arg(key)
                            .arg(tKey));
            EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
        }
    }
}

TEST(SetTest , set_endLogicTrue_if_value_is_non_number_string_should_return_number_parser_err){
    foreach(QString key , QStringList() << "--endLogicTrue" << "-elt"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 nonnumberstr\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_NUMBER_PARSE_ERR);
    }
}

TEST(SetTest , set_endLogicTrue_if_value_is_different_from_one_or_zero_should_return_invalid_value){
    foreach(QString key , QStringList() << "--endLogicTrue" << "-elt"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 2\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_INVALID_VALUE);
    }
}

TEST(SetTest , set_endLogicTrue_if_value_isnt_sent_should_return_value_not_specified_err){
    foreach(QString key , QStringList() << "--endLogicTrue" << "-elt"){
        Step1Link::reload();
        Step1Link::send(QString("set %0\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_VALUE_NOT_SPECIFIED);
    }
}


TEST(SetTest , set_refMoveDirection_1){
    foreach(QString key , QStringList() << "--refMoveDir" << "-rmd"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 1\n")
                        .arg(key));
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
    }
}

TEST(SetTest , set_refMoveDirection_minus_one){
    foreach(QString key , QStringList() << "--refMoveDir" << "-rmd"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 -1\n")
                        .arg(key));
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
    }
}

TEST(SetTest , set_refMoveDirection_with_temp_option){
    foreach(QString key , QStringList() << "--refMoveDir" << "-rmd"){
        foreach(QString tKey , QStringList() << "--temp" << "-t"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 %1 1\n")
                        .arg(key)
                        .arg(tKey));
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
        }
    }
}

TEST(SetTest , set_refMoveDirection_if_value_is_non_number_string_should_return_number_parser_err){
    foreach(QString key , QStringList() << "--refMoveDir" << "-rmd"){

        Step1Link::reload();
        Step1Link::send(QString("set %0 nonnumberstr\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_NUMBER_PARSE_ERR);
    }
}

TEST(SetTest , set_refMoveDirection_if_value_is_different_from_one_or_minus_one_should_return_invalid_value){
    foreach(QString key , QStringList() << "--refMoveDir" << "-rmd"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 2\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_INVALID_VALUE);
    }
}

TEST(SetTest , set_refMoveDirection_if_value_isnt_sent_should_return_value_not_specified_err){
    foreach(QString key , QStringList() << "--refMoveDir" << "-rmd"){
        Step1Link::reload();
        Step1Link::send(QString("set %0\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_VALUE_NOT_SPECIFIED);
    }
}

TEST(SetTest , set_stopOnLimit_0){
    foreach(QString key , QStringList() << "--stopOnLimit" << "-sol"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 0\n")
                        .arg(key));
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
    }
}

TEST(SetTest , set_stopOnLimit_1){
    foreach(QString key , QStringList() << "--stopOnLimit" << "-sol"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 1\n")
                        .arg(key));
        EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
    }
}

TEST(SetTest , set_stopOnLimit_should_apply_temp_option){
    foreach(QString key , QStringList() << "--stopOnLimit" << "-sol"){
        foreach(QString tKey , QStringList() << "--temp" << "-t"){
            Step1Link::reload();
            Step1Link::send(QString("set %0 %1 1\n")
                            .arg(key)
                            .arg(tKey));
            EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
        }
    }
}

TEST(SetTest , set_stopOnLimit_if_value_is_non_number_string_should_return_number_parser_err){
    foreach(QString key , QStringList() << "--stopOnLimit" << "-sol"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 nonnumberstr\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_NUMBER_PARSE_ERR);
    }
}

TEST(SetTest , set_stopOnLimit_if_value_is_different_from_one_or_zero_should_return_invalid_value){
    foreach(QString key , QStringList() << "--stopOnLimit" << "-sol"){
        Step1Link::reload();
        Step1Link::send(QString("set %0 2\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_INVALID_VALUE);
    }
}

TEST(SetTest , set_stopOnLimit_if_value_isnt_sent_should_return_value_not_specified_err){
    foreach(QString key , QStringList() << "--stopOnLimit" << "-sol"){
        Step1Link::reload();
        Step1Link::send(QString("set %0\n")
                        .arg(key));
        EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_VALUE_NOT_SPECIFIED);
    }
}
