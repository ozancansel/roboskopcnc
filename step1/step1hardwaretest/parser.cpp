#include "parser.h"
#include <QStringRef>

int Parser::extractResult(QString str){
    if(str.trimmed().startsWith("ERR:")){
        int startIdx = str.indexOf(":");
        if(startIdx < 0)
            return 0;
        QStringRef ref(&str , startIdx + 1, str.length() - startIdx - 1);
        return ref.toInt();
    } else if(str.trimmed().startsWith(";")){
        return 1;
    }
}

bool Parser::yes(QString str){
    return str.trimmed() == "yes";
}

bool Parser::no(QString str){
    return str.trimmed() == "no";
}

bool Parser::ok(QString str){
    return str.trimmed() == ";";
}

bool Parser::isNumber(QString str){
    bool ok;
    str.trimmed().toInt(&ok);
    return ok;
}

bool Parser::isBootMsg(QString str){
    return str.trimmed() == "[Step-1 v1.0]";
}
