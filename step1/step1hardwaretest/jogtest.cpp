#include "jogtest.h"
#include "step1link.h"
#include "parser.h"
#include "step1resultcodes.h"
#include <QTime>

TEST(JogTest , vector_and_speed_specified_should_return_ok){
    Step1Link::reload();
    //Disable end sensors
    EXPECT_TRUE(Step1Link::setStopOnLimit(false));
    Step1Link::send("jog 12500 500\n");

    EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
}

TEST(JogTest , negative_vector_and_speed_specified_should_return_ok){
    Step1Link::reload();
    //Disable end sensors
    EXPECT_TRUE(Step1Link::setStopOnLimit(false));
    Step1Link::send("jog -12500 500\n");

    EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
}

TEST(JogTest , sync_option){
    foreach(QString syncKey , QStringList() << "--sync"  << "-s"){
        Step1Link::reload();
        //Disable end sensor
        EXPECT_TRUE(Step1Link::setStopOnLimit(false));
        Step1Link::send(QString("jog %0 4000 1000\n")
                        .arg(syncKey));
        QTime t;
        t.start();

        EXPECT_TRUE(Parser::ok(Step1Link::readLine(-1)));
        EXPECT_TRUE(t.elapsed() > 3000);
    }
}

TEST(JogTest , sync_rest_option){
    foreach(QString syncKey , QStringList() << "--sync"  << "-s"){
        foreach(QString restKey , QStringList() << "--rest" << "-r"){
            Step1Link::reload();
            //Disable end sensor
            EXPECT_TRUE(Step1Link::setStopOnLimit(false));
            Step1Link::send(QString("jog %0 %1 4000 1000\n")
                            .arg(syncKey)
                            .arg(restKey));
            QTime t;
            t.start();
            EXPECT_TRUE(Parser::ok(Step1Link::readLine(7000)));
            EXPECT_TRUE(t.elapsed() > 3000);
        }
    }
}

TEST(JogTest , if_vector_is_not_number_should_return_number_parser_error){
    Step1Link::reload();

    Step1Link::send("jog qwoijdq 12000\n");

    EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_NUMBER_PARSE_ERR);
}

TEST(JogTest , if_speed_is_not_number_should_return_number_parse_error){
    Step1Link::reload();

    Step1Link::send("jog 5000 qqw223q\n");

    EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_NUMBER_PARSE_ERR);
}

TEST(JogTest , if_vector_and_speed_is_not_number_should_return_number_parser_error){
    Step1Link::reload();

    Step1Link::send("jog qwd111 qqw223q\n");

    EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_NUMBER_PARSE_ERR);
}

TEST(JogTest , if_speed_is_zero_should_return_value_should_be_positive_error){
    Step1Link::reload();

    Step1Link::send("jog 5000 0\n");

    EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_VALUE_SHOULD_BE_POSITIVE);
}

TEST(JogTest , if_speed_is_negative_should_return_value_should_be_positive_error){
    Step1Link::reload();

    Step1Link::send("jog 5000 -1\n");

    EXPECT_EQ(Parser::extractResult(Step1Link::readLine(100)) , ERR_VALUE_SHOULD_BE_POSITIVE);
}
