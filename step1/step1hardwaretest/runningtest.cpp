#include "runningtest.h"
#include "step1link.h"
#include "parser.h"

TEST(RunningTest , running_if_stepper_is_moving_should_return_yes){
    Step1Link::reload();

    //Moving
    EXPECT_TRUE(Step1Link::setStopOnLimit(false));
    Step1Link::send("move 15000000 1000\n");
    EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
    Step1Link::send("running\n");
    EXPECT_TRUE(Parser::yes(Step1Link::readLine(100)));
}

TEST(RunningTest , running_if_stepper_is_not_moving_should_return_no){
    Step1Link::reload();

    //Moving
    Step1Link::send("running\n");
    EXPECT_TRUE(Parser::no(Step1Link::readLine(100)));
}
