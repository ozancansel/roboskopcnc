#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <QTimer>
#include "step1link.h"
#include "testrunner.h"

int main(int argc, char **argv) {
  QCoreApplication app(argc, argv);
  ::testing::InitGoogleTest(&argc, argv);
  ::testing::GTEST_FLAG(filter) = "*";

  TestRunner runner;
  QTimer::singleShot(0 , &runner , SLOT(runTest()));
  QObject::connect(&runner , SIGNAL(ended()) , &app , SLOT(quit()));

  app.exec();
  return 0;
}
