#include "wakeuptest.h"
#include "step1link.h"
#include "parser.h"

TEST(WakeUpTest , wakeup){
    Step1Link::reload();
    Step1Link::send("wakeUp\n");
    EXPECT_TRUE(Parser::ok(Step1Link::readLine(100)));
}
