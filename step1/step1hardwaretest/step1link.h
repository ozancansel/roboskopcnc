#ifndef STEP1LINK_H
#define STEP1LINK_H

#include "serial/serialport.h"

class Step1Link
{

public:

    static SerialPort   serialPort;
    static void reload();
    static void send(QString str);
    static QString readLine(int timeout = -1);
    static void clear();
    static bool readSettings();
    static int  getSettingsValue(QString settingsLine);

    static bool setAccel(int val);
    static bool setI2cAddr(int val);
    static bool setMicrostep(int microstep);
    static bool setRefForwardSpeed(int speed);
    static bool setRefBackwardSpeed(int speed);
    static bool setRefBackwardVector(int vector);
    static bool setRefAfterPos(int pos);
    static bool setEndLogicTrue(bool high);
    static bool setRefOnStart(bool yes);
    static bool setRefMoveDir(bool positive);
    static bool setStopOnLimit(bool yes);


    static int acceleration;
    static int i2cAddr;
    static int mode;
    static int algorithm;
    static int microstepping;
    static int refForwardSpeed;
    static int refBackwardSpeed;
    static int refBackwardVector;
    static int refAfterPos;
    static int endLogicTrue;
    static int refOnStart;
    static int refMovedir;
    static int stopOnLimit;

};

#endif // STEP1LINK_H
