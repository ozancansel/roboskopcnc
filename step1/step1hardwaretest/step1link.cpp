#include "step1link.h"
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDebug>
#include <QThread>
#include "parser.h"

SerialPort Step1Link::serialPort;

int Step1Link::acceleration;
int Step1Link::i2cAddr;
int Step1Link::mode;
int Step1Link::algorithm;
int Step1Link::microstepping;
int Step1Link::refForwardSpeed;
int Step1Link::refBackwardSpeed;
int Step1Link::refBackwardVector;
int Step1Link::refAfterPos;
int Step1Link::endLogicTrue;
int Step1Link::refOnStart;
int Step1Link::refMovedir;
int Step1Link::stopOnLimit;

void Step1Link::reload(){
    if(serialPort.getState() == SerialComm::Connected)
        serialPort.closeSocket();

    QList<QSerialPortInfo> infos = QSerialPortInfo::availablePorts();

    serialPort.setBaudRate(SerialComm::B9600);

    foreach(QSerialPortInfo info , infos){
//        qDebug() << "pid\t" << info.productIdentifier() << "\tvid\t" << info.vendorIdentifier();
        if(info.productIdentifier() == 29987 && info.vendorIdentifier() == 6790){
            serialPort.connectTo(info.portName());
            serialPort.device()->waitForReadyRead(-1);
            //Clear port
            serialPort.device()->readAll();

            return;
        }
    }
}

void Step1Link::send(QString str){
    serialPort.send(str);
}

QString Step1Link::readLine(int timeout){
    serialPort.device()->waitForReadyRead(timeout);
    return QString::fromLatin1(serialPort.device()->readLine());
}

void Step1Link::clear(){
    serialPort.device()->readAll();
}

bool Step1Link::readSettings(){
    send("settings --all\n");

    QStringList l;

    for(int i = 0; i < 11; i++){
        QString str = readLine(500);

        //If str is empty return false
        if(str.isEmpty())
            return false;

        //Push to list
        l << str;
    }
    int checkSum = 0;
    foreach(QString line,  l){
        if(line.startsWith("accel")){
            acceleration = getSettingsValue(line);
            checkSum++;
        } else if(line.startsWith("i2cAddr")){
            i2cAddr = getSettingsValue(line);
            checkSum++;
        }
        else if(line.startsWith("microstepping")) {
            microstepping = getSettingsValue(line);
            checkSum++;
        }
        else if(line.startsWith("refForwardS")) {
            refForwardSpeed = getSettingsValue(line);
            checkSum++;
        }
        else if(line.startsWith("refBackwardS")) {
            refBackwardSpeed = getSettingsValue(line);
            checkSum++;
        } else if(line.startsWith("refBackwardV")){
            refBackwardVector = getSettingsValue(line);
            checkSum++;
        }
        else if(line.startsWith("refAfterPos")){
            refAfterPos = getSettingsValue(line);
            checkSum++;
        }
        else if(line.startsWith("endLogicTrue")) {
            endLogicTrue = getSettingsValue(line);
            checkSum++;
        }
        else if(line.startsWith("refOnStart")){
            refOnStart = getSettingsValue(line);
            checkSum++;
        }
        else if(line.startsWith("refMoveDir")){
            refMovedir = getSettingsValue(line);
            checkSum++;
        }
        else if(line.startsWith("stopOnLimit")){
            stopOnLimit = getSettingsValue(line);
            checkSum++;
        }
    }

    return checkSum == 11;
}

int Step1Link::getSettingsValue(QString settingsLine){
    return settingsLine.split(" ").at(1).trimmed().toInt();
}

bool Step1Link::setAccel(int val){
    send(QString("set -a -t %0\n")
            .arg(val));
    return Parser::ok(readLine(500));
}

bool Step1Link::setI2cAddr(int val){
    send(QString("set -i %0\n")
            .arg(val));
    return Parser::ok(readLine(500));
}

bool Step1Link::setMicrostep(int microstep){
    send(QString("set -m -t %0\n")
            .arg(microstep));
    return Parser::ok(readLine(500));
}

bool Step1Link::setRefForwardSpeed(int speed){
    send(QString("set -rfs %0\n")
            .arg(speed));
    return Parser::ok(readLine(500));
}

bool Step1Link::setRefBackwardSpeed(int speed){
    send(QString("set -rbs %0\n")
            .arg(speed));
    return Parser::ok(readLine(500));
}

bool Step1Link::setRefBackwardVector(int vector){
    send(QString("set -rbv %0\n")
            .arg(vector));
    return Parser::ok(readLine(500));
}

bool Step1Link::setRefAfterPos(int pos){
    send(QString("set -rap %0\n")
            .arg(pos));
    return Parser::ok(readLine(500));
}

bool Step1Link::setEndLogicTrue(bool high){
    send(QString("set -elt %0\n")
            .arg(high ? 1 : 0));
    return Parser::ok(readLine(500));
}

bool Step1Link::setRefOnStart(bool yes){
    send(QString("set -ros %0\n")
            .arg(yes ? 1 : 0));
    return Parser::ok(readLine(500));
}

bool Step1Link::setRefMoveDir(bool positive){
    send(QString("set -rmd %0\n")
            .arg(positive ? 1 : -1));
    return Parser::ok(readLine(500));
}

bool Step1Link::setStopOnLimit(bool yes){
    send(QString("set -sol %0\n")
         .arg(yes ? 1 : 0));
    return Parser::ok(readLine(500));
}

