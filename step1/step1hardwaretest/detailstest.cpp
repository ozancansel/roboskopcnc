#include "detailstest.h"
#include "step1link.h"
#include "parser.h"
#include "step1resultcodes.h"

TEST(DetailsTest , if_no_option_is_specified_should_return_no_option_specified){
    Step1Link::reload();
    Step1Link::clear();
    Step1Link::send("details\n");
    EXPECT_EQ(Parser::extractResult(Step1Link::readLine()) , ERR_OPTION_NOT_SPECIFIED);
}

TEST(DetailsTest , move_should_return){
    Step1Link::reload();
    Step1Link::clear();
    Step1Link::send("details move\n");
    EXPECT_FALSE(Step1Link::readLine().isEmpty());
    Step1Link::clear();
}

TEST(DetailsTest , jog_should_return){
    Step1Link::reload();
    Step1Link::send("details jog\n");
    EXPECT_FALSE(Step1Link::readLine(-1).isEmpty());
}

TEST(DetailsTest , pos_should_return){
    Step1Link::reload();

    Step1Link::send("details pos\n");
    EXPECT_FALSE(Step1Link::readLine(-1).isEmpty());
}

TEST(DetailsTest , set_should_return){
    Step1Link::reload();

    Step1Link::send("details set\n");
    EXPECT_FALSE(Step1Link::readLine().isEmpty());
}

TEST(DetailsTest , running_should_return){
    Step1Link::reload();

    Step1Link::send("details running\n");
    EXPECT_FALSE(Step1Link::readLine().isEmpty());
}

TEST(DetailsTest , stop_should_return){
    Step1Link::reload();

    Step1Link::send("details stop\n");
    EXPECT_FALSE(Step1Link::readLine().isEmpty());
}

TEST(DetailsTest , settings_should_return){
    Step1Link::reload();

    Step1Link::send("details settings\n");
    EXPECT_FALSE(Step1Link::readLine().isEmpty());
}

TEST(DetailsTest , reset_should_return){
    Step1Link::reload();

    Step1Link::send("details reset\n");
    EXPECT_FALSE(Step1Link::readLine().isEmpty());
}

TEST(DetailsTest , check_should_return){
    Step1Link::reload();

    Step1Link::send("details check\n");
    EXPECT_FALSE(Step1Link::readLine().isEmpty());
}

TEST(DetailsTest , frequency_should_return){
    Step1Link::reload();

    Step1Link::send("details frequency\n");
    EXPECT_FALSE(Step1Link::readLine().isEmpty());
}

TEST(DetailsTest , reference_should_return){
    Step1Link::reload();

    Step1Link::send("details reference\n");
    EXPECT_FALSE(Step1Link::readLine().isEmpty());
}

TEST(DetaailsTest , rpm_should_return){
    Step1Link::reload();

    Step1Link::send("details reference\n");
    EXPECT_FALSE(Step1Link::readLine().isEmpty());
}

TEST(DetailsTest , override_should_return){
    Step1Link::reload();

    Step1Link::send("details override\n");
    EXPECT_FALSE(Step1Link::readLine().isEmpty());
}

TEST(DetailsTest , rest_should_return){
    Step1Link::reload();

    Step1Link::send("details rest\n");
    EXPECT_FALSE(Step1Link::readLine().isEmpty());
}

TEST(DetailsTest , wakeUp_should_return){
    Step1Link::reload();

    Step1Link::send("details wakeUp\n");
    EXPECT_FALSE(Step1Link::readLine().isEmpty());
}

TEST(DetailsTest , onLimit_should_return){
    Step1Link::reload();

    Step1Link::send("details onLimit\n");
    EXPECT_FALSE(Step1Link::readLine().isEmpty());
}

TEST(DetailsTest , version_should_return){
    Step1Link::reload();

    Step1Link::send("details version\n");
    EXPECT_FALSE(Step1Link::readLine().isEmpty());
}
