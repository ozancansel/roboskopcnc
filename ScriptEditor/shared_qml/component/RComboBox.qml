import QtQuick 2.9
import QtQuick.Controls 2.1
import "../"

ComboBox {

    property int contentHAlignment  :   Text.AlignLeft

    id      :   control
    width   :   Responsive.getH(200)
    height  :   Responsive.getV(100)
    font.pixelSize  :   Responsive.getV(28)
    font.family     :   "Linux Libertine"
    popup.background    :   Rectangle { color   :   "#111111"  }

    delegate:   ItemDelegate{
        id          :   delegateItem
        width       :   control.width
        height      :   Responsive.getH(70)

        contentItem :   Text {
            id                  :   delegateText
            text                :   key
            color               :   "white"
            font                :   control.font
            elide               :   Text.ElideRight
            verticalAlignment   :   Text.AlignVCenter
            horizontalAlignment :   contentHAlignment
            z                   :   2
            anchors.fill        :   parent
            leftPadding         :   Responsive.getH(20)
        }

        Rectangle{
            anchors.fill    :   parent
            color           :   pressed ? "#222222" : "#111111"
        }

        Rectangle{
            visible         :   index < control.count
            height          :   Responsive.getH(1)
            width           :   parent.width * 0.9
            anchors.horizontalCenter    :   parent.horizontalCenter
            color           :   "white"
            z               :   3
        }
    }

    indicator: Canvas {
        id              :   canvas
        x               :   control.width - width - control.rightPadding
        y               :   control.topPadding + (control.availableHeight - height) / 2
        width           :   control.font.pixelSize * 0.7
        height          :   width * 0.75
        contextType     :   "2d"

        Connections {
            target              :   control
            onPressedChanged    :   canvas.requestPaint()
        }

        onPaint: {
            context.reset();
            context.moveTo(0, 0);
            context.lineTo(width, 0);
            context.lineTo(width / 2, height);
            context.closePath();
            context.fillStyle = "white";
            context.fill();
        }
    }

    contentItem: Item{
        Text {
            anchors.verticalCenter  :   parent.verticalCenter
            leftPadding             : Responsive.getH(20)
            rightPadding            : control.indicator.width + control.spacing
            text                    : control.displayText
            font                    : control.font
            color                   :   "white"
            horizontalAlignment     :   Text.AlignLeft
            verticalAlignment       :   Text.AlignVCenter
            elide                   :   Text.ElideRight
        }
    }

    background: Rectangle {
        implicitWidth   :   control.width
        implicitHeight  :   control.down ? control.height + radius : control.height
        border.color    :   "#A8A8A8"
        border.width    :   Responsive.getV(1)
        radius          :   control.height * 0.1
        color           :   pressed ? "#222222" : "#111111"
    }
}
