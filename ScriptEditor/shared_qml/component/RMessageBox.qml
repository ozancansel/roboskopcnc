import QtQuick 2.7
import QtQuick.Controls 2.2
import "../"

Dialog  {

    property string     message     :   ""
    property string     headerText  :   ""
    property alias      messageFont :   connectingText.font
    property alias      headerFont  :   headerT.font

    id          :   messageDialog
    z           :   2
    modal       :   true
    closePolicy :   Dialog.NoAutoClose

    enter       :   Transition {
        NumberAnimation { property : "opacity"; from : 0.0; to : 1.0 }
    }

    header      :   Item    {

        id                  :   headerItem
        implicitHeight      :   Responsive.getV(185)

        Text {
            id              :   headerT
            text            :   headerText
            x               :   Responsive.getH(80)
            y               :   Responsive.getV(50)
            font.pixelSize  :   Responsive.getV(80)
            font.family     :   "Linux Libertine"
            color           :   "white"
        }

        Rectangle{
            id              :   bottomLine
            anchors.left    :   parent.left
            anchors.right   :   parent.right
            anchors.bottom  :   parent.bottom
            height          :   Responsive.getV(2)
        }

        RCloseButton{
            id                  :   closeDialog
            width               :   Responsive.getH(110)
            height              :   width
            anchors.right       :   parent.right
            anchors.rightMargin :   Responsive.getH(50)
            anchors.top         :   parent.top
            anchors.topMargin   :   Responsive.getV(40)
            onClicked           :   reject()
        }
    }

    contentItem :   Item {
        id              :   panel
        implicitHeight  :   connectingText.height + Responsive.getV(80)
        implicitWidth   :   connectingText.width + connectingText.x * 2

        Text {
            id                  :   connectingText
            x                   :   Responsive.getH(80)
            y                   :   Responsive.getV(70)
            text                :   message
            font.pixelSize      :   Responsive.getV(40)
            color               :   "white"
            font.family     :   "Linux Libertine"
        }
    }

    footer      :   DialogButtonBox {
        id              :   dialogBox
        standardButtons :   messageDialog.standardButtons
        position        :   DialogButtonBox.Footer
        alignment       :   Qt.AlignTop | Qt.AlignRight
        spacing         :   Responsive.getH(50)
        implicitHeight  :   Responsive.getV(190)
        anchors.right   :   parent.right
        anchors.rightMargin     :   Responsive.getH(110)

        RButton {
            implicitHeight              :   Responsive.getV(120)
            implicitWidth               :   Responsive.getH(240)
            text                        :   "Evet"
            font.pixelSize              :   Responsive.getV(50)
            DialogButtonBox.buttonRole  :   DialogButtonBox.AcceptRole
        }

        RButton {
            implicitHeight              :   Responsive.getV(120)
            implicitWidth               :   Responsive.getH(240)
            text                        :   "Hayır"
            font.pixelSize              :   Responsive.getV(50)
            DialogButtonBox.buttonRole  :   DialogButtonBox.RejectRole
        }
        background      :   Item {  }
    }

    background  :   BackgroundPanel {
        height          :   messageDialog.height
        width           :   messageDialog.width
        borderVisible   :   true
    }
}
