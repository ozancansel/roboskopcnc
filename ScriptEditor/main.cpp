#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDir>
#include <QDebug>
#include <QQmlContext>
#ifdef Q_OS_ANDROID
#include <QtWebView>
#endif
#include "cnc/axis3machine.h"
#include "axis3macroengine.h"
#include "serial/bluetoothport.h"
#include "serial/serialport.h"
#include "platform.h"
#include "blocklyenginefiles.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

#ifdef Q_OS_ANDROID
//    QtWebView::initialize();
#endif

    QCoreApplication::setOrganizationName("MachInventor");
    QCoreApplication::setOrganizationDomain("www.machinventor.com");
    QCoreApplication::setApplicationName("MachInventor");

    //Registers qml types
    Axis3Machine::registerQmlType();
    Axis3MacroEngine::registerQmlType();
    BluetoothPort::registerQmlType();
    SerialPort::registerQmlType();
    SerialComm::registerQmlType();
    Platform::registerQmlType();
    BlocklyEngineFiles::registerQmlType();

    //Creating engine
    QQmlApplicationEngine engine;

    //Starting app
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
