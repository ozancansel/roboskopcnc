import QtQuick 2.0
import QtQuick.Controls 2.2
import RLib 1.0
import QtWebView 1.1
import ScriptEditor 1.0
import "../shared_qml/component"

PageTemplate {

    property Axis3MacroEngine engine        :   ({})
    property SerialComm     port            :   ({})
    property WebView        blocklyEngineWeb:   ({})
    property Database       db              :   ({})
    property CurrentProgram currentProgram  :   ({})

    id              :   controlPage
    headerText      :   "MachInventor"
    headerBackground.opacity    :   0.7

    signal swipeToBluetoothPage();

    MouseArea   {
        width       :   parent.width
        height      :   Responsive.getV(100)
    }

    RMessageBox{
        id          :   connectToStartWarning
        headerText  :   "Bağlantı Yok !"
        message     :   "Programı başlatmak için bağlantı sekmesine geçerek\nherhangi bir MachInventor cihazına bağlanın."
        onAccepted  :   swipeToBluetoothPage()
        x           :   parent.width / 2 - width / 2
        y           :   parent.height / 2 - height / 2
    }



    Item    {
        width           :   Math.max(Responsive.getH(1650) , currentProgramDisplayText.width + Responsive.getH(120))
        height          :   Responsive.getV(200)
        y               :   Responsive.getV(200)
        anchors.horizontalCenter    :   parent.horizontalCenter

        BackgroundPanel{
            id              :   currentProgramPanel
            borderVisible   :   currentProgram.name !== ""
            opacity         :   0.7
            anchors.fill    :   parent
        }

        Text {
            id                      :   currentProgramDisplayText
            x                       :   Responsive.getH(30)
            text                    :   "Program : " + (currentProgram.name === "" ? "Henüz açılmadı." : currentProgram.name)
            font.family             :   "Linux Libertine"
            color                   :   "white"
            font.pixelSize          :   Responsive.getV(60)
            anchors.centerIn    :   parent
        }
    }

    RButton {
        id              :   startButton2
        width           :   Responsive.getH(1650)
        height          :   Responsive.getV(550)
        text            :   engine.running ? "Durdur" : "Başla"
        font.pixelSize  :   Responsive.getV(80)
        anchors.horizontalCenter    :   parent.horizontalCenter
        y               :   Responsive.getV(420)
        buttonType      :   engine.running ? error : normal
        background.opacity  :   0.9
        onClicked       :   {
            if(port.state !== SerialComm.Connected){
                connectToStartWarning.open()
                return
            }

            //If engine running
            if(engine.running){
                //Abort
                engine.abortExecution()
                engine.machine.reset()
            } else {
                //If engine is idle
                //Clear serial buffer firstly
                port.clearBuffer()
                //Then generate the code
                blocklyEngineWeb.runJavaScript("generate();" , function(result){

                    var def = "machine.unlock();\n";

                    //Start the engine
                    engine.script = def + result;
                    engineRunTimer.start();
                });
            }
        }
    }

    Connections{
        target  :   engine
        onRunningChanged    :   {
            currentProgram.isRunning = engine.running
        }
    }

    Timer{
        id          :   engineRunTimer
        interval    :   0
        onTriggered :   engine.run()
    }
}
