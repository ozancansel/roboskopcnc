import QtQuick 2.0

Item {

    id      :   item

    property string name  :   ""
    property string xml             :   ""
    property bool   isRunning       :   false

    function open(n , xml){
        item.xml = xml
        name = n
        isRunning = false
    }

    function close(){
        item.xml = ""
        name = ""
        isRunning = false
    }
}
