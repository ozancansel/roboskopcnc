import QtQuick 2.0
import QtQuick.LocalStorage 2.0

Item {
    function init(){
        var db = LocalStorage.openDatabaseSync("AppInventor_DB" , "" , "AppInventor" , 10000000)
        try{
            db.transaction(function(tx){
//              tx.executeSql('DROP TABLE script')
              tx.executeSql('CREATE TABLE IF NOT EXISTS script (id INTEGER PRIMARY KEY AUTOINCREMENT, name text,xml text)')
            })
        } catch(err){
            console.log(err)
        }
    }

    function getHandle()
    {
        try {
            var db = LocalStorage.openDatabaseSync("AppInventor_DB", "",
                                                   "Track exercise", 1000000)
        } catch (err) {
            console.log("Error opening database: " + err)
        }
        return db
    }

    function insertScriptIfNotExists(scriptName , xml){
        var db = getHandle()
        var rowid = 0;
        db.transaction(function(tx){
            var exists = tx.executeSql('SELECT count(*) FROM script WHERE name=?' , [scriptName]).rows.item(0)["count(*)"]
            if(exists > 0){
                rowid = -1;
                return
            }
            tx.executeSql('INSERT INTO script (name , xml) VALUES(? , ?)' ,
                          [scriptName , xml])
            var result = tx.executeSql('SELECT last_insert_rowid()')
            rowid = result.insertId
        })
        return rowid
    }

    function updateScript(scriptName , xml){
        var db = getHandle()
        db.transaction(function(tx){
            var exists = tx.executeSql('UPDATE script SET xml=? WHERE name=?' , [ xml , scriptName ]);
            console.log(exists)
        })
    }

    function getScriptById(id){
        var db = getHandle()
        var script;

        db.transaction(function (tx) {
            var results = tx.executeSql( 'SELECT id,name,xml FROM script WHERE id=?' , [id] )
            var le = results.rows.length;
            if(le > 0)
                script = results.rows.item(0)
        })

        return script
    }

    function readAllScripts()
    {
        var db = getHandle()
        var scripts = [];

        db.transaction(function (tx) {
            var results = tx.executeSql(
                        'SELECT rowid,name,xml FROM script order by rowid desc')
            for (var i = 0; i < results.rows.length; i++) {
                scripts.push(results.rows.item(i))
            }
        })

        return scripts;
    }

    function deleteScript(id){
        var db = getHandle()

        db.transaction(function(tx){
            var result = tx.executeSql('DELETE FROM script WHERE id=?' , [id])
        })
    }
}
