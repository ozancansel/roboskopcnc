import QtQuick 2.0
import QtWebView 1.1
import ScriptEditor 1.0
import "../qml"

Item {

    property BlocklyEngineFiles engineFiles     :   ({})
    property CurrentProgram     currentProgram  :   ({})
    property alias  blocklyEngine               :   web

    WebView {
        id              :   web
        y               :   Responsive.getV(5)
        anchors.fill    :   parent
        url             :   engineFiles.indexHtmlPath
    }

    Connections{
        target  :   currentProgram
        onNameChanged    :   {
            //Xml to blocks
            web.runJavaScript("bindXml('" + currentProgram.xml + "');" , function(result){

            });
        }
    }
}
