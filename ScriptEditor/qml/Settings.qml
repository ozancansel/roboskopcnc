import QtQuick 2.9
import QtQuick.Controls 2.1
import RLib 1.0
import "component"
import "../shared_qml"
import "../shared_qml/component"

PageTemplate {

    property SerialComm port    :   ({})

    id          :   settingsPage
    headerText  :   "Ayarlar"
    headerBackground.opacity    :   0.7

    BackgroundPanel{
        width               :   Responsive.getH(1000)
        height              :   Responsive.getV(500)
        anchors.centerIn    :   parent
    }

    Grid    {
        y       :   Responsive.getV(240)
        spacing :   Responsive.getV(30)
        anchors.horizontalCenter    :   parent.horizontalCenter
        columns :   2
        visible :   port.state === SerialComm.Connected

        SettingsSwitch  {
            id      :   sw
            width   :   Responsive.getH(800)
            height  :   Responsive.getV(150)
            font.pixelSize  :   Responsive.getV(40)
        }

        SettingsSwitch  {
            width   :   Responsive.getH(800)
            height  :   Responsive.getV(150)
            font.pixelSize  :   Responsive.getV(40)
        }

        SettingsSwitch  {
            width   :   Responsive.getH(800)
            height  :   Responsive.getV(150)
            font.pixelSize  :   Responsive.getV(40)
        }
    }
}
