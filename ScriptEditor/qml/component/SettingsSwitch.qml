import QtQuick 2.6
import "../../shared_qml"
import "../../shared_qml/component"

SettingsCompBase {

    id  :   settingsSwitchControl


    Item{
        anchors.verticalCenter  :   parent.verticalCenter
        anchors.right           :   parent.right
        anchors.rightMargin     :   Responsive.getH(20)
        height                  :   parent.height * 0.5
        width                   :   parent.width / 3 - anchors.rightMargin

        RSwitch{
            anchors.left        :   parent.left
            width               :   parent.width * 0.8
            height              :   parent.height
        }
    }
}
