#ifndef AXIS3MACROENGINE_H
#define AXIS3MACROENGINE_H

#include "script/macroengine.h"
#include "cnc/axis3machine.h"

class Axis3MacroEngine : public MacroEngine
{

    Q_OBJECT
    Q_PROPERTY(Axis3Machine* machine READ machine WRITE setMachine NOTIFY machineChanged)

public:

    static void     registerQmlType();
    Axis3MacroEngine(QQuickItem* parent = nullptr);

    Axis3Machine*   machine();
    void            setMachine(Axis3Machine* machine);

signals:

    void            machineChanged();

protected:

    void            beforeRun();

private:

    Axis3Machine*   m_machine;

};

#endif // AXIS3MACROENGINE_H
