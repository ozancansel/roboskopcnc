QT += quick script serialport bluetooth webview
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

sharedQml.commands = cp -rf $$absolute_path($$PWD/../Lib/qml/*) $$PWD/shared_qml

QMAKE_EXTRA_TARGETS += sharedQml
PRE_TARGETDEPS += sharedQml

SOURCES += main.cpp \
    axis3macroengine.cpp \
    blocklyenginefiles.cpp \
    step3machine.cpp \

RESOURCES +=    qml.qrc \
                res.qrc \
                editor.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML2_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML2_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Lib/release/ -lLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Lib/debug/ -lLib
else:unix: LIBS += -L$$OUT_PWD/../Lib/ -lLib

INCLUDEPATH += $$PWD/../Lib
DEPENDPATH += $$PWD/../Lib

DISTFILES += \
    main.qml \
    qml/Control.qml \
    shared_qml/Connection.qml \
    shared_qml/PageTemplate.qml \
    shared_qml/Responsive.qml \
    shared_qml/Config.qml \
    shared_shared_qml/component/BackgroundPanel.qml \
    shared_shared_qml/component/RButton.qml \
    shared_shared_qml/component/RCloseButton.qml \
    shared_qml/component/RDelayButton.qml \
    shared_qml/component/RDoubleSpinBox.qml \
    shared_qml/component/RDoubleSpinText.qml \
    shared_qml/component/RIconButton.qml \
    shared_qml/component/RMessageBox.qml \
    shared_qml/component/RRadioButton.qml \
    shared_qml/component/RSpinBox.qml \
    shared_qml/component/RSpinner.qml \
    shared_qml/component/RStageVisualizer.qml \
    shared_qml/component/RToastMessage.qml \
    shared_qml/component/RToolTip.qml \
    shared_qml/component/RDialogTemplate.qml \
    shared_qml/component/RTextArea.qml \
    shared_qml/component/RTextField.qml \
    shared_qml/component/RBackground.qml \
    shared_qml/component/RComboBox.qml \
    shared_qml/component/RSwitch.qml \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    qml/Programmer.qml \
    qml/Database.qml \
    qml/ScriptList.qml \
    qml/CurrentProgram.qml \
    qml/Settings.qml \
    qml/SettingsSpinControl.qml \
    qml/component/SettingsSwitch.qml \
    qml/component/SettingsCompBase.qml

HEADERS += \
    axis3macroengine.h \
    blocklyenginefiles.h \
    step3machine.h

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
