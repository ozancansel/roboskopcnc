#include "axis3macroengine.h"
#include <QScriptValue>
#include <QScriptEngine>
#include <QFile>
#include <QDebug>

void Axis3MacroEngine::registerQmlType(){
    qmlRegisterType<Axis3MacroEngine>("ScriptEditor" , 1 , 0 , "Axis3MacroEngine");
}

Axis3MacroEngine::Axis3MacroEngine(QQuickItem* parent)
    :
      MacroEngine(parent) ,
      m_machine(nullptr)
{
    QFile   f(":/res/script/script1");

    if(f.open(QIODevice::ReadOnly)){
        setScript(f.readAll());
    }
}

//Getter
Axis3Machine* Axis3MacroEngine::machine(){
    return m_machine;
}

//Setter
void Axis3MacroEngine::setMachine(Axis3Machine *machine){
    m_machine = machine;

    QScriptValue val = engine()->newQObject(m_machine);
    engine()->globalObject().setProperty("machine" , val);
    emit machineChanged();
}

void Axis3MacroEngine::beforeRun(){

}
