#include "macrohighlighteritem.h"

void MacroHighlighterItem::registerQmlType(){
    qmlRegisterType<MacroHighlighterItem>("SingleStage" , 1 , 0 , "MacroHighlighter");
}

MacroHighlighterItem::MacroHighlighterItem(QQuickItem* parent) : QQuickItem(parent)
{

}

//Getter
QQuickTextDocument* MacroHighlighterItem::doc(){
    return m_doc;
}

//Setter
void MacroHighlighterItem::setDoc(QQuickTextDocument *doc){
    m_doc = doc;
    m_highlighter.setDoc(m_doc);
    emit docChanged();
}
