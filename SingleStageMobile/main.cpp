#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "serial/serialcomm.h"
#include "serial/bluetoothport.h"
#include "serial/serialport.h"
#include "cnc/grblmessageparser.h"
#include "cnc/grblsettings.h"
#include "cnc/stageproxy.h"
#include "script/stagemacroengine.h"
#include "platform.h"
#include "singlestageappsettings.h"
#include "macrohighlighteritem.h"
#include "script/stagemacroengine.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QCoreApplication::setOrganizationName("Roboskop Endüstriyel");
    QCoreApplication::setOrganizationDomain("www.roboskop.com");
    QCoreApplication::setApplicationName("Roboskop Kızak Kontrol");

    SerialComm::registerQmlType();
    SerialPort::registerQmlType();
    BluetoothPort::registerQmlType();
    GrblMessageParser::registerQmlType();
    GrblSettings::registerQmlType();
    StageProxy::registerQmlType();
    Platform::registerQmlType();
    SingleStageAppSettings::registerQmlType();
    StageMacroEngine::registerQmlType();
    MacroHighlighterItem::registerQmlType();

    QQmlApplicationEngine engine;

    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
