#ifndef SINGLESTAGEAPPSETTINGS_H
#define SINGLESTAGEAPPSETTINGS_H

#include <QSettings>

#define DECIMALS_KEY        "decimals"
#define FEED_RATE_MAX_KEY   "feedRateMax"
#define FEED_RATE_KEY       "feedRate"
#define FEED_RATE_MIN_KEY   "feedRateMin"
#define MOVEMENT_RADIO_IDX_KEY  "movementRadioIdx"
#define POS_MIN_KEY         "posMin"
#define POS_MAX_KEY         "posMax"
#define SYS_UNIT_KEY        "sysUnit"

class SingleStageAppSettings : public QSettings
{

    Q_OBJECT
    Q_ENUMS(Units)
    Q_PROPERTY(int decimals READ decimals WRITE setDecimals NOTIFY decimalsChanged)
    Q_PROPERTY(int feedRateMax READ feedRateMax WRITE setFeedRateMax NOTIFY feedRateMaxChanged)
    Q_PROPERTY(int feedRate READ feedRate WRITE setFeedRate NOTIFY feedRateChanged)
    Q_PROPERTY(int feedRateMin READ feedRateMin WRITE setFeedRateMin NOTIFY feedRateMinChanged)
    Q_PROPERTY(int movementRadioIdx READ movementRadioIdx WRITE setMovementRadioIdx NOTIFY movementRadioIdxChanged)
    Q_PROPERTY(int posMax READ posMax WRITE setPosMax NOTIFY posMaxChanged)
    Q_PROPERTY(int posMin READ posMin WRITE setPosMin NOTIFY posMinChanged)
    Q_PROPERTY(float unitMultiplier READ unitMultiplier NOTIFY unitMultiplierChanged)
    Q_PROPERTY(Units sysUnit READ sysUnit WRITE setSysUnit NOTIFY sysUnitChanged)

public:

    static void registerQmlType();
    SingleStageAppSettings(QObject* parent = nullptr);
    enum Units{ MM = 0, CM = 1 };

    //Getter
    int     decimals();
    int     feedRateMax();
    int     feedRate();
    int     feedRateMin();
    int     movementRadioIdx();
    int     posMax();
    int     posMin();
    float   unitMultiplier();
    Units   sysUnit();

    //Setter
    void    setDecimals(int val);
    void    setFeedRateMax(int val);
    void    setFeedRate(int val);
    void    setFeedRateMin(int val);
    void    setMovementRadioIdx(int val);
    void    setPosMax(int val);
    void    setPosMin(int val);
    void    setSysUnit(Units unit);

public slots:

    void    save();

signals:

    void    decimalsChanged();
    void    feedRateMaxChanged();
    void    feedRateChanged();
    void    feedRateMinChanged();
    void    movementRadioIdxChanged();
    void    posMinChanged();
    void    posMaxChanged();
    void    unitMultiplierChanged();
    void    sysUnitChanged();

};

#endif // SINGLESTAGEAPPSETTINGS_H
