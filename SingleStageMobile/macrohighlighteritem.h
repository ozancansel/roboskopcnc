#ifndef MACROHIGHLIGHTERITEM_H
#define MACROHIGHLIGHTERITEM_H

#include <QQuickItem>
#include <QQuickTextDocument>
#include "macrohighlighter.h"

class MacroHighlighterItem : public QQuickItem
{

    Q_OBJECT
    Q_PROPERTY(QQuickTextDocument* doc READ doc WRITE setDoc NOTIFY docChanged)

public:

    static void         registerQmlType();
    MacroHighlighterItem(QQuickItem* parent = nullptr);
    QQuickTextDocument* doc();
    void                setDoc(QQuickTextDocument* doc);

signals:

    void    docChanged();

private:

    QQuickTextDocument* m_doc;
    MacroHighlighter    m_highlighter;

};

#endif // MACROHIGHLIGHTERITEM_H
