#ifndef MACROHIGHLIGHTER_H
#define MACROHIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QTextCharFormat>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QQuickTextDocument>

class MacroHighlighter : public QSyntaxHighlighter
{

public:

    MacroHighlighter(QTextDocument* parent = nullptr);
    QQuickTextDocument* doc();
    void                setDoc(QQuickTextDocument* val);

protected   :

    void highlightBlock(const QString &text) override;

private:

    struct HighlightingRule
    {
        QRegularExpression pattern;
        QTextCharFormat format;
    };

    QRegularExpression m_gitExpr;
    QRegularExpression m_bekleExpr;

    QTextCharFormat m_gitCharFormat;
    QTextCharFormat m_bekleCharFormat;

    QList<HighlightingRule> m_rules;

    QQuickTextDocument*     m_doc;

};

#endif // MACROHIGHLIGHTER_H
