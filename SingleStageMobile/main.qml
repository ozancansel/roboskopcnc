import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.0
import RLib 1.0
import SingleStage 1.0
import "qml"
import "shared_qml"
import "shared_qml/component"

ApplicationWindow {

    readonly property SerialComm    port    :   Platform.isMobile ? bt : serial

    id          :   mainWindow
    visible     :   true
    width       :   640
    height      :   480
    visibility  :   Window.FullScreen
    title       :   qsTr("Roboskop Step-1 Kontrol Uygulaması")

    onClosing           :   applicationSettings.save()

    SerialPort  {
        id              :   serial
        debugEnabled    :   true
        baud            :   SerialComm.B9600
    }

    BluetoothPort   {
        id              :   bt
        debugEnabled    :   true
    }

    StageProxy  {
        id              :   proxy
        comm            :   port
        parser          :   parser
        grblSettings    :   grblSettings
    }

    GrblSettings    {
        id              :   grblSettings
        comm            :   port
        parser          :   parser
    }

    GrblMessageParser   {
        id              :   parser
        comm            :   port
    }

    SingleStageAppSettings{
        id              :   applicationSettings
    }

    background  :   Rectangle{
        width   :   mainWindow.width
        height  :   mainWindow.height
        color   :   "#383838"
    }

    FontLoader{
        source  :   "/res/font/LinLibertine_Re-4.7.5.ttf"
    }

    SwipeView   {
        id              :   swipeView
        currentIndex    :   tabBar.currentIndex
        anchors.fill    :   parent

        Controller{
            id              :   controller
            stageProxy      :   proxy
            appSettings     :   applicationSettings
            view            :   swipeView
            isCurrentView   :   swipeView.currentIndex === 0
            stageSettings   :   grblSettings
        }

        FastControl{
            id              :   fastControl
            stage           :   proxy
            grblSettings    :   grblSettings
            settings        :   applicationSettings
        }

        Connection  {
            id      :   connection
            comm    :   port
        }

        Settings    {
            id              :   settings
            settings        :   grblSettings
            swipe           :   swipeView
            isCurrentView   :   swipeView.currentIndex === 3
            appSettings     :   applicationSettings
            comm            :   port
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("Kontrol Detay")
        }
        TabButton {
            text: qsTr("Hızlı Kontrol")
        }
        TabButton {
            text: qsTr("Baglantı")
        }
        TabButton{
            text: qsTr("Ayarlar")
        }
    }
}
