#include "macrohighlighter.h"

MacroHighlighter::MacroHighlighter(QTextDocument* parent)
    :
      QSyntaxHighlighter(parent)
{

    m_gitExpr= QRegularExpression("git\\s*\\(\\s*\\d+\\s*,\\s*\\d+s*\\)");
    m_bekleExpr = QRegularExpression("bekle\\s*\\(\\s*\\d+\\s*\\)");

    m_gitCharFormat.setForeground(QColor("red"));
    m_bekleCharFormat.setForeground(QColor("green"));
    HighlightingRule gitRule;

    gitRule.format = m_gitCharFormat;
    gitRule.pattern = m_gitExpr;

    m_rules.append(gitRule);

    HighlightingRule bekleRule;

    bekleRule.format = m_bekleCharFormat;
    bekleRule.pattern = m_bekleExpr;

    m_rules.append(bekleRule);
}


//Getter
QQuickTextDocument* MacroHighlighter::doc(){
    return m_doc;
}

//setter
void MacroHighlighter::setDoc(QQuickTextDocument *val){
    m_doc = val;

    if(val != nullptr){
        setDocument(val->textDocument());
    }
}

//Virtual override
void MacroHighlighter::highlightBlock(const QString &text){
    foreach (const HighlightingRule &rule, m_rules) {
        QRegularExpressionMatch matchResult = rule.pattern.match(text);
        if(matchResult.hasMatch()){
            int start = matchResult.capturedStart(0);
            int end = matchResult.capturedEnd(0);
            setFormat(start , end - start , rule.format);
        }
    }
}
