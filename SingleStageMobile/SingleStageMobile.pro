QT += quick bluetooth serialport script
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    singlestageappsettings.cpp \
    macrohighlighter.cpp \
    macrohighlighteritem.cpp

RESOURCES += qml.qrc \
    res.qrc
sharedQml.commands = cp -rf $$absolute_path($$PWD/../Lib/qml/*) $$PWD/shared_qml

QMAKE_EXTRA_TARGETS += sharedQml
PRE_TARGETDEPS += sharedQml

# Additional import path used to resolve QML modules in Qt Creator's code model
QML2_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    main.qml \
    qml/Controller.qml \
    qml/component/BackgroundPanel.qml \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    qml/Settings.qml \
    qml/FastControl.qml \
    qml/MacroEditor.qml

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Lib/release/ -lLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Lib/debug/ -lLib
else:unix: LIBS += -L$$OUT_PWD/../Lib/ -lLib

INCLUDEPATH += $$PWD/../Lib
DEPENDPATH += $$PWD/../Lib

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

HEADERS += \
    singlestageappsettings.h \
    macrohighlighter.h \
    macrohighlighteritem.h
