#include "singlestageappsettings.h"
#include <QtQml>

void SingleStageAppSettings::registerQmlType(){
    qmlRegisterType<SingleStageAppSettings>("SingleStage" , 1 , 0 , "SingleStageAppSettings");
}

SingleStageAppSettings::SingleStageAppSettings(QObject* parent) : QSettings(parent)
{
    setDefaultFormat(QSettings::IniFormat);
}

//Getter
int SingleStageAppSettings::decimals(){
    return value(DECIMALS_KEY , 2).toInt();
}

int SingleStageAppSettings::feedRateMax(){
    return value(FEED_RATE_MAX_KEY , 500).toInt();
}

int SingleStageAppSettings::feedRate(){
    return value(FEED_RATE_KEY , 200).toInt();
}

int SingleStageAppSettings::feedRateMin(){
    return value(FEED_RATE_MIN_KEY , 1).toInt();
}

int SingleStageAppSettings::movementRadioIdx(){
    return value(MOVEMENT_RADIO_IDX_KEY , 2).toInt();
}

int SingleStageAppSettings::posMin(){
    return value(POS_MIN_KEY).toInt();
}

int SingleStageAppSettings::posMax(){
    return value(POS_MAX_KEY).toInt();
}

float SingleStageAppSettings::unitMultiplier(){
    Units currUnit = (Units)value(SYS_UNIT_KEY).toInt();
    if(currUnit == SingleStageAppSettings::CM)
        return 10;

    return 1;
}

SingleStageAppSettings::Units SingleStageAppSettings::sysUnit(){
    return (Units)value(SYS_UNIT_KEY).toInt();
}

//Setter
void SingleStageAppSettings::setDecimals(int val){
    setValue(DECIMALS_KEY , QVariant(val));
    emit decimalsChanged();
}

void SingleStageAppSettings::setFeedRateMax(int val){
    if(val != value(FEED_RATE_MAX_KEY).toInt()){
        setValue(FEED_RATE_MAX_KEY , QVariant(val));
        emit feedRateMaxChanged();
    }
}

void SingleStageAppSettings::setFeedRate(int val){
    if(val != value(FEED_RATE_KEY).toInt()){
        setValue(FEED_RATE_KEY , QVariant(val));
        emit feedRateChanged();
    }
}

void SingleStageAppSettings::setFeedRateMin(int val){
    if(val != value(FEED_RATE_MIN_KEY).toInt()){
        setValue(FEED_RATE_MIN_KEY , QVariant(val));
        emit feedRateMinChanged();
    }
}

void SingleStageAppSettings::setMovementRadioIdx(int val){
    if(val != value(MOVEMENT_RADIO_IDX_KEY).toInt()){
        setValue(MOVEMENT_RADIO_IDX_KEY , QVariant(val));
        emit movementRadioIdxChanged();
    }
}

void SingleStageAppSettings::setPosMax(int val){
    if(val != value(POS_MAX_KEY).toInt()){
        setValue(POS_MAX_KEY , QVariant(val));
        emit posMaxChanged();
    }
}

void SingleStageAppSettings::setPosMin(int val){
    if(val != value(POS_MIN_KEY).toInt()){
        setValue(POS_MIN_KEY , QVariant(val));
        emit posMinChanged();
    }
}

void SingleStageAppSettings::setSysUnit(Units unit){
    if(unit != (Units)value(SYS_UNIT_KEY).toInt()){
        setValue(SYS_UNIT_KEY , QVariant(unit));
        emit sysUnitChanged();
        emit unitMultiplierChanged();
    }
}

//Slots
void SingleStageAppSettings::save(){
    //Ayarlar yaziliyor
    sync();
}
