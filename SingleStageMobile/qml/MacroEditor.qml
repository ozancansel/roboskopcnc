import QtQuick 2.0
import QtQuick.Controls 2.2
import RLib 1.0
import SingleStage 1.0
import "../qml"
import "component"

Item {

    property StageMacroEngine   engine  :   ({})

    id          :   macroEditor

//    Rectangle{
//        anchors.fill        :   parent
//        color               :   "#383838"
//    }

    FontLoader{
        id          :   editorFont
        source      :   "/res/font/inconsolata/inconsolata.otf"
    }

    MacroHighlighter{
        id          :   highligter
        doc         :   editorTextArea.textDocument
    }

    BackgroundPanel {
        id          :   headerPanel
        y           :   Responsive.getV(20)
        width       :   Responsive.getH(1850)
        height      :   Responsive.getV(150)
        anchors.horizontalCenter    :   parent.horizontalCenter

        Text {
            id              :   headerText
            text            :   qsTr("Makro Editörü")
            color           :   "white"
            font.pixelSize  :   Responsive.getV(80)
            anchors.centerIn:   parent
        }


        Row{
            anchors.right       :   parent.right
            anchors.rightMargin :   Responsive.getH(30)
            spacing             :   Responsive.getH(20)
            RIconButton{
                height      :   Responsive.getV(150)
                width       :   Responsive.getH(110)
                source      :   "/res/icon/saved.png"
            }

            RIconButton{
                height      :   Responsive.getV(150)
                width       :   Responsive.getH(110)
                source      :   "/res/icon/compile.png"
            }

            RIconButton{
                height      :   Responsive.getV(150)
                width       :   Responsive.getH(110)
                source      :   "/res/icon/loop.png"
                onClicked   :   engine.run(editorTextArea.text)
            }
        }
    }


    Flickable {
        //Readonly
        readonly property real  logoOpacity :   0.05
        readonly property real  logoWidth   :   0.4

        id                  :   flickable
        flickableDirection  :   Flickable.VerticalFlick
        width               :   Responsive.getH(1850)
        height              :   Responsive.getH(750)
        anchors.horizontalCenter    :   parent.horizontalCenter
        y                   :   Responsive.getV(220)

        TextArea.flickable  :   TextArea    {

            id                  :   editorTextArea
            persistentSelection :   true
            selectByKeyboard    :   true
            selectByMouse       :   true
            selectionColor      :   "lightsteelblue"
            y                   :   Responsive.getV(20)
            x                   :   Responsive.getH(20)

            background      :   Rectangle   {
                id          :   editorBackground
                color       :   "white"
                radius      :   Responsive.getH(30)
            }
            font.pixelSize  :   Responsive.getV(30)
            font.family     :   editorFont.name
            text            :   "git(50 , 200)\nbekle(500)\ngit(10 , 350)\nbekle(200)\n"
        }

        ScrollBar.vertical  :   ScrollBar   {   id  :   verticalScrollBar  }
    }
}
