import QtQuick 2.0
import RLib 1.0
import SingleStage 1.0
import "../shared_qml/component"
import "../qml"

Item {

    property StageProxy     stage           :   ({})
    property GrblSettings   grblSettings    :   ({})
    property SingleStageAppSettings settings:   ({})
    readonly property int multiplier        :   grblSettings.homingCycleEnabled ? -1 : 1
    property string         enteredNumber   :   ""

    Rectangle   {
        id          :   background
        anchors.fill:   parent
        color       :   "#383838"
    }

    FontLoader  {
        id          :   numericFont
        source      :   "/res/font/digital-7/digital-7.ttf"
    }

    function go(x){
        var pos = stage.position
        var movement = (x * multiplier) - pos
        stage.move(movement , settings.feedRate)
    }

    function rebindPos(){
        stagePosition.text = Qt.binding(function(){
            return  ((stage.position * multiplier) / settings.unitMultiplier).toFixed(settings.decimals)
        });
    }

    Timer   {
        id          :   toPosTimer
        interval    :   2000
        onTriggered :   {
            rebindPos()
        }
    }

    Row {
        x                       :   Responsive.getH(20)
        y                       :   Responsive.getV(20)
        spacing                 :   Responsive.getH(40)

        Column{
            spacing         :   Responsive.getV(20)

            BackgroundPanel {
                id                  :   posDisplay
                width               :   Responsive.getH(650)
                height              :   Responsive.getV(700)

                Text {
                    id                  :   stagePosition
                    text                :   {
                        return ((stage.position * multiplier) / settings.unitMultiplier).toFixed(settings.decimals)
                    }
                    font.family         :   numericFont.name
                    font.pixelSize      :   Responsive.getV(500)
                    anchors.fill        :   parent
                    anchors.margins     :   height * 0.1
                    color               :   "white"
                    verticalAlignment   :   Text.AlignVCenter
                    horizontalAlignment :   Text.AlignHCenter
                    fontSizeMode        :   Text.Fit
                }

                color           :   {
                    if(stage.comm.state !== SerialComm.Connected)
                        return "#191919"

                    if(stage.machineState === StageProxy.Idle)
                        return "#191919"
                    else if(stage.machineState === StageProxy.Run)
                        return "#1E90FF"
                    else if(stage.machineState === StageProxy.Alarm)
                        return "#ff1a1a"
                    else if(stage.machineState === StageProxy.Jog)
                        return "#1E90FF"
                    else if(stage.machineState === StageProxy.Hold)
                        return "#ffff66"
                    else if(stage.machineState === StageProxy.Unknown)
                        return "white"
                }
            }

            RButton {
                id              :   calibrateButton
                width           :   Responsive.getH(650)
                height          :   Responsive.getV(300)
                text            :   stage.machineState === StageProxy.Hold ? "Devam" : "Dur"
                font.pixelSize  :   Responsive.getV(250)
                onClicked       :   {
                    if(stage.machineState === StageProxy.Hold)
                        stage.resume()
                    else if (stage.machineState === StageProxy.Run ||
                             stage.machineState === StageProxy.Jog ||
                             stage.machineState === StageProxy.Idle )
                        stage.hold()
                }
            }
        }

        Row{
            spacing             :   Responsive.getH(20)
            Column{

                Grid{
                    id          :   numericGrid
                    rows        :   4
                    columns     :   3
                    spacing     :   Responsive.getV(10)
                    Repeater{
                        model   :   9

                        RButton {
                            id  :   numberButton
                            width   :   Responsive.getH(300)
                            height  :   Responsive.getV(250)
                            text    :   index + 1
                            font.pixelSize  :   Responsive.getV(180)
                            font.family     :   numericFont.name
                            onClicked       :   {
                                enteredNumber = enteredNumber.concat(numberButton.text)
                                stagePosition.text = enteredNumber
                                toPosTimer.restart()
                            }
                        }
                    }
                }

                Row{
                    spacing     :   Responsive.getV(10)

                    RButton{
                        id                  :   clearButton
                        width               :   Responsive.getH(300)
                        height              :   Responsive.getV(250)
                        text                :   "Temizle"
                        font.pixelSize      :   Responsive.getV(50)
                        onClicked           :   {
                            enteredNumber = ""
                            stagePosition.text = enteredNumber
                        }
                    }

                    RButton{
                        width       :   Responsive.getH(300)
                        height      :   Responsive.getV(250)
                        text        :   "0"
                        font.family :   numericFont.name
                        font.pixelSize  :   Responsive.getV(180)
                        onClicked   :   {
                            enteredNumber = enteredNumber.concat("0")
                            stagePosition.text = enteredNumber
                            toPosTimer.restart()
                        }
                    }

                    RButton{
                        width       :   Responsive.getH(300)
                        height      :   Responsive.getV(250)
                        text        :   "."
                        font.pixelSize  :   Responsive.getV(180)
                        onClicked   :   {
                            if(enteredNumber.length == 0)
                                return
                            if(enteredNumber.indexOf(".") > 0){
                                return
                            } else {
                                enteredNumber += "."
                            }

                            stagePosition.text = enteredNumber

                            toPosTimer.restart()
                        }
                    }
                }
            }

            RButton {
                height              :   Responsive.getV(1020)
                width               :   Responsive.getH(250)
                text                :   "Git"
                font.pixelSize      :   Responsive.getV(120)
                onClicked           :   {
                    if(stage.machineState !== StageProxy.Idle)
                        return

                    var val = parseFloat(enteredNumber)
                    go(val * settings.unitMultiplier)
                    enteredNumber = ""
                    rebindPos()
                }
            }
        }
    }
}
