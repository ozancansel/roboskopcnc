import QtQuick 2.0
import QtQuick.Controls 2.2
import RLib 1.0
import SingleStage 1.0
import "../shared_qml/component"
import "../qml"

Item {

    property SwipeView      swipe       :   ({})
    property GrblSettings   settings    :   ({})
    property SingleStageAppSettings appSettings :   ({})
    property SerialComm     comm        :   ({})

    readonly property real textWidth    :   Responsive.getH(400)
    readonly property real panelWidth   :   Responsive.getH(900)
    readonly property real panelHeight  :   Responsive.getV(150)
    readonly property real textSize     :   Responsive.getV(35)
    readonly property real spinboxSize  :   Responsive.getV(50)

    property bool   isCurrentView       :   false
    property bool   settingsLoaded      :   false

    Connections{
        target              :   settings
        onErrorOccurredWhileSaving  :   {
            if(isCurrentView)
                errorOccurredWhileSavingDialog.open()
        }
        onSaveSuccess               :   {
            saveSuccessDialog.open()
        }
    }

    onIsCurrentViewChanged  :   {
        if(isCurrentView){
            settings.requestSettings()
        }
    }

    //    Rectangle{
    //        id          :   mainBackground
    //        anchors.fill:   parent
    //        color       :   "#383838"
    //    }

    RToastMessage {
        id          :   errorOccurredWhileSavingDialog
        width       :   Responsive.getH(830)
        height      :   Responsive.getV(500)
        headerText  :   "Hata Oluştu"
        message     :   "Ayarlar kaydedilirken bir hata oluştu.\nAyarlar tekrardan yüklenecek\nLütfen bekleyiniz..."
        x           :   parent.width / 2 - errorOccurredWhileSavingDialog.width / 2
        y           :   parent.height / 2 - errorOccurredWhileSavingDialog.height / 2
        messageFont.pixelSize   :   Responsive.getV(45)
        onAccepted  :   settings.requestSettings()
        onClosed    :   settings.requestSettings()
    }

    RToastMessage {
        id          :   saveSuccessDialog
        width       :   Responsive.getH(630)
        height      :   Responsive.getV(390)
        headerText  :   "İşlem Başarılı!"
        message     :   "Ayarlar kaydedildi."
        x           :   parent.width / 2 - saveSuccessDialog.width / 2
        y           :   parent.height / 2 - saveSuccessDialog.height / 2
        messageFont.pixelSize   :   Responsive.getV(55)
        alignCenter :   true
    }

    RMessageBox{
        id          :   askFactorySettings
        width       :   Responsive.getH(1400)
        headerText  :   "Fabrika Ayarlarına Dön ?"
        message     :   "Eğer bu işlemi onaylarsanız bütün verileriniz ve ayarlarınız silinecek.\nFabrika ayarlarında dönmek istediğinize emin misiniz ?"
        x           :   parent.width / 2 - askFactorySettings.width / 2
        y           :   parent.height / 2 - askFactorySettings.height / 2
        onAccepted  :   {
            if(settings.completeReset()){
                factoryResetSuccessDialog.open()
            }
        }
    }

    RToastMessage{
        id          :   factoryResetSuccessDialog
        width       :   Responsive.getH(900)
        height      :   Responsive.getV(410)
        headerText  :   "İşlem Başarılı !"
        message     :   "Sistem fabrika ayarlarına başarıyla döndürüldü."
        x           :   parent.width / 2 - factoryResetSuccessDialog.width / 2
        y           :   parent.height / 2 - factoryResetSuccessDialog.height / 2
        messageFont.pixelSize   :   Responsive.getV(37)
        displayTime :   -1
    }

    RMessageBox {
        id          :   askReloadSettings
        width       :   Responsive.getH(740)
        height      :   Responsive.getV(320)
        headerText  :   "Hata Oluştu"
        message     :   "Ayarlar yüklenirken bir hata oluştu. Tekrardan yüklensin mi ?"
        x           :   parent.width / 2 - askReloadSettings.width / 2
        y           :   parent.height / 2 - askReloadSettings.height / 2
        onAccepted  :   settings.requestSettings()
    }

    RMessageBox {
        id          :   askSave
        width       :   Responsive.getH(1050)
        height      :   Responsive.getV(600)
        headerText  :   "Ayarları Kaydet"
        message     :   "Ayarları kaydetmek istediğinize emin misiniz ? "
        x           :   parent.width / 2 - askSave.width / 2
        y           :   parent.height / 2 - askSave.height / 2
        onAccepted  :   settings.saveSettings()
    }

    RMessageBox {
        id          :   askRefresh
        width       :   Responsive.getH(1100)
        height      :   Responsive.getV(640)
        headerText  :   "Ayarları Tekrar Al"
        message     :   "Ayarları tekrar yüklerseniz yaptığınız değişiklikler\nkaybolacak. Tekrardan yüklemek istiyor musunuz ?"
        x           :   parent.width / 2 - askSave.width / 2
        y           :   parent.height / 2 - askSave.height / 2
        onAccepted  :   settings.requestSettings()
    }

    FontLoader{
        id          :   numericFont
        source      :   "/res/font/digital-7/digital-7.ttf"
    }

    BackgroundPanel {
        id          :   headerPanel
        x           :   Responsive.getH(20)
        y           :   Responsive.getV(20)
        width       :   Responsive.getH(1850)
        height      :   Responsive.getV(150)

        Text {
            id              :   headerText
            text            :   qsTr("Ayarlar")
            color           :   "white"
            font.pixelSize  :   Responsive.getV(80)
            anchors.centerIn:   parent
        }

        Row {
            anchors.verticalCenter  :   parent.verticalCenter
            anchors.right           :   parent.right
            anchors.rightMargin     :   Responsive.getH(20)
            spacing                 :   Responsive.getH(20)

            RIconButton {
                id          :   refreshButton
                source      :   "/res/icon/refresh.png"
                height      :   Responsive.getV(120)
                width       :   height
                onClicked   :   askRefresh.open()
            }

            RIconButton{
                id          :   saveButton
                source      :   "/res/icon/saved.png"
                height      :   Responsive.getV(120)
                width       :   height
                onClicked   :   askSave.open()
            }

            RIconButton{
                id                  :   lockButton
                source              :   !checked ? "/res/icon/lock.png" : "/res/icon/locked.png"
                height              :   Responsive.getV(120)
                width               :   height
                checkable           :   true
                checked             :   !swipe.interactive
                onCheckedChanged    :   {
                    swipe.interactive = !checked
                }
            }
        }
    }

    Flickable   {
        id              :   settingsFlickable
        anchors.left    :   parent.left
        anchors.right   :   parent.right
        anchors.top     :   headerPanel.bottom
        anchors.bottom  :   parent.bottom
        contentWidth    :   width
        contentHeight   :   settingsColumn.height + Responsive.getV(65)
        clip            :   true
                enabled         :   comm.state === SerialComm.Connected

        Column  {

            id          :   settingsColumn
            y           :   Responsive.getV(25)
            spacing     :   Responsive.getV(40)

            Grid  {
                id              :   grblSettings
                x               :   Responsive.getH(20)
                columns         :   2
                rowSpacing      :   Responsive.getV(20)
                columnSpacing   :   Responsive.getH(40)

                BackgroundPanel {
                    id          :   pulseDelay
                    width       :   panelWidth
                    height      :   panelHeight

                    Row {
                        anchors.centerIn        :   parent
                        Text {
                            id                  :   pulseDelayText
                            text                :   qsTr("Sinyal Periyodu(μs)")
                            width               :   textWidth
                            font.pixelSize      :   textSize
                            color               :   "white"
                            height              :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   pulseDelayTextArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   pulseDelayTexTTooltip
                                parent          :   pulseDelayText
                                visible         :   pulseDelayTextArea.pressed
                                text            :   "Step Periyodunu Belirtir. \nMotor sürücüsüne göre değişkenlik gösterir."
                            }
                        }

                        RSpinBox{
                            id              :   pulseDelaySpinBox
                            from            :   1
                            to              :   10000
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                        }
                    }

                    //Step Pulse
                    Binding {
                        target  :   pulseDelaySpinBox
                        property:   "value"
                        value   :   settings.stepPulse
                    }

                    Binding{
                        target  :   settings
                        property:   "stepPulse"
                        value   :   pulseDelaySpinBox.value
                    }
                }

                BackgroundPanel {
                    id      :   stageStepPerMm
                    width   :   panelWidth
                    height  :   panelHeight

                    Row {
                        anchors.centerIn    :   parent

                        Text {
                            id              :   stepPerMmmText
                            text            :   qsTr("Step Sayisi(1mm)")
                            width           :   textWidth
                            font.pixelSize  :   textSize
                            color           :   "white"
                            height          :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   stepPerMmTextArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   stageStepPerMmTooltip
                                parent          :   stageStepPerMm
                                visible         :   stepPerMmTextArea.pressed
                                text            :   "1mm için gerekli olan step sayısını belirtir"
                            }
                        }

                        RDoubleSpinBox{
                            id              :   stageStepPerMmSpinBox
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                            realFrom        :   1
                            realTo          :   5000
                            realStepSize    :   1
                            decimals        :   3
                        }
                    }

                    //Stage Step Per MM
                    Binding{
                        target  :   stageStepPerMmSpinBox
                        property:   "realValue"
                        value   :   settings.xStepPerMm
                    }

                    Binding{
                        target  :   settings
                        property:   "xStepPerMm"
                        value   :   stageStepPerMmSpinBox.getVal()
                    }
                }

                BackgroundPanel {
                    id          :   stepIdleDelay
                    width       :   panelWidth
                    height      :   panelHeight

                    Row {
                        anchors.centerIn        :   parent

                        Text {
                            id                  :   stepIdleDelayText
                            text                :   qsTr("Step süresini(μs)")
                            width               :   textWidth
                            font.pixelSize      :   textSize
                            color               :   "white"
                            height              :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   stepIdleDelayArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   stepIdleDelayTooltip
                                parent          :   stepIdleDelayText
                                visible         :   stepIdleDelayArea.pressed
                                text            :   "Step bekleme süresini(ms) belirtir.\n Step için sinyal gönderildikten sonra  motorun \nhareketi gerçekleştirmesi için gereken süredir."
                            }
                        }

                        RSpinBox{
                            id              :   stepIdleDelaySpinBox
                            from            :   1
                            to              :   255
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                        }
                    }


                    //Step Idle Delay
                    Binding{
                        target  :   stepIdleDelaySpinBox
                        property:   "value"
                        value   :   settings.stepIdleDelay
                    }

                    Binding{
                        target  :   settings
                        property:   "stepIdleDelay"
                        value   :   stepIdleDelaySpinBox.value
                    }
                }

                BackgroundPanel {
                    id      :   maxFeedRate
                    width   :   panelWidth
                    height  :   panelHeight

                    Row {
                        anchors.centerIn    :   parent

                        Text {
                            id              :   maxFeedRateText
                            text            :   qsTr("Maksimum Hız")
                            width           :   textWidth
                            font.pixelSize  :   textSize
                            color           :   "white"
                            height          :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   maxFeedRateArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   maxFeedRateTooltip
                                parent          :   maxFeedRateText
                                visible         :   maxFeedRateArea.pressed
                                text            :   "Kızağın maksimum hızını belirtir.\nKontrol ekranındaki hız bu değerden\ndaha yüksek olsa bile bu değer baz alınır."
                            }
                        }

                        RDoubleSpinBox {
                            id              :   maxFeedRateSpinBox
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                            realFrom        :   1
                            realTo          :   10000
                            realStepSize    :   1
                            decimals        :   3
                        }
                    }

                    //Max FeedRate
                    Binding{
                        target  :   maxFeedRateSpinBox
                        property:   "realValue"
                        value   :   settings.xMaxMmRatePerMin
                    }

                    Binding{
                        target  :   settings
                        property:   "xMaxMmRatePerMin"
                        value   :   maxFeedRateSpinBox.getVal()
                    }
                }

                BackgroundPanel {
                    id          :   directionPortMask
                    width       :   panelWidth
                    height      :   panelHeight

                    Row {
                        anchors.centerIn        :   parent

                        Text {
                            id                  :   directionPortMaskText
                            text                :   qsTr("Yon Ters Sinyal")
                            width               :   textWidth
                            font.pixelSize      :   textSize
                            color               :   "white"
                            height              :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   directionPortMaskArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   directionPortMaskTooltip
                                parent          :   directionPortMaskText
                                visible         :   directionPortMaskArea.pressed
                                text            :   "DIR sinyalini degistirir..\n0 = LOW\n1 = HIGH"
                            }
                        }

                        RSpinBox{
                            id              :   directionPortMaskSpinBox
                            from            :   0
                            to              :   1
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                        }
                    }

                    //Direction Port Mask
                    Binding {
                        target  :   directionPortMaskSpinBox
                        property:   "value"
                        value   :   settings.directionPortInvertMask
                    }

                    Binding{
                        target  :   settings
                        property:   "directionPortInvertMask"
                        value   :   directionPortMaskSpinBox.value
                    }
                }

                BackgroundPanel {
                    id      :   acceleration
                    width   :   panelWidth
                    height  :   panelHeight

                    Row {
                        anchors.centerIn    :   parent

                        Text {
                            id              :   accelerationText
                            text            :   qsTr("Hizlanma Katsayisi")
                            width           :   textWidth
                            font.pixelSize  :   textSize
                            color           :   "white"
                            height          :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   accelerationArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   accelerationTooltip
                                parent          :   acceleration
                                visible         :   accelerationArea.pressed
                                text            :   "Hizlanma katsayisini belirtir. (mm/sec^2) fonksiyonunu kullanir."
                            }
                        }

                        RDoubleSpinBox{
                            id              :   accelerationSpinBox
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                            realFrom        :   0
                            realTo          :   10000
                            realStepSize    :   1
                            decimals        :   3
                        }
                    }

                    //Acceleration
                    Binding{
                        target  :   accelerationSpinBox
                        property:   "realValue"
                        value   :   settings.xAcceleration
                    }

                    Binding{
                        target  :   settings
                        property:   "xAcceleration"
                        value   :   accelerationSpinBox.getVal()
                    }
                }


                BackgroundPanel {
                    id          :   stepPortMask
                    width       :   panelWidth
                    height      :   panelHeight

                    Row {
                        anchors.centerIn        :   parent

                        Text {
                            id                  :   stepPortMaskText
                            text                :   qsTr("Step Ters Sinyal")
                            width               :   textWidth
                            font.pixelSize      :   textSize
                            color               :   "white"
                            height              :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   stepPortMaskArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   stepPortMaskTooltip
                                parent          :   stepPortMaskText
                                visible         :   stepPortMaskArea.pressed
                                text            :   "PULSE sinyalinin sirasini degistirir.\n0 = HIGH -> LOW\n1 = LOW -> HIGH"
                            }
                        }

                        RSpinBox{
                            id              :   stepPortMaskSpinBox
                            from            :   0
                            to              :   1
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                        }
                    }

                    //Step Port Mask
                    Binding{
                        target  :   stepPortMaskSpinBox
                        property:   "value"
                        value   :   settings.stepPortInvertMask
                    }

                    Binding{
                        target  :   settings
                        property:   "stepPortInvertMask"
                        value   :   stepPortMaskSpinBox.value
                    }
                }

                BackgroundPanel {
                    id      :   stageMaxMovementLength
                    width   :   panelWidth
                    height  :   panelHeight

                    Row {
                        anchors.centerIn    :   parent

                        Text {
                            id              :   stageMaxMovementLengthText
                            text            :   qsTr("Hareket Mesafesi")
                            width           :   textWidth
                            font.pixelSize  :   textSize
                            color           :   "white"
                            height          :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   stageMaxMovementLengthArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   stageMaxMovementLengthTooltip
                                parent          :   stageMaxMovementLengthText
                                visible         :   stageMaxMovementLengthArea.pressed
                                text            :   "Kızağın gidebileceği maksimum uzaklığı belirtir.\nKızak belirtilen değerden daha uzağa gidemez."
                            }
                        }

                        RDoubleSpinBox {
                            id              :   stageMaxMovementLengthSpinBox
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                            realFrom        :   0
                            realTo          :   10000
                            realValue       :   1
                            realStepSize    :   1
                            decimals        :   3
                        }
                    }


                    //Stage Max Travel
                    Binding{
                        target  :   stageMaxMovementLengthSpinBox
                        property:   "realValue"
                        value   :   settings.xMaxTravel
                    }

                    Binding{
                        target  :   settings
                        property:   "xMaxTravel"
                        value   :   stageMaxMovementLengthSpinBox.getVal()
                    }
                }


                BackgroundPanel {
                    id          :   enableSoftLimit
                    width       :   panelWidth
                    height      :   panelHeight

                    Row {
                        anchors.centerIn        :   parent

                        Text {
                            id                  :   enableSoftLimitText
                            text                :   qsTr("Kızak Limit Mesafe")
                            width               :   textWidth
                            font.pixelSize      :   textSize
                            color               :   "white"
                            height              :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   enableSoftLimitArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   enableSoftLimitTooltip
                                parent          :   enableSoftLimitText
                                visible         :   enableSoftLimitArea.pressed
                                text            :   "Kızağın gideceği pozisyonu limitlemek için kullanır.\n Ayrıca homing mesafesini de belirler.\n Eğer ki kızağın homing yapması için\ngereken mesafe bu değerden fazlaysa\nhoming hata vericektir."
                            }
                        }

                        RSpinBox{
                            id              :   enableSoftLimitSpinBox
                            from            :   0
                            to              :   1
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                        }
                    }

                    //Soft Limit Enabled
                    Binding{
                        target  :   enableSoftLimitSpinBox
                        property:   "value"
                        value   :   settings.softLimitsEnabled
                    }

                    Binding{
                        target  :   settings
                        property:   "softLimitsEnabled"
                        value   :   enableSoftLimitSpinBox.value
                    }
                }

                BackgroundPanel {
                    id          :   enableHardLimits
                    width       :   panelWidth
                    height      :   panelHeight

                    Row {
                        anchors.centerIn        :   parent

                        Text {
                            id                  :   enableHardLimitsText
                            text                :   qsTr("End Sensörü Aktif/Pasif")
                            width               :   textWidth
                            font.pixelSize      :   textSize
                            color               :   "white"
                            height              :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   enableHardLimitsArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   enableHardLimitsTooltip
                                parent          :   enableHardLimitsText
                                visible         :   enableHardLimitsArea.pressed
                                text            :   "End sensörlerini aktif/pasif yapar."
                            }
                        }

                        RSpinBox{
                            id              :   enableHardLimitsSpinBox
                            from            :   0
                            to              :   1
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                        }
                    }

                    //Soft Limit Enabled
                    Binding{
                        target  :   enableHardLimitsSpinBox
                        property:   "value"
                        value   :   settings.hardLimitsEnabled
                    }

                    Binding{
                        target  :   settings
                        property:   "hardLimitsEnabled"
                        value   :   enableHardLimitsSpinBox.value
                    }
                }
            }

            BackgroundPanel{
                id                  :   homingSeperator
                height              :   Responsive.getV(12)
                width               :   Responsive.getH(1740)
                x                   :   Responsive.getH(75)
            }

            Grid    {
                id                  :   homingSettingsGrid
                x                   :   Responsive.getH(20)
                rowSpacing          :   Responsive.getV(20)
                columnSpacing       :   Responsive.getH(40)
                columns             :   2

                BackgroundPanel {

                    id      :   homingEnabled
                    width   :   panelWidth
                    height  :   panelHeight

                    Row {
                        anchors.centerIn    :   parent

                        Text {
                            id              :   homingActiveText
                            text            :   qsTr("Homing Aktif")
                            width           :   textWidth
                            font.pixelSize  :   textSize
                            color           :   "white"
                            height          :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   homingActiveArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   homingActiveTooltip
                                parent          :   homingActiveText
                                visible         :   homingActiveArea.pressed
                                text            :   "Homing aktif veya pasif yapar."
                            }
                        }

                        RSpinBox{
                            id              :   homingActiveSpinBox
                            from            :   0
                            to              :   1
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                        }
                    }

                    Binding{
                        target  :   homingActiveSpinBox
                        property:   "value"
                        value   :   settings.homingCycleEnabled
                    }

                    Binding{
                        target  :   settings
                        property:   "homingCycleEnabled"
                        value   :   homingActiveSpinBox.value
                    }
                }

                BackgroundPanel {

                    id      :   homingDirInvert
                    width   :   panelWidth
                    height  :   panelHeight
                    enabled :   homingActiveSpinBox.value

                    Row {
                        anchors.centerIn    :   parent

                        Text {
                            id              :   homingDirInvertText
                            text            :   qsTr("Homing Yön Sinyal")
                            width           :   textWidth
                            font.pixelSize  :   textSize
                            color           :   "white"
                            height          :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   homingDirInvertArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   homingDirInvertTooltip
                                parent          :   homingDirInvertText
                                visible         :   homingDirInvertArea.pressed
                                text            :   "Homing için gidilecek yönü belirtir."
                            }
                        }

                        RSpinBox{
                            id              :   homingDirInvertSpinBox
                            from            :   0
                            to              :   1
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                        }
                    }

                    //Homing Dir Invert
                    Binding {
                        target  :   homingDirInvertSpinBox
                        property:   "value"
                        value   :   settings.homingDirInvertMask
                    }

                    Binding{
                        target  :   settings
                        property:   "homingDirInvertMask"
                        value   :   homingDirInvertSpinBox.value
                    }
                }

                BackgroundPanel {
                    id      :   homingDebounce
                    width   :   panelWidth
                    height  :   panelHeight
                    enabled :   homingActiveSpinBox.value

                    Row {
                        anchors.centerIn    :   parent

                        Text {
                            id              :   homingDebounceText
                            text            :   qsTr("Homing Debounce")
                            width           :   textWidth
                            font.pixelSize  :   textSize
                            color           :   "white"
                            height          :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   homingDebounceArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   homingDebounceTooltip
                                parent          :   homingDebounceText
                                visible         :   homingDebounceArea.pressed
                                text            :   "Sensörde oluşan dalgalanmaları engellemek için eklenmiştir.\nSensörün tepki süresine göre değişkenlik gösterir."
                            }
                        }

                        RSpinBox {
                            id              :   homingDebounceSpinBox
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                        }
                    }

                    Binding{
                        target  :   homingDebounceSpinBox
                        property:   "value"
                        value   :   settings.homingDebounce
                    }

                    Binding{
                        target :    settings
                        property:   "homingDebounce"
                        value   :   homingDebounceSpinBox.value
                    }
                }

                BackgroundPanel {
                    id      :   homingSeekRate
                    width   :   panelWidth
                    height  :   panelHeight
                    enabled :   homingActiveSpinBox.value

                    Row {
                        anchors.centerIn    :   parent

                        Text {
                            id              :   homingSeekRateText
                            text            :   qsTr("Homing Tarama Hizi")
                            width           :   textWidth
                            font.pixelSize  :   textSize
                            color           :   "white"
                            height          :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   homingSeekRateArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   homingSeekRateTooltip
                                parent          :   homingSeekRateText
                                visible         :   homingSeekRateArea.pressed
                                text            :   "End sensorlerini tarama hizini belirtir."
                            }
                        }

                        RDoubleSpinBox {
                            id              :   homingSeekRateSpinBox
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                            realFrom        :   0
                            realTo          :   10000
                            realStepSize    :   1
                            decimals        :   3
                        }
                    }

                    //Homing Seek Rate
                    Binding {
                        target  :   homingSeekRateSpinBox
                        property:   "realValue"
                        value   :   settings.homingSeek
                    }

                    Binding{
                        target  :   settings
                        property:   "homingSeek"
                        value   :   homingSeekRateSpinBox.getVal()
                    }
                }

                BackgroundPanel {
                    id      :   homingFeedRate
                    width   :   panelWidth
                    height  :   panelHeight
                    enabled :   homingActiveSpinBox.value

                    Row {
                        anchors.centerIn    :   parent

                        Text {
                            id              :   homingFeedRateText
                            text            :   qsTr("Homing Merkez Hiz")
                            width           :   textWidth
                            font.pixelSize  :   textSize
                            color           :   "white"
                            height          :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   homingFeedRateArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   homingFeedRateTooltip
                                parent          :   homingFeedRateText
                                visible         :   homingFeedRateArea.pressed
                                text            :   "End sensorlerine ulastiktan sonra belirlenen pozisyona giderkenki hizini belirtir."
                            }
                        }

                        RDoubleSpinBox{
                            id              :   homingFeedRateSpinBox
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                            realFrom        :   0
                            realTo          :   5000
                            realStepSize    :   1
                            decimals        :   3
                        }
                    }

                    //Homing Feed
                    Binding{
                        target  :   homingFeedRateSpinBox
                        property:   "realValue"
                        value   :   settings.homingFeed
                    }

                    Binding{
                        target  :   settings
                        property:   "homingFeed"
                        value   :   homingFeedRateSpinBox.getVal()
                    }
                }

                BackgroundPanel {
                    id      :   homingPullOff
                    width   :   panelWidth
                    height  :   panelHeight
                    enabled :   homingActiveSpinBox.value

                    Row {
                        anchors.centerIn    :   parent

                        Text {
                            id              :   homingPullOffText
                            text            :   qsTr("Homing Donus Mesafesi")
                            width           :   textWidth
                            font.pixelSize  :   textSize
                            color           :   "white"
                            height          :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   homingPullOffArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   homingPullOffTooltip
                                parent          :   homingPullOffText
                                visible         :   homingPullOffArea.pressed
                                text            :   "Kizak end sensörüne yaklaştıktan sonra sensörün\n mesafesinden çıkmak için gereken mesafedir.\n Yani eğer ki 8mm lik end sensörünüz varsa bu\n değeri en az 8mm yapmalısınız."
                            }
                        }

                        RDoubleSpinBox {
                            id              :   homingPullOffSpinBox
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                            realFrom        :   0
                            realTo          :   10000
                            realStepSize    :   1
                            decimals        :   3
                        }
                    }

                    //Homing Pull Off
                    Binding{
                        target  :   homingPullOffSpinBox
                        property:   "realValue"
                        value   :   settings.homingPullOff
                    }

                    Binding{
                        target  :   settings
                        property:   "homingPullOff"
                        value   :   homingPullOffSpinBox.getVal()
                    }
                }
            }


            BackgroundPanel{
                id                  :   appSettingsSeperator
                width               :   Responsive.getH(1740)
                height              :   Responsive.getV(12)
                x                   :   Responsive.getV(75)
            }


            Grid    {
                id                  :   appSettingsGrid
                x                   :   Responsive.getH(20)
                rowSpacing          :   Responsive.getV(20)
                columnSpacing       :   Responsive.getH(40)
                columns             :   2

                BackgroundPanel {
                    id                  :   decimalsPanel
                    width               :   panelWidth
                    height              :   panelHeight
                    x                   :   Responsive.getH(20)

                    Row {
                        anchors.centerIn    :   parent

                        Text {
                            id              :   decimalsText
                            text            :   qsTr("Kontrol Hassasiyeti")
                            width           :   textWidth
                            font.pixelSize  :   textSize
                            color           :   "white"
                            height          :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   decimalsArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   decimalsTooltip
                                parent          :   decimalsText
                                visible         :   decimalsArea.pressed
                                text            :   "Virgülden sonraki nokta sayısını belirtir.\n Pozisyonlama yaparken onda 1 yüzde 1\n binde 1 gibi değerlere ulaşmak için bu değeri kullanmalısınız."
                            }
                        }

                        RSpinBox {
                            id              :   decimalsSpinBox
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                            value           :   appSettings.decimals
                            from            :   0
                            to              :   3
                        }
                    }
                    Binding{
                        target  :   appSettings
                        property:   "decimals"
                        value   :   decimalsSpinBox.value
                    }
                }

                BackgroundPanel {
                    id                  :   sysUnitPanel
                    width               :   panelWidth
                    height              :   panelHeight
                    x                   :   Responsive.getH(20)

                    Row {
                        anchors.centerIn    :   parent

                        Text {
                            id              :   sysUnitText
                            text            :   qsTr("Birim (mm = 0, cm = 1)")
                            width           :   textWidth
                            font.pixelSize  :   textSize
                            color           :   "white"
                            height          :   parent.height
                            verticalAlignment   :   Text.AlignVCenter

                            MouseArea   {
                                id              :   sysUnitArea
                                anchors.fill    :   parent
                            }

                            RToolTip    {
                                id              :   sysUnitTooltip
                                parent          :   sysUnitText
                                visible         :   sysUnitArea.pressed
                                text            :   "Sistemin kullanacağı birimi belirtir. (mm = 0, cm = 1)"
                            }
                        }

                        RSpinBox {
                            id              :   sysUnitSpinBox
                            width           :   Responsive.getH(400)
                            height          :   Responsive.getV(100)
                            font.pixelSize  :   spinboxSize
                            font.family     :   numericFont.name
                            value           :   appSettings.sysUnit
                            from            :   0
                            to              :   1
                        }
                    }

                    Binding {
                        target  :   appSettings
                        property:   "sysUnit"
                        value   :   sysUnitSpinBox.value
                    }
                }
            }


            BackgroundPanel{
                id                  :   factorySettingsSeperator
                width               :   Responsive.getH(1740)
                height              :   Responsive.getV(12)
                x                   :   Responsive.getV(75)
            }

            RDelayButton    {
                id          :   returnToFactorySettings
                width       :   Responsive.getH(1820)
                height      :   Responsive.getV(200)
                x           :   Responsive.getH(20)
                text        :   "Fabrika Ayarlarına Dön"
                buttonType  :   error
                font.pixelSize  :   Responsive.getV(40)
                delay       :   1000
                enabled     :   comm.state === SerialComm.Connected
                onActivated :   {
                    progress = 0
                    askFactorySettings.open()
                }
            }
        }
    }
}
