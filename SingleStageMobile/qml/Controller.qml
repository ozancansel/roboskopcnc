import QtQuick 2.0
import QtQuick.Controls 2.2
import RLib 1.0
import SingleStage 1.0
import "../qml"
import "../shared_qml/component"

Item    {
    property StageProxy stageProxy                  :   ({})
    property SingleStageAppSettings appSettings     :   ({})
    property GrblSettings   stageSettings           :   ({})
    readonly property real  fontSize                :   40
    property real  feedRate                         :   200
    property double pos                             :   stageProxy.position
    property int precision                          :   1000
    property SwipeView  view                        :   ({})
    property bool  isCurrentView                    :   false
    property bool  anyDialogOpen                    :   homingRequiredDialog.visible ||
                                                        hardLimitsTriggeredDialog.visible ||
                                                        endSensorsNotFoundDialog.visible ||
                                                        shortPullOffDialog.visible ||
                                                        unlockToContinueDialog.visible
    property string     resetToContMessage          :   "" //Reset to continue icin kullanilacaktir
    property string unlockStageMessage              :   "Kızak güvenlik nedeniyle kitli. Açmak istiyor musunuz ?"
    property bool  homingRequiredShowDialog         :   false
    property bool  hardLimitsTriggeredShowDialog    :   false
    property bool  endSensorsNotFoundShowDialog     :   false
    property bool  shortPullOffShowDialog           :   false
    property bool  resetToContinueShowDialog        :   false
    property bool   unlockToContinueShowDialog      :   false
    readonly property real xLimit                   :   0
    readonly property int   advancedControlsTopLine :   Responsive.getV(840)

    id              :   controller

    //Baglantinin ardindan kontrol sayfasina gecildiginde dialoglar gosterilecek.
    onIsCurrentViewChanged      :   {
        checkDialogs()
    }

    onAnyDialogOpenChanged      :   {
        checkDialogs()
    }

    function checkDialogs(){

        if(!isCurrentView)
            return

        if(anyDialogOpen)
            return

        if(homingRequiredShowDialog){
            homingRequiredDialog.open()
            homingRequiredShowDialog = false
        }

        if(hardLimitsTriggeredShowDialog){
            hardLimitsTriggeredDialog.open()
            hardLimitsTriggeredShowDialog = false
        }

        if(endSensorsNotFoundShowDialog){
            endSensorsNotFoundDialog.open()
            resetToContinueShowDialog = false
        }

        if(shortPullOffShowDialog){
            shortPullOffDialog.open()
            shortPullOffShowDialog = false
        }

        if(resetToContinueShowDialog){
            resetToContinueDialog.open()
            resetToContinueShowDialog = false
        }

        if(unlockToContinueShowDialog){
            unlockToContinueDialog.open()
            unlockToContinueShowDialog = false
        }
    }

    Connections{
        target                  :   stageProxy
        onPositionChanged       :   {
            if(stageProxy.position > appSettings.posMax)
                appSettings.posMax = stageProxy.position
            else if(stageProxy.position <  appSettings.posMin)
                appSettings.posMin = stageProxy.position
        }
        onHomingRunningChanged  :   {
            if(stageProxy.homingRunning)
                homingDialog.open()
            else
                homingDialog.close()
        }
        onHomingRequired        :   {
            homingRequiredShowDialog = true
            checkDialogs()
        }
        onHardLimitsTriggered   :   {
            resetToContinueShowDialog = true
            resetToContMessage = "Kızak end sensörlerini tetikledi.\nDevam etmek için reset atılması gerekiyor.\nOnaylıyor musunuz ?"
            checkDialogs()
        }
        onHomingFailSensorNotFound  :   {
            endSensorsNotFoundShowDialog = true
            checkDialogs()
        }
        onHomingFailShortPullOff    :   {
            shortPullOffShowDialog = true
            checkDialogs()
        }
        onResetToContinue           :   {
            resetToContinueShowDialog = true
            checkDialogs()
        }
        onUnlockRequired            :   {
            unlockToContinueShowDialog = true
            checkDialogs()
        }
        onExceedsMachineTravel  :   {
            resetToContMessage = "Bu pozisyona gidemezsiniz.\nReset atılması gerekiyor onaylıyor musunuz ?"
        }
        onExceedsMachineTravelNoReset   :   {
            exceedTargetLevelNoResetDialog.open()
        }
        onCalibrationMovementEnd    :   {
            isCalibratingEnoughDialog.open()
        }
    }

    Rectangle{
        id          :   mainBackground
        anchors.fill:   parent
        color       :   "#383838"
    }

    FontLoader{
        id          :   numericFont
        source      :   "/res/font/digital-7/digital-7.ttf"
    }

    RMessageBox {
        id          :   askHoming
        width       :   Responsive.getH(950)
        height      :   Responsive.getV(550)
        headerText  :   "Homing"
        message     :   "Homing yapmak istediğinize emin misiniz ?"
        x           :   parent.width / 2 - askHoming.width / 2
        y           :   parent.height / 2 - askHoming.height / 2
        onAccepted  :   {
            stageProxy.doHoming()
        }
    }

    RMessageBox{
        id          :   homingRequiredDialog
        x           :   parent.width / 2 - width / 2
        y           :   parent.height / 2 - height / 2
        width       :   Responsive.getH(1150)
        z           :   2
        modal       :   true
        closePolicy :   Dialog.NoAutoClose
        headerText  :   "Homing"
        message     :   "Pozisyonlama için homing yapılması gerekiyor.\nOnaylıyor musunuz ?"
        onAccepted  :   {
            homingRequiredShowDialog = false
            stageProxy.doHoming()
        }
    }


    RMessageBox{
        id          :   askResumeDialog
        x           :   parent.width / 2 - width / 2
        y           :   parent.height / 2 - height / 2
        width       :   Responsive.getH(1150)
        z           :   2
        modal       :   true
        closePolicy :   Dialog.NoAutoClose
        headerText  :   "Kızak Durdurulmuş"
        message     :   "Kızağı durdurduğunuz için hareket ettiremezsiniz,\nKızağı tekrardan aktif hale getirmek istiyor musunuz ?"
        onAccepted  :   {
            stageProxy.resume()
        }
    }

    RMessageBox {
        id          :   resetToContinueDialog
        x           :   parent.width / 2 - width / 2
        y           :   parent.height / 2 - height / 2
        width       :   Responsive.getH(1150)
        z           :   2
        modal       :   true
        closePolicy :   Dialog.NoAutoClose
        headerText  :   "Hata !"
        message     :   controller.resetToContMessage
        onAccepted  :   {
            stageProxy.reset()
        }
    }

    RMessageBox {
        id          :   unlockToContinueDialog
        x           :   parent.width / 2 - width / 2
        y           :   parent.height / 2 - height / 2
        width       :   Responsive.getH(1150)
        z           :   2
        modal       :   true
        closePolicy :   Dialog.NoAutoClose
        headerText  :   "Hata !"
        message     :   controller.unlockStageMessage
        onAccepted  :   {
            stageProxy.unlockStage()
        }
    }

    RToastMessage {
        id                      :   endSensorsNotFoundDialog
        x                       :   parent.width / 2 - width / 2
        y                       :   parent.height / 2 - height / 2
        width                   :   Responsive.getH(900)
        height                  :   Responsive.getV(480)
        modal                   :   true
        closePolicy             :   Dialog.NoAutoClose
        headerText              :   "End Sensörleri Bulunamadı !"
        message                 :   "Homing işleminin gerçekleştirilebilmesi için\n end sensörlerini takılı olması gerekir.\n Lütfen sensörleri bağladığınızdan emin olun."
        displayTime             :   -1
        onClosed                :   endSensorsNotFoundShowDialog = false
        messageFont.pixelSize   :   Responsive.getV(40)
        alignCenter             :   true
    }

    RToastMessage{
        id                      :   homingNotEnabledDialog
        x                       :   parent.width / 2 - width / 2
        y                       :   parent.height / 2 - height / 2
        width                   :   Responsive.getH(1050)
        height                  :   Responsive.getV(450)
        modal                   :   true
        closePolicy             :   Dialog.CloseOnPressOutside
        headerText              :   "Homing Aktif Değil !"
        message                 :   "Bu özelliği kullanabilmeniz için ayarlar bölümünden\n'Homing Aktif' seçeneğini '1' konumuna getiriniz."
        messageFont.pixelSize   :   Responsive.getV(40)
        alignCenter              :  true
    }

    RToastMessage{
        id          :   exceedTargetLevelNoResetDialog
        x           :   parent.width / 2 - width / 2
        y           :   parent.height / 2 - height / 2
        width       :   Responsive.getH(900)
        height      :   Responsive.getV(560)
        modal       :   true
        alignCenter :   true
        headerText  :   "Hata !"
        message     :   "Bu pozisyona gidemezsiniz.\nLimitleri kapatmak için ayarlar kısmından\n'Mesafeyi Limitle' seçeneğini 0 yapın veya \n'Hareket Mesafesini' değiştirin.\nLimitleriniz 0  =>  " + -stageSettings.xMaxTravel
        messageFont.pixelSize   :   Responsive.getV(40)
        displayTime :   -1
    }

    RToastMessage{
        id              :   macrosNotImplementedDialog
        x               :   parent.width  / 2 - width / 2
        y               :   parent.height / 2 - height / 2
        width           :   Responsive.getH(900)
        height          :   Responsive.getV(450)
        modal           :   true
        closePolicy     :   Dialog.CloseOnPressOutside
        headerText      :   "Üzgünüz..."
        message         :   "Makrolar henüz aktif değil.\nİlerleyen versiyonlarda eklenecektir."
        messageFont.pixelSize   :   Responsive.getV(40)
        alignCenter     :   true
    }

    RToastMessage {
        id          :   shortPullOffDialog
        x           :   parent.width / 2 - width / 2
        y           :   parent.height / 2 - height / 2
        width       :   Responsive.getH(1200)
        height      :   Responsive.getV(470)
        modal       :   true
        closePolicy :   Dialog.NoAutoClose
        headerText  :   "Uzaklaşma Payı Yeterli Değil"
        message     :   "Uzaklaşma payını artırın.\nKızak sensörden belirtildiği miktarda\n uzaklaşmasına rağmen hala end sensörü kızağı algılamakta."
        displayTime :   -1
        onClosed    :   shortPullOffShowDialog = false
        messageFont.pixelSize   :   Responsive.getV(40)
        alignCenter :   true
    }

    RMessageBox{
        id          :   hardLimitsTriggeredDialog
        x           :   parent.width / 2 - width / 2
        y           :   parent.height / 2 - height / 2
        width       :   Responsive.getH(1000)
        height      :   Responsive.getV(650)
        modal       :   true
        closePolicy :   Dialog.NoAutoClose
        headerText  :   "Kritik Hata !"
        message     :   "Kızak end sensörlerini tetikledi.\nDevam etmek için reset atılması gerekiyor.\nOnaylıyor musunuz ?"
        onAccepted  :   {
            stageProxy.reset()
            hardLimitsTriggeredShowDialog = false
        }
    }

    RMessageBox{
        id          :   calibrateStartDialog
        x           :   parent.width / 2 - width / 2
        y           :   parent.height / 2 - height / 2
        message     :   "Kalibrasyon başlatılacak onaylıyor musunuz ?"
        headerText  :   "Kalibrasyon"
        onAccepted  :   startCalibration()
    }

    RMessageBox{
        id          :   isCalibratingEnoughDialog
        x           :   parent.width / 2 - width / 2
        y           :   parent.height / 2 - height / 2
        width       :   Responsive.getH(800)
        message     :   "Uzaklık yeterli mi ?"
        headerText  :   "Uzaklık"
        onAccepted  :   {
            enterCalibrationMovementDialog.open()
        }
        onRejected  :   {
            recalibrationAskDialog.open()
        }
    }

    RMessageBox{
        id          :   recalibrationAskDialog
        x           :   parent.width / 2 - width / 2
        y           :   parent.height / 2 - height / 2
        message     :   "Mesafe artırılarak tekrardan kalibrasyon yapılsın mı ?"
        headerText  :   "Kalibrasyon Devam ?"
        onAccepted  :   stageProxy.resumeCalibration()
        onRejected  :   stageProxy.cancelCalibration()
    }

    RMessageBox{
        x           :   parent.width / 2 - width / 2
        y           :   parent.height / 2 - height / 2
        message     :   "Mesafe atrırılarak devam kalibrasyona devam edilsin mi ?"
        headerText  :   "Kalibrasyon"
        onAccepted  :   {
        }
    }

    Dialog  {
        id          :   enterCalibrationMovementDialog
        x           :   parent.width / 2 - width / 2
        y           :   parent.height / 2 - height / 2
        width       :   Responsive.getH(950)
        height      :   Responsive.getV(450)
        contentItem :   Item    {

            Text {
                id      :   enterDistanceText
                x       :   Responsive.getH(60)
                y       :   Responsive.getV(100)
                text    :   "Mesafeyi giriniz..."
                color   :   "white"
                font.pixelSize  :   Responsive.getV(50)
            }

            RDoubleSpinBox{
                id                      :   spin
                anchors.left            :   enterDistanceText.right
                anchors.leftMargin      :   Responsive.getH(25)
                anchors.verticalCenter  :   enterDistanceText.verticalCenter
                width                   :   Responsive.getH(450)
                height                  :   Responsive.getV(150)
                font.pixelSize          :   Responsive.getV(60)
                decimals                :   appSettings.decimals
                realFrom                :   0
                realTo                  :   9999
            }

            Row{
                anchors.right       :   parent.right
                anchors.rightMargin :   Responsive.getH(50)
                y                   :   Responsive.getV(240)
                spacing             :   Responsive.getH(20)

                RButton{
                    width           :   Responsive.getH(250)
                    height          :   Responsive.getV(140)
                    text            :   "Hayır"
                    font.pixelSize  :   Responsive.getV(40)
                    onClicked       :   enterCalibrationMovementDialog.reject()
                }

                RButton{
                    width           :   Responsive.getH(250)
                    height          :   Responsive.getV(140)
                    text            :   "Evet"
                    font.pixelSize  :   Responsive.getV(40)
                    onClicked       :   enterCalibrationMovementDialog.accept()
                }
            }
        }
        background  :   BackgroundPanel {
            borderVisible   :   true
        }

        onAccepted  :   {
            stageProxy.finishCalibration(spin.getVal())
        }
    }

    function    startCalibration(){
        stageProxy.startCalibration(0)
    }


    BackgroundPanel {
        id          :   headerPanel
        width       :   Responsive.getH(1880)
        height      :   Responsive.getV(150)
        x           :   Responsive.getH(20)
        y           :   Responsive.getV(20)
        z           :   2

        Column  {
            anchors.centerIn    :   parent
            spacing :   Responsive.getV(5)
            Text {
                id              :   headerText
                text            :   "Roboskop Endüstriyel"
                font.pixelSize  :   Responsive.getV(50)
                color           :   "white"
                anchors.horizontalCenter    :   parent.horizontalCenter
            }

            Text {
                id              :   name
                text            :   qsTr("Step 1 Kontrol Uygulaması")
                font.pixelSize  :   Responsive.getV(35)
                color           :   "white"
                anchors.horizontalCenter    :   parent.horizontalCenter
            }
        }

        Row {
            anchors.verticalCenter  :   parent.verticalCenter
            anchors.right           :   parent.right
            anchors.rightMargin     :   Responsive.getH(20)
            spacing                 :   Responsive.getH(20)

            RIconButton{
                id                  :   lockButton
                source              :   !checked ? "/res/icon/lock.png" : "/res/icon/locked.png"
                height              :   Responsive.getV(120)
                width               :   height
                checkable           :   true
                checked             :   !view.interactive
                onCheckedChanged    :   view.interactive = !checked
            }
        }
    }

    Dialog  {
        id          :   homingDialog
        x           :   parent.width / 2 - (width / 2)
        y           :   parent.height / 2 - (height / 2)
        width       :   Responsive.getH(800)
        height      :   Responsive.getV(450)
        z           :   2
        modal       :   true
        closePolicy :   Dialog.NoAutoClose
        onRejected  :   stageProxy.cancelHoming()

        enter       :   Transition {
            NumberAnimation { property : "opacity"; from : 0.0; to : 1.0 }
        }

        contentItem :   BackgroundPanel {
            id              :   panel
            borderVisible   :   true

            Text {
                id                  :   connectingText
                text                :   qsTr("Homing...")
                anchors.centerIn    :   parent
                font.pixelSize      :   Responsive.getV(90)
                color               :   "white"
            }

            RCloseButton{
                id                  :   cancelHomingButton
                width               :   Responsive.getH(85)
                height              :   Responsive.getV(85)
                anchors.right       :   parent.right
                anchors.rightMargin :   Responsive.getH(40)
                anchors.top         :   parent.top
                anchors.topMargin   :   Responsive.getV(40)
                onClicked           :   homingDialog.reject()
            }

            SequentialAnimation{
                readonly property int stepDelay :   400

                id          :   homingInProgressAnimation
                loops       :   Animation.Infinite
                running     :   homingDialog.visible

                ScriptAction{
                    script: connectingText.text = "Homing.  "
                }

                PauseAnimation {
                    duration    :   homingInProgressAnimation.stepDelay
                }

                ScriptAction{
                    script: connectingText.text = "Homing.. "
                }

                PauseAnimation {
                    duration    :   homingInProgressAnimation.stepDelay
                }
                ScriptAction{
                    script: connectingText.text = "Homing..."
                }
                PauseAnimation {
                    duration    :   homingInProgressAnimation.stepDelay
                }
            }
        }
        background  :   Item
        {

        }
    }

    Flickable   {
        id                  :   controlsFlickable
        anchors.top         :   headerPanel.bottom
        anchors.bottom      :   expandControlsButton.top
        anchors.left        :   parent.left
        anchors.right       :   parent.right
        contentHeight       :   Responsive.getV(1600)
        contentWidth        :   width
        clip                :   true
        interactive         :   false

        RStageVisualizer    {
            id              :   stageVisualizer
            x               :   Responsive.getH(20)
            y               :   Responsive.getV(20)
            width           :   Responsive.getH(1750)
            height          :   stageSettings.homingCycleEnabled ? Responsive.getV(120) : 0
            tickCount       :   14
            from            :   {
                if(stageSettings.softLimitsEnabled)
                    return 0;
                return Math.floor(appSettings.posMin)
            }
            to              :   {
                if(stageSettings.softLimitsEnabled)
                    return -Math.ceil(Math.max(stageSettings.xMaxTravel))
                return appSettings.posMax
            }
            pos             :   {
                return Math.abs((stageProxy.position - from) / Math.abs(to - from))
            }
            stageColor      :   stateBackground.color
            velocity        :   {
                if(stageSettings.softLimitsEnabled) return Math.abs(appSettings.feedRate / stageSettings.xMaxTravel) / 60 * stageVisualizer.width
                return Math.abs(appSettings.feedRate / (appSettings.posMax - appSettings.posMin)) / 60 * stageVisualizer.width
            }
            onDraggedTo     :   stageProxy.goTo(((to - from) * pos) + from , appSettings.feedRate)
        }

        BackgroundPanel{
            anchors.top     :   stageVisualizer.top
            anchors.bottom  :   stageVisualizer.bottom
            anchors.left    :   stageVisualizer.right
            anchors.leftMargin  :   Responsive.getH(20)
            width           :   Responsive.getH(100)
            scale           :   resetVisualizerButton.pressed ? 0.8 : 1
            RIconButton{
                id              :   resetVisualizerButton
                source          :   "/res/icon/reset.png"
                anchors.fill    :   parent
                onClicked       :   {
                    appSettings.posMin = 0
                    appSettings.posMax = 0
                }
            }
        }

        BackgroundPanel {

            id                          :   posValuePanel
            anchors.horizontalCenter    :   parent.horizontalCenter
            anchors.top                 :   stageVisualizer.bottom
            anchors.topMargin           :   Responsive.getV(20)
            width   :   Responsive.getH(1200)
            height  :   Responsive.getV(300 - stageVisualizer.height)

            Item    {

                anchors.left        :   parent.left
                anchors.right       :   statusItem.left
                height              :   parent.height

                Text    {
                    id              :   posValueText
                    anchors.centerIn:   parent
                    text            :   "P: " + (pos / appSettings.unitMultiplier).toFixed(appSettings.decimals)
                    font.pixelSize  :   Responsive.getV(200)
                    color           :   "white"
                    font.family     :   numericFont.name
                }
            }

            Button {
                id              :   statusItem
                width           :   Responsive.getH(240)
                height          :   parent.height
                anchors.right   :   parent.right
                font.pixelSize  :   Responsive.getV(30)
                text            :   {
                    if(stageProxy.comm.state !== SerialComm.Connected)
                        return "Bağlantı Yok"
                    if(stageProxy.machineState === StageProxy.Idle){
                        return "Hazır"
                    } else if(stageProxy.machineState === StageProxy.Run){
                        return "Çalışıyor"
                    } else if(stageProxy.machineState === StageProxy.Alarm){
                        return "Alarm"
                    } else if(stageProxy.machineState === StageProxy.Jog){
                        return "Çalışıyor"
                    } else if (stageProxy.machineState === StageProxy.Unknown){
                        return "Bilinmiyor"
                    } else if (stageProxy.machineState === StageProxy.Hold) {
                        return "Durduruldu"
                    }
                }

                background          :   Rectangle {
                    id              :   stateBackground
                    color           :   {
                        if(stageProxy.comm.state !== SerialComm.Connected)
                            return "#ff1a1a"

                        if(stageProxy.machineState === StageProxy.Idle)
                            return "#00b359"
                        else if(stageProxy.machineState === StageProxy.Run)
                            return "#1E90FF"
                        else if(stageProxy.machineState === StageProxy.Alarm)
                            return "#ff1a1a"
                        else if(stageProxy.machineState === StageProxy.Jog)
                            return "#1E90FF"
                        else if(stageProxy.machineState === StageProxy.Hold)
                            return "#ffff66"
                        else if(stageProxy.machineState === StageProxy.Unknown)
                            return "white"

                    }

                    radius          :   height * 0.1
                    border.width    :   Responsive.getV(4)
                    border.color    :   "white"
                }

                contentItem         :   Item    {
                    Text {
                        id                  :   buttonText
                        text                :   statusItem.text
                        anchors.centerIn    :   parent
                        font                :   statusItem.font
                        color               :   stageProxy.machineState === StageProxy.Hold ? "#282828" : "white"
                    }
                }

                onClicked               :   {
                    if( stageProxy.machineState === StageProxy.Alarm ){
                        unlockStageMessage = "Kızak alarm modunda, kilidi açmak istiyor musunuz ?"
                        unlockToContinueDialog.open()
                    } else if(stageProxy.machineState === StageProxy.Hold){
                        askResumeDialog.open()
                    }
                }
            }
        }

        RButton {
            id              :   moveLeftButton
            anchors.top     :   posValuePanel.top
            anchors.bottom  :   posValuePanel.bottom
            anchors.left    :   parent.left
            anchors.right   :   posValuePanel.left
            anchors.rightMargin :   Responsive.getH(20)
            anchors.leftMargin  :   Responsive.getH(20)
            width           :   parent.width / parent.columns - ( parent.spacing / 2 )
            height          :   parent.height / parent.rows - ( parent.spacing / 2 )
            text            :   "<"
            font.pixelSize  :   Responsive.getH(150)
            onClicked       :   stageProxy.move(-precision / 1000 , appSettings.feedRate)
            enabled         :   stageProxy.machineState === StageProxy.Jog ||
                                stageProxy.machineState === StageProxy.Idle ||
                                stageProxy.machineState === StageProxy.Run
        }

        RButton {
            id              :   moveRightButton
            anchors.top     :   posValuePanel.top
            anchors.bottom  :   posValuePanel.bottom
            anchors.left    :   posValuePanel.right
            anchors.right   :   parent.right
            anchors.rightMargin :   Responsive.getH(20)
            anchors.leftMargin  :   Responsive.getH(20)
            width           :   parent.width / parent.columns - ( parent.spacing / 2 )
            height          :   parent.height / parent.rows - ( parent.spacing / 2 )
            text            :   ">"
            font.pixelSize  :   Responsive.getH(150)
            onClicked       :   stageProxy.move(precision / 1000 , appSettings.feedRate)
            enabled         :   stageProxy.machineState === StageProxy.Jog ||
                                stageProxy.machineState === StageProxy.Idle ||
                                stageProxy.machineState === StageProxy.Run
        }

        Column  {
            id      :   controlsRow
            y       :   Responsive.getV(400)
            x       :   Responsive.getH(20)
            spacing :   Responsive.getH(20)
            enabled :   stageProxy.machineState !== StageProxy.Unknown

            RDoubleSpinBox  {
                id          :   spinner
                realValue   :   0.0
                realStepSize:   precision / 1000
                realFrom    :   -99999
                realTo      :   99999
                decimals    :   appSettings.decimals
                width       :   Responsive.getH(500)
                height      :   Responsive.getV(250)
                font.family :   numericFont.name
                font.pixelSize  :   Responsive.getV(115)
            }

            RButton {
                id      :   moveButton
                anchors.left    :   spinner.left
                anchors.right   :   spinner.right
                height          :   Responsive.getV(120)
                text            :   "İlerle"
                font.pixelSize  :   Responsive.getH(fontSize)
                onClicked       :   stageProxy.goTo(spinner.getVal() , appSettings.feedRate)
                enabled         :   stageProxy.machineState === StageProxy.Jog ||
                                    stageProxy.machineState === StageProxy.Run ||
                                    stageProxy.machineState == StageProxy.Idle
            }
        }


        Column  {
            id                  :   precisionColumn1
            anchors.left        :   controlsRow.right
            anchors.leftMargin  :   Responsive.getH(65)
            anchors.top         :   controlsRow.top
            anchors.topMargin   :   Responsive.getV(5)
            spacing             :   Responsive.getV(20)
            enabled             :   stageProxy.machineState !== StageProxy.Unknown

            RRadioButton {
                id          :   tenMmStepRadioButton
                height      :   Responsive.getV(80)
                text        :   "10mm"
                font.pixelSize  :   Responsive.getV(35)
                checked     :   appSettings.movementRadioIdx === 0
                onCheckedChanged:   {
                    if(checked){
                        precision = 10000
                        appSettings.movementRadioIdx = 0
                    }
                }
            }
            RRadioButton {
                id          :   threeMmStepRadioButton
                height      :   Responsive.getV(80)
                text        :   "3mm"
                font.pixelSize  :   Responsive.getV(35)
                checked     :   appSettings.movementRadioIdx === 1
                onCheckedChanged:   {
                    if(checked){
                        precision = 3 * 1000
                        appSettings.movementRadioIdx = 1
                    }
                }
            }

            RRadioButton {
                id          :   mmStepRadioButton
                height      :   Responsive.getV(80)
                text        :   "1mm"
                checked     :   appSettings.movementRadioIdx === 2
                font.pixelSize  :   Responsive.getV(35)
                onCheckedChanged:   {
                    if(checked){
                        precision = 1000
                        appSettings.movementRadioIdx = 2
                    }
                }
            }

            RRadioButton {
                id          :   hunderUsStepRadioButton
                height      :   Responsive.getV(80)
                text        :   ".1 mm"
                font.pixelSize  :   Responsive.getV(35)
                checked     :   appSettings.movementRadioIdx === 3
                enabled     :   appSettings.decimals > 0
                onCheckedChanged:   {
                    if(checked){
                        precision = 100
                        appSettings.movementRadioIdx = 3
                    }
                }
            }
        }

        Column{
            anchors.left        :   precisionColumn1.right
            anchors.leftMargin  :   Responsive.getH(50)
            anchors.top         :   precisionColumn1.top
        }

        Row {

            id          :   controlButtonsRow
            x           :   Responsive.getH(825)
            y           :   Responsive.getV(400)
            spacing     :   Responsive.getH(20)

            RButton{
                id              :   homingButton
                text            :   "Homing"
                width           :   Responsive.getH(310)
                height          :   Responsive.getV(390)
                font.pixelSize  :   Responsive.getH(fontSize)
                onClicked       :   {
                    if(!stageSettings.homingCycleEnabled)
                        homingNotEnabledDialog.open()
                    else
                        askHoming.open()
                }
                enabled         :   stageProxy.machineState === StageProxy.Jog ||
                                    stageProxy.machineState === StageProxy.Alarm ||
                                    stageProxy.machineState === StageProxy.Idle
            }

            Grid    {
                spacing     :   Responsive.getV(20)
                rows        :   2
                columns     :   2

                RButton {
                    id      :   calibrateButton
                    text    :   "Kalibre Et"
                    width   :   Responsive.getH(310)
                    height  :   Responsive.getV(185)
                    font.pixelSize  :   Responsive.getH(fontSize)
                    onClicked       :   {
                        calibrateStartDialog.open()
                    }
                    enabled     :   {
                        stageProxy.machineState === StageProxy.Run ||
                                stageProxy.machineState === StageProxy.Idle ||
                                stageProxy.machineState === StageProxy.Jog
                    }
                }

                RButton{
                    id              :   connectionStatus
                    text            :   "Sıfırla"
                    width           :   Responsive.getH(310)
                    height          :   Responsive.getV(185)
                    font.pixelSize  :   Responsive.getH(fontSize)
                    onClicked       :   stageProxy.setZero()
                    enabled         :   stageProxy.machineState !== StageProxy.Unknown
                }

                RButton{
                    id              :   unlockButton
                    text            :   "Kilidi Aç"
                    width           :   Responsive.getH(310)
                    height          :   Responsive.getV(185)
                    font.pixelSize  :   Responsive.getH(fontSize)
                    onClicked       :   stageProxy.unlockStage()
                    enabled         :   stageProxy.comm.state === SerialComm.Connected &&
                                        stageProxy.machineState !== StageProxy.Hold
                }

                RButton{
                    id              :   resetButton
                    text            :   "Reset"
                    width           :   Responsive.getH(310)
                    height          :   Responsive.getV(185)
                    font.pixelSize  :   Responsive.getH(fontSize)
                    onClicked       :   stageProxy.reset()
                    enabled         :   stageProxy.comm.state === SerialComm.Connected
                }
            }
        }

        BackgroundPanel {
            borderVisible           :   true
            x                       :   Responsive.getH(20)
            y                       :   advancedControlsTopLine
            anchors.bottomMargin    :   Responsive.getV(10)
            height                  :   Responsive.getV(120)
            width                   :   Responsive.getH(550)

            Text {
                id                  :   feedRateText
                text                :   "Feed : " + feedRateSlider.value.toFixed(2) + " mm/dk"
                anchors.centerIn    :   parent
                font.family         :   numericFont.name
                font.pixelSize      :   Responsive.getV(40)
                color               :   "white"
                horizontalAlignment :   Text.AlignHCenter
                verticalAlignment   :   Text.AlignVCenter
            }
        }

        Slider  {
            id          :   feedRateSlider
            x           :   Responsive.getH(40)
            y           :   advancedControlsTopLine + Responsive.getV(150)
            height      :   Responsive.getV(570)
            width       :   Responsive.getH(200)
            from        :   feedMin.value
            to          :   feedMax.value
            value       :   appSettings.feedRate
            snapMode    :   Slider.SnapAlways
            orientation :   Qt.Vertical
            stepSize    :   1

            background  :   BackgroundPanel{
                width   :   feedRateSlider.width
                height  :   feedRateSlider.height
            }

            handle      :   BackgroundPanel{
                anchors.horizontalCenter    :   feedRateSlider.horizontalCenter
                y               : feedRateSlider.visualPosition * (feedRateSlider.height - height)
                implicitWidth   : feedRateSlider.availableWidth * 1.25
                implicitHeight  : feedRateSlider.availableHeight * 0.15
                borderVisible   :   true
                color           :   "red"
            }
        }

        Text {
            id                          :   feedMinText
            anchors.horizontalCenter    :   feedMin.horizontalCenter
            anchors.bottom              :   feedMin.top
            anchors.bottomMargin        :   Responsive.getV(20)
            text                        :   qsTr("Feed Min")
            font.family                 :   numericFont.name
            font.pixelSize              :   Responsive.getV(45)
            color                       :   "white"
        }

        RSpinBox {
            id                  :   feedMin
            anchors.left        :   feedRateSlider.right
            anchors.leftMargin  :   Responsive.getH(50)
            y                   :   advancedControlsTopLine + Responsive.getV(210)
            width               :   Responsive.getH(250)
            height              :   Responsive.getV(100)
            from                :   0
            to                  :   9999
            font.family         :   numericFont.name
            font.pixelSize      :   Responsive.getV(40)
            value               :   appSettings.feedRateMin
        }

        Binding{
            target  :   appSettings
            property:   "feedRateMin"
            value   :   feedMin.value
        }

        Text    {
            id                          :   feedRateSpinnerText
            anchors.horizontalCenter    :   feedRateSpinner.horizontalCenter
            anchors.bottom              :   feedRateSpinner.top
            anchors.bottomMargin        :   Responsive.getV(20)
            text                        :   qsTr("Feed Rate")
            font.family                 :   numericFont.name
            font.pixelSize              :   Responsive.getV(45)
            color                       :   "white"
        }

        RSpinBox{
            id                  :   feedRateSpinner
            anchors.left        :   feedRateSlider.right
            anchors.leftMargin  :   Responsive.getH(50)
            y                   :   advancedControlsTopLine + Responsive.getV(385)
            width               :   Responsive.getH(250)
            height              :   Responsive.getV(100)
            from                :   feedMin.value
            to                  :   feedMax.value
            value               :   appSettings.feedRate
            font.family         :   numericFont.name
            font.pixelSize      :   Responsive.getV(40)
        }

        Binding {
            target  :   appSettings
            property:   "feedRate"
            value   :   feedRateSpinner.value
        }

        Text    {
            id                          :   feedMaxText
            anchors.horizontalCenter    :   feedMax.horizontalCenter
            anchors.bottom              :   feedMax.top
            anchors.bottomMargin        :   Responsive.getV(20)
            text                        :   qsTr("Feed Max")
            font.family                 :   numericFont.name
            font.pixelSize              :   Responsive.getV(45)
            color                       :   "white"
        }

        RSpinBox{
            id                  :   feedMax
            anchors.left        :   feedRateSlider.right
            anchors.leftMargin  :   Responsive.getH(50)
            y                   :   advancedControlsTopLine + Responsive.getV(565)
            width               :   Responsive.getH(250)
            height              :   Responsive.getV(100)
            from                :   feedMin.value
            to                  :   99999
            font.family         :   numericFont.name
            font.pixelSize      :   Responsive.getV(40)
            value               :   appSettings.feedRateMax
        }

        Binding {
            target  :   appSettings
            property:   "feedRateMax"
            value   :   feedMax.value
        }

        Binding{
            target  :   appSettings
            property:   "feedRate"
            value   :   feedRateSlider.value
        }

        Binding {
            target  :   appSettings
            property:   "feedRate"
            value   :   feedRateSpinner.value
        }

        BackgroundPanel{
            id      :   makroText
            anchors.left            :   macroControls.left
            anchors.right           :   macroControls.right
            anchors.bottom          :   macroControls.top
            anchors.bottomMargin    :   Responsive.getV(40)
            height                  :   Responsive.getV(120)
            borderVisible           :   true

            Text {
                id              :   macrosHeader
                text            :   qsTr("Makrolar")
                font.pixelSize  :   Responsive.getV(35)
                anchors.centerIn:   parent
                color           :   "white"
            }
        }

        Grid    {
            id      :   macroControls
            rows    :   2
            columns :   4
            spacing :   Responsive.getH(20)
            y       :   advancedControlsTopLine + Responsive.getV(160)
            x       :   Responsive.getH(700)

            Repeater{
                model   :   8
                RButton{
                    width           :   Responsive.getH(270)
                    height          :   Responsive.getV(270)
                    text            :   "Makro " + (index + 1)
                    font.pixelSize  :   Responsive.getV(35)
                    onClicked       :   macrosNotImplementedDialog.open()
                }
            }
        }
    }

    RButton{
        id                          :   expandControlsButton
        anchors.horizontalCenter    :   parent.horizontalCenter
        anchors.bottom              :   parent.bottom
        anchors.bottomMargin        :   Responsive.getV(25)
        width                       :   Responsive.getH(1800)
        height                      :   Responsive.getV(60)
        text                        :   checked ? "Gelişmiş Kontroller -" : "Gelişmiş Kontroller +"
        font.pixelSize              :   Responsive.getV(30)
        z                           :   2
        checkable                   :   true
        checked                     :   false
        onCheckedChanged            :   {
            if(checked){
                controlsFlickable.flick(0 , Responsive.getV(-4000))
                controlsFlickable.interactive = true
            } else {
                var val = controlsFlickable.visibleArea.yPosition * controlsFlickable.contentHeight * 4
                controlsFlickable.flick(0 , val)
                controlsFlickable.interactive = false
            }
        }
    }
}
