import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3

Item{
    property Window   alignTo     :   Window { }
    property string sourceHd    :   ""
    property string source2k    :   ""
    property string source4k    :   ""

    width               :   alignTo.width
    height              :   alignTo.height
    clip                :   true

    Image   {
        source          :   {
            //If 4k required
            if(alignTo.height > 1600){
                if(source4k != "")
                    return source4k
                else if(source2k !== "")
                    return source2k
                else
                    return sourceHd
            }

            //If 2k required
            if(alignTo.height > 1080){
                if(source2k !== "")
                    return source2k
                else if(source4k !== "")
                    return source4k
                else
                    return sourceHd
            }

            //If hd is enough
            if(sourceHd !== "")
                return sourceHd
            else if(source2k !== "")
                return source2k
            else if(source4k !== "")
                return source4k
        }
        x               :   -((width - parent.width) / 2)
        y               :   -((height - parent.height) / 2)
    }
}
