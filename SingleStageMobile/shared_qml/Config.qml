pragma Singleton
import QtQuick 2.0

Item {

    readonly property string    buttonFont      :   "Linux Libertine"
    readonly property string    dialogFont      :   "Linux Libertine"
    readonly property string    headerFont      :   "Linux Libertine"

}
