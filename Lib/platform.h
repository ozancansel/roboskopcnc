#ifndef PLATFORM_H
#define PLATFORM_H

#include <QQuickItem>

static QObject *platform_singleton_provider(QQmlEngine *engine , QJSEngine *scriptEngine);

class Platform : public QQuickItem
{

    Q_OBJECT
    Q_PROPERTY(bool isMobile READ isMobile NOTIFY isMobileChanged)
    Q_PROPERTY(bool isDesktop READ isDesktop NOTIFY isDesktopChanged)

public:

    static void registerQmlType();
    Platform(QQuickItem* parent = nullptr);
    bool        isMobile();
    bool        isDesktop();

signals:

    void        isMobileChanged();
    void        isDesktopChanged();

public slots:

private:

    void        setDesktop(bool val);
    void        setMobile(bool val);

};

#endif // PLATFORM_H
