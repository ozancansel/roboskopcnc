import QtQuick 2.0
import "../"

Rectangle{

    property real   containerRadius     :   0.2
    property bool   borderVisible       :   false
    property bool   pressed             :   false
    property color  pressedColor        :   "#222222"

    id              :   control

    color           :   "#111111"

    border.width    :   borderVisible ? Responsive.getV(5) : 0
    border.color    :   "white"
    radius          :   Math.min(width , height) * containerRadius
}
