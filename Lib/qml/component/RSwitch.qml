import QtQuick 2.0
import QtQuick.Controls 2.2

Switch {
    id          :   control

    background  :   Item {
        id          :   backItem
        Rectangle{
            id      :   backRect
            width   :   parent.width - indicator.width
            height  :   parent.height * 0.7
//            color   :   control.checked ? "green" : "#e5e7e4"
//            color   :   control.checked ? "#008131" : "#e5e7e4"
            color   :   "#e5e7e4"
            anchors.verticalCenter  :   parent.verticalCenter
            anchors.horizontalCenter:   parent.horizontalCenter
            radius  :   height / 2
            border.width    :   height * 0.04
        }

        Rectangle   {
            anchors.centerIn: parent
            width: parent.width / 2
            height: parent.height / 2
            color : control.checked ? "#008131" : "#7e0000"
            radius: height / 2
            x       :   backRect.x + backRect.height / 2
            Behavior on color {
                ColorAnimation {
                    duration: 200
                }
            }
        }
    }

    indicator   :   Rectangle{
        height  :   control.height
        width   :   height
        radius  :   height / 2
        border.width    :   height * 0.03
        x       :   {
            if(control.checked)
                return control.width - width
            else
                return 0
        }

        Behavior on x{
            NumberAnimation{
            }
        }
    }
}
