import QtQuick 2.9
import QtQuick.Controls 2.2
import "../"

Drawer {
    id          :   appMenu
    background  :   Item {
        RButton {
            width   :   Responsive.getH(200)
            height  :   Responsive.getV(100)
            text    :   "<"
            anchors.right   :   parent.right
            anchors.bottom  :   parent.bottom
            anchors.rightMargin :   Responsive.getH(20)
            anchors.bottomMargin    :   Responsive.getV(10)
            onClicked   :   appMenu.close()
        }

        Rectangle{
            anchors.right   :   parent.right
            anchors.top     :   parent.top
            anchors.bottom  :   parent.bottom
            width           :   Responsive.getH(3)
            color           :   "white"
        }
    }
}
