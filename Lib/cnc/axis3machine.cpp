#include "axis3machine.h"
#include <QThread>
#include <QAbstractEventDispatcher>
#include <QtGlobal>
#include <math.h>

void Axis3Machine::registerQmlType(){
    qmlRegisterType<Axis3Machine>("RLib" , 1 , 0 , "Axis3Machine");
}

Axis3Machine::Axis3Machine(QQuickItem* parent) : QQuickItem(parent)
{

}

//Getter
SerialComm* Axis3Machine::port(){
    return m_comm;
}

//Setter
void    Axis3Machine::setPort(SerialComm *comm){
    m_comm = comm;
    if(port() != nullptr){
        m_device = comm->device();
    }
    emit portChanged();
}

//Slots
void Axis3Machine::incMoveXYZ(float x, float y, float z, float feedRate){
    float calculatedPosX = m_virtualX + x;
    float calculatedPosY = m_virtualY + y;
    float calculatedPosZ = m_virtualZ + z;
    QString cmd = QString("G90 G01 X%0 Y%1 Z%2 F%3\n")
            .arg(QString::number(calculatedPosX , 'f' , 3))
            .arg(QString::number(calculatedPosY , 'f' , 3))
            .arg(QString::number(calculatedPosZ , 'f' , 3))
            .arg(QString::number(feedRate , 'f' , 3));

    port()->send(cmd);

    //Wait for processed message
    //Does not mean that motion finished
    //It means just command processed or not
    //If processed enter block
    if(waitForOk()){
        //Apply motion to virtual pos
        m_virtualX = calculatedPosX;
        m_virtualY = calculatedPosY;
        m_virtualZ = calculatedPosZ;
    }
}

void Axis3Machine::linearMoveXYZ(float x, float y, float z, float feedRate){
    //Creating command
    QString cmd = QString("G90 G01 X%0 Y%1 Z%2 F%3\n")
            .arg(QString::number(x , 'f' , 3))
            .arg(QString::number(y , 'f' , 3))
            .arg(QString::number(z , 'f' , 3))
            .arg(QString::number(feedRate , 'f' , 3));

    //Sending to machine
    port()->send(cmd);

    //Wait for processed message
    //Does not mean that motion finished
    //It means just command processed
    if(waitForOk()){
        //Apply motion to virtual pos
        m_virtualX = x;
        m_virtualY = y;
        m_virtualZ = z;
    }
}

void Axis3Machine::rapidMoveXYZ(float x, float y, float z){
    //Creating command
    QString cmd = QString("G90 G01 X%0 Y%1 Z%2\n")
            .arg(QString::number(x , 'f' , 3))
            .arg(QString::number(y , 'f' , 3))
            .arg(QString::number(z , 'f' , 3));
    //Sending to machine
    port()->send(cmd);

    //Wait for processed message
    //Does not mean that motion finished
    //It means just command processed
    if(waitForOk()){
        //Apply motion to virtual pos
        m_virtualX = x;
        m_virtualY = y;
        m_virtualZ = z;
    }
}

void Axis3Machine::linearMoveX(float x, float feedRate){
    //Create command
    QString cmd = QString("G90 G01 X%0 F%1\n")
            .arg(QString::number(x , 'f' , 3))
            .arg(QString::number(feedRate , 'f' , 3));

    //Send to machine
    port()->send(cmd);

    if(waitForOk()){
        m_virtualX = x;
    }
}

void Axis3Machine::rapidMoveX(float x){
    QString cmd = QString("G90 G00 X%0\n")
            .arg(QString::number(x , 'f' , 3));

    port()->send(cmd);

    if(waitForOk()){
        m_virtualX = x;
    }
}

void Axis3Machine::linearMoveY(float y, float feedRate){
    //Create command
    QString cmd = QString("G90 G01 X%0 F%1\n")
            .arg(QString::number(y , 'f' , 3)
                 .arg(QString::number(feedRate , 'f' , 3)));

    //Send to machine
    port()->send(cmd);

    if(waitForOk()){
        m_virtualX = y;
    }
}

void Axis3Machine::rapidMoveY(float y){
    QString cmd = QString("G90 G00 Y%0")
            .arg(QString::number(y , 'f' , 3));

    port()->send(cmd);

    if(waitForOk()){
        m_virtualX = y;
    }
}

void Axis3Machine::linearMoveZ(float z, float feedRate){
    //Create command
    QString cmd = QString("G90 G01 Z%0 F%1\n")
            .arg(QString::number(z , 'f' , 3)
                 .arg(QString::number(feedRate , 'f' , 3)));

    //Send to machine
    port()->send(cmd);

    if(waitForOk()){
        m_virtualX = z;
    }
}

void Axis3Machine::rapidMoveZ(float z){
    QString cmd = QString("G90 G00 Z%0\n")
            .arg(QString::number(z , 'f' , 3));

    port()->send(cmd);

    if(waitForOk()){
        m_virtualX = z;
    }
}

void Axis3Machine::arcCw(float radius, float feedRate){
    arcCw(m_virtualX , m_virtualY , m_virtualZ , radius , feedRate);
}

void Axis3Machine::arcCw(float x, float y, float z, float radius, float feedRate){

}

void Axis3Machine::arcCcw(float radius, float feedRate){
    arcCcw(m_virtualX , m_virtualY , m_virtualZ , radius , feedRate);
}

void Axis3Machine::arcCcw(float x, float y, float z, float radius, float feedRate){

}

void Axis3Machine::dwell(int ms){
    QString cmd = QString("G04 P%0\n")
            .arg(QString::number(ms / 1000.0 , 'f' , 3));

    port()->send(cmd);
    waitForOk();
}

void Axis3Machine::reset(){
    QByteArray byte;

    byte.append(0x18);

    port()->write(byte);
    m_virtualX = 0;
    m_virtualY = 0;
    m_virtualZ = 0;
}

void Axis3Machine::hold(){
    QString cmd("!\n");

    port()->send(cmd);

    waitForOk();
}

void Axis3Machine::resume(){
    QString cmd("~\n");

    port()->send(cmd);

    waitForOk();
}

void Axis3Machine::unlock(){
    QString cmd("$X\n");

    port()->send(cmd);

    waitForOk();
}

void Axis3Machine::waitForFinish(){

}

bool Axis3Machine::waitForOk(){

    //If responded then enter loop
    while(!m_device->canReadLine()){
        thread()->eventDispatcher()->processEvents(QEventLoop::AllEvents);
    }

    QString line = m_device->readLine();
    qDebug() << "<< " << line;
    return line.contains("ok");
}
