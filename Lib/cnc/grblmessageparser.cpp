#include "grblmessageparser.h"
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QDebug>

void GrblMessageParser::registerQmlType(){
    qmlRegisterType<GrblMessageParser>("RLib" , 1 , 0 , "GrblMessageParser");
}

GrblMessageParser::GrblMessageParser(QQuickItem* parent)
    :
      QQuickItem(parent) ,
      m_statusExpr(QRegularExpression("<(.+)>")),
      m_stateExpr(QRegularExpression("^Idle|Run|Hold|Jog|Alarm|Door|Check|Home|Sleep$")) ,
      m_positionExpr(QRegularExpression("(WPos|MPos):(-?\\d+.\\d+),(-?\\d+.\\d+),(-?\\d+.\\d+)")) ,
      m_workOffsetExpr(QRegularExpression("^WCO:(-?\\d+.\\d+),(-?\\d+.\\d+),(-?\\d+.\\d+)$")) ,
      m_settingsExpr(QRegularExpression("(\\$\\d{1,3})=((\\d+)(\\.(\\d{1,3}))?)")) ,
      m_welcomeExpr(QRegularExpression("(Roboskop Stage Controller v1\\.0)|(Grbl 1\\.1f \\['$' for help\\])")) ,
      m_errorExpr(QRegularExpression("error:(\\d{1,2})")) ,
      m_alarmExpr(QRegularExpression("ALARM:(\\d{1,2})")) ,
      m_msgReceivedExpr(QRegularExpression("\\[MSG:.+\\]"))
{

    m_msgMap.insert(MSG_RESET_TO_CONTINUE , "[MSG:Reset to continue]");
    m_msgMap.insert(MSG_HOMING_OR_RESET_TO_UNLOCK , "[MSG:'$H'|'$X' to unlock]");
    m_msgMap.insert(MSG_ALARM_UNLOCKED , "[MSG:Caution: Unlocked]");
    m_msgMap.insert(MSG_ENABLED , "[MSG:Enabled]");
    m_msgMap.insert(MSG_DISABLED , "[MSG:Disabled]");
    m_msgMap.insert(MSG_CHECK_DOOR , "[MSG:Check Door]");
    m_msgMap.insert(MSG_CHECK_LIMITS , "[MSG:Check Limits]");
    m_msgMap.insert(MSG_PGM_END , "[MSG:Pgm End]");
    m_msgMap.insert(MSG_RESTORING_DEFAULTS , "[MSG:Restoring defaults]");
    m_msgMap.insert(MSG_SLEEPING ,  "[MSG:Sleeping]");

    m_errorCodeMap.insert(1 , "G-code words consist of a letter and a value. Letter was not found." );
    m_errorCodeMap.insert(2 , "Numeric value format is not valid or missing an expected value." );
    m_errorCodeMap.insert(3 , "Grbl '$' system command was not recognized or supported.");
    m_errorCodeMap.insert(4 , "Negative value received for an expected positive value.");
    m_errorCodeMap.insert(5 , "Homing cycle is not enabled via settings.");
    m_errorCodeMap.insert(6 , "Minimum step pulse time must be greater than 3usec");
    m_errorCodeMap.insert(7 , "EEPROM read failed. Reset and restored to default values.");
    m_errorCodeMap.insert(8 , "Grbl '$' command cannot be used unless Grbl is IDLE. Ensures smooth operation during a job.");
    m_errorCodeMap.insert(9 , "G-code locked out during alarm or jog state");
    m_errorCodeMap.insert(10 , "Soft limits cannot be enabled without homing also enabled.");
    m_errorCodeMap.insert(11 , "Max characters per line exceeded. Line was not processed and executed.");
    m_errorCodeMap.insert(12 , "(Compile Option) Grbl '$' setting value exceeds the maximum step rate supported.");
    m_errorCodeMap.insert(13 , "Safety door detected as opened and door state initiated.");
    m_errorCodeMap.insert(14 , "(Grbl-Mega Only) Build info or startup line exceeded EEPROM line length limit.");
    m_errorCodeMap.insert(15 , "Jog target exceeds machine travel. Command ignored.");
    m_errorCodeMap.insert(16 , "Jog command with no '=' or contains prohibited g-code.");
    m_errorCodeMap.insert(17 , "Laser mode requires PWM output.");
    m_errorCodeMap.insert(20 , "Unsupported or invalid g-code command found in block.");
    m_errorCodeMap.insert(21 , "More than one g-code command from same modal group found in block.");
    m_errorCodeMap.insert(22 , "Feed rate has not yet been set or is undefined.");
    m_errorCodeMap.insert(23 , "G-code command in block requires an integer value.");
    m_errorCodeMap.insert(24 , "Two G-code commands that both require the use of the XYZ axis words were detected in the block.");
    m_errorCodeMap.insert(25 , "A G-code word was repeated in the block.");
    m_errorCodeMap.insert(26 , "A G-code command implicitly or explicitly requires XYZ axis words in the block, but none were detected.");
    m_errorCodeMap.insert(27 , "N line number value is not within the valid range of 1 - 9,999,999.");
    m_errorCodeMap.insert(28 , "A G-code command was sent, but is missing some required P or L value words in the line.");
    m_errorCodeMap.insert(29 , "Grbl supports six work coordinate systems G54-G59. G59.1, G59.2, and G59.3 are not supported.");
    m_errorCodeMap.insert(30 , "The G53 G-code command requires either a G0 seek or G1 feed motion mode to be active. A different motion was active.");
    m_errorCodeMap.insert(31 , "There are unused axis words in the block and G80 motion mode cancel is active.");
    m_errorCodeMap.insert(32 , "A G2 or G3 arc was commanded but there are no XYZ axis words in the selected plane to trace the arc.");
    m_errorCodeMap.insert(33 , "The motion command has an invalid target. G2, G3, and G38.2 generates this error, if the arc is impossible to generate or if the probe target is the current position.");
    m_errorCodeMap.insert(34 , "A G2 or G3 arc, traced with the radius definition, had a mathematical error when computing the arc geometry. Try either breaking up the arc into semi-circles or quadrants, or redefine them with the arc offset definition.");
    m_errorCodeMap.insert(35 , "A G2 or G3 arc, traced with the offset definition, is missing the IJK offset word in the selected plane to trace the arc.");
    m_errorCodeMap.insert(36 , "There are unused, leftover G-code words that aren't used by any command in the block.");
    m_errorCodeMap.insert(37 , "The G43.1 dynamic tool length offset command cannot apply an offset to an axis other than its configured axis. The Grbl default axis is the Z-axis.");

    m_alarmCodeMap.insert(1 , "Kızak end sensörlerine fazla yaklaştı. Tekrardan 'Homing' yapınız.");
    m_alarmCodeMap.insert(2 , "Kızağın pozisyonu belirlenen limitleri geçti.");
    m_alarmCodeMap.insert(3 , "Hareket esnasında reset atıldı. Tekrardan 'Homing' yapınız.");
    m_alarmCodeMap.insert(4 , "Probe fail. The probe is not in the expected initial state before starting probe cycle, where G38.2 and G38.3 is not triggered and G38.4 and G38.5 is triggered.");
    m_alarmCodeMap.insert(5 , "Probe fail. Probe did not contact the workpiece within the programmed travel for G38.2 and G38.4.");
    m_alarmCodeMap.insert(6 , "Homing yapılamadı. Homing sırasında sistem kapatıldı.");
    m_alarmCodeMap.insert(7 , "Homing yapılamadı. Safety door was opened during active homing cycle.");
    m_alarmCodeMap.insert(8 , "Homing yapılamadı. Problemi çözmek için geri dönüş mesafesini artırınız.");
    m_alarmCodeMap.insert(9 , "Homing yapılamadı. End sensörleri bulunamadı. Maksimum mesafeyi artırınız.");

}

SerialComm* GrblMessageParser::comm(){
    return m_comm;
}

void GrblMessageParser::setComm(SerialComm *comm){
    m_comm = comm;

    if(comm != nullptr){
        m_socket = comm->device();
        if(m_socket != nullptr)
            connect(m_socket , SIGNAL(readyRead()) , this , SLOT(readyRead()));
    }

    emit commChanged();
}

void GrblMessageParser::readyRead(){

    if(!isEnabled())
        return;


    while(m_socket->canReadLine()){
        QString str;

        if(m_socket->canReadLine())
            str.append(m_socket->readLine());

        str.replace("\r" , "");
        str.replace("\n" , "");

        if(str.length() == 0)
            continue;

        parse(str);
    }
}

void GrblMessageParser::parse(QString str){

    qDebug() << "Received => str " << str;

    if(str.at(0) == "<"){

        QRegularExpressionMatch sentenceMatch = m_statusExpr.match(str);
        QString content = sentenceMatch.captured(1);
        QStringList blocks = content.split("|");

        GrblStatus status;

        foreach (QString block, blocks) {
            QRegularExpressionMatch stateMatch = m_stateExpr.match(block);
            if(stateMatch.hasMatch()){
                status.state = stateMatch.captured(0);
                continue;
            }
            QRegularExpressionMatch positionMatch = m_positionExpr.match(block);
            if(positionMatch.hasMatch()){
                status.x = positionMatch.captured(2).toDouble();
                status.y = positionMatch.captured(3).toDouble();
                status.z = positionMatch.captured(4).toDouble();
                continue;
            }
            QRegularExpressionMatch wcoMatch = m_workOffsetExpr.match(block);
            if(wcoMatch.hasMatch()){
                status.workOffsetReceived = true;
                status.workOffsetX = wcoMatch.captured(1).toDouble();
                status.workOffsetY = wcoMatch.captured(2).toDouble();
                status.workOffsetZ = wcoMatch.captured(3).toDouble();
            }
        }

        emit statusUpdate(status);
    } else if(str.length() == 2 && str == "ok"){
        emit okMessageReceived();
    } else if(str.startsWith("er")){
        QRegularExpressionMatch errorMatch = m_errorExpr.match(str);
        if(errorMatch.hasMatch()){
            QString errorCodeStr = errorMatch.captured(1);
            int errorCode = errorCodeStr.toInt();
            QString errorStr = m_errorCodeMap.value(errorCode);
            emit errorMessageReceived(errorCode , errorStr);
        }
    }  else if(str.at(0) == "$"){
        QRegularExpressionMatch settingsMatch = m_settingsExpr.match(str);

        if(settingsMatch.hasMatch()){
            QString key = settingsMatch.captured(1);
            if(settingsMatch.lastCapturedIndex() == 5){
                QVariant value(settingsMatch.captured(2).toDouble());
                emit settingsUpdate(key , value);
            } else if (settingsMatch.lastCapturedIndex() < 5){
                QVariant value(settingsMatch.captured(2).toInt());
                emit settingsUpdate(key , value);
            }
        }
    } else if(str.at(0) == "R"){
        QRegularExpressionMatch welcomeMatch = m_welcomeExpr.match(str);

        if(welcomeMatch.hasMatch()){
            emit welcomeCommandReceived();
        }
    } else if(str.contains("ALARM")){
        QRegularExpressionMatch alarmMatch = m_alarmExpr.match(str);

        if  (alarmMatch.hasMatch()) {
            QString alarmCodeStr = alarmMatch.captured(1);
            int     alarmCode = alarmCodeStr.toInt();
            QString messageStr = m_alarmCodeMap[alarmCode];
            emit alarmCodeReceived(alarmCode , messageStr);
        }
    } else if(str.startsWith("[") && str.endsWith("]")) {
        foreach (int key, m_msgMap.keys()) {
            if(m_msgMap.value(key) == str){
                emit messageReceived(key);
            }
        }
    }
}

bool GrblMessageParser::parseSettings(QString &str, QString &key, QVariant &val){
    QRegularExpressionMatch settingsMatch = m_settingsExpr.match(str);

    if(settingsMatch.hasMatch()){
        key = settingsMatch.captured(1);
        if(settingsMatch.lastCapturedIndex() == 5){
            val.setValue(settingsMatch.captured(2).toDouble());
        } else if (settingsMatch.lastCapturedIndex() < 5){
            val.setValue(settingsMatch.captured(2).toInt());
        }
        return true;
    } else
        return false;
}
