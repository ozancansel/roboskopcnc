#ifndef GRBLSTATUS_H
#define GRBLSTATUS_H

#include <QString>

struct GrblStatus
{
    QString state;
    double x;
    double y;
    double z;
    bool   workOffsetReceived;
    double workOffsetX;
    double workOffsetY;
    double workOffsetZ;
    GrblStatus() : x(0) , y(0) , z(0) , workOffsetReceived(false) , workOffsetX(0) , workOffsetY(0) , workOffsetZ(0){ }
};

#endif // GRBLSTATUS_H
