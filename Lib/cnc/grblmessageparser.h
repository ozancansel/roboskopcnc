#ifndef GRBLMESSAGEPARSER_H
#define GRBLMESSAGEPARSER_H

#include <QQuickItem>
#include <QRegularExpression>
#include <QMap>
#include "serial/serialcomm.h"
#include "grblstatus.h"

//Alarms
#define ALARM_HARD_LIMIT_TRIGGERED          1
#define ALARM_EXCEEDS_MACHINE_TRAVEL        2
#define ALARM_RESET_IN_MOTION               3
#define ALARM_HOMING_FAIL_RESET             6
#define ALARM_HOMING_FAIL_SAFETY_DOOR       7
#define ALARM_HOMING_FAIL_SHORT_PULL_OFF    8
#define ALARM_HOMING_FAIL_SENSOR_NOT_FOUND  9

#define MSG_RESET_TO_CONTINUE           1
#define MSG_HOMING_OR_RESET_TO_UNLOCK   2
#define MSG_ALARM_UNLOCKED              3
#define MSG_ENABLED                     4
#define MSG_DISABLED                    5
#define MSG_CHECK_DOOR                  6
#define MSG_CHECK_LIMITS                7
#define MSG_PGM_END                     8
#define MSG_RESTORING_DEFAULTS          9
#define MSG_SLEEPING                    10

class GrblMessageParser : public QQuickItem
{

    Q_OBJECT
    Q_PROPERTY(SerialComm* comm READ comm WRITE setComm NOTIFY commChanged)

public:

    static void     registerQmlType();
    GrblMessageParser(QQuickItem* parent = nullptr);
    SerialComm*     comm();
    void            setComm(SerialComm* comm);
    bool            parseSettings(QString &str , QString &key , QVariant &val);

private slots:

    void            readyRead();

signals:

    void            commChanged();
    void            settingsUpdate(QString key , QVariant value);
    void            statusUpdate(GrblStatus status);
    void            okMessageReceived();
    void            errorMessageReceived(int errorCode , QString errorMessage);
    void            welcomeCommandReceived();
    void            alarmCodeReceived(int code , QString message);
    void            messageReceived(int code);

private:

    SerialComm*             m_comm;
    QIODevice*              m_socket;
    QString                 m_buffer;
    QRegularExpression      m_statusExpr;
    QRegularExpression      m_stateExpr;
    QRegularExpression      m_positionExpr;
    QRegularExpression      m_workOffsetExpr;
    QRegularExpression      m_settingsExpr;
    QRegularExpression      m_welcomeExpr;
    QRegularExpression      m_errorExpr;
    QRegularExpression      m_alarmExpr;
    QRegularExpression      m_msgReceivedExpr;

    QMap<int , QString>     m_msgMap;
    QMap<int , QString>     m_errorCodeMap;
    QMap<int , QString>     m_alarmCodeMap;

    void                    parse(QString str);

};

#endif // GRBLMESSAGEPARSER_H

