#ifndef CNCPROXY_H
#define CNCPROXY_H

#include <QQuickItem>
#include <QQueue>
#include <QTimer>
#include <QMap>
#include "serial/serialcomm.h"
#include "grblmessageparser.h"

class GrblSettings;

class StageProxy : public QQuickItem
{

    Q_OBJECT
    Q_ENUMS(State)
    Q_PROPERTY(SerialComm* comm READ comm WRITE setComm NOTIFY commChanged)
    Q_PROPERTY(GrblMessageParser* parser READ parser WRITE setParser NOTIFY parserChanged)
    Q_PROPERTY(GrblSettings* grblSettings READ grblSettings WRITE setGrblSettings NOTIFY grblSettingsChanged)
    Q_PROPERTY(State machineState READ machineState NOTIFY machineStateChanged)
    Q_PROPERTY(double position READ position NOTIFY positionChanged)
    Q_PROPERTY(double workOffset READ workOffset NOTIFY workOffsetChanged)
    Q_PROPERTY(bool homingRunning READ homingRunning NOTIFY homingRunningChanged)
    Q_PROPERTY(bool calibrating READ calibrating WRITE setCalibrating NOTIFY calibratingChanged)

public  :

    enum State{ Idle , Run , Hold , Jog , Alarm , Door , Check, Home, Sleep , Unknown };
    static void     registerQmlType();
    StageProxy(QQuickItem* parent = nullptr);
    SerialComm*         comm();
    GrblMessageParser*  parser();
    GrblSettings*       grblSettings();
    State               machineState();
    double              position();
    double              workOffset();
    void                setComm(SerialComm* comm);
    void                setParser(GrblMessageParser* parser);
    void                setGrblSettings(GrblSettings* settings);
    void                setWorkOffset(double val);
    bool                homingRunning();
    bool                calibrating();
    void                setCalibrating(bool val);

public slots:

    void                doHoming();
    void                cancelHoming();
    void                goHome();
    void                move(double x , double feedRate);
    void                goTo(double x , double feedRate);
    void                setZero();
    void                goZero();
    void                unlockStage();
    void                reset();
    void                hold();
    void                resume();
    void                startCalibration(int val);
    void                resumeCalibration();
    bool                finishCalibration(double val);
    void                cancelCalibration();

private slots:

    void            statusTick();
    void            statusUpdate(GrblStatus status);
    void            connectionStatusChanged();
    void            alarmMessageReceived(int alarmCode , QString message);
    void            messageReceived(int code);
    void            okMessageReceived();
    void            errorMessageReceived(int errCode , QString errMsg);

signals:

    void            commChanged();
    void            connectionIsNotOpen();
    void            commNotSet();
    void            machineStateChanged();
    void            parserChanged();
    void            positionChanged();
    void            workOffsetChanged();
    void            commandQueueChanged();
    void            homingRunningChanged();
    void            calibratingChanged();
    void            homingError();
    void            hardLimitsTriggered();
    void            resetInMotion();
    void            homingFailResetWhile();
    void            homingFailSafetyDoor();
    void            homingFailShortPullOff();
    void            homingFailSensorNotFound();
    void            homingRequired();
    void            unlockRequired();
    void            exceedsMachineTravel();
    void            exceedsMachineTravelNoReset();
    void            resetToContinue();
    void            grblSettingsChanged();
    void            calibrationMovementEnd();


private:


    SerialComm*         m_comm;
    GrblMessageParser*  m_parser;
    GrblSettings*       m_grblSettings;
    double              m_position;
    double              m_workOffset;
    QQueue<QStringList> mCommands;
    QTimer              m_statusTimer;
    State               m_machineState;
    QMap<QString,State> m_strToStateMap;
    QMap<State,QString> m_stateToStrMap;
    QMap<int , int>     m_degreeMap;
    bool                m_homingRunning;
    bool                m_calibrating;
    double              m_calibrationStartPos;
    int                 m_calibrationStage;
    double              m_target;

    bool                checkPort();
    void                setPosition(double pos);
    void                setMachineState(State state);
    void                setHomingRunning(bool enabled);

};

#endif // CNCPROXY_H
