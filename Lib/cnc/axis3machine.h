#ifndef AXIS3MACHINE_H
#define AXIS3MACHINE_H

#include <QQuickItem>
#include "serial/serialcomm.h"

class Axis3Machine : public QQuickItem
{

    Q_OBJECT
    Q_PROPERTY(SerialComm* port READ port WRITE setPort NOTIFY portChanged)

public:

    static void registerQmlType();
    Axis3Machine(QQuickItem* parent = nullptr);
    void        setPort(SerialComm* comm);
    SerialComm* port();

signals:

    void        portChanged();

public slots:

    void        incMoveXYZ(float x, float y, float z , float feedRate);
    void        linearMoveXYZ(float x , float y , float z , float feedRate);
    void        rapidMoveXYZ(float x , float y , float z);
    void        linearMoveX(float x , float feedRate);
    void        rapidMoveX(float x);
    void        linearMoveY(float y , float feedRate);
    void        rapidMoveY(float y);
    void        linearMoveZ(float z , float feedRate);
    void        rapidMoveZ(float z);
    void        arcCw(float radius , float feedRate);
    void        arcCw(float x , float y , float z , float radius , float feedRate);
    void        arcCcw(float radius , float feedRate);
    void        arcCcw(float x , float y , float z , float radius , float feedRate);
    void        dwell(int ms);
    void        waitForFinish();
    void        reset();
    void        hold();
    void        resume();
    void        unlock();

private:

    SerialComm* m_comm;
    QIODevice*  m_device;
    float       m_virtualX;
    float       m_virtualY;
    float       m_virtualZ;

    bool        waitForOk();

};

#endif // AXIS3MACHINE_H
