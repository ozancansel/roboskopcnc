#include "stageproxy.h"
#include "grblsettings.h"
#include <QtMath>

void StageProxy::registerQmlType(){
    qmlRegisterType<StageProxy>("RLib" , 1 , 0 , "StageProxy");
}

StageProxy::StageProxy(QQuickItem* parent)
    :
      QQuickItem(parent) ,
      m_comm(nullptr) ,
      m_parser(nullptr) ,
      m_grblSettings(nullptr) ,
      m_position(0) ,
      m_workOffset(0) ,
      m_machineState(Unknown) ,
      m_homingRunning(false) ,
      m_calibrating(false) ,
      m_calibrationStartPos(0)
{
    connect(&m_statusTimer , SIGNAL(timeout()) , this , SLOT(statusTick()));
    m_statusTimer.setInterval(250);
    m_statusTimer.start();

    m_strToStateMap.insert("Idle" , Idle);
    m_strToStateMap.insert("Run" , Run);
    m_strToStateMap.insert("Hold" , Hold);
    m_strToStateMap.insert("Jog" , Jog);
    m_strToStateMap.insert("Alarm" , Alarm);
    m_strToStateMap.insert("Door" , Door);
    m_strToStateMap.insert("Check" , Check);
    m_strToStateMap.insert("Home" , Home);
    m_strToStateMap.insert("Sleep" , Sleep);

    m_stateToStrMap.insert(Idle , "Idle");
    m_stateToStrMap.insert(Run , "Run");
    m_stateToStrMap.insert(Hold , "Hold" );
    m_stateToStrMap.insert(Jog , "Jog");
    m_stateToStrMap.insert(Alarm , "Alarm" );
    m_stateToStrMap.insert(Door , "Door");
    m_stateToStrMap.insert(Check , "Check");
    m_stateToStrMap.insert(Home , "Home");
    m_stateToStrMap.insert(Sleep , "Sleep");

    m_degreeMap[0] = 10;
    m_degreeMap[1] = 30;
    m_degreeMap[2] = 50;
}

//Getter
SerialComm* StageProxy::comm(){
    return m_comm;
}
GrblMessageParser* StageProxy::parser(){
    return m_parser;
}
GrblSettings* StageProxy::grblSettings(){
    return m_grblSettings;
}
StageProxy::State StageProxy::machineState(){
    return m_machineState;
}
bool StageProxy::homingRunning(){
    return m_homingRunning;
}
double StageProxy::position(){
    return m_position;// - m_workOffset;
}
double StageProxy::workOffset(){
    return m_workOffset;
}
bool StageProxy::calibrating(){
    return m_calibrating;
}
//Getter END!

//Setter
void StageProxy::setParser(GrblMessageParser *parser){
    m_parser = parser;

    if(m_parser != nullptr) {
        connect(m_parser , SIGNAL(statusUpdate(GrblStatus)) , this , SLOT(statusUpdate(GrblStatus)));
        connect(m_parser ,  SIGNAL(alarmCodeReceived(int,QString)) , this , SLOT(alarmMessageReceived(int,QString)));
        connect(m_parser , SIGNAL(messageReceived(int)) , this , SLOT(messageReceived(int)));
        connect(m_parser , SIGNAL(okMessageReceived()) , this , SLOT(okMessageReceived()));
        connect(m_parser , SIGNAL(errorMessageReceived(int,QString)) , this , SLOT(errorMessageReceived(int,QString)));
    }

    emit parserChanged();
}
void StageProxy::setComm(SerialComm *comm){
    m_comm = comm;

    if(m_comm != nullptr){
        connect(m_comm , SIGNAL(stateChanged()) , this , SLOT(connectionStatusChanged()));
    }

    emit commChanged();
}
void StageProxy::setPosition(double pos){
    m_position = pos;
    emit positionChanged();
}
void StageProxy::setGrblSettings(GrblSettings *settings){
    m_grblSettings = settings;
    emit grblSettingsChanged();
}
void StageProxy::setMachineState(State state){
    m_machineState = state;
    emit machineStateChanged();
}
void StageProxy::setHomingRunning(bool enabled){
    m_homingRunning = enabled;
    emit homingRunningChanged();
}
void StageProxy::setWorkOffset(double val){
    m_workOffset = val;
    emit  workOffsetChanged();
}
void StageProxy::setCalibrating(bool val){
    m_calibrating = val;
    emit calibratingChanged();
}
//Setter END !

//Slots
void StageProxy::doHoming(){
    if(!checkPort())    return;

    qDebug() << "Limit pins " << grblSettings()->limitPinsInvert();
    if(!grblSettings()->limitPinsInvert()){
        grblSettings()->saveSettings(LIMIT_PINS_INVERT , QVariant(1));
    }

    //Stage disable ediliyor
    setEnabled(false);

    setHomingRunning(true);
    comm()->send("$H\n");
}

void StageProxy::cancelHoming(){
    //Not implemented
    if(comm()->getState() != SerialComm::Connected)
        return;

    reset();
    setEnabled(true);
}

void StageProxy::goHome(){
    if(!checkPort())    return;
}

void StageProxy::move(double x , double speed){
    if(!checkPort())    return;
    QString command = QString("$j=G91 G21 X%0F%1\n")
            .arg(QString::number(x , 'f' , 3))
            .arg(speed);
    comm()->send(command);
}

void StageProxy::goTo(double x , double feedRate){
    if(!checkPort())    return;

    if(grblSettings()->loaded()){
        if(grblSettings()->softLimitsEnabled() && (x < -grblSettings()->xMaxTravel() || x > 0)){
            exceedsMachineTravelNoReset();
            return;
        }
    }

    QString command = QString("G90 G01 X%0 F%1\n")
            .arg(QString::number(x , 'f' , 3))
            .arg(QString::number(feedRate , 'f' , 3));
    comm()->send(command);
}

void StageProxy::goZero(){
    if(!checkPort())    return;

    comm()->send(QString("G90 G0 X0 Y0\n"));
}

void StageProxy::setZero(){
    if(!checkPort())    return;

    comm()->send(QString("G10 P0 L20 X0 Y0 Z0\n"));
}

void StageProxy::statusTick(){
    if(!checkPort())    return;

    comm()->send("?");
}

void StageProxy::statusUpdate(GrblStatus status){
    if(grblSettings()->reportInches()){
        grblSettings()->saveSettings(REPORT_INCHES , QVariant(0));
    }
    setMachineState(m_strToStateMap[status.state]);
    setPosition(status.x);
    if(calibrating()){
        if(m_target == position()){
            emit calibrationMovementEnd();
            setCalibrating(false);
        }
    }

    if(status.workOffsetReceived)
        setWorkOffset(status.workOffsetX);
}

void StageProxy::connectionStatusChanged(){
    if(comm()->getState() == SerialComm::Disconnected || comm()->getState() == SerialComm::None){
        setMachineState(Unknown);
        setPosition(0);
    }
}

void StageProxy::unlockStage(){
    if(comm()->getState() != SerialComm::Connected)
        return;

    comm()->send("$X\n");
}

void StageProxy::hold(){
    if(!checkPort())
        return;

    if(!checkPort())
        return;

    comm()->send("!");
}

void StageProxy::resume(){
    if(!checkPort())
        return;

    comm()->send("~");
}

void StageProxy::reset(){
    if(comm()->getState() != SerialComm::Connected)
        return;

    QByteArray byte;

    byte.append(0x18);

    comm()->write(byte);
}

void StageProxy::messageReceived(int code){

    //Eger ki homing yapilirken mesaj geldiyse
    if(homingRunning()){
        setHomingRunning(false);
        setEnabled(true);
    }

    if(code == MSG_HOMING_OR_RESET_TO_UNLOCK){
        if(!m_grblSettings->loaded())
            m_grblSettings->requestSettingsSync();

        if(m_grblSettings->homingCycleEnabled())
            emit homingRequired();
        else
            emit unlockRequired();
    }
    else if( code == MSG_RESET_TO_CONTINUE)
        emit resetToContinue();
}

void StageProxy::okMessageReceived(){
    if(homingRunning()){
        setHomingRunning(false);
        setEnabled(true);
    }
}

void StageProxy::alarmMessageReceived(int alarmCode, QString message){

    Q_UNUSED(message)

    if(alarmCode == ALARM_HARD_LIMIT_TRIGGERED)
        emit hardLimitsTriggered();
    else if(alarmCode == ALARM_RESET_IN_MOTION)
        emit resetInMotion();
    else if (alarmCode == ALARM_HOMING_FAIL_RESET)
        emit homingFailResetWhile();
    else if (alarmCode == ALARM_HOMING_FAIL_SAFETY_DOOR)
        emit homingFailSafetyDoor();
    else if (alarmCode == ALARM_HOMING_FAIL_SHORT_PULL_OFF)
        emit homingFailShortPullOff();
    else if (alarmCode == ALARM_HOMING_FAIL_SENSOR_NOT_FOUND)
        emit homingFailSensorNotFound();
    else if (alarmCode == ALARM_EXCEEDS_MACHINE_TRAVEL)
        emit exceedsMachineTravel();

}

void StageProxy::errorMessageReceived(int errCode, QString errMsg){
    //Makina limitlerini astiniz
    if(errCode == 15){
        qDebug() << "Err " << errCode << " Msg => " << errMsg;
        emit exceedsMachineTravelNoReset();
    }
}

void StageProxy::startCalibration(int val){
    double beforeCalibrationPos;
    if(val == 0)
        beforeCalibrationPos = position();
    else
        beforeCalibrationPos = m_calibrationStartPos;
    double movement = -m_degreeMap.value(val);
    double recoverMovement =  beforeCalibrationPos - position();
    m_calibrationStage = val;
    m_calibrationStartPos = position() +  recoverMovement;
    m_target = position() + movement + recoverMovement;
    move(recoverMovement , m_grblSettings->xMaxMmRatePerMin());
    move(movement , m_grblSettings->xMaxMmRatePerMin());
    setCalibrating(true);
}

void StageProxy::resumeCalibration(){
    m_calibrationStage++;
    startCalibration(m_calibrationStage);
}

bool StageProxy::finishCalibration(double val){
    double movement = qAbs(position() - m_calibrationStartPos);
    double newStepNumber = (movement / val  ) * grblSettings()->xStepPerMm();

    bool result = grblSettings()->saveSettings(X_STEP_PER_MM , QVariant(qRound(newStepNumber)));

    setCalibrating(false);
    return result;
}

void StageProxy::cancelCalibration(){
    setCalibrating(false);
}

//Slots END !
bool StageProxy::checkPort(){

    if(m_comm == nullptr){
        emit commNotSet();
        return false;
    }

    if(m_comm->getState() != SerialComm::Connected){
        emit connectionIsNotOpen();
        return false;
    }

    if(!isEnabled())
        return false;

    if(homingRunning())
        return false;

    return true;
}
