#ifndef GRBLSETTINGS_H

#define GRBLSETTINGS_H

#include <QQuickItem>
#include <QVariant>
#include <QQueue>
#include <QRegularExpression>
#include "serial/serialcomm.h"
#include "cnc/grblmessageparser.h"
#include "stageproxy.h"

#define STEP_PULSE                  "$0"
#define STEP_IDLE_DELAY             "$1"
#define STEP_PORT_INVERT_MASK       "$2"
#define DIRECTION_PORT_INVERT_MASK  "$3"
#define STEP_ENABLE_INVERT          "$4"
#define LIMIT_PINS_INVERT           "$5"
#define PROBE_PIN_INVERT            "$6"
#define STATUS_REPORT_MASK          "$10"
#define JUNCTION_DEVIATION          "$11"
#define ARC_TOLERANCE               "$12"
#define REPORT_INCHES               "$13"
#define SOFT_LIMITS_ENABLED         "$20"
#define HARD_LIMITS_ENABLED         "$21"
#define HOMING_CYCLE_ENABLED        "$22"
#define HOMING_DIR_INVERT_MASK      "$23"
#define HOMING_FEED                 "$24"
#define HOMING_SEEK                 "$25"
#define HOMING_DEBOUNCE             "$26"
#define HOMING_PULL_OFF             "$27"
#define MAX_SPINDLE_RPM             "$30"
#define MIN_SPINDLE_RPM             "$31"
#define LASER_MODE_ENABLED          "$32"
#define X_STEP_PER_MM               "$100"
#define Y_STEP_PER_MM               "$101"
#define Z_STEP_PER_MM               "$102"
#define X_MAX_MM_RATE_PER_MIN       "$110"
#define Y_MAX_MM_RATE_PER_MIN       "$111"
#define Z_MAX_MM_RATE_PER_MIN       "$112"
#define X_ACCELERATION              "$120"
#define Y_ACCELERATION              "$121"
#define Z_ACCELERATION              "$122"
#define X_MAX_TRAVEL                "$130"
#define Y_MAX_TRAVEL                "$131"
#define Z_MAX_TRAVEL                "$132"

class GrblSettings : public QQuickItem
{

    Q_OBJECT
    Q_PROPERTY(SerialComm* comm READ comm WRITE setComm NOTIFY commChanged)
    Q_PROPERTY(GrblMessageParser* parser READ parser WRITE setParser NOTIFY parserChanged)
    Q_PROPERTY(bool loaded READ loaded NOTIFY loadedChanged)
    Q_PROPERTY(int stepPulse READ stepPulse WRITE setStepPulse NOTIFY stepPulseChanged)
    Q_PROPERTY(int stepIdleDelay READ stepIdleDelay WRITE setStepIdleDelay NOTIFY stepIdleDelayChanged)
    Q_PROPERTY(int stepPortInvertMask READ stepPortInvertMask WRITE setStepPortInvertMask NOTIFY stepPortInvertMaskChanged)
    Q_PROPERTY(int directionPortInvertMask READ directionPortInvertMask WRITE setDirectionPortInvertMask NOTIFY directionPortInvertMaskChanged)
    Q_PROPERTY(bool stepEnableInvert READ stepEnableInvert WRITE setStepEnableInvert NOTIFY stepEnableInvertChanged)
    Q_PROPERTY(int limitPinsInvert READ limitPinsInvert WRITE setLimitPinsInvert NOTIFY limitPinsInvertChanged)
    Q_PROPERTY(int probePinInvert READ probePinInvert WRITE setProbePinInvert NOTIFY probePinInvertChanged)
    Q_PROPERTY(int statusReportMask READ statusReportMask WRITE setStatusReportMask NOTIFY statusReportMaskChanged)
    Q_PROPERTY(double junctionDeviation READ junctionDeviation WRITE setJunctionDeviation NOTIFY junctionDeviationChanged)
    Q_PROPERTY(double arcTolerance READ arcTolerance WRITE setArcTolerance NOTIFY arcToleranceChanged)
    Q_PROPERTY(bool reportInches READ reportInches WRITE setReportInches NOTIFY reportInchesChanged)
    Q_PROPERTY(bool softLimitsEnabled READ softLimitsEnabled WRITE setSoftLimitsEnabled NOTIFY softLimitsEnabledChanged)
    Q_PROPERTY(bool hardLimitsEnabled READ hardLimitsEnabled WRITE setHardLimitsEnabled NOTIFY hardLimitsEnabledChanged)
    Q_PROPERTY(bool homingCycleEnabled READ homingCycleEnabled WRITE setHomingCycleEnabled NOTIFY homingCycleEnabledChanged)
    Q_PROPERTY(int homingDirInvertMask READ homingDirInvertMask WRITE setHomingDirInvertMask NOTIFY homingDirInvertMaskChanged)
    Q_PROPERTY(double homingFeed READ homingFeed WRITE setHomingFeed NOTIFY homingFeedChanged)
    Q_PROPERTY(double homingSeek READ homingSeek WRITE setHomingSeek NOTIFY homingSeekChanged)
    Q_PROPERTY(int homingDebounce READ homingDebounce WRITE setHomingDebounce NOTIFY homingDebounceChanged)
    Q_PROPERTY(double homingPullOff READ homingPullOff WRITE setHomingPullOff NOTIFY homingPullOffChanged)
    Q_PROPERTY(int maxSpindleRpm READ maxSpindleRpm WRITE setMaxSpindleRpm NOTIFY maxSpindleRpmChanged)
    Q_PROPERTY(int minSpindleRpm READ minSpindleRpm WRITE setMinSpindleRpm NOTIFY minSpindleRpmChanged)
    Q_PROPERTY(bool laserModeEnabled READ laserModeEnabled WRITE setLaserModeEnabled NOTIFY laserModeEnabledChanged)
    Q_PROPERTY(double xStepPerMm READ xStepPerMm WRITE setXStepPerMm NOTIFY xStepPerMmChanged)
    Q_PROPERTY(double yStepPerMm READ yStepPerMm WRITE setYStepPerMm NOTIFY yStepPerMmChanged)
    Q_PROPERTY(double zStepPerMm READ zStepPerMm WRITE setZStepPerMm NOTIFY zStepPerMmChanged)
    Q_PROPERTY(double xMaxMmRatePerMin READ xMaxMmRatePerMin WRITE setXMaxMmRatePerMin NOTIFY xMaxMmRatePerMinChanged)
    Q_PROPERTY(double yMaxMmRatePerMin READ yMaxMmRatePerMin WRITE setYMaxMmRatePerMin NOTIFY yMaxMmRatePerMinChanged)
    Q_PROPERTY(double zMaxMmRatePerMin READ zMaxMmRatePerMin WRITE setZMaxMmRatePerMin NOTIFY zMaxMmRatePerMinChanged)
    Q_PROPERTY(double xAcceleration READ xAcceleration WRITE setXAcceleration NOTIFY xAccelerationChanged)
    Q_PROPERTY(double yAcceleration READ yAcceleration WRITE setYAcceleration NOTIFY yAccelerationChanged)
    Q_PROPERTY(double zAcceleration READ zAcceleration WRITE setZAcceleration NOTIFY zAccelerationChanged)
    Q_PROPERTY(double xMaxTravel READ xMaxTravel WRITE setXMaxTravel NOTIFY xMaxTravelChanged)
    Q_PROPERTY(double yMaxTravel READ yMaxTravel WRITE setYMaxTravel NOTIFY yMaxTravelChanged)
    Q_PROPERTY(double zMaxTravel READ zMaxTravel WRITE setZMaxTravel NOTIFY zMaxTravelChanged)

public:

    static void         registerQmlType();
    GrblSettings(QQuickItem* parent = nullptr);

    //Getter
    SerialComm*         comm();
    GrblMessageParser*  parser();
    bool                loaded();
    int                 stepPulse();
    int                 stepIdleDelay();
    int                 stepPortInvertMask();
    int                 directionPortInvertMask();
    bool                stepEnableInvert();
    bool                limitPinsInvert();
    bool                probePinInvert();
    int                 statusReportMask();
    double              junctionDeviation();
    double              arcTolerance();
    bool                reportInches();
    bool                softLimitsEnabled();
    bool                hardLimitsEnabled();
    bool                homingCycleEnabled();
    int                 homingDirInvertMask();
    double              homingFeed();
    double              homingSeek();
    int                 homingDebounce();
    double              homingPullOff();
    int                 maxSpindleRpm();
    int                 minSpindleRpm();
    bool                laserModeEnabled();
    double              xStepPerMm();
    double              yStepPerMm();
    double              zStepPerMm();
    double              xMaxMmRatePerMin();
    double              yMaxMmRatePerMin();
    double              zMaxMmRatePerMin();
    double              xAcceleration();
    double              yAcceleration();
    double              zAcceleration();
    double              xMaxTravel();
    double              yMaxTravel();
    double              zMaxTravel();

    //Setters
    void                setComm(SerialComm* comm);
    void                setParser(GrblMessageParser* parser);
    void                setStepPulse(int val);
    void                setStepIdleDelay(int val);
    void                setStepPortInvertMask(int val);
    void                setDirectionPortInvertMask(int val);
    void                setStepEnableInvert(bool val);
    void                setLimitPinsInvert(bool val);
    void                setProbePinInvert(bool val);
    void                setStatusReportMask(int val);
    void                setJunctionDeviation(double val);
    void                setArcTolerance(double val);
    void                setReportInches(bool val);
    void                setSoftLimitsEnabled(bool val);
    void                setHardLimitsEnabled(bool val);
    void                setHomingCycleEnabled(bool val);
    void                setHomingDirInvertMask(int val);
    void                setHomingFeed(double val);
    void                setHomingSeek(double val);
    void                setHomingDebounce(int val);
    void                setHomingPullOff(double val);
    void                setMaxSpindleRpm(int val);
    void                setMinSpindleRpm(int val);
    void                setLaserModeEnabled(bool val);
    void                setXStepPerMm(double val);
    void                setYStepPerMm(double val);
    void                setZStepPerMm(double val);
    void                setXMaxMmRatePerMin(double val);
    void                setYMaxMmRatePerMin(double val);
    void                setZMaxMmRatePerMin(double val);
    void                setXAcceleration(double val);
    void                setYAcceleration(double val);
    void                setZAcceleration(double val);
    void                setXMaxTravel(double val);
    void                setYMaxTravel(double val);
    void                setZMaxTravel(double val);


public slots:

    void                requestSettings();
    bool                requestSettingsSync();
    bool                saveSettings();
    bool                saveSettings(QString str , QVariant(val));
    bool                completeReset();

private slots:

    void                welcomeMessageReceived();
    void                settingMessageReceived(QString key , QVariant value);
    void                connectionStateChanged();

signals:

    void                errorOccurredWhileSaving();
    void                saveSuccess();

    void                commChanged();
    void                parserChanged();
    void                loadedChanged();
    void                stepPulseChanged();
    void                stepIdleDelayChanged();
    void                stepPortInvertMaskChanged();
    void                directionPortInvertMaskChanged();
    void                stepEnableInvertChanged();
    void                limitPinsInvertChanged();
    void                probePinInvertChanged();
    void                statusReportMaskChanged();
    void                junctionDeviationChanged();
    void                arcToleranceChanged();
    void                reportInchesChanged();
    void                softLimitsEnabledChanged();
    void                hardLimitsEnabledChanged();
    void                homingCycleEnabledChanged();
    void                homingDirInvertMaskChanged();
    void                homingFeedChanged();
    void                homingSeekChanged();
    void                homingDebounceChanged();
    void                homingPullOffChanged();
    void                maxSpindleRpmChanged();
    void                minSpindleRpmChanged();
    void                laserModeEnabledChanged();
    void                xStepPerMmChanged();
    void                yStepPerMmChanged();
    void                zStepPerMmChanged();
    void                xMaxMmRatePerMinChanged();
    void                yMaxMmRatePerMinChanged();
    void                zMaxMmRatePerMinChanged();
    void                xAccelerationChanged();
    void                yAccelerationChanged();
    void                zAccelerationChanged();
    void                xMaxTravelChanged();
    void                yMaxTravelChanged();
    void                zMaxTravelChanged();

private:

    QVariantMap         m_map;
    SerialComm*         m_comm;
    GrblMessageParser*  m_parser;
    bool                m_loaded;
    QRegularExpression  m_completeResetExpr;

    bool                checkConnection();
    void                setLoaded(bool val);

};

#endif // GRBLSETTINGS_H
