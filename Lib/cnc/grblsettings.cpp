#include "grblsettings.h"
#include <QRegularExpressionMatch>
#include <QThread>
#include <QTime>

void GrblSettings::registerQmlType(){
    qmlRegisterType<GrblSettings>("RLib" , 1 , 0 , "GrblSettings");
}

GrblSettings::GrblSettings(QQuickItem* parent)
    :
      QQuickItem(parent) ,
      m_comm(nullptr) ,
      m_parser(nullptr) ,
      m_loaded(false) ,
      m_completeResetExpr("\\[MSG:Restoring defaults\\]")
{
    setStepPulse(10);
    setStepIdleDelay(25);
    setStepPortInvertMask(1);
    setDirectionPortInvertMask(1);
    setStepEnableInvert(1);
    setLimitPinsInvert(1);
    setProbePinInvert(1);
    setStatusReportMask(1);
    setJunctionDeviation(1);
    setArcTolerance(1);
    setReportInches(0);
    setSoftLimitsEnabled(0);
    setHardLimitsEnabled(1);
    setHomingCycleEnabled(1);
    setHomingDirInvertMask(1);
    setHomingFeed(1);
    setHomingSeek(1);
    setHomingDebounce(1);
    setHomingPullOff(1);
    setMaxSpindleRpm(1);
    setMinSpindleRpm(1);
    setLaserModeEnabled(1);
    setXStepPerMm(200);
    setYStepPerMm(1);
    setZStepPerMm(1);
    setXMaxMmRatePerMin(1);
    setYMaxMmRatePerMin(1);
    setZMaxMmRatePerMin(1);
    setXAcceleration(1);
    setYAcceleration(1);
    setZAcceleration(1);
    setXMaxTravel(1);
    setYMaxTravel(1);
    setZMaxTravel(1);
}

//Getter
GrblMessageParser* GrblSettings::parser(){
    return m_parser;
}
SerialComm*     GrblSettings::comm(){
    return m_comm;
}
bool            GrblSettings::loaded(){
    return m_loaded;
}
int             GrblSettings::stepPulse(){
    return m_map[STEP_PULSE].toInt();
}
int             GrblSettings::stepIdleDelay(){
    return m_map[STEP_IDLE_DELAY].toInt();
}
int             GrblSettings::stepPortInvertMask(){
    return m_map[STEP_PORT_INVERT_MASK].toInt();
}
int             GrblSettings::directionPortInvertMask(){
    return m_map[DIRECTION_PORT_INVERT_MASK].toInt();
}
bool            GrblSettings::stepEnableInvert(){
    return m_map[STEP_ENABLE_INVERT].toInt();
}
bool             GrblSettings::limitPinsInvert(){
    return m_map[LIMIT_PINS_INVERT].toInt();
}
bool             GrblSettings::probePinInvert(){
    return m_map[PROBE_PIN_INVERT].toInt();
}
int             GrblSettings::statusReportMask(){
    return m_map[STATUS_REPORT_MASK].toInt();
}
double          GrblSettings::junctionDeviation(){
    return m_map[JUNCTION_DEVIATION].toDouble();
}
double          GrblSettings::arcTolerance(){
    return m_map[ARC_TOLERANCE].toDouble();
}
bool            GrblSettings::reportInches(){
    return m_map[REPORT_INCHES].toInt();
}
bool            GrblSettings::softLimitsEnabled(){
    return m_map[SOFT_LIMITS_ENABLED].toInt();
}
bool            GrblSettings::hardLimitsEnabled(){
    return m_map[HARD_LIMITS_ENABLED].toInt();
}
bool            GrblSettings::homingCycleEnabled(){
    return m_map[HOMING_CYCLE_ENABLED].toInt();
}
int             GrblSettings::homingDirInvertMask(){
    return m_map[HOMING_DIR_INVERT_MASK].toInt();
}
double          GrblSettings::homingFeed(){
    return m_map[HOMING_FEED].toInt();
}
double          GrblSettings::homingSeek(){
    return m_map[HOMING_SEEK].toInt();
}
int             GrblSettings::homingDebounce(){
    return m_map[HOMING_DEBOUNCE].toInt();
}
double          GrblSettings::homingPullOff(){
    return m_map[HOMING_PULL_OFF].toDouble();
}
int             GrblSettings::maxSpindleRpm(){
    return m_map[MAX_SPINDLE_RPM].toInt();
}
int             GrblSettings::minSpindleRpm(){
    return m_map[MIN_SPINDLE_RPM].toInt();
}
bool            GrblSettings::laserModeEnabled(){
    return m_map[LASER_MODE_ENABLED].toInt();
}
double          GrblSettings::xStepPerMm(){
    return m_map[X_STEP_PER_MM].toDouble();
}
double          GrblSettings::yStepPerMm(){
    return m_map[Y_STEP_PER_MM].toDouble();
}
double          GrblSettings::zStepPerMm(){
    return m_map[Z_STEP_PER_MM].toDouble();
}
double          GrblSettings::xMaxMmRatePerMin(){
    return m_map[X_MAX_MM_RATE_PER_MIN].toDouble();
}
double          GrblSettings::yMaxMmRatePerMin(){
    return m_map[Y_MAX_MM_RATE_PER_MIN].toDouble();
}
double          GrblSettings::zMaxMmRatePerMin(){
    return m_map[Z_MAX_MM_RATE_PER_MIN].toDouble();
}
double          GrblSettings::xAcceleration(){
    return m_map[X_ACCELERATION].toDouble();
}
double          GrblSettings::yAcceleration(){
    return m_map[Y_ACCELERATION].toDouble();
}
double          GrblSettings::zAcceleration(){
    return m_map[Z_ACCELERATION].toDouble();
}
double          GrblSettings::xMaxTravel(){
    return m_map[X_MAX_TRAVEL].toDouble();
}
double          GrblSettings::yMaxTravel(){
    return m_map[Y_MAX_TRAVEL].toDouble();
}
double          GrblSettings::zMaxTravel(){
    return m_map[Z_MAX_TRAVEL].toDouble();
}

//Setters
void            GrblSettings::setComm(SerialComm *comm){
    m_comm = comm;
    if(m_comm != nullptr){
        connect(m_comm , SIGNAL(stateChanged()) , this , SLOT(connectionStateChanged()));
    }
    emit commChanged();
}
void            GrblSettings::setParser(GrblMessageParser *parser){
    m_parser = parser;
    if(m_parser != nullptr){
        connect(m_parser , SIGNAL(settingsUpdate(QString,QVariant)) , this , SLOT(settingMessageReceived(QString,QVariant)));
        connect(m_parser , SIGNAL(welcomeCommandReceived()) , this , SLOT(welcomeMessageReceived()));
    }

    emit parserChanged();
}
void            GrblSettings::setLoaded(bool val){
    m_loaded = val;
    emit loadedChanged();
}
void            GrblSettings::setStepPulse(int val){
    m_map[STEP_PULSE] = QVariant(val);
    emit stepPulseChanged();
}
void            GrblSettings::setStepIdleDelay(int val){
    if(val <= 255 && val >= 0){
        m_map[STEP_IDLE_DELAY] = QVariant(val);
    }
    emit stepIdleDelayChanged();
}
void            GrblSettings::setStepPortInvertMask(int val){
    if(val <= 7 && val >= 0)
        m_map[STEP_PORT_INVERT_MASK] = QVariant(val);
    emit stepPortInvertMask();
}

void            GrblSettings::setDirectionPortInvertMask(int val){

    if(val <= 7 && val >= 0){
        m_map[DIRECTION_PORT_INVERT_MASK] = QVariant(val);
    }
    emit directionPortInvertMaskChanged();
}

void            GrblSettings::setStepEnableInvert(bool val){
    m_map[STEP_ENABLE_INVERT] = QVariant((int)val);
    emit stepEnableInvertChanged();
}

void            GrblSettings::setLimitPinsInvert(bool val){
    m_map[LIMIT_PINS_INVERT] = QVariant((int)val);
    emit limitPinsInvertChanged();
}

void            GrblSettings::setProbePinInvert(bool val){
    m_map[PROBE_PIN_INVERT] = QVariant((int)val);
    emit probePinInvertChanged();
}

void            GrblSettings::setStatusReportMask(int val){
    if(val >= 0 && val <= 2)
        m_map[STATUS_REPORT_MASK] = QVariant((int)val);
    emit statusReportMask();
}

void            GrblSettings::setJunctionDeviation(double val){
    if(val >= 0)
        m_map[JUNCTION_DEVIATION] = QVariant(val);
    emit junctionDeviationChanged();
}

void            GrblSettings::setArcTolerance(double val){
    if(val >= 0)
        m_map[ARC_TOLERANCE] = QVariant(val);
    emit arcToleranceChanged();
}

void            GrblSettings::setReportInches(bool val){
    m_map[REPORT_INCHES] = QVariant(0);
    emit reportInchesChanged();
}

void            GrblSettings::setSoftLimitsEnabled(bool val){
    m_map[SOFT_LIMITS_ENABLED] = QVariant((int)val);
    emit softLimitsEnabledChanged();
}

void            GrblSettings::setHardLimitsEnabled(bool val){
    m_map[HARD_LIMITS_ENABLED] = QVariant((int)val);
    emit hardLimitsEnabledChanged();
}

void            GrblSettings::setHomingCycleEnabled(bool val){
    m_map[HOMING_CYCLE_ENABLED] = QVariant((int)val);
    emit homingCycleEnabledChanged();
}

void            GrblSettings::setHomingDirInvertMask(int val){
    if(val >= 0 && val <= 7)
        m_map[HOMING_DIR_INVERT_MASK] = QVariant(val);
    emit homingDirInvertMaskChanged();
}

void            GrblSettings::setHomingFeed(double val){
    if(val >= 0)
        m_map[HOMING_FEED] = QVariant(val);
    emit homingFeedChanged();
}

void            GrblSettings::setHomingSeek(double val){
    if(val >= 0)
        m_map[HOMING_SEEK] = QVariant(val);
    emit homingSeekChanged();
}

void            GrblSettings::setHomingDebounce(int val){
    if(val >= 0)
        m_map[HOMING_DEBOUNCE] = QVariant(val);
    emit homingDebounceChanged();
}

void            GrblSettings::setHomingPullOff(double val){
    if(val >= 0)
        m_map[HOMING_PULL_OFF] = QVariant(val);
    else
        setHomingPullOff(1);
    emit homingPullOffChanged();
}

void            GrblSettings::setMaxSpindleRpm(int val){
    if(val >= 0)
        m_map[MAX_SPINDLE_RPM] = QVariant(val);
    emit maxSpindleRpm();
}

void            GrblSettings::setMinSpindleRpm(int val){
    if(val >= 0)
        m_map[MIN_SPINDLE_RPM] = QVariant(val);
    emit minSpindleRpm();
}

void            GrblSettings::setLaserModeEnabled(bool val){
    m_map[LASER_MODE_ENABLED] = QVariant((int)val);
    emit laserModeEnabledChanged();
}

void            GrblSettings::setXStepPerMm(double val){
    if(val >= 0)
        m_map[X_STEP_PER_MM] = QVariant(val);

    emit xStepPerMmChanged();
}

void            GrblSettings::setYStepPerMm(double val){
    if(val >= 0)
        m_map[Y_STEP_PER_MM] = QVariant(val);
    emit yStepPerMmChanged();
}

void            GrblSettings::setZStepPerMm(double val){
    if(val >= 0)
        m_map[Z_STEP_PER_MM] = QVariant(val);
    emit zStepPerMmChanged();
}

void            GrblSettings::setXMaxMmRatePerMin(double val){
    if(val >= 0)
        m_map[X_MAX_MM_RATE_PER_MIN] = QVariant(val);
    emit xMaxMmRatePerMinChanged();
}

void            GrblSettings::setYMaxMmRatePerMin(double val){
    if(val >= 0)
        m_map[Y_MAX_MM_RATE_PER_MIN] = QVariant(val);
    emit yMaxMmRatePerMinChanged();
}

void            GrblSettings::setZMaxMmRatePerMin(double val){
    if(val >= 0)
        m_map[Z_MAX_MM_RATE_PER_MIN] = QVariant(val);
    emit zMaxMmRatePerMinChanged();
}

void            GrblSettings::setXAcceleration(double val){
    if(val >= 0)
        m_map[X_ACCELERATION] = QVariant(val);
    emit xAccelerationChanged();
}

void            GrblSettings::setYAcceleration(double val){
    if(val >= 0)
        m_map[Y_ACCELERATION] = QVariant(val);
    emit yAccelerationChanged();
}

void            GrblSettings::setZAcceleration(double val){
    if(val >= 0)
        m_map[Z_ACCELERATION] = QVariant(val);
    emit zAccelerationChanged();
}

void            GrblSettings::setXMaxTravel(double val){
    if(val >= 0)
        m_map[X_MAX_TRAVEL] = QVariant(val);
    emit xMaxTravelChanged();
}

void            GrblSettings::setYMaxTravel(double val){
    if(val >= 0)
        m_map[Y_MAX_TRAVEL] = QVariant(val);
    emit yMaxTravelChanged();
}

void           GrblSettings::setZMaxTravel(double val){
    if(val >= 0)
        m_map[Z_MAX_TRAVEL] = QVariant(val);
    emit zMaxTravelChanged();
}

//Slots
void GrblSettings::requestSettings(){
    if(!checkConnection())
        return;

    comm()->send("$$\n");
}

bool GrblSettings::requestSettingsSync(){
    if(!checkConnection())
        return false;

    parser()->setEnabled(false);
    comm()->send("$$\n");
    bool ready = comm()->device()->waitForReadyRead(5000);

    if(ready){
        int readCount = 0;

        while(readCount < 35){
            if(!comm()->device()->canReadLine()){
                comm()->device()->waitForReadyRead(50);
                continue;
            }
            QString key;
            QVariant val;
            QString line = comm()->device()->readLine();
            if(parser()->parseSettings(line , key , val)){
                settingMessageReceived(key , val);
            }
            readCount++;
        }
    }

    parser()->setEnabled(true);
    return true;
}

void GrblSettings::welcomeMessageReceived(){
    requestSettings();
}

bool GrblSettings::saveSettings(){

    //Baglanti yoksa geri donuluyor
    if(comm()->getState() != SerialComm::Connected)
        return false;

    parser()->setEnabled(false);
    QIODevice*  device = comm()->device();

    QTime timeCounter;
    timeCounter.start();
    int start = timeCounter.elapsed();

    foreach (QString key, m_map.keys()){
        QString command = QString("%0=%1\n")
                .arg(key)
                .arg(m_map[key].toString());

        bool isOk = false;

        while(!isOk){
            bool canReadLine = false;
            QString line;
            int attempt = 0;
            while(!canReadLine && attempt < 5){
                comm()->send(command);
                device->waitForReadyRead(500);
                if(!device->canReadLine()){
                    if(timeCounter.elapsed() - start > 5000){
                        parser()->setEnabled(true);
                        emit errorOccurredWhileSaving();
                        return false;
                    }
                    attempt++;
                    continue;
                }
                canReadLine = true;
                line = device->readLine();

                if(timeCounter.elapsed() - start > 5000){
                    parser()->setEnabled(true);
                    emit errorOccurredWhileSaving();
                    return false;
                }
            }

            if(line.contains("ok"))
                isOk = true;
        }
    }

    parser()->setEnabled(true);
    emit saveSuccess();
    return true;
}

bool GrblSettings::saveSettings(QString val , QVariant value){
    parser()->setEnabled(false);
    QString settingsMessage = QString("%0=%1\n")
                                    .arg(val)
                                    .arg(value.toString());

    bool isOk = false;
    int  attempCount = 0;

    QTime time;
    time.start();
    int start = time.elapsed();
    while(!isOk && attempCount < 5){
        comm()->send(settingsMessage);
        comm()->device()->waitForReadyRead(100);

        if(comm()->device()->canReadLine()){
            QString line = comm()->device()->readLine();
            if(line.contains("ok")){
                parser()->setEnabled(true);
                return true;
            }
        } else {
            if(time.elapsed() - start > 2500){
                parser()->setEnabled(true);
                return false;
            }
            attempCount++;
        }

        if(time.elapsed() - start > 2500){
            parser()->setEnabled(true);
            return false;
        }
    }

    parser()->setEnabled(true);
    return true;
}

void GrblSettings::settingMessageReceived(QString key, QVariant value)  {

    setLoaded(true);

    if(key == STEP_PULSE)
        setStepPulse(value.toInt());
    else if(key == STEP_IDLE_DELAY)
        setStepIdleDelay(value.toInt());
    else if(key == STEP_PORT_INVERT_MASK)
        setStepPortInvertMask(value.toInt());
    else if(key == DIRECTION_PORT_INVERT_MASK)
        setDirectionPortInvertMask(value.toInt());
    else if (key == STEP_ENABLE_INVERT)
        setStepEnableInvert(value.toInt());
    else if (key == LIMIT_PINS_INVERT)
        setLimitPinsInvert(value.toInt());
    else if (key == PROBE_PIN_INVERT)
        setProbePinInvert(value.toInt());
    else if (key == STATUS_REPORT_MASK)
        setStatusReportMask(value.toInt());
    else if (key == JUNCTION_DEVIATION)
        setJunctionDeviation(value.toDouble());
    else if (key == ARC_TOLERANCE)
        setArcTolerance(value.toDouble());
    else if (key == REPORT_INCHES)
        setReportInches(value.toInt());
    else if (key == SOFT_LIMITS_ENABLED)
        setSoftLimitsEnabled(value.toInt());
    else if (key == HARD_LIMITS_ENABLED)
        setHardLimitsEnabled(value.toInt());
    else if (key == HOMING_CYCLE_ENABLED)
        setHomingCycleEnabled(value.toInt());
    else if (key == HOMING_DIR_INVERT_MASK)
        setHomingDirInvertMask(value.toInt());
    else if (key == HOMING_FEED)
        setHomingFeed(value.toDouble());
    else if (key == HOMING_SEEK)
        setHomingSeek(value.toDouble());
    else if (key == HOMING_DEBOUNCE)
        setHomingDebounce(value.toInt());
    else if (key == HOMING_PULL_OFF)
        setHomingPullOff(value.toDouble());
    else if (key == MAX_SPINDLE_RPM)
        setMaxSpindleRpm(value.toInt());
    else if (key == MIN_SPINDLE_RPM)
        setMinSpindleRpm(value.toInt());
    else if (key == LASER_MODE_ENABLED)
        setLaserModeEnabled(value.toInt());
    else if (key == X_STEP_PER_MM)
        setXStepPerMm(value.toDouble());
    else if (key == Y_STEP_PER_MM)
        setYStepPerMm(value.toDouble());
    else if (key == Z_STEP_PER_MM)
        setZStepPerMm(value.toDouble());
    else if (key == X_MAX_MM_RATE_PER_MIN)
        setXMaxMmRatePerMin(value.toDouble());
    else if (key == Y_MAX_MM_RATE_PER_MIN)
        setYMaxMmRatePerMin(value.toDouble());
    else if (key == Z_MAX_MM_RATE_PER_MIN)
        setZMaxMmRatePerMin(value.toDouble());
    else if (key == X_ACCELERATION)
        setXAcceleration(value.toDouble());
    else if (key == Y_ACCELERATION)
        setYAcceleration(value.toDouble());
    else if(key == Z_ACCELERATION)
        setZAcceleration(value.toDouble());
    else if (key == X_MAX_TRAVEL)
        setXMaxTravel(value.toInt());
    else if (key == Y_MAX_TRAVEL)
        setYMaxTravel(value.toDouble());
    else if (key == Z_MAX_TRAVEL){
        setZMaxTravel(value.toDouble());
    }
}

void GrblSettings::connectionStateChanged(){
    if(comm()->getState() == SerialComm::Connected){
        setLoaded(false);
        QTimer::singleShot(1000 , Qt::PreciseTimer , this , SLOT(requestSettings()));
    }
}

bool GrblSettings::completeReset(){
    if(!checkConnection())  return false;

    parser()->setEnabled(false);
    comm()->send("$RST=$\n");

    bool res = comm()->device()->waitForReadyRead(500);

    if(res){
        QThread::msleep(100);
        QString read = comm()->device()->readAll();
        QRegularExpressionMatch match = m_completeResetExpr.match(read);
        //Yeni ayarlar aliniyor
        res = match.hasMatch();
        requestSettings();
        setLoaded(false);
    } else {
        res = false;
    }

    parser()->setEnabled(true);

    return res;
}

//Slots END !
bool GrblSettings::checkConnection(){
    if(m_comm != nullptr && m_comm->getState() != SerialComm::Connected)
        return false;

    return true;
}
