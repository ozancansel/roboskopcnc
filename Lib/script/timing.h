#ifndef TIMING_H
#define TIMING_H

#include <QThread>
#include <QObject>

class Timing  : public QObject
{

    Q_OBJECT

public:

    Timing(QObject* parent = nullptr);

public slots:

    void        wait(int ms);

};

#endif // TIMING_H
