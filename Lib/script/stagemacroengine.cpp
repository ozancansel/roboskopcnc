#include "stagemacroengine.h"
#include <QScriptValue>
#include <QtQml>
#include "cnc/stageproxy.h"

void StageMacroEngine::registerQmlType(){
    qmlRegisterType<StageMacroEngine>("RLib" , 1 , 0 , "StageMacroEngine");
}

StageMacroEngine::StageMacroEngine(QQuickItem* parent)
    :
      QQuickItem(parent) ,
      m_stage(nullptr)
{
    m_engine = new QScriptEngine();
    m_timing = new Timing();

    QScriptValue timingObj = m_engine->newQObject(m_timing);

    m_engine->globalObject().setProperty("timing" , timingObj);
}

void StageMacroEngine::run(QString str){

    QFile f(":/files/stage-macro-def");

    if(!f.open(QIODevice::ReadOnly)){
        return;
    }

    QString script = f.readAll();

    script.append(str);

    m_engine->evaluate(script);
}

StageProxy* StageMacroEngine::stage(){
    return m_stage;
}

void StageMacroEngine::setStage(StageProxy *val){
    m_stage = val;
    QScriptValue stage = m_engine->newQObject(m_stage);
    m_engine->globalObject().setProperty("stage" , stage);
    emit stageChanged();
}
