#include "macroengine.h"
#include <QScriptValue>

MacroEngine::MacroEngine(QQuickItem* parent)
    :
      QQuickItem(parent) ,
      m_running(false)
{
}


//Getter
QString MacroEngine::script(){
    return m_script;
}

QScriptEngine*  MacroEngine::engine(){
    return &m_engine;
}

bool MacroEngine::running(){
    return m_running;
}

void MacroEngine::inject(QObject *obj, QString name){
    QScriptValue val = engine()->newQObject(obj);
    engine()->globalObject().setProperty(name , val);
}

//Setter
void MacroEngine::setScript(QString str){
    m_script = str;
}

void MacroEngine::setRunning(bool val){
    m_running = val;
    emit runningChanged();
}

//Slots
//Virtual
void MacroEngine::run(){
    QFile f(":/files/MacroLib");

    if(!f.open(QIODevice::ReadOnly))
        return;

    QString script = f.readAll();
    script.append(m_script);
    setRunning(true);
    m_engine.evaluate(script);
    setRunning(false);
}

void MacroEngine::abortExecution(){
    m_engine.abortEvaluation();
    setRunning(false);
}

