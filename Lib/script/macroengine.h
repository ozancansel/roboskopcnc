#ifndef MACROENGINE_H
#define MACROENGINE_H

#include <QQuickItem>
#include <QScriptEngine>
#include "timing.h"

class MacroEngine : public QQuickItem
{

    Q_OBJECT
    Q_PROPERTY(QString script READ script WRITE setScript NOTIFY scriptChanged )
    Q_PROPERTY(bool running READ running WRITE setRunning NOTIFY runningChanged)

public:

    MacroEngine(QQuickItem* parent = nullptr);
    QString             script();
    bool                running();
    void                setScript(QString str);
    void                setRunning(bool val);

public slots:

    void    run();
    void    abortExecution();
    void    inject(QObject* obj , QString name);

signals:

    void    scriptChanged();
    void    runningChanged();

protected:

    QScriptEngine*      engine();

private:

    QScriptEngine       m_engine;
    QString             m_script;
    bool                m_running;

};

#endif // MACROENGINE_H
