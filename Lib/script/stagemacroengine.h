#ifndef STAGEMACROEVALUATE_H
#define STAGEMACROEVALUATE_H

#include <QtScript>
#include <QObject>
#include "cnc/stageproxy.h"
#include "timing.h"

class StageMacroEngine : public QQuickItem
{

    Q_OBJECT
    Q_PROPERTY(StageProxy*  stage READ stage WRITE setStage NOTIFY stageChanged)

public:

    static void         registerQmlType();
    StageMacroEngine(QQuickItem* parent = nullptr);
    void                setStage(StageProxy* val);
    StageProxy*         stage();

public slots:

    void                run(QString str);

signals:

    void                stageChanged();
    void                start();

private:

    QScriptEngine*      m_engine;
    QString             m_preDefined;
    StageProxy*         m_stage;
    Timing*             m_timing;
    QThread             m_thread;

};

#endif // STAGEMACROEVALUATE_H
