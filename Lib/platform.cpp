#include "platform.h"

void Platform::registerQmlType(){
    qmlRegisterSingletonType<Platform>("RLib" , 1 , 0 , "Platform" , platform_singleton_provider);
}

Platform::Platform(QQuickItem *parent) : QQuickItem(parent){

}

bool Platform::isMobile(){
#ifdef Q_OS_ANDROID
    return true;
#endif
    return false;
}

bool Platform::isDesktop(){
#ifndef Q_OS_ANDROID
    return true;
#endif
    return false;
}

static QObject *platform_singleton_provider(QQmlEngine *engine , QJSEngine *scriptEngine){
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    Platform *platform = new Platform();
    return platform;
}
