#include "bluetoothport.h"
#include <QDebug>
#include <QtQml>
#include <QTime>

void BluetoothPort::registerQmlType()   {
    //    qRegisterMetaType<BluetoothUtil::ConnectionState>("bluetooth::BtState");
    qmlRegisterType<BluetoothPort>("RLib" , 1 , 0 , "BluetoothPort");
}

BluetoothPort::BluetoothPort(QQuickItem* parent) : SerialComm(parent)
{

    _socket = new QBluetoothSocket(QBluetoothServiceInfo::RfcommProtocol, this);
    m_localDevice = new QBluetoothLocalDevice();
    m_socket = _socket;
    m_agent = new QBluetoothDeviceDiscoveryAgent(this);


    setSocket(_socket);

    connect(_socket , SIGNAL(connected()) , this , SLOT(btConnected()));
    connect(_socket , SIGNAL(disconnected()) , this , SLOT(btDisconnected()));
    connect(m_agent , SIGNAL(deviceDiscovered(QBluetoothDeviceInfo)) , this , SLOT(btDeviceDiscovered(QBluetoothDeviceInfo)));
    connect(m_agent , SIGNAL(finished()) , this , SLOT(btScanFinished()));
    connect(_socket , SIGNAL(stateChanged(QBluetoothSocket::SocketState)) , this , SLOT(btStateChanged(QBluetoothSocket::SocketState)));
}

void BluetoothPort::btConnected(){
    setState(Connected);
}

void BluetoothPort::btDisconnected(){
    setState(Disconnected);
}

//Private slots
void BluetoothPort::btDeviceDiscovered(const QBluetoothDeviceInfo &device){
    qDebug() << device.name() << " " << device.address() << " discovered" << " Service : ";

    QString name = device.name();
    QString addr = device.address().toString();
    appendIfNotExists(device);
    sendDeviceDiscoveredSignal(name , addr);
}

void BluetoothPort::btScanFinished(){

    if(getState() == Discovering)
        setState(None);

    sendScanStatusChangedSignal();
}

//Public slots
void BluetoothPort::connectTo(QString address){

    m_agent->stop();

    //Eğer soket açıksa kapatılıyor
    if(_socket->isOpen())
        _socket->close();

    QBluetoothDeviceInfo info = byAddress(address);

    QVariantMap map;

    map["name"] = info.name();
    map["address"] = info.address().toString();
    setConnectingDevice(map);
    _socket->connectToService(QBluetoothAddress(address) , QBluetoothUuid(QBluetoothUuid::SerialPort));
}

void BluetoothPort::startScan(){

    setState(Discovering);

    if(m_localDevice->isValid()){

        m_localDevice->powerOn();
        m_agent->stop();
        m_agent->start();
    }
}

void BluetoothPort::btStateChanged(QBluetoothSocket::SocketState state){

    if(state == QBluetoothSocket::UnconnectedState && (getState() != Disconnected || getState() != Error)){
        setState(None);
    }else if(state == QBluetoothSocket::ConnectingState){
        setState(Connecting);
    }else if(state == QBluetoothSocket::ConnectedState){
        setState(Connected);
    }
}

void BluetoothPort::appendIfNotExists(QBluetoothDeviceInfo newOne){

    foreach (QBluetoothDeviceInfo info, m_discoveredDevices) {
        if(info.address() == newOne.address()){
            if(info.name() != newOne.name()){
                m_discoveredDevices.removeOne(info);
                m_discoveredDevices.append(newOne);
                return;
            }
        }
    }

    m_discoveredDevices.append(newOne);
}

QBluetoothDeviceInfo BluetoothPort::byAddress(QString addr){
    foreach (QBluetoothDeviceInfo info, m_discoveredDevices) {
        if(info.address().toString() == addr)
            return info;
    }

    return QBluetoothDeviceInfo();
}
