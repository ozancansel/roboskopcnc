#ifndef SERIALPORT_H
#define SERIALPORT_H

#include "serialcomm.h"
#include <QList>
#include <QSerialPort>

class SerialPort : public SerialComm
{

    Q_OBJECT

public:

    static void     registerQmlType();
    SerialPort(QQuickItem* parent = nullptr);

public slots:

    void            connectTo(QString address) override;
    void            startScan() override;

private slots:

    void            errorOccurred(QSerialPort::SerialPortError err);

private:

    struct IdPair {
        int vid;
        int pid;
        IdPair(int vid , int pid) : vid(vid) , pid(pid) { }
    };

    struct PortInfo {
        QString         board;
        QList<IdPair>   pairs;
    };

    QSerialPort     m_port;
    QList<PortInfo> m_definedPorts;
    QString         retrieveBoardName(int vid,  int pid);
    QMap<SerialComm::BaudRate , int>    m_baudRateMap;

};

#endif // SERIALPORT_H
